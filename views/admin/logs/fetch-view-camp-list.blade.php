

<style>

.btn.btn-success {
    color: #fff;
    background-color: rgb(147, 213, 0);
    border-color: rgb(147, 213, 0);

}
.btn-warning.btn {
    border: #ffa300;
    background: #ffa300;

}
.btn.btn-danger {
    background-color: #dc3545;
    border-color: #dc3545;

}
.set_btn_width{
         width: 76px;
        padding: 12px;
}
</style>
<?php
  $dayArr = array(
       'mon'=>'mon',
       'tue'=>'tue',
       'wed'=>'wed',
       'thu'=>'thu',
       'fri'=>'fri'
    );
?>
<div class="table-responsive">
    <div clas="table">
<table class="table" style="display: inline-table;" border="1" bordercolor="#e9ecef">


    @if(isset($user_data->firstName))
        <tr>
            <td style="width:50%;"><span ><b>Customer Name :</b></span></td>
            <td><span class="details_now"> {{ $user_data->firstName.' '.$user_data->lastName }}</span></td>
        </tr>
    @endif
    @if(isset($user_data->email))
        <tr>
            <td style="width:50%;"><span ><b>Customer Email :</b></span></td>
            <td><span class="details_now"> {{ $user_data->email}}</span></td>
        </tr>
    @endif
    @if(isset($leadData->campaign_details->business->business_name))
        <tr>
            <td style="width:50%;"><span ><b>Business Name :</b></span></td>
            <td><span class="details_now"> {{ $leadData->campaign_details->business->business_name}}</span></td>
        </tr>
    @endif
    @if(isset($userCamp->title))
        <tr>
            <td style="width:50%;"><span ><b>Campaign Title :</b></span></td>
            <td><span class="details_now"> {{ $userCamp->title }}</span></td>
        </tr>
    @endif
    @if(isset($userCamp->email))
        <tr>
            <td style="width:50%;"><span ><b>Campaign Email Address :</b></span></td>
            <td><span class="details_now"> {{ $userCamp->email }}</span></td>
        </tr>
    @endif

     @if(isset($leadData->expectedCall))
                  <tr>
                    <td style="width:50%;" ><span><b>Original Lead Time :</b></span></td>
                    <td><span class="details_now">
                      @if(!empty($leadData->expectedCall) && isset($leadData->campaign_details->coutry_details) && $leadData->campaign_details->coutry_details->countryPhoneCode==""){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($leadData->campaign_details,$lead->expectedCall) !!} @else {!! date('d/m/Y h:i:s A', strtotime($leadData->expectedCall))  !!} @endif</span></td>
                  </tr>
                @endif
                @if(isset($leadData->triggerAt))
                  <tr>
                    <td style="width:50%;"><span><b>Call Triggered Time :</b></span></td>
                    <td><span class="details_now"> {!!  date('m-d-Y h:i:s A',strtotime($leadData->triggerAt)) !!}</span></td>
                  </tr>
                @endif
    @if($leadData->status==11)
    <tr>
            <td><span ><b>Template Mismatched :</b></span></td>
            <td>
                <?php
                    if(!empty($leadData->api_response))
                    {
                        $api_response = json_decode($leadData->api_response);
                        if(!empty($api_response) && isset($api_response->content))
                        {
                            if(isset($api_response->content->error_description))
                            {
                                echo $api_response->content->error_description;
                            }
                        }
                        else
                        {
                            echo "N/A";
                        }
                    }
                    else
                    {
                        echo "N/A";
                    }
                ?>
            </td>
        </tr>
    @endif
    <div class="table-responsive">
<table class="table">
    <thead>
        <th style="width:50%"> <b>Original Lead Format :</b></th>
        <th style="width:50%"><b>NEW lead format received :</b></th>
    </thead>
    <tbody>
    @if(isset($leadData->lead_data_history->campTestMail))

        <tr>
            <td>
                <span class="details_now"> {!! html_entity_decode($leadData->lead_data_history->campTestMail) !!}</span>
            </td>
            <td>

                <span class="details_now">
                    <?php
                        if( isset($leadData->original_lead) )
                        {
                            $lines = explode(PHP_EOL, $leadData->original_lead);
                            if( is_array($lines))
                            {
                                foreach ($lines as $key => $value)
                                {
                                    echo '<p class="testMail">'.$value."</p>";
                                }
                            }
                            else
                            {
                                echo $leadData->original_lead;
                            }
                        }
                    ?>
                </span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
</div>
</tbody>
</table>
</div>
<div class="table-responsive">
    <table class="table">
        <tbody>
    @if(isset($userCamp->template))
        <!--<tr>-->
        <!--    <td><span ><b>Campaign Template :</b></span></td>-->
        <!--    <td><span class="details_now"> {{ $userCamp->template }}</span></td>-->
        <!--</tr>-->
    @endif
    @if(isset($userCamp->country_id) && $userCamp->country_id!=0)
        <tr>
            <td><span ><b>Campaign Country :</b></span></td>
            <td><span class="details_now"> {{ $userCamp->coutry_details->countryName }}</span></td>
        </tr>
    @endif

     <?php
        $availHours="";
        $days="";
     ?>
    @if(isset($leadData->lead_data_history->available_hours) && !empty($leadData->lead_data_history->available_hours))
         <?php  $availHours = json_decode($leadData->lead_data_history->available_hours); ?>
         <?php  $days = json_decode($leadData->lead_data_history->available_days);?>
    @else
      @if(isset($userCamp->available_hours))
             <?php  $availHours = json_decode($userCamp->available_hours); ?>
             <?php  $days = json_decode($userCamp->available_days);?>
      @endif
    @endif

    @if(!empty($availHours))
                  <tr class="mb-2">
                        <td><span ><b>Available Hours :</b></span></td>
                        <td>
                            <p class="details_now">
                           @if(isset($availHours->from->working))
                                @if(!empty($days))
                                    @foreach($days as $day)
                                         @if(in_array($day,$dayArr))
                                            {{ ucfirst($day.' ') }}
                                         @endif
                                    @endforeach
                            @endif
                             {{ $availHours->from->working }} - {{ $availHours->to->working }} @else N/A @endif</p>
                             <p class="details_now">@if(isset($availHours->from->sat) && !empty($availHours->from->sat)) Sat {{ $availHours->from->sat }} - {{ $availHours->to->sat }} @else N/A @endif</p>
                             <p class="details_now">@if(isset($availHours->from->sun) && !empty($availHours->from->sun)) Sun {{ $availHours->from->sun }} - {{ $availHours->to->sun }} @else N/A @endif</p>
                        </td>
                    </tr>
    @endif

    <?php $breakHours = json_decode($userCamp->break_hours); ?>
    @if(isset($userCamp->break_hours))
        <tr class="d-none">
            <td><span ><b>Break Hours :</b></span></td>
            <td><span class="details_now"> {{ $breakHours->from }} - {{ $breakHours->to }}</span></td>
        </tr>
    @endif

    <tr><td><span ><b>Company Contacts</b></span></td><td></td></tr>
</tbody>
</table>
</div>
</div>
<div class="table-responsive">
<table class="table" style="display: inline-table;" border="1" bordercolor="#e9ecef">
    <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($leadData->lead_contacts))
                  @forelse($leadData->lead_contacts as $ct)
                  <tr>
                    <td>@if(isset($ct->name)){{ $ct->name }}@else N/A @endif</td>
                    <td>@if(isset($ct->contact)){{ $ct->contact }}@else N/A @endif</td>
                  </tr>
                  @empty
                  <tr><td colspan="2">No Record Found !</td></tr>
                  @endforelse
                  @endif
    </tbody>

</table>
</div>
<div class="table-responsive">
<table class="table" border="1" bordercolor="#e9ecef">
      <tr><td><span ><b>Call Info</b></span></td><td></td><td></td><td></td></tr>
</table>
</div>

<div class="table-responsive admin_table">
     <div class="table">
<table class="table" style="display: inline-table;" border="1" bordercolor="#e9ecef">
    <!--<thead>-->
    <!--    <tr>-->
    <!--        <th>Lead Id</th>-->
    <!--        <th>Lead Name</th>-->
    <!--        <th>Phone Number</th>-->
    <!--        <th>Message</th>-->
    <!--        <th>Phone Recording</th>-->
    <!--    </tr>-->
    <!--</thead>-->
    <tbody>
        <tr>
            <td><b>Lead Id :</b></td><td>@if(isset($leadData->lead_id)){{ $leadData->lead_id }}@else N/A @endif</td>
        </tr>
        <tr>
            <td><b>Lead Name :</b></td><td>@if(isset($leadData->leadName)){{ $leadData->leadName }}@else N/A @endif</td>
        </tr>
         <tr>
            <td><b>Call Status :</b></td><td>

              <?php

                           $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($leadData);
                           extract($return_data);
                           if(isset($status_message) && !empty($status_message))
                           {
                                  echo $status_message;
                           }
                           else
                           {
                                  echo App\Helpers\GlobalFunctions::returnCallStatus($leadData);
                           }
                          // if(!empty($leadData->api_response))
                          // {
                          //           $api_response=json_decode($leadData->api_response);
                          //           if(!empty($api_response) && isset($api_response->content))
                          //           {
                          //                if($leadData->status==1)
                          //                 {
                          //                     if(isset($api_response->content->message)){echo $api_response->content->message;}else{ echo 'N/A'; }
                          //                 }
                          //                 else{
                          //                     if(isset($api_response->content->error))
                          //                     {
                          //                        echo $api_response->content->error;
                          //                     }
                          //                     else if(isset($api_response->content->error_message))
                          //                     {
                          //                         echo $api_response->content->error_message;
                          //                     }
                          //                     else if(isset($api_response->content->message))
                          //                     {
                          //                         echo $api_response->content->message;
                          //                     }
                          //                     else{
                          //                       echo 'N/A';
                          //                     }

                          //                 }

                          //           }
                          //           else{
                          //              echo "N/A";
                          //            }
                          // }
                          // else{
                          //      echo "N/A";
                          // }
                        ?></td>
        </tr>
         <tr>
            <td><b>Call Time :</b></td><td>@if(isset($leadData->api_leads->startdate)){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($userCamp,$leadData->api_leads->startdate) !!}@else N/A @endif</td>
        </tr>
        <tr>
            <td><b>Answered By :</b></td><td>
             <?php


                                  if(isset($leadData->api_leads->answered_by))
                                  echo ucfirst($leadData->api_leads->answered_by);
                                  ?>
                                  <?php /*if(isset($leadData->api_leads->sc1) && $leadData->api_leads->sc1 == 'success' ){ ?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-success set_btn_width">Success!</button>
                                  <?php } else if(isset($leadData->api_leads->sc1) && $leadData->api_leads->sc1 == 'calling' ){?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-warning set_btn_width">Not Connected</button>
                                  <?php } else {?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-danger set_btn_width">Not Connected</button>
                                  <?php } ?>


                                  */
                                  ?>
                                  @if(isset($business_btn_message))
                                   <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif"  class="not_connected_call call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo $business_btn_message; ?></button>
                                   @endif
            </td>


        </tr>
        <tr>
            <td><b>Visitor :</b></td><td>

                                 <?php
                                     if(isset($leadData->api_leads->lead_contact))
                                     echo ucfirst($leadData->api_leads->lead_contact);
                                   ?>
                                   @if(isset($customer_btn_message))
                                   <button type="button" class="not_connected_call call-status-button btn <?php echo  $customer_btn_color;?> set_btn_width"><?php echo $customer_btn_message; ?></button>
                                   @endif
                                  <?php /*if(isset($leadData->api_leads->sc2) && $leadData->api_leads->sc2 == 'success' ){ ?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-success set_btn_width">Success!</button>
                                  <?php } else if(isset($leadData->api_leads->sc2) && $leadData->api_leads->sc2 == 'calling' ){?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-warning set_btn_width">Not Connected</button>
                                  <?php } else {?>
                                      <button type="button" class="pullRightBtn call-status-button btn btn-danger set_btn_width">Not Connected</button>
                                  <?php }*/ ?>
            </td>
        </tr>
         <tr>
            <td><b>Whisper :</b></td><td>@if(isset($leadData->api_leads->whisper)){{ $leadData->api_leads->whisper }}@else N/A @endif</td>
        </tr>

         <tr>
            <td><b>Recording :</b></td><td> @if(isset($leadData->api_leads))
                    <audio controls controlsList="nodownload">
                          <source src="<?php echo $leadData->api_leads->recording;?>" type="audio/wav">
                      </audio>
                      <!--a href="{{$leadData->api_leads->recording}}" download="{{$leadData->api_leads->recording}}"><i  class="fa fa-download" aria-hidden="true"></i></a-->
                     @else N/A
                @endif</td>
        </tr>
        <tr>
            <td colspan="2" class="showCallTrnascription">
                <label for="collapsible" class="lbl-toggle">Show Call Transcription</label>
                <div class="collapsible-content">
                    <div class="content-inner">
                    <p>
                        <?php if(isset($leadData->api_leads->call_transcription)) echo $leadData->api_leads->call_transcription; else echo 'N/A'; ?>
                    </p>
                    </div>
                </div>
            </td>
        </tr>

    </tbody>

</table>
</div>
</div>
<div class="table-responsive">
                                            <table class="table" border="1" bordercolor="#e9ecef">

                                    <tr><td><span ><b>Call Attempts</b></span></td><td></td><td></td><td></td></tr>
                                    </table>
                                    </div>
<div class="table-responsive">
    <div class="table">
    <table class="table" style="display: inline-table;" border="1" bordercolor="#e9ecef">

    <thead>
        <tr>
            <th>Lead Id</th>
            <th>Contact Name</th>
            <th>Contact Number</th>
            <th>Contact Email</th>
            <th>Answered By Call</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($leadData->call_attempt))
         @forelse($leadData->call_attempt as $call_attempt)
         <tr>
            <td>{!! $call_attempt->lead_id !!}</td>
            <td>@if(isset($call_attempt->contact_name)) {!! $call_attempt->contact_name !!} @else N/A @endif</td>
            <td>{!! $call_attempt->number !!}</td>
            <td>@if(!empty($call_attempt->contact_email)){!! $call_attempt->contact_email !!} @else N/A @endif</td>
            <td>@if(!empty($call_attempt->sc1_answered)) Yes @else No @endif</td>
         </tr>
         @empty
         <tr><td colspan="5">No Record Found !</td></tr>
         @endforelse
         @else
         <tr><td colspan="5">No Record Found !</td></tr>

         @endif
    </tbody>

</table>
</div>
</div>
<script type="text/javascript">
function hightLight (element, start, end,tagName , id, isCustomTag) {
    var str = element[0].innerHTML;
    str = `${ str.substr(0, start) }<mark data-name="${tagName.replace("\"", "")}" class="campaignTag" data-custom-tag="${isCustomTag}" data-id="${id}">${str.substr(start, end - start)}</mark>${str.substr(end)}`;
    element[0].innerHTML = str.replace("\"", "");

}
<?php
if( !empty($campTags))
{
    foreach ($campTags as $key => $value) { ?>
        jQuery('.testMail').each(function(index)
        {
            var thiss = $(this);
            var tagnamee = '<?php echo $value->tagName;?>';
            if( tagnamee != '' )
            {
                if (index == '<?php echo $value->indexRow?>') {
                    hightLight(thiss, '<?php echo $value->positionStart?>', '<?php echo $value->positionEnd?>','<?php echo $value->tagName?>','<?php echo $value->id?>','<?php echo $value->isCustomTag; ?>')
                }
            }
        });
<?php }
} ?>
</script>
<script>
                        $(document).ready(function () {
    $("button").tooltip({container:'body'});
});
                        </script>
<style>
.testMail .mark, mark {
  padding: .2em;
  background-color: #CCEFFC;
}
</style>
