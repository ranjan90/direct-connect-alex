@extends('layouts.admin')
@section('content')
<style>

.btn.btn-success {
    color: #fff;
    background-color: rgb(147, 213, 0);
    border-color: rgb(147, 213, 0);
    
}
.btn-warning.btn {
    border: #ffa300;
    background: #ffa300;

}
.btn.btn-danger {
    background-color: #dc3545;
    border-color: #dc3545;
    
}
.set_btn_width{
    width: 76px;
        padding: 12px;
}
</style>
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-history"></i>Leads Logs</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item">Leads Logs</li>
            </ul>
        </div>
       
              

                <div class="tile">
                   <div class="row ">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <!--<div class="form-group pull-left  col-md-4 nopadding">
                            <input type="text" data-url="#" class="form-control" id="searchKey-stop" placeholder="Search Top Up by Name">
                        </div>-->
                        <div class="form-group pull-left  col-md-4">
                              <label>Customer</label>
                            <select class="form-control get_customer_email" name="users_email">
                                <option value="">Select Customer</option>
                                @foreach($users_email as $users_email_key => $users_email_value)

                                  <option value="{!! \Crypt::encrypt($users_email_value->id) !!}">{{ $users_email_value->firstName.' '.$users_email_value->lastName.'('.$users_email_value->email.')' }}</option>

                                @endforeach
                            </select>
                        </div>
                         <div class="form-group pull-left  col-md-4" style="margin-top:29px;">
                             <select name="select_busiess_data" class="form-control select_busiess_data" multiple="multiple">
                             </select>
                        </div>


                        <div class="form-group pull-left  col-md-4">
                            <label>Search</label>
                             <label></label>
                             <!--<input type="text" data-url="{{url('ctwdr_dmlogin/logs')}}" class="form-control" id="searchKey" placeholder="Search by Lead Id,Lead Name,Campaign Email Id,Phone No">-->
                                 <input type="text" data-url="{{url('ctwdr_dmlogin/logs')}}" class="form-control" id="searchKey" placeholder="Search by Campaign Name,Lead Id">
                     
                        </div>
                         <!--
                         <div class="form-group pull-left  col-md-2">
                              <label>Status</label>
                              <label></label>
                            {!!Form::select('statusBox', [''=>'Select Status','success'=>'Success','failed'=>'Failed'], null, ['class' => 'form-control filterCheckBox','id'=>'statusBox'])!!}
                        </div>
                        -->
                               <div class="form-group pull-left  col-md-4">
                                                  <label>From</label>
                                                  <div class="input-group">
                                                    <div class="input-group mb-3">
                                                      <div class="input-group-prepend">
                                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                                      </div>
                                                      {!! Form::text('date_from',null,['class'=>'form-control datepicker', 'id'=>'fromDate','autocomplete'=>'off'])!!}
                                                    </div>
                                                  </div>
                                        </div>
                                        <div class="form-group pull-left  col-md-4">
                                                  <label>To</label>
                                                  <div class="input-group">
                                                    <div class="input-group mb-3">
                                                      <div class="input-group-prepend">
                                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                                      </div>
                                                      {!! Form::text('date_to',null,['class'=>'form-control datepicker', 'id'=>'toDate','autocomplete'=>'off'])!!}
                                                    </div>
                                                  </div>
                                        </div>


                                          <div class="form-group pull-left  col-md-4">
                                              <label style="opacity:0;" class="d-block">button</label>
          <a href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
               </div>

                    </div>

                    <div class="tile-body   table-responsive recordsTable">
                           @include('admin.logs.lead_data',['data'=>$data])
                    </div>
                </div>
    </main>
 <!-- View Modal -->
    <div id="confirm-view-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">More information</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body view_campaign_data">

                </div>
            </div>
        </div>
    </div>
    <!-- View Enddds -->
<link href="{{ asset('assets/customer/css/bootstrap-multiselect.css') }}" rel="stylesheet">
@section('js')
<script src="{{ asset('assets/customer/js/jquery.timepicker.js') }}"></script>
<link href="{{ asset('assets/customer/css/jquery.timepicker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/customer/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript">

var route_url="<?php echo url(request()->route()->getPrefix())?>";
var get_data_url=route_url+'/fetch-lead-log-list';
var camp=route_url+'/view-campaign-detail';
var get_business=route_url+'/get-customer-business';

    $('.select_busiess_data').multiselect({
         nonSelectedText: 'None selected Business'
    });
    //select_busiess_data
    jQuery(document).on('click','.confirm_view_pop_up',function(e)
    {
            var promotr = localStorage.getItem("promotr");
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:camp+'/'+jQuery(this).attr('data-id')+'?lead_id='+jQuery(this).attr('data-lead-id'),
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (res)
                {
                    hideLoader();
                    $(".view_campaign_data").html(res.data)
                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                     hideLoader();
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        //tostrerror(message);
                         $(".view_campaign_data").html(message)
                    });
                }
            });

});
jQuery(document).on('change','.get_customer_email',function(e)
    {
         if(jQuery(this).val()!="")
         {
         $(".select_busiess_data").multiselect('dataprovider');
         jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:get_business+'/'+jQuery(this).val(),
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (res)
                {
                    //hideLoader();
                    $(".select_busiess_data").multiselect('dataprovider', res);
                    $(".select_busiess_data").multiselect('refresh');
                     $("#searchKey").trigger('keyup');


                },
                complete: function() {
                   // hideLoader();
                },
                error: function (xhr) {
                     hideLoader();
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                         //$(".view_campaign_data").html(message)
                    });
                }
            });
         }else{
               $(".select_busiess_data").multiselect('dataprovider');
              $(".select_busiess_data").multiselect('refresh');
              $("#searchKey").trigger('keyup');
         }
});
//#statusBox
$("#statusBox").change(function(){
    $("#searchKey").trigger('keyup');

 });
 $(function() {
          $( ".datepicker" ).datepicker();
      });
$(document).on('change', '#fromDate', function(){
       var date1 = $(this).val();
       $("#toDate").datepicker("option","minDate",date1);
       if($("#toDate").val()!="")
       $("#searchKey").trigger('keyup');
});
$(document).on('change', '#toDate', function(){
       if($("#fromDate").val()!="")
       $("#searchKey").trigger('keyup');
  });
$(document).on('change', '.select_busiess_data', function(){
       $("#searchKey").trigger('keyup');
});
$(".reset_filter").click(function(){
    if($(".form-control").val(""))
    {    $(".select_busiess_data").multiselect('dataprovider', [{label: "", value: "Select Business"}]);
         $("#searchKey").trigger('keyup');

    }

 });
</script>
@endsection
@endsection
