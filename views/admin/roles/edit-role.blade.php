@extends('layouts.admin')
@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> Roles</h1>
        </div>

        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/role-management') }}">Role Management</a></li>

                <li class="breadcrumb-item"><a  href = "javascript:void(0) ">Role</a></li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">

                <h3>Update Role</h3>

            <div class="tile">

            {{ Form::model($role,array('url' => $roleData['form_action'],'id' => 'role-form','class' => 'plan-form','autocomplete' => 'off')) }}

            <div class="row">
                <div class="col-md-4 plans">
                    <div class="form-group">
                         <b>Role Name : {{ucfirst($role->name)}}</b>
                         {{ Form::hidden('name', $role->name, array('class' => 'form-control','readonly'=>'true')) }}

                         {!!  Form::hidden('role_id',$roleData['record_id']) !!}
                    </div>
                </div>
            </div>
           @foreach ($permissions as $permission_key=>$permission_value)
              <h4>{{$permission_key}}</h4>
              @foreach($permission_value as $sub_permission_key =>$sub_permission_value)
                {{Form::checkbox('permissions[]',  $sub_permission_value->id, $role->permissions ) }}
                {{Form::label($sub_permission_value->title, $sub_permission_value->title) }}<br>
             @endforeach
             @endforeach
              <div class="row">
                <div class="col-md-12 text-right">
                 @if(isset($roleData['record_id']) and $roleData['record_id']!="")
                    <button type="submit" class="btn btn-primary submitBtn">Update</button>
                 @else
                    <button type="submit" class="btn btn-primary submitBtn">Save</button>
                 @endif
                </div>
              </div>
{{ Form::close() }}
            </div>
        </div>
    </div>
</main>
@endsection
