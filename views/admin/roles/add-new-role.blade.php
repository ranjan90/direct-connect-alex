@extends('layouts.admin')
@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> Roles</h1>
        </div>

        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/role-management') }}">Role</a></li>
            @if(isset($roleData['record_id']) and $roleData['record_id']!="")
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">Update Role</a></li>
            @else
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">Add New Role</a></li>
            @endif
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(isset($roleData['record_id']) and $roleData['record_id']!="")
                <h3>Update Role</h3>
            @else
                <h3>Add New Role</h3>
            @endif
            <div class="tile">
            {{ Form::model($roleData['plan_detail'],array('url' => $roleData['form_action'],'id' => 'role-form','class' => 'plan-form','autocomplete' => 'off')) }}
            <div class="row">
                <div class="col-md-4 plans">
                    <div class="form-group">
                        <label for="">Role Name<span class="star">*</span></label>
                        {!!  Form::text('name',null,['placeholder'=>"Role Name",'class'=>'form-control']) !!}
                        {!!  Form::hidden('record_id',$roleData['record_id']) !!}
                    </div>
                </div>
            </div>
            @foreach($permissions as $row)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="checkbox" value="{{$row['id']}}"  id="{{$row['name']}}" name="permissions[]" class="permissions">
                    <label for="remember_me"> {{$row['title']}} </label>
                  </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            @endforeach
              <div class="row">
                <div class="col-md-12 text-right">
                 @if(isset($roleData['record_id']) and $roleData['record_id']!="")
                    <button type="submit" class="btn btn-primary submitBtn">Update</button>
                 @else
                    <button type="submit" class="btn btn-primary submitRoleBtn">Save</button>
                 @endif
                </div>
              </div>
{{ Form::close() }}
            </div>
        </div>
    </div>
</main>
@endsection
