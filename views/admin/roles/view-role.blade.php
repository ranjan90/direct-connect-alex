@extends('layouts.admin')
@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> Roles</h1>
        </div>

        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/add-new-role') }}">Role</a></li>
            
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">View Role</a></li>
           
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
           
                <h3>Update Role</h3>
           
            <div class="tile">

            {{ Form::model($role,array('url' => '','id' => 'role-form','class' => 'plan-form','autocomplete' => 'off')) }}

            <div class="row">
                <div class="col-md-4 plans">
                    <div class="form-group">
                        <label for="">Role Name<span class="star">*</span></label>
                         {{ Form::text('name', null, array('class' => 'form-control'),array('disabled')) }}
                         {!!  Form::hidden('role_id',$roleData['record_id']) !!}
                    </div>
                </div>
            </div>
           @foreach ($permissions as $permission)

                {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                {{Form::label($permission->name, $permission->name) }}<br>

             @endforeach
              <div class="row">
                <div class="col-md-12 text-right">
                <a href="{{url('/admin/role-management') }}"
                    <button type="button" class="btn btn-primary submitBtn">Back</button>
                 </a>
                </div>
              </div>
{{ Form::close() }}
            </div>
        </div>
    </div>
</main>
@endsection
