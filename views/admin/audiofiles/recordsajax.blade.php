<style>
.btn-success{
	color: #FFF;
	background-color: #218838 !important;
	border-color: #1e7e34 !important;
}
.btn-danger {
	color: #FFF;
	background-color: #dc3545 !important;
	border-color: #dc3545 !important;
}
</style>
<?php
  $testUrl = explode('/',$url);
  array_pop($testUrl);
  $testUrl = implode('/', $testUrl);
?>
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th>S.No</th>
            <th>Business Name</th>
            <th>Audio</th>
            <th>Status</th>
            <th>Rejection Reason</th>
            <th>Action Taken On</th>
            <th>Uploaded On</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="show_records">
        @if($data->count()==0)
            <tr>
                <td colspan="7" class="text-center">No Record Found.</td>
            </tr>
        @else
            @foreach($data as $key=>$value)
                <tr id="row_<?php echo $key;?>">
                    <td>{{$key+1}}</td>
                    <td>@if( isset($value->business) && !empty($value->business)){{ucfirst($value->business->business_name)}} @else N/A @endif</td>
                    <td>
                        <?php if( $value->file != '' ){?>
                            <audio controls controlsList="nodownload">
                                <source src="<?php echo $testUrl.'/get_welcome_recording?file='.$value->file.'&bb='.$value->business_id;?>" type="audio/wav">
                            </audio>
                        <?php } ?>
                    </td>
                    <td class="tdStatus">
                        <?php if( $value->isApproved == 'yes' ){ ?>
                            <a href="javascript:void(0)" class="btn btn-success active">Approved</a>
                        <?php } else if( $value->isRejected == 'yes' ){?>
                            <a href="javascript:void(0)" class="btn btn-danger active">Rejected</a>
                        <?php } else if( $value->isApproved == 'no' &&  $value->isRejected == 'no' && $value->deleted_at == '' ){?>
                            <a href="javascript:void(0)" class="btn btn-info active">Waiting for approval</a>
                        <?php } else if( $value->isApproved == 'no' &&  $value->isRejected == 'no' && $value->deleted_at != '' ){?>
                            <a href="javascript:void(0)" class="btn btn-warning active">Deleted</a>
                        <?php } ?>
                    </td>
                    <td class="tdRejectionReason">
                        <?php if($value->rejectionReason != '' && $value->isRejected == 'yes'){
                            if( strlen($value->rejectionReason) > 20 ) { ?>
                                <span title="<?php echo ucfirst($value->rejectionReason);?>" data-original-title="<?php echo ucfirst($value->rejectionReason);?>" data-toggle="tooltip"><?php echo substr($value->rejectionReason,0,20).' ...';?></span>
                            <?php } else {?>
                                <span><?php echo ucfirst($value->rejectionReason);?></span>
                        <?php }
                        } ?>
                    </td>
                    <td class="tdactionTaken">
                        <?php if( $value->isApproved == 'yes' ){
                            echo date('d/m/Y H:i:s',strtotime($value->approvedAt));
                         } else if( $value->isRejected == 'yes' ){
                            echo date('d/m/Y H:i:s',strtotime($value->rejectedAt));
                        } else if( $value->deleted_at  != '' ){
                            echo date('d/m/Y H:i:s',strtotime($value->deleted_at));
                         } else {
                            echo 'N/A';
                        } ?>
                    </td>
                    <td><?php echo date('d/m/Y H:i:s',strtotime($value->created_at));?></td>
                    <td class="tdAction">
                        <?php if( $value->isApproved == 'no' &&  $value->isRejected == 'no' && $value->deleted_at == '' ){ ?>
                            <a style="margin-bottom:10px" class="btn btn-success approveRejectLink" data-type="approve" href="javascript:void(0)" data-row="row_<?php echo $key;?>" data-id="{!! Crypt::encrypt($value->id) !!}" >Approve</a>
                            <a class="confirm_view_pop_up btn btn-danger approveRejectLink" data-type="reject" href="javascript:void(0)" data-row="row_<?php echo $key;?>" data-id="{!! Crypt::encrypt($value->id) !!}" >Reject</a>
                        <?php } ?>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
{!! $data->appends(['searchKey'=>$searchKey,'statusBox'=>$statusBox])->links()!!}
