@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-history"></i>Welcome Audio Files</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item">Welcome Audio Files</li>
            </ul>
        </div>
        <div class="row mb-4">
            @if ($message = Session::get('flash_message'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="form-group pull-left  col-md-6">
                <input type="text" data-url="{{url('ctwdr_dmlogin/welcome-audio-files')}}" class="form-control" id="searchKey" placeholder="Search by Business name">
            </div>
             <div class="form-group pull-left  col-md-4">
                {!!Form::select('statusBox', [''=>'Filter By Status','approved'=>'Approved','rejected'=>'Rejected','waiting'=>'Waiting for approval'], null, ['class' => 'form-control filterCheckBox','id'=>'statusBox'])!!}
            </div>
            <div class="form-group pull-left  col-md-4">
                <label style="opacity:0;" class="d-block">button</label>
                <a href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
             </div>
         </div>
        <div class="tile">
            <div class="tile-body   table-responsive recordsTable">
                @include('admin.audiofiles.recordsajax',['data'=>$data])
            </div>
        </div>
    </main>

 <!-- View Modal -->
<div id="approve-audio-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Approve Audio</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Are you sure to <b>Approve</b> this audio ? </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="approveAudioBtn">Yes</button>
                <button class="btn btn-cancel" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div id="reject-audio-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reject Audio</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="call-transfer-hours">Reason</label>
                        {{ Form::textarea('rejectionReason',old('message'),['id'=>'rejectionReasonTextarea','class'=>'form-control','cols'=>10,'rows'=>2])}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="rejectAudioBtn">Reject</button>
                <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
            </div>
            </div>
        </div>
    </div>
<!-- View Enddds -->
@section('js')
<script src="{{ asset('assets/customer/js/jquery.timepicker.js') }}"></script>
<link href="{{ asset('assets/customer/css/jquery.timepicker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script type="text/javascript">
    var route_url       = "<?php echo url(request()->route()->getPrefix())?>";

    jQuery(document).on('click','.approveRejectLink',function(e)
    {
        var row   = $(this).attr('data-row');
        var id    = $(this).attr('data-id');
        var type  = $(this).attr('data-type');
        if( id != '' && row != '' && type != '')
        {
            localStorage.setItem('rid',id);
    		localStorage.setItem('rowid',row);
            if( type == 'approve')
                $('#approve-audio-modal').modal('show');
            else
                $('#reject-audio-modal').modal('show');
        }
        else
        {
            localStorage.setItem('rid','');
    		localStorage.setItem('rowid','');
        }
    });

    jQuery(document).on('click','#approveAudioBtn',function(e)
    {
        var recordId   = localStorage.getItem('rid');
		var rowId 	   = localStorage.getItem('rowid');
		if( recordId == '' || rowId == '' )
		{
			$.toast({
				heading             : 'Error',
				text                : 'Something is missing. Please try again.',
				loader              : true,
				loaderBg            : '#fff',
				showHideTransition  : 'fade',
				icon                : 'error',
				hideAfter           : 3000,
				position            : 'top-right'
			});
		}
		else
		{
            $.ajax({
                url                : route_url + '/update-audio-status',
                type               : 'post',
                data               : { id:recordId,'type':'approve'},
                headers        : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType     : "json",
                beforeSend : function() {
                    $(".loader_div").show();
                },
                complete   : function() {
                    $(".loader_div").hide();
                },
                success    : function(response) {
                    $(".loader_div").hide();
                    $('#approve-audio-modal').modal('hide');
                    if (response.success)
                    {
                        $.toast({
                            heading               : 'Success',
                            text                  : response.success_message,
                            loader                : true,
                            loaderBg              : '#fff',
                            showHideTransition    : 'fade',
                            icon                  : 'success',
                            hideAfter             : 3000,
                            position              : 'top-right'
                        });
                        $(document).find('.recordsTable tr#'+rowId).children('.tdStatus').html('<a href="javascript:void(0)" class="btn btn-success active">Approved</a>');
                        $(document).find('.recordsTable tr#'+rowId).children('.tdAction').html('');
                        $(document).find('.recordsTable tr#'+rowId).children('.tdactionTaken').html(response.approvedAt);
                    } else {
                        $.toast({
                            heading               : 'Error',
                            text                  : response.error_message,
                            loader                : true,
                            loaderBg              : '#fff',
                            showHideTransition    : 'fade',
                            icon                  : 'error',
                            hideAfter             : 3000,
                            position              : 'top-right'
                        });
                    }
                    $(document).find('.modal-backdrop').remove();

                },
                error     : function(response) {
                  $.toast({
                      heading                           : 'Error',
                      text                              : 'Connection error.',
                      loader                        : true,
                      loaderBg                      : '#fff',
                      showHideTransition    : 'fade',
                      icon                              : 'error',
                      hideAfter                     : delayTime,
                      position                      : 'top-right'
                  });
                }
            });
        };
    });

    jQuery(document).on('click','#rejectAudioBtn',function(e)
    {
        var recordId   = localStorage.getItem('rid');
		var rowId 	   = localStorage.getItem('rowid');
		var reason 	   = $('#rejectionReasonTextarea').val();
		if( recordId == '' || rowId == '' )
		{
			$.toast({
				heading             : 'Error',
				text                : 'Something is missing. Please try again.',
				loader              : true,
				loaderBg            : '#fff',
				showHideTransition  : 'fade',
				icon                : 'error',
				hideAfter           : 3000,
				position            : 'top-right'
			});
		}
        else if( reason == '' )
        {
            $.toast({
				heading             : 'Error',
				text                : 'Please enter a reason for rejection.',
				loader              : true,
				loaderBg            : '#fff',
				showHideTransition  : 'fade',
				icon                : 'error',
				hideAfter           : 3000,
				position            : 'top-right'
			});
        }
		else
		{
            $.ajax({
                url                : route_url + '/update-audio-status',
                type               : 'post',
                data               : { id:recordId,'type':'reject','reason':reason},
                headers        : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType     : "json",
                beforeSend : function() {
                    $(".loader_div").show();
                },
                complete   : function() {
                    $(".loader_div").hide();
                },
                success    : function(response) {
                    $(".loader_div").hide();
                    if (response.success)
                    {
                        $('#reject-audio-modal').modal('hide');
                        $.toast({
                            heading               : 'Success',
                            text                  : response.success_message,
                            loader                : true,
                            loaderBg              : '#fff',
                            showHideTransition    : 'fade',
                            icon                  : 'success',
                            hideAfter             : 3000,
                            position              : 'top-right'
                        });
                        $(document).find('.recordsTable tr#'+rowId).children('.tdStatus').html('<a href="javascript:void(0)" class="btn btn-danger active">Rejected</a>');
                        $(document).find('.recordsTable tr#'+rowId).children('.tdAction').html('');
                        $(document).find('.recordsTable tr#'+rowId).children('.tdactionTaken').html(response.rejectedAt);
                        $(document).find('.recordsTable tr#'+rowId).children('.tdRejectionReason').html(reason);
                    } else {
                        $.toast({
                            heading               : 'Error',
                            text                  : response.error_message,
                            loader                : true,
                            loaderBg              : '#fff',
                            showHideTransition    : 'fade',
                            icon                  : 'error',
                            hideAfter             : 3000,
                            position              : 'top-right'
                        });
                    }
                    $(document).find('.modal-backdrop').remove();
                },
                error     : function(response) {
                  $.toast({
                      heading               : 'Error',
                      text                  : 'Connection error.',
                      loader                : true,
                      loaderBg              : '#fff',
                      showHideTransition    : 'fade',
                      icon                  : 'error',
                      hideAfter             : delayTime,
                      position              : 'top-right'
                  });
                }
            });
        };
    });

//#statusBox
$("#statusBox").change(function(){
    $("#searchKey").trigger('keyup');
});

$(".reset_filter").click(function()
{
    if($(".form-control").val(""))
    {
         $("#searchKey").trigger('keyup');
    }
});
</script>
@endsection
@endsection
