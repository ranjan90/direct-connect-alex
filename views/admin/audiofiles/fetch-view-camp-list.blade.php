<table class="table table-responsive">

                            	@if(isset($user_data->firstName))
									<tr>
										<td><span class="details-user"><b>Customer Name :</b></span></td>
										<td><span class="details_now"> {{ $user_data->firstName.' '.$user_data->lastName }}</span></td>
									</tr>
								@endif
								@if(isset($user_data->email))
									<tr>
										<td><span class="details-user"><b>Customer Email :</b></span></td>
										<td><span class="details_now"> {{ $user_data->email}}</span></td>
									</tr>
								@endif
								@if(isset($leadData->campaign_details->business->business_name))
									<tr>
										<td><span class="details-user"><b>Business Name :</b></span></td>
										<td><span class="details_now"> {{ $leadData->campaign_details->business->business_name}}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->title))
									<tr>
										<td><span class="details-user"><b>Campaign Title :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->title }}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->email))
									<tr>
										<td><span class="details-user"><b>Campaign Email Address :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->email }}</span></td>
									</tr>
								@endif
								@if(isset($leadData->campaign_details->testMail))
									<tr>
										<td><span class="details-user"><b>Campaign Test Mail :</b></span></td>
										<td><span class="details_now"> {!! $leadData->campaign_details->testMail !!}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->template))
									<tr>
										<td><span class="details-user"><b>Campaign Template :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->template }}</span></td>
									</tr>
								@endif
                                @if(isset($leadData->original_lead))
                                    <tr>
                                        <td><span class="details-user"><b>Original Lead :</b></span></td>
                                        <td>
                                            <span class="details_now">
                                                <?php
                                                    $lines = explode(PHP_EOL, $leadData->original_lead);
                                                    if( is_array($lines))
                                                    {
                                                        foreach ($lines as $key => $value)
                                                        {
                                                            echo $value."<br>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo $leadData->original_lead;
                                                    }
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                @endif
								@if(isset($userCamp->country_id) && $userCamp->country_id!=0)
									<tr>
										<td><span class="details-user"><b>Campaign Country :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->coutry_details->countryName }}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->available_days))
									<tr>
										<td><span class="details-user"><b>Available Days :</b></span></td>
										<td><span class="details_now">

										<?php  $days = json_decode($userCamp->available_days);
                                         ?>
										@foreach($days as $day)
											{{ ucfirst($day.' ') }}
										@endforeach
										</span></td>
									</tr>
								@endif
								@if(isset($userCamp->available_hours))
									<?php  $availHours = json_decode($userCamp->available_hours); ?>

									<tr class="mb-2">
										<td><span class="details-user"><b>Available Hours :</b></span></td>
										<td>
											 <p class="details_now"> Mon -Fri {{ $availHours->from->working }} - {{ $availHours->to->working }}</p>
											 <p class="details_now"> Sat {{ $availHours->from->sat }} - {{ $availHours->to->sat }}</p>
											 <p class="details_now"> Sun {{ $availHours->from->sun }} - {{ $availHours->to->sun }}</p>
										</td>
									</tr>
								@endif
								<?php $breakHours = json_decode($userCamp->break_hours); ?>
								@if(isset($userCamp->break_hours))
									<tr class="d-none">
										<td><span class="details-user"><b>Break Hours :</b></span></td>
										<td><span class="details_now"> {{ $breakHours->from }} - {{ $breakHours->to }}</span></td>
									</tr>
								@endif

								<tr><td><span class="details-user"><b>Company Contacts</b></span></td><td></td></tr>
							</table>
							
							<table class="table table-responsive">
								<thead>
									<tr>
										<th>Name</th>
										<th>Phone Number</th>
									</tr>
								</thead>
								<tbody>
								    @if(isset($userCamp->campaigns_contacts))
									@forelse($userCamp->campaigns_contacts as $ct)
									<tr>
										<td>@if(isset($ct->contact->name)){{ $ct->contact->name }}@else N/A @endif</td>
										<td>@if(isset($ct->contact->contact)){{ $ct->contact->contact }}@else N/A @endif</td>
									</tr>
									@empty
									<tr><td colspan="2">No Record Found !</td></tr>
									@endforelse
									@endif
								</tbody>
							
							</table>

	<table class="table">
					
							   					    				<tr><td><span class="details-user"><b>Lead Detail</b></span></td><td></td><td></td><td></td></tr>
							   					    			</table>
							<table class="table table-responsive">
								<thead>
									<tr>
										<th>Lead Id</th>
										<th>Lead Name</th>
										<th>Phone Number</th>
										<th>Message</th>
										<th>Phone Recording</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>@if(isset($leadData->lead_id)){{ $leadData->lead_id }}@else N/A @endif</td>
										<td>@if(isset($leadData->leadName)){{ $leadData->leadName }}@else N/A @endif</td>
										<td>@if(isset($leadData->mobileNo)){{ $leadData->mobileNo }}@else N/A @endif</td>
										<td>

                                                    <?php
                                                      if(!empty($leadData->api_response))
                                                      {
                                                                $api_response=json_decode($leadData->api_response);
                                                                if(!empty($api_response) && isset($api_response->content))
                                                                {
                                                                     if($leadData->status==1)
                                                                      {
                                                                          if(isset($api_response->content->message)){echo $api_response->content->message;}else{ echo 'N/A'; }
                                                                      }
                                                                      else{
                                                                          if(isset($api_response->content->error))
                                                                          {
                                                                             echo $api_response->content->error;
                                                                          }
                                                                          else if(isset($api_response->content->error_message))
                                                                          {
                                                                              echo $api_response->content->error_message;
                                                                          }
                                                                          else if(isset($api_response->content->message))
                                                                          {
                                                                              echo $api_response->content->message;
                                                                          }
                                                                          else{
                                                                            echo 'N/A';
                                                                          }

                                                                      }

                                                                }
                                                                else{
                                                                   echo "N/A";
                                                                 }
                                                      }
                                                      else{
                                                           echo "N/A";
                                                      }
                                                    ?>
										</td>
										<td>
                                            @if(isset($leadData->api_leads))
                                                <audio controls controlsList="nodownload">
                                                      <source src="<?php echo $leadData->api_leads->recording;?>" type="audio/wav">
                                                  </audio>
                                                  <!--a href="{{$leadData->api_leads->recording}}" download="{{$leadData->api_leads->recording}}"><i  class="fa fa-download" aria-hidden="true"></i></a-->
                                                 @else N/A
                                            @endif
                                        </td>
									</tr>
								</tbody>
						
							</table>
							   					    					<table class="table">
					
							   					    			<tr><td><span class="details-user"><b>Call Attempts</b></span></td><td></td><td></td><td></td></tr>
							   					    			</table>
					
								<table class="table table-responsive">
								    
								<thead>
									<tr>
										<th>Lead Id</th>
										<th>Contact Name</th>
										<th>Contact Number</th>
										<th>Contact Email</th>
										<th>Answered By Call</th>
									</tr>
								</thead>
								<tbody>
								    @if(isset($leadData->call_attempt))
								     @forelse($leadData->call_attempt as $call_attempt)
									 <tr> 
									    <td>{!! $call_attempt->lead_id !!}</td>
									    <td>@if(isset($call_attempt->contact_name)) {!! $call_attempt->contact_name !!} @else N/A @endif</td>
										<td>{!! $call_attempt->number !!}</td>
										<td>@if(!empty($call_attempt->contact_email)){!! $call_attempt->contact_email !!} @else N/A @endif</td>
										<td>@if(!empty($call_attempt->sc1_answered)) Yes @else No @endif</td>
									 </tr>
									 @empty
									 <tr><td colspan="5">No Record Found !</td></tr>
									 @endforelse
									 @else
									 <tr><td colspan="5">No Record Found !</td></tr>
									 
									 @endif
								</tbody>
						
							</table>