@extends('layouts.admin')
@section('content')
  <main class="app-content">
      <div class="app-title">
        <div>
          <h1><img class="img-responsive" src="{{ asset('assets/admin/images/home.png') }}" alt="" />
<!--              <i class="fa fa-dashboard"></i>-->
              Dashboard</h1>
        </div>
      </div>
      <div class="row dashrow">
        <div class="col-md-6 col-lg-4">
          <div class="widget-small primary coloured-icon">
            <a href="{{url('ctwdr_dmlogin/customers')}}"><i class="icon fa fa-users fa-3x"></i></a>
            <div class="info">
              <h4><a href="{{url('ctwdr_dmlogin/customers')}}">Customers</a></h4>
              <p><a href="{{url('ctwdr_dmlogin/customers')}}"><b>@if($records['customers']!=0) {{ $records['customers'] }} @else No Records ! @endif</b></a></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="widget-small warning coloured-icon">
            <a href="{{url('ctwdr_dmlogin/regions')}}"><i class="icon fa fa-globe fa-3x"></i></a>
            <div class="info">
              <h4><a href="{{url('ctwdr_dmlogin/regions')}}">Regions</a></h4>
              <p><a href="{{url('ctwdr_dmlogin/regions')}}"><b>@if($records['regions']!=0) {{ $records['regions'] }} @else No Records ! @endif</b></a></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="widget-small info coloured-icon">
            <a href="{{url('ctwdr_dmlogin/plans')}}"><i class="icon fa fa-list fa-3x"></i></a>
            <div class="info">
              <h4><a href="{{url('ctwdr_dmlogin/plans')}}">Plans</a></h4>
              <p><a href="{{url('ctwdr_dmlogin/plans')}}"><b>@if($records['plans']!=0) {{ $records['plans'] }} @else No Records ! @endif</b></a></p>
            </div>
          </div>
        </div>
      </div>
    </main>
@endsection
