@extends('layouts.admin')
@section('content')
<link href="{{ asset('assets/customer/css/wickedpicker.min.css') }}" rel="stylesheet">

<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-phone"></i> Call Settings</h1>
        </div>

        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Call Settings</li>
        </ul>
    </div>
    <div class="row">
        <div id="call-setting" class="tab-pane active
            "><br>
              <div class="update-password">
                <div class="">
                @if(isset($callSetting) && !empty($callSetting))
                  {{ Form::model($callSetting,array('id'=>'call_setting_form','url'=>URL('ctwdr_dmlogin/call-setting'))) }}
                @else
                  {{ Form::open(array('id'=>'call_setting_form','url'=>URL('ctwdr_dmlogin/call-setting'))) }}
                @endif
                  <div class="col-md-12">
                    <div class="row">
                    	<div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_recording_display">Call Company Contacts</label>
                          </br>
                          {{ Form::radio('call_method',3)}} <span class="yes-text">In Order - We will call contacts in the order they are provided for each Campaign.</span>
                          </br>
                          {{ Form::radio('call_method',4)}} <span class="yes-text">Random Order - We will call the Campaign contacts in random order each time.  Ideal for Sales teams!</span>
                        </div>
                      </div>

                      	

                      <div class="col-md-12 d-none">
                        <div class="form-group">
                          <label for="call-transfer-hours">Call Transfer Hours</label>
                          {{ Form::textarea('call_transfer_hours',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <!-- <label for="call_announcement">CALL ANNOUNCEMENT – THIS MESSAGE WILL ANNOUNCE THE CALL TO YOUR COMPANY CONTACTS..</label>
                          {{ Form::textarea('call_announcement',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                          </br> -->
                           <!-- <label for="call_announcement">Call Recording Announcement – Legal Disclaimer This message must be delivered if the call recording is turned ON.</label>
                           {{ Form::textarea('call_announcement_2',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}} -->
                        </div>
                      </div>


                     


                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="welcome_message">Call Welcome Message This will announce the call to your Company contacts.</label>
                          {{ Form::textarea('call_announcement',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>

                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="welcome_message">Lead Concierge Announcement</label>
                          {{ Form::textarea('lead_concierge_announcement',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      

                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="customer_wait_message">CALL INSTRUCTIONS – THESE INSTRUCTIONS ARE GIVEN TO YOUR COMPANY CONTACT BEFORE DIALLING THE CUSTOMERS PHONE NUMBER.</label>
                          {{ Form::textarea('call_instructions',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group --call-recording">
                          <label for="--call_recording_display">CALL RECORDING DISPLAY</label>
                          </br>
                          {{ Form::radio('call_recording_display',1)}} <span class="yes-text">Yes</span>
                          {{ Form::radio('call_recording_display',2)}} <span class="yes-text">No</span>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_announcement_email">Call Summary Email</label>
                          {{ Form::radio('call_announcement_email',1,false,array('class'=>'annEmail')) }} <span class="yes-text">Yes</span>
                          {{ Form::radio('call_announcement_email',2,false,array('class'=>'annEmail')) }} <span class="yes-text">No</span>
                        </div>
                      </div>
                      <div class="col-md-12 emailDiv">
                        <div class="form-group">
                          <!--<label for="email_subject">Email Subject</label> {{ Form::text('email_subject',null,['class'=>'form-control']) }}-->
                          {{ Form::hidden('email_subject',null,['class'=>'form-control']) }}
                        </div>
                      </div>
                      <div class="col-md-12 emailDiv">
                        <div class="form-group">
                          <!--<label for="email_body">Email Body</label>{{ Form::textarea('email_body',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}-->
                          {{ Form::hidden('email_body',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      {{ Form::hidden('retry_time',0,['class'=>'form-control','min'=>'1','max'=>'120']) }}
                       {{ Form::hidden('retry_delay_second',5,['class'=>'form-control','min'=>'1','max'=>'60']) }}
                      <!--<div class="col-md-6">
                        <div class="form-group">
                          <label for="retry_time">Retry Time</label>
                          {{ Form::text('retry_time',null,['class'=>'form-control','min'=>'1','max'=>'120']) }}
                        </div>
                      </div>-->
                      <!--<div class="col-md-6">
                        <div class="form-group">
                          <label for="retry_delay_second">Retry Delay Time (in seconds)</label>
                        {{ Form::text('retry_delay_second',null,['class'=>'form-control','min'=>'1','max'=>'60']) }}
                        {{ Form::select('retry_delay_second',$retry_delay_second,'null',['class'=>'form-control'])}}
                         </div>
                      </div>-->
                      <div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_recording_display">Business Caller ID</label>
                          </br>
                          {{ Form::text('callerID_sc1',null,['class'=>'form-control'])}}
                          </br>
                           <label for="call_recording_display">Customer Caller ID</label>
                          {{ Form::text('callerID_sc2',null,['class'=>'form-control'])}}
                        </div>
                      </div>

                      <div class="col-md-12">
                        <input type="submit" class="btn btn-primary" value="Submit">
                      </div>
                    </div>
                  </div>
                {{ Form::close() }}
                </div>
              </div>
            </div>
    </div>
</main>
@section('js')
<script src="{{ asset('assets/customer/js/wickedpicker.js')}}"></script>
<script type="text/javascript">
    $('.timepicker').wickedpicker({twentyFour: true});
    $("#call_setting_form").validate({
    errorClass   : "has-error",
    highlight    : function(element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight  : function(element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:{
      call_announcement: {
        required: true,
        noSpace: true,
      },
      call_announcement_2: {
        required: true,
        noSpace: true,
      },
      welcome_message: {
        required: true,
        noSpace: true,
      },
       customer_wait_message: {
        required: true,
        noSpace: true,
      },
       callerID_sc1: {
                required: true,
                noSpace: true,
                minlength: 10,
                maxlength: 11,
                 number: true
      },
      callerID_sc2: {
                required: true,
                noSpace: true,
                minlength: 10,
                maxlength: 11,
                 number: true
      }
    },
    messages:{
    	callerID_sc1:{
    	  'minlength':'Please enter 10 digit business caller id',
          'maxlength':'Please enter 11 digit business caller id'
    	},
    	callerID_sc2:{
    	  'minlength':'Please enter 10 digit customer caller id',
          'maxlength':'Please enter 11 digit customer caller id'
    	}

    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
</script>
@endsection
@endsection
