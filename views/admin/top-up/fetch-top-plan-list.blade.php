@if($data->count()==0)
  <tr>
      <td colspan="4" class="text-center">
        No Record Found.
      </td>
  </tr>
  @else
  <?php $no=1;?>
  @foreach($data as $key=>$value)
      <tr>
        <td>@if($value->top_up_type==2) Group Top Up  @else Normal Top Up @endif</td>
       <!--  <td>@if($value->top_up_type==2) {{$value->number_of_business}}  @else N/A @endif</td> -->
        <td>Top Up {{ $no++ }}</td>
        <td> 
        <?php $planPrices = ''; ?>
        @foreach($value->top_up_prices as $price)
            <?php $planPrices .= $price->plan_currency->currencyCode.' '.$price->price.', '; ?>
        @endforeach
        {{ rtrim($planPrices,', ') }}
         </td>
        <td><?php $creditPrices = ''; ?>
        @foreach($value->top_up_prices as $price)
            <?php $creditPrices .= $price->plan_currency->currencyCode.' '.$price->credit.', '; ?>
        @endforeach
        {{ rtrim($creditPrices,', ') }}</td>
        <td>
        <a href="{{ $url }}/edit-top-up/{{ Crypt::encrypt($value->id) }}"><i class="fa fa-pencil"></i></a>
        <a class="confirm_delete_pop_up" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" data-id="{!! Crypt::encrypt($value->id) !!}"><i class="fa fa-trash" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>
        <!--<a href="{!! $url.'/purchase-order/'.Crypt::encrypt($value->id) !!}"><i class="fa fa-eye"></i></a>-->
       </td>
     </tr>
  @endforeach
@endif