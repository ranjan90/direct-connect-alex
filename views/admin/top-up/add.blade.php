@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-list"></i> Manage Top Up</h1>
            </div>

            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ URL(config('app.newadmintemplatename').'/manage-top-up') }}">Manage Top Up</a></li>
                @if(!empty($top_up))
                <li class="breadcrumb-item"><a href="#">Edit Top Up</a></li>
                @else

                <li class="breadcrumb-item"><a href="#">Add Top Up</a></li>
  @endif

               </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
              @if(!empty($top_up))
                <h3>Edit Top Up</h3>
              @else

                <h3>Add Top Up</h3>
@endif
                <div class="tile">
                {{ Form::model($top_up,array('url' =>$url.'/create-update-top-up','id' => 'top-up-form','class' => 'top-up-form','autocomplete' => 'off')) }}

                 <div class="row">
                
                       <div class="col-md-4 plans">
                            <div class="form-group">
                                <label for="">Plan Type<span class="star">*</span></label>
                                {!! Form::select('top_up_type',[null=>'select type',1=>'Normal Plan',2=>'Group Plan'],null, ['class'=>'form-control top_up_type']) !!}
                            </div>
                      </div>

                      <!--  <div class="col-md-4 lans no_of_business_class" style="display:none;">
                        <div class="form-group">
                            <label for="">No of business<span class="star">*</span></label> -->
                            {!!  Form::hidden('number_of_business',null,['placeholder'=>"No of business",'class'=>'number_of_business form-control parseInt']) !!}
                      <!--   </div>
                    </div> -->

                </div>
                <div class="row">
                    <div class="col-md-12 ">
                         <table class="table table-responsive priceTable">
                             <thead>
                               <tr><th>Plan Price</th><th>Credit Amount</th><th  colspan="2">Currency</th></tr>
                           </thead>
                            <tbody class="priceTableBody">

                            </tbody>
                        </table>
                    </div>
               </div>
               <div class="row">
                <div class="col-md-6 text-left">
                    <span class="add-more-top-price">Add More <i class="fa fa-plus-square"></i></span>
                </div>
            </div>

                <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Description<span class="star">*</span></label>
                         {!! Form::hidden('id', $hidden_id) !!}
                        {!!  Form::textarea('description',null,['placeholder'=>"Description",'class'=>'form-control','rows'=>'4']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary submitBtn">Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
		</div>
        </div>
    </main>
 @section('js')
  <script type="text/javascript">
       $(document).on('change','.top_up_type',function($d){
           if($(this).val()==2)
           {
            $(".number_of_business").each(function (item) {
            $(this).rules("add", {
                required: true,
              });
          });
             $(".no_of_business_class").show();
           }
           else{
            $(".number_of_business").each(function (item) {
            $(this).rules("add", {
                required: false,
              });
          });
              $(".no_of_business_class").hide();
           }
       });
 </script>
 <script type="text/javascript">
    //forgot Password
    var top_price="";
    var c=0
    var currency=<?php echo json_encode($currency);?>;
    console.log(currency);
    var TopUpPriceArray=<?php echo json_encode($TopUpPriceArray);?>;
    $("#top-up-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
                {
                    "data[][price]":
                    {
                      required: true,
                    },
                    "data[][credit]":
                    {
                      required: true,
                    },
                    description:{
                         required: true,

                    },
                    number_of_business:{
                        number:true
                    },
                    top_up_type:{
                         //topupCheckValue:true,
                          required: true
                    }
                },
        messages:
                {
                    "data[][price]": {
                        required: "Plan price is required.",
                    },
                    "data[][credit]": {
                        required: "Plan credit is required.",
                    },
                    description: {
                        required: "Plan description is required.",
                    },


                },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
    $(document).on('keypress', '.validNumber', function (eve) {
    if (eve.which == 0) {
        return true;
    } else {
        if (eve.which == '.') {
            eve.preventDefault();
        }
        if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) {
            if (eve.which != 8)
            {
                eve.preventDefault();
            }
        }

        $('.validNumber').keyup(function (eve) {
            if ($(this).val().indexOf('.') == 0) {
                $(this).val($(this).val().substring(1));
            }
        });
    }


});
function itemS(price="",credit="",id="",currencyval="")
{
       var currencyData="";
       $.each(currency,function(k,v){
                var selected="";
                if(currencyval==k)
                {
                     selected="selected";
                }
                currencyData+="<option "+selected+" value="+k+">"+v+"</option>"
       });
       top_price='<tr><td class="form-group"><input type="hidden" name="data['+c+'][id]" value='+id+'><input placeholder="Price" class="form-control validNumber priceForm" name="data['+c+'][price]" type="text" value='+price+'></td><td class="form-group creditfield"><input placeholder="Credit"  class="form-control creditForm validNumber" name="data['+c+'][credit]" type="text" value='+credit+'></td><td class="form-group"><select class="form-control currency_id" name="data['+c+'][currency_id]">'+currencyData+'</select></td><td><span class="top_delete_price"><i class="fa fa-close"></i></span></td></tr>';

       $('.priceTableBody').append(top_price);
          var tableRows = $(document).find(".priceTable tbody tr").length;
        if(tableRows==1){
            jQuery(".top_delete_price").hide();
        }else{
           jQuery(".top_delete_price").show();
        }
       c++;

}
$(document).on('click','.add-more-top-price',function(){

    itemS("","","","");
});
//top_delete_price
$(document).on('click','.top_delete_price',function(key_val,key_valule){
        var rows = $(document).find(".priceTable tbody tr").length;
        if(rows==2){
            jQuery(".top_delete_price").hide();
            //jQuery(".del-item").hide();
        }else{
             jQuery(".top_delete_price").show();
        }
        //c--;
        $(this).parents("tr").remove();
});

    if(TopUpPriceArray.length>0)
    {
         $.each(TopUpPriceArray,function(s,b){
             itemS(b.price,b.credit,b.id,b.currency_id);
         });

    }
    else
    {
     $('.add-more-top-price').trigger('click');
    }
       $('.top_up_type').trigger('change');
    
 </script>
 @endsection
@endsection
