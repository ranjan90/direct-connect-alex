@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><img class="img-responsive" src="{{ asset('assets/admin/images/plans_billing.png') }}" alt="" />
<!--                    <i class="fa fa-money"></i>-->
                    Manage Top Up</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item">Manage Top Up</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <!--<div class="form-group pull-left  col-md-4 nopadding">
                            <input type="text" data-url="#" class="form-control" id="searchKey-stop" placeholder="Search Top Up by Name">
                        </div>-->
                        <div class="form-group text-right">
                            <a href="{!! $url.'/add-new-top-up'!!}" class="btn btn-primary">Add Top Up</a>
                        </div>
                    </div>
                </div>
                <div class="tile">
                    <div class="tile-body   table-responsive recordsTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Top Type</th>
                                   <!--  <th>Number Of Business</th> -->
                                    <th>Top Up Name</th>
                                    <th>Price</th>
                                    <th>Credit Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                             <tbody class="show_records">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@include('includes.delete_pop_up')
@section('js')
<script type="text/javascript">
var route_url="<?php echo url(request()->route()->getPrefix())?>";
var get_data_url=route_url+'/fetch-top-plan-list';
var confirm_delete=route_url+'/delete-top-up';
var redirect_url=route_url+'/manage-top-up';

var deleteLocalStorge=1;

</script>
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script src="{{ asset('assets/js/delete.js') }}"></script>
<script src="{{ asset('assets/js/fetch.js') }}"></script>
@endsection
@endsection
