@if($data->count()==0)
    <tr>
        <td colspan="5" class="text-center">No Record Found.</td>
    </tr>
@else

@foreach($data as $key=>$value)
    <tr id="promocode_{{ $key }}">
        <td>{{ $value->title }}</td>
        <td>{{ $value->credits }}</td>
        <td>
            <?php
            $whereData = [
                  ['id', $value->plan_id]
            ];
            $parserOutput = Helper::getRecordData('plans', $whereData);
            if(!empty($parserOutput))
                echo $parserOutput->name;
            ?>
        </td>
        <td>{{ $value->noofmonths }}</td>
        <td><?php echo "<pre>";print_r(date('d/m/Y', strtotime($value->expireon)));?></td>
        <td>{{ $value->noofuses }}</td>
        <td>{{ $value->usedcount }}</td>
         <td>{{ $value->promo_code_count->count() }}</td>
        <td>
            <a   class="view_promo_customer" href="javascript:void(0)" data-target="#customer_promo_modal" data-toggle="modal" data-id="{!! Crypt::encrypt($value->id) !!}" ><i class="fa fa-eye" title="View Promo Customer" data-original-title="View Promo Customer" data-toggle="tooltip"></i></a>
            <a  href="{{ $url }}/edit-promo-code/{{ Crypt::encrypt($value->id) }}"><i class="fa fa-pencil"></i></a>
            <a data-no="{{$key}}" class="confirm_delete_pop_up" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" data-id="{!! Crypt::encrypt($value->id) !!}"><i class="fa fa-trash" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>
        </td>
    </tr>
@endforeach
@endif
