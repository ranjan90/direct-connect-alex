@extends('layouts.admin')
@section('content')
 <style type="text/css">
      .agency_check_box
      {
             vertical-align:middle;
      }

     .add_email_class{
         display: inline-block;
         width: auto;
      }
 </style>
        <main class="app-content">
            <div class="app-title">
                <div>
                    <h1><i class="fa fa-list"></i> Manage Promo Code</h1>
                </div>
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ URL(config('app.newadmintemplatename').'/promo-codes') }}">Manage Promo Code</a></li>
                    @if(!empty($promo_code))
                        <li class="breadcrumb-item"><a href="#">Edit Promo Code</a></li>
                    @else
                        <li class="breadcrumb-item"><a href="#">Add Promo Code</a></li>
                    @endif
                </ul>
            </div>
            <div class="container">
            <div class="row">
                    @if(!empty($promo_code))
                        <h3>Edit Promo Code</h3>
                    @else
                        <h3>Add Promo Code</h3>
                    @endif
                    <div class="tile w-100">
                        <?php
                        $check_d=0;
                             if(!empty($promo_code))
                              {
                                if(!empty($promo_code->agency_check_box))
                                {
                                     $check_d=1;

                                }
                              }
                        ?>
                        {{ Form::model($promo_code,array('url' =>$url.'/create-update-promo-code','id' => 'promo-code-form','class' => 'promo-code-form','autocomplete' => 'off')) }}
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Title<span class="star">*</span></label>
                                            {!! Form::hidden('id',$hidden_id) !!}
                                            {!! Form::text('title',null,['placeholder'=>"Title",'class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="creditfield col-md-6">
                                        <div class="form-group">
                                            <label for="">No Of Credits</label>
                                            {!! Form::text('credits',null,['placeholder'=>"100",'class'=>'promoinput form-control validNumber','min'=>'1','max'=>'100000','id'=>'promocodeCredit']) !!}
                                            <label id="promocodeCredit-error" class="has-error" for="promocodeCredit" style="display:none">Please choose credits or plan or both.</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="">Select Plan</label>
                                            {!! Form::select('plan_id',$plansData,null, ['class'=>'promoinput form-control','id'=>'promocodePlan']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="">No Of Months</label>
                                            {!! Form::number('noofmonths',null,['placeholder'=>"No of Months",'class'=>'form-control','min'=>0,'id'=>'noofmonths']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Expire On<span class="star">*</span></label>
                                            {!! Form::text('expireon',null,['placeholder'=>"Expire On",'class'=>'form-control datepicker']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No Of Uses<span class="star">*</span></label>
                                                {!! Form::number('noofuses',null,['placeholder'=>"No of uses",'class'=>'form-control','min'=>0]) !!}
                                            </div>
                                    </div>

                                    <div class="col-md-6">
                                            <div class="form-group">


                                                 {{ Form::checkbox('agency_check_box', null, null,['class'=>'agency_check_box']) }}
                                                 <label for="" class="d_l_class">Is Agency Promo Code ? </label>


                                                </div>
                                                   <div class="form-group new_sign_up_email" style="display:none">
                                                            <label for="">Enter new client sign up email<span class="star">*</span></label>
                                                            {!! Form::text('agency_email',null,['placeholder'=>"New Client Email",'class'=>'form-control']) !!}

                                                             <label for="">{!! Form::checkbox('email_notification',null,null,['class'=>'add_email_class form-control']) !!} Turn email notifications  when a new client signs up using this promo code. ? </label>


                                                             <label for="">{!! Form::checkbox('add_email_address',null,null,['class'=>'add_email_class form-control']) !!}Add email address as an Agency to any account that signs up using this promo code ?</label>

                                                   </div>
                                    </div>




                               <div class="col-md-12 text-right">
                                   <button type="submit" class="btn btn-primary submitBtn">Save</button>
                               </div>
                      {{ Form::close() }}
                                </div>
                            </div>

                    </div>
            </div>
        </main>
 @section('js')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script type="text/javascript">
    var check_d="<?php echo $check_d?>";
    if(check_d==1)
    {
      $(".new_sign_up_email").show();
    }
    $( window ).on("load", function() {
        $('#promocodePlan').trigger('change');
    });
    $("#promo-code-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            title:
            {
              required: true,
            },
            expireon:{
                 required: true,
            },
            noofmonths:{
                 freesubscriptionmonths: true,
            },
            noofuses:{
                 required: true,
            }
        },
        messages:
        {
            title: {
                required: "This field is required.",
            },
            expireon: {
                required: "This field is required.",
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });


    $(document).on('click','.agency_check_box',function(){

              if($(this).is(':checked'))
              {
                $(".new_sign_up_email").show();
              }else{
               $(".new_sign_up_email").hide();
              }
    });

    $(document).on('change', '#promocodePlan', function (eve)
    {
        var promocode = $('#promocodePlan').val();
        if( promocode > 0 )
            $('#noofmonths').attr('readonly',false);
        else
            $('#noofmonths').attr('readonly',true).val('');
        if($(this).val()=='audio_promo_code')
        {
              $(".agency_check_box").hide();
              $(".d_l_class").hide();
              $("#promocodeCredit").attr('readonly',true).val('');
        }
        else{
                 $('#promocodeCredit').attr('readonly',false);
                  $(".agency_check_box").show();
                  $(".d_l_class").show();
        }

    });

    $(document).on('keypress', '.validNumber', function (eve)
    {
        if (eve.which == 0)
        {
            return true;
        }
        else
        {
            if (eve.which == '.') {
                eve.preventDefault();
            }
            if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57))
            {
                if (eve.which != 8)
                {
                    eve.preventDefault();
                }
            }
            $('.validNumber').keyup(function (eve)
            {
                if ($(this).val().indexOf('.') == 0)
                {
                    $(this).val($(this).val().substring(1));
                }
            });
        }
    });

    $(function() {
        $( ".datepicker" ).datepicker();
    });
 </script>
 @endsection
@endsection
