@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-tag"></i>Manage Promo Codes</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item">Manage Promo Codes</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group text-right">
                            <a href="{!! $url.'/add-promo-code'!!}" class="btn btn-primary">Add Promo Code </a>
                        </div>
                    </div>
                </div>
                <div class="tile">
                    <div class="tile-body   table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Credits</th>
                                    <th>Plan</th>
                                    <th>No Of Months</th>
                                    <th>Expire On</th>
                                    <th>Total Limit</th>
                                    <th>Used limit</th>
                                    <th>No Of Promo Code Usage</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                             <tbody class="show_records"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
     <!-- View Modal -->
    <div id="customer_promo_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Promo Code Users</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body view_customer_promo_modal">

                </div>
            </div>
        </div>
    </div>
    <!-- View Enddds -->
@include('includes.delete_pop_up')
@section('js')
<script type="text/javascript">
var route_url="<?php echo url(request()->route()->getPrefix())?>";
var get_data_url=route_url+'/fetch-promo-code';
var confirm_delete=route_url+'/delete-promo-code';
var redirect_url=route_url+'/promo-codes';
var promo_url=route_url+'/view-promo-customer-detail';

</script>
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script src="{{ asset('assets/js/delete.js') }}"></script>
<script src="{{ asset('assets/js/fetch.js') }}"></script>
<script>
        jQuery(document).on('click','.view_promo_customer',function(e)
    {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:promo_url+'/'+jQuery(this).attr('data-id'),
                dataType: "json",
                 beforeSend: function() {
                    showLoader();
                },
                success: function (res)
                {
                    hideLoader();
                    $(".view_customer_promo_modal").html(res)
                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                     hideLoader();
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        //tostrerror(message);
                         $(".view_customer_promo_modal").html(message)
                    });
                }
            });

});

</script>
@endsection
@endsection
