<h6>Promo Code :{{$PromoCodeData}}</h6>
<table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Business Name</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Apply By</th>
                                    <th>Apply Date</th>


                                </tr>
                            </thead>
                             <tbody class="show_records">

                                  @foreach($data as $key=>$value)


                                      <tr>
                                              <td>
                                               #
                                               </td>
                                                <td>
                                               @if(!empty($value->business->business_name)){{$value->business->business_name}} @else N/A @endif
                                               </td>
                                                  <td>
                                               @if(isset($value->business->users[0]->firstName)){{$value->business->users[0]->firstName}} @else N/A @endif
                                               </td>
                                                <td>
                                               @if(isset($value->business->users[0]->email)){{$value->business->users[0]->email}} @else N/A @endif
                                               </td>
                                                <td>
                                                @if($value->apply_by==1) Admin @else Customer @endif
                                               </td>
                                               <td>
                                               @if(isset($value->created_at)){!! \Carbon\Carbon::parse($value->created_at)->format('d/m/Y') !!} @else N/A @endif
                                               </td>



                                     </tr>
                                  @endforeach

                            </tbody>
                        </table>

                         {!! $data->links()!!}
