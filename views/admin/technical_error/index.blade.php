@extends('layouts.admin')
@section('content')
<style>

.btn.btn-success {
    color: #fff;
    background-color: rgb(147, 213, 0);
    border-color: rgb(147, 213, 0);
    
}
.btn-warning.btn {
    border: #ffa300;
    background: #ffa300;

}
.btn.btn-danger {
    background-color: #dc3545;
    border-color: #dc3545;
    
}
.set_btn_width{
    width: 76px;
        padding: 12px;
}
</style>
    <main class="app-content">
                    <div class="app-title">
                        <div>
                            <h1><i class="fa fa-history"></i>Technical Error</h1>
                        </div>
                        <ul class="app-breadcrumb breadcrumb">
                             <li class="breadcrumb-item">Technical Error</li>
                        </ul>
                    </div>
                    <div class="row mb-4">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                       
                    </div>
                            <div class="form-group  col-md-6">
                            <label>Search</label>
                             <label></label>
                             <!--<input type="text" data-url="{{url('ctwdr_dmlogin/logs')}}" class="form-control" id="searchKey" placeholder="Search by Lead Id,Lead Name,Campaign Email Id,Phone No">-->
                                 <input type="text" data-url="{{url('ctwdr_dmlogin/technical-error')}}" class="form-control" id="searchKey" placeholder="Search by user email,user name">
                     
                        </div>

                <div class="tile">
                    <div class="tile-body   table-responsive recordsTable">
                           @include('admin.technical_error.fetch-log-list',['data'=>$data])
                    </div>
                </div>
    </main>

@section('js')

@endsection
@endsection
