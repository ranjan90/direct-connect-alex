<table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>User Name</th>
                                    <th>User Email</th>
                                    <th>Business Name</th>
                                    <th>Error Message</th>
                                    <th>Error File Path</th>
                                    <th>Error Line Number</th>
                                    <th>Error Controller Name</th>
                                    <th>Error Method</th>
                                    <th>Error Type</th>
                                    <th>Request Data</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                             <tbody class="show_records">
                                  @if($data->count()==0)
                                  <tr>
                                      <td colspan="9" class="text-center">
                                        No Record Found.
                                      </td>
                                  </tr>
                                  @else
                                  @foreach($data as $key=>$value)
                                      <tr>
                                              <td>#</td>
                                              <td>@if(isset($value->user_name)){{ $value->user_name }} @else N/A @endif</td>
                                              <td>@if(isset($value->user_email)){{ $value->user_email }} @else N/A @endif</td>
                                               <td>@if(isset($value->business->business_name)){{ $value->business->business_name }} @else N/A @endif</td>
                                            <td>@if(isset($value->message)){{ $value->message }} @else N/A @endif</td>
                                            <td>@if(isset($value->file_path)){{ $value->file_path }} @else N/A @endif</td>
                                             <td>@if(isset($value->line_number)){{ $value->line_number }} @else N/A @endif</td>
                                             <td>@if(isset($value->controller_name)){{ $value->controller_name }} @else N/A @endif</td>
                                              <td>@if(isset($value->method)){{ $value->method }} @else N/A @endif</td>
                                               <td>@if(isset($value->error)){{ $value->error }} @else N/A @endif</td>
                                                <td>@if(isset($value->request_data)){{ $value->request_data }} @else N/A @endif</td>
                                               <td>@if(isset($value->created_at)){{ $value->created_at }} @else 0 @endif</td>
                                               
                                     </tr>
                                     
                              
                                  @endforeach
                                @endif
                            </tbody>
                        </table>
                        {!! $data->appends(['searchKey'=>$searchKeyData])->links()!!}
