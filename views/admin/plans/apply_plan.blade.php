@extends('layouts.admin')
@section('content')
<link href="{{ asset('assets/admin/css/sb-admin.css') }}" rel="stylesheet">
<style type="text/css">
	.price-dollar{
		background: none !importent;
	}
</style>
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-list"></i>Apply Group Plan</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item">Apply Group Plan</li>
            </ul>
        </div>
        <div class="div-main_row">
            <h2>{{$user_data->firstName}} {{$user_data->lastName}}</h2>
        <div class="row"> 
             <div class="col-md-6">
                 <div class="details-div"> 
                <table>
                        <tbody>
                                <tr>
                                    <td><span class="details-user"><b>Email :</b></span></td>
                                    <td><span class="details_now">{{$user_data->email}}</span></td>
                                </tr>
                                  @if(isset($user_data->is_group) && empty($user_data->is_group)) 
                                <tr>
                                    <td><span class="details-user"><b> Total business :</b></span></td>
                                    <td><span class="details_now">{{$total_business}}</span></td>
                                </tr> 
                                   <tr>
                                    <td><span class="details-user"><b>Total active subscription :</b></span></td>
                                    <td><span class="details_now">{{$plan_type_1+$plan_type_2}}</span></td>
                                </tr>
                                @endif 
                                 
                                </tbody>
               </table>
              </div> 
              <div class="text-left">
            @if(isset($user_data->is_group) && !empty($user_data->is_group)) 
                          <button class="btn btn-rounded cancel_group_plan_btn"  target="#cancel-plan-group-modal" data-id="\Crypt::encrypt($user_data->id)" type="button">Cancel Group Plan</button>
           </div>
           </div>
           @endif
                <div class="col-md-6">
                                        <div class="detil-ds"> 
                                        <table class="table">
                                               <tbody>
                                                  <tr> 
                                                  <td><span class="details-user"><b>Add Number Of business:</b></span></b></td>  
                                                      <td> 
                                                        <span class="details_now">  {!!  Form::number('number_of_business',isset($UserGroupAccount->number_of_business) ? $UserGroupAccount->number_of_business :null,['placeholder'=>"No of business",'class'=>'number_of_business form-control parseInt details_now']) !!}</span>
                                                    </td> 
                                                 <tr> 
                                                     <tr> 
                                                  <td><span class="details-user"><b>Set Top Recharge Credit:</b></span></b></td>  
                                                      <td> 
                                                        <span class="details_now"> {!!  Form::number('top_recharge_credit',isset($UserGroupAccount->top_recharge_credit) ? $UserGroupAccount->top_recharge_credit :null,['placeholder'=>"No of credit",'class'=>'top_recharge_credit form-control parseInt details_now']) !!}</span>
                                                    </td> 
                                                 <tr>
                                                    </tbody>
                                                </table> 
                                                </div> 
                                                 </div> 
                                                 </div>
                                                
        </div>
         <div class="row">
                                   <div class="col-md-12 show_records" id="PlanBilling"> 

                                   </div> 
                                   </div>
    </main>



    <div id="cancel-plan-group-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                     <h6>Are you sure you want to cancel group plan ? after that user will be moved on normal business plan</h6>
                  
                 
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" id="cancel_yes" class="btn btn-primary cancelConfirmDeleteModal">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


       <!-- Delete Modal -->
    <div id="subTopUpPlanData-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                     <h6>Are you sure you want to active this plan so please choose at least one card </h6>
                	 <div class="tile-body   table-responsive recordsTable">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            	<th>Choose Card</th>
                                                <th>Business Name</th>
                                                <th>Name</th>
                                                <th>Last Digit </th>
                                                <th>Exp Month</th>
                                                <th>Exp Year</th>
                                                <th>Brand</th>
                                                <th>Default</th>
                                            </tr>
                                        </thead>
                                        <tbody id="show_record_card" class="show_records_card">
                                        </tbody>
                                    </table>
                                </div>
                 
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" id="business_yes" class="btn btn-primary businessConfirmDeleteModal">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->
  @section('js')
<script type="text/javascript">
var customer_id="<?php echo $id ?>"; 
var is_group="<?php echo $user_data->is_group ?>";
var route_url="<?php echo url(request()->route()->getPrefix())?>";
var get_data_url=route_url+'/fetch-group-plan-list/'+customer_id;
var get_card_url=route_url+'/fetch-business-card-list?customer_id='+customer_id;
var set_group_plan=route_url+'/set-group-plan';
var cancel_group_plan=route_url+'/cancel-group-plan';
var refresh_url=route_url+"/customers/apply-group-plan/"+customer_id;
$(document).on('click','.checkTopUpPrc',function(){
        var Obj         =   $(this);
        $(".checkTopUpPrc").removeClass('activeCls');
        $(".checkTopUpPrc").text('Select');
        // $('.checkTopUpPrc').css('background','#fff');
        // $('.checkTopUpPrc').css('color','#2961af');
        $('.checkTopUpPrc').css('background','#223c4b');
        Obj.addClass('activeCls');
        Obj.text('Selected');
        Obj.css('background','#2961af');
        Obj.css('color','#fff');
        $(".topupId").val($(this).attr('data-plainid'));
        if($(".payId").val()=="" && is_group==0)
        $(".subTopUpPlanData"). prop("disabled", true);
        else
        $(".subTopUpPlanData"). prop("disabled", false);  
        //$(".checkPlainPrc").trigger('click');  
  });

 $(document).on('click','.checkPlainPrc',function(){
        var Obj         =   $(this);
        var parentCls   =   $(Obj).parents('div.plan-bx');
        var planid      =   $(Obj).attr('data-plainid');
        var payId       =   $(Obj).attr('data-payId');
        var modelData   =   $(Obj).attr('data-mod');
        if(planid != "" && planid > 0 )
        {
            $('.checkPlainPrc').text('Buy Now');
            $('.checkPlainPrc').removeClass('activeCls');
            $('.checkPlainPrc').css('background','#2961af');
            $('.checkPlainPrc').css('color','#fff');
            $('.plan-bx').removeClass('activeClsse');
            $(this).addClass('activeCls');
            $(this).text('Selected');
            $(this).css('background','#2961af');
            $(this).css('color','#fff');
            $(parentCls).addClass('activeClsse');
            $('.usedPlainId').val(planid);
            $('.payId').val(payId);
            $('.modelToken').val($.trim(modelData));
        }
        //disableBtn('.usedPlainId','.subPlanData');
        if($('.checkTopUpPrc').hasClass('activeCls'))
        $(".subTopUpPlanData"). prop("disabled", false);
        else
        if(is_group==0)
        $(".subTopUpPlanData"). prop("disabled", true);
        else
        $(".subTopUpPlanData"). prop("disabled", false);
          
        //$(".checkTopUpPrc").trigger('click');
    });
  
   $(document).on('click','.nextPlanButton',function(){  
                    
                        if($(".number_of_business").val()=="")
                        {
                          tostrerror("Please enter number of business.");
                          return false;
                        } 
                          else if($(".top_recharge_credit").val()=="")
                        {
                          tostrerror("Please set top recharge credit.");
                          return false;
                        } 
                         else if($(".payId").val()=="" && is_group==0)
                         {
                          tostrerror("Please select plan.");
                          return false;
                         }
                         else{
                             $("#tab-recharge").trigger('click');
                         }
                        
                         
    
   });


 function fetchAllRecordData()
    {
              jQuery.ajax({
                                  headers: {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                                  type: "GET",
                                  url:get_card_url,
                                  cache: false,
                                  dataType: "json",
                                   beforeSend: function() {
                                      showLoader();
                                  },
                                  success: function (result) {
                                  	
                                     //hideLoader();
                                     //console.log(result.data);
                                     jQuery(".show_records_card").html(result.data);
                                     //jQuery('#'+dataTableId).DataTable({"ordering": false});
                                  },
                                  complete: function() {
                                   hideLoader();
                                  },
                                  error: function (xhr) {
                                     hideLoader();
                                     //console.log(xhr);
                                  }
                      });
     }



      var cancelModel=function(callback){

                      $(document).on("click",'.cancel_group_plan_btn',function(){

                               $('#cancel-plan-group-modal').modal('show');
                               $("#cancel_yes").attr('data-id',customer_id);
                       
                      });

                      $(document).on("click",'#cancel_yes',function(){
                         callback($(this).attr('data-id'));
                         $("#cancel-plan-group-modal").modal('hide');
                      });


      };

       cancelModel(function(confirm)
      { 

              if(confirm)
            {
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    url:cancel_group_plan,
                    data:{'customer_id':customer_id},
                    dataType: "json",
                    beforeSend: function() {
                        showLoader();
                    },
                    success: function (data)
                    {
                        tostrOnHidden(refresh_url,'reload');
                        tostrsuccess(data.message);
                        
                    },
                    complete: function() {
                        $('#cancel-plan-group-modal').modal('hide');
                        hideLoader();
                    },
                    error: function (xhr) {
                        $('#cancel-plan-group-modal').modal('hide');
                        jQuery.each(xhr.responseJSON.errors,function(k,message){
                            tostrerror(message);
                        });
                    }
                });
            }


      }); 



       var modalConfirm = function(callback){

                      $(document).on("click",'.subTopUpPlanData',function(){
                          if($(".number_of_business").val()=="")
                        {
                          tostrerror("Please enter number of business.");
                          return false;
                        } 
                          if($(".top_recharge_credit").val()=="")
                        {
                          tostrerror("Please set top recharge credit.");
                          return false;
                        } 
                        if($(".payId").val()!="")
                        {
                          	   $('#subTopUpPlanData-modal').modal('show');
                          	   $("#business_yes").attr('data-id',$(this).attr('data-id'));
                               fetchAllRecordData();
                        }else{
                               callback(1);
                        }	   
                           //$('#subTopUpPlanData-modal').modal('show');
                      });
                     
                              $(document).on("click",'#business_yes',function(){
                              	if($('input[name="card_id"]:checked').val()=="")
                              	{
                              		tostrerror("Please choose at least one card.");
                              		return false;
                              	}	
        
                                
                                 callback($(this).attr('data-id'));
                                 $("#business-confirm-delete-modal").modal('hide');
                              });
                      

     };
    modalConfirm(function(confirm)
    {


        if(confirm)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                url:set_group_plan,
                data:{'customer_id':customer_id,'payId':$(".payId").val(),'top_id':$('.topupId').val(),'currency':$(".currency").val(),'card_id':$('input[name="card_id"]:checked').val(),'number_of_business':$(".number_of_business").val(),"top_recharge_credit":$(".top_recharge_credit").val()},
                dataType: "json",
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                        tostrOnHidden(refresh_url,'reload');
                        tostrsuccess(data.message);


                },
                complete: function() {
                    $('#subTopUpPlanData-modal').modal('hide');
                    hideLoader();
                },
                error: function (xhr) {
                    $("#show_record_card").html("");
                    $('#subTopUpPlanData-modal').modal('hide');
                    //jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(xhr.responseJSON.errors.message);
                    //});
                }
            });
        }
    });
</script>
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script src="{{ asset('assets/js/fetch.js') }}"></script>
@endsection

@endsection
