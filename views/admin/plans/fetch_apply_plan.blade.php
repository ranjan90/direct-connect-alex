                                                     <div class="tabbable-panel Campaigns-tabs">
                                                       
                                                            <ul class="nav nav-tabs reporting-tabs">
                                                                <li class="nav-item">
                                                                    <a class="active swith-plan-trigger leadTabs" data-toggle="tab" href="#new_plan">Group PLANS</a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="" data-toggle="tab" id="tab-recharge" href="#credit-recharge">Group TOP UP </a>
                                                                </li>
                                                            </ul>
                                                        
                                                    </div>

                                                    <div class="tab-content">
                @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                    <!-- Leads Summary -->
                        <div id="new_plan" class="tab-pane active">
                            <div class="plains_main_div">
                                <h2 class="campaign_name">Group Plan.</h2>
                                <div class="plan-section">
                                    <div class="planPanel">
                                        <div class="row">
                                            @php
                                                $choosePlanId = '';
                                                $planplan_id  = '';
                                            @endphp
                                            @if(!$plansData->isEmpty())
                                                @foreach($plansData as $plan)
                                                <?php
                                                if ($plan->plans->billingType == 4) {
                                                    $span = '<span class="planspan">Group Plan</span>';
                                                    $Class = 'first-plan';
                                                }
                                                $btnSelect              = 'Select';
                                                $color                  = '#2961af';
                                                $acClass                = '';
                                                $selectedCurrentClass   = "checkPlainPrc";
                                                if(isset($UserGroupAccount->plan_id) && $plan->stripe_plan_id == $UserGroupAccount->plan_id)
                                                {
                                                    $choosePlanId           = $plan->stripe_plan_id;
                                                    $btnSelect              = 'Current';
                                                    $acClass                = '';
                                                    $planplan_id            = $plan->plan_id;
                                                    $vClass                 = true;
                                                    $color                  = '#999';
                                                    $selectedCurrentClass   = "";
                                                    $currentPlanName        = $plan->plans->name.' '.$plan->plan_currency->currencySymbol.' '.$plan->amount;
                                                }

                                                $noc         = $plan->plans->no_of_contacts;
                                                // $contactNow  = isset($user_data->user_plan->plans->no_of_contacts) ? \$user_data->user_plan->plans->no_of_contacts :0;
                                                // $userContact = isset($user_data->user_contacts) ? $user_data->->user_contacts->count() : 0;
                                                $check       = 0;
                                                // if ($userContact > 0 )
                                                // {
                                                //     if( $noc < $contactNow)
                                                //     {
                                                //         if ($userContact > $noc)
                                                //         {
                                                //             $check = 1;
                                                //         }
                                                //     }
                                                //     else
                                                //     {
                                                //         $check = 0;
                                                //     }
                                                // }
                                                $number_of_business=0; 
                                                if(isset($UserGroupAccount->number_of_business))
                                                {
                                                    $number_of_business=$UserGroupAccount->number_of_business;
                                                }    
                                                $patterns             = array('/{contactNumber}/','/{creditNumber}/','/{businesNumber}/');
                                                $replacements         = array($plan->plans->no_of_contacts,$plan->credit,$number_of_business);
                                                $emailstring          = (new \App\Helpers\GlobalFunctions)->emailReplacement($patterns,$replacements,$plan->plans->description);
                                                $emailstring          = $emailstring['content'];

                                            ?>
                                                <div class="col-md-4">
                                                    <div class="{{$Class}}">
                                                        <h5>{{$plan->plans->name}}<br>
                                                        <?php if( $plan->amount == 99 ){?>
                                                            <span style="font-size:12px;display: block;text-transform: initial;">Most popular</span>
                                                        <?php } ?>
                                                        </h5>
                                                        <div class="price-dollar-"  id="<?php echo $plan->plans->name.' '.$plan->plan_currency->currencySymbol.' '.$plan->amount?>">
                                                            @if($selectedCurrentClass == 'checkPlainPrc')
                                                                <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</span>
                                                                <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}/month</b></p>
                                                            @else
                                                                <br><br><br>
                                                                <h2 class="text-center">CURRENT PLAN</h2>
                                                                 <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</span>
                                                                <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}/month</b></p>
                                                                <br><br>
                                                            @endif
                                                        </div>
                                                        <div class="plan-text">
                                                            <ul>
                                                                {!!html_entity_decode($emailstring)!!}
                                                            </ul>
                                                            @if($selectedCurrentClass == 'checkPlainPrc')
                                                                <div class="plan-btn">
                                                                    <a href="javascript:void(0)"  style="background:<?php echo $color; ?>" data-payId="{!! \Crypt::encrypt($plan->stripe_plan_id)!!}" data-plainid="{{$plan->plan_id}}" data-mod="{{$check}}" class="<?php echo $selectedCurrentClass; ?> btn btn-primary btn-block {{$acClass}}">{{$btnSelect}}</a>
                                                                </div>
                                                            @else
                                                                <div class="plan-btn">
                                                                    <br><br>
                                                                </div>
                                                            @endif
                                                             
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                           
                                            <input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($plansData) }}" />
                                            @else
                                                <p>No plans found..</p>
                                            @endif
                                           <?php /*
                                            <!-- <input id="modelData" type="hidden" name="modelData" class="modelToken" value="0">
                                            <input type="hidden" name="choosePlanId" class="usedPlainId" value="{{trim($planplan_id)}}"> -->
                                          <!--   <input type="hidden" name="payId" class="payId" value="{{trim($choosePlanId)}}"/>
                                            <input type="hidden" id="sub_id" name="sub_id" value="{{trim($sub_id)}}">
                                            <input type="hidden" id="currentPlanName" value="<?php echo $currentPlanName?>"/> -->
                                            */?>
                                             <input type="hidden" name="currency" class="currency" value="{!! \Crypt::encrypt($currency)!!}"/>
                                             <input type="hidden" name="payId" class="payId" value=""/>
                                             <input type="hidden" name="choosePlanId" class="usedPlainId" value="">
                                             <input id="modelData" type="hidden" name="modelData" class="modelToken" value="0">
                                           
                                        </div>
                                          <div class="Bottom_button text-right">
                                            <button class="btn btn-primary btn-rounded nextPlanButton" type="button">Next</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                @else
                    <tr><td colspan="10"><div class="alert alert-danger">Please set your card details first.</div></td></tr>
                @endif

                @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                        <div id="credit-recharge" class="tab-pane ">
                                        <div class="plains_main_div">
                                            <div class="plan-section">
                                                <!-- {{ Form::open(array('url' => url('/recharge-credit'),'id' => 'recharge_form','class' => 'plan-form','autocomplete' => 'off')) }} -->
                                                    <div class="container">
                                                        <div class="row">
                                                            @php
                                                                $choosePlanId = '';
                                                            @endphp
                                                            @foreach($topupData as $plan)
                                                                <?php
                                                                    $btnSelect      = 'Select';
                                                                    $acClass        = '';
                                                                    $check          = 0;
                                                                    $patterns       = array('/{Credit_Amount}/');
                                                                    $replacements   = array($plan->credit);
                                                                    $emailstring    = preg_replace($patterns, $replacements,$plan->tops->description);
                                                                    $Class          = 'first-plan';
                                                                    $color          = '#2961af';
                                                                    $clickClass     = "checkTopUpPrc";
                                                                    if(isset($UserGroupAccount->top_price_id))
                                                                    {
                                                                    if($plan->id==$UserGroupAccount->top_price_id)
                                                                    {
                                                                        $btnSelect  = 'Current';
                                                                        $color      = '#999';
                                                                        $clickClass = "";
                                                                    }
                                                                }
                                                                ?>
                                                             

                                                                <div class="col-md-4 plan_row">
                                                                    <div class="{{$Class}}">
                                                                        <h5>Group Top Up<br></h5>
                                                                        @if($btnSelect=='Select')
                                                                            <div class="price-dollar-" id="{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}">
                                                                                <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}</span>
                                                                                <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}</b></p>
                                                                            </div>
                                                                        @else
                                                                        <br><br><br>
                                                                         <h2 class="text-center">CURRENT Top Up</h2>
                                                                          <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}</span>
                                                                           <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}</b></p>

                                                                        @endif
                                                                        <div class="plan-text">
                                                                            <ul>
                                                                                {!!html_entity_decode($emailstring)!!}
                                                                            </ul>
                                                                          @if($btnSelect=='Select')

                                                                            <div class="plan-btn">
                                                                                <a href="javascript:void(0)"  style="background:<?php echo $color; ?>" data-price="{{ $plan->price }}" data-credit="{{ $plan->credit }}" data-plainid="{!! \Crypt::encrypt($plan->top_id)!!}" data-mod="{{$check}}" class="<?php echo $clickClass ?> btn btn-primary btn-block {{$acClass}} topPlanClass">{{$btnSelect}}</a>
                                                                            </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                               <input type="hidden" name="topupId" class="topupId" value="" />
                                                                <input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($topupData) }}" />
                                                        </div>
                                                        <div class="Bottom_button text-right">
                                                            <button class="btn btn-rounded subTopUpPlanData"   target="#top-up-confirm-payment-modal" data-id="{{$id}}"type="button">Save Changes</button>
                                                        </div>
                                                    </div>
                                                <!-- {{ Form::close() }} -->
                                            </div>
                                        </div>
                                   

                        </div>
                @endif
               
            </div>