@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div><h1><i class="fa fa-list"></i> Plans</h1></div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">Plans</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group text-right">
                            <a href="{{ URL(config('app.newadmintemplatename').'/plans') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <div class="tile">
                    <h2>{{$planData->name}}</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Active Subscriptions</h5>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered">
                                <tr>



                                     <th>Type</th>
                                      @if($planData->billingType==2)
                                    <th>Business</th>
                                    @endif
                                    <th>Customer</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                                @if($planData->billingType==2)
                                @forelse($planusers as $user)
                                    <tr>
                                             <td>@if($user['0']['type']=='subscription') Subscription @else Promo Plan @endif</td>
                                        <td>{{ ucfirst($user['0']['business']['business_name']) }}</td>
                                        <td>
                                            <?php if( isset($user['0']['userData']) && isset($user['0']['userData']['firstName'])){?>
                                                {{ ucfirst($user['0']['userData']['firstName']) }} {{ ucfirst($user['0']['userData']['lastName']) }}
                                            <?php } ?>

                                        </td>
                                        <td>{{date('d/m/Y',$user['0']['startDate'])}}</td>
                                        <td>{{date('d/m/Y',$user['0']['endDate'])}}</td>
                                    </tr>
                                    @empty
                                    <tr><td>No Record Found.</td></tr>
                                @endforelse
                                @else
                                   @forelse($planusers as $user_d)
                                   @foreach($user_d as $user)
                                    <tr>
                                             <td>@if($user['type']=='subscription') Subscription @else Promo Plan @endif</td>
                                         @if($planData->billingType==2) <td>{{ ucfirst($user['business']['business_name']) }}</td>@endif
                                        <td>
                                            <?php if( isset($user['userData']) && isset($user['userData']['firstName'])){?>
                                                {{ ucfirst($user['userData']['firstName']) }} {{ ucfirst($user['userData']['lastName']) }}
                                            <?php } ?>

                                        </td>
                                        <td>{{date('d/m/Y',$user['startDate'])}}</td>
                                        <td>{{date('d/m/Y',$user['endDate'])}}</td>
                                    </tr>
                                 @endforeach   
                                    @empty
                                    <tr><td>No Record Found.</td></tr>
                                @endforelse
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
