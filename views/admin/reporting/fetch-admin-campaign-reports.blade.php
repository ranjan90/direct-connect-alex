<div class="row">
                     <?php $userPlan = App\Model\UserPlan::where('business_id',$business_id)->first(); ?>
                <div class="col-md-4 plans">
                            <div class="form-group">
                                <label for="">Select Business<span class="star">*</span></label>
                                <select class="form-control business_id" name="business_id">
                                    <?php
                                    if(!empty($business_data))
                                    {
                                        foreach($business_data as $business_data_key=>$business_data_value){
                                         $select="";
                                         if($business_data_value->id==$business_id)
                                         {
                                              $select="selected";

                                         }
                                         ?>
                                           <option <?php echo $select ?> value="<?php echo $business_data_value->id;?>"><?php echo $business_data_value->business_name;?></option>
                                         <?php
                                    }
                                    ?>

                                    <?php }else
                                      {
                                          ?>
                                          <option value="">Select Business</option>
                                          <?php

                                    }?>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="tile">
                    <div class="row">

                        <div class="col-md-6"><b>Customer Name:</b> {{ $customer_data->firstName }} {{ $customer_data->lastName }}</div>
                        <div class="col-md-6 text-right"><b>Remaining Credit Amount:</b>@if(isset($userPlan->balanceAmount)) {{ $userPlan->balanceAmount }} @else N/A @endif</div>
                    </div>
                    <div class="tile-body  table-responsive recordsTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Campaign Name</th>
                                    <th>Credits Consumed</th>
                                    <th>Total Leads</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="show_records">
                            <?php $i=1; $totalMin=0; $totalCall=0;    $cmp = App\Model\Campaign::where('business_id',$business_id); ?>
                             @if($cmp->count()!=0)
                                @forelse($cmp->get() as $camps)
                                  @php
                                     $totalLeadQuery= App\Model\ApiLeads::where('campaign_id',$camps->id);
                                     $total_leads =  $totalLeadQuery->count();
                                     $totalCredit=$totalLeadQuery->sum('credit_amount');
                                  @endphp
                                <tr>
                                    <td scope="col">{{ $i }}.</td>
                                    <td scope="col">{{ $camps->title }}</td>
                                    <td scope="col">{{ $totalCredit }}</td>
                                    <td scope="col">{{ $total_leads }}</td>
                                    <?php $cmp = App\Model\Campaign::where('api_dept_id',$camps->id)->first(); ?>
                                    <!--<td scope="col">-->
                                        <!-- <a href="@if(isset($cmp->id)) {{ URL('admin/campaign-leads/'.Crypt::encrypt($cmp->id)) }} @else href = " javascript:void(0) "@endif" data-toggle="tooltip" title="View Leads"><span><i class="fa fa-list"></i> </span></a> -->
                                         {{--@can('Checkbox-to-allow-listening-to-recordings')--}}
                                        <!--<a href="@if(isset($cmp->id)) {{$url.'/campaign-leads/'.Crypt::encrypt($cmp->id) }} @else href = " javascript:void(0) "@endif" data-toggle="tooltip" title="View Leads"><span><i class="fa fa-list"></i> </span></a>-->
                                        {{--@endcan--}}
                                    <!--</td>-->
                                </tr>
                                <?php
                                $totalCall += $totalCredit;
                                $i++; ?>
                                @empty
                                <tr>
                                  <td scope="col" colspan="4">No Record Found !</td>
                                </tr>
                                @endforelse
                                <tr>
                                    <th scope="col" colspan="2">Total Credits Consumed</th>
                                    <th>{{ $totalCall }}</th>
                                    <th></th>
                                </tr>
                              @else
                              <tr>
                                <td scope="col" colspan="4">No Record Found !</td>
                              </tr>
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
