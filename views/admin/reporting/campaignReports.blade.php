@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-file-o"></i>Campaign Reports</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Campaign Reports</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group text-right">
                           <!--  <a href="{{ URL(config('app.admintemplatename').'/reporting') }}" class="btn btn-primary">Back</a> -->
                            <a href="{{ $url.'/reporting' }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
               
              <div class="show_records">
                  
              </div>
            </div>
        </div>
    </main>
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->
     <!-- Delete Ends -->
    @section('js')
        <script type="text/javascript">
            var userId="<?php echo $userID ?>";
            var route_url="<?php echo url(request()->route()->getPrefix())?>";
            var get_data_url=route_url+'/fetch-admin-campaign-reports?userId='+userId;
             $(document).on('change','.business_id',function(){
                    get_data_url+='&business_id='+$(this).val();
                    fetchAdminCampaign();
             });
        </script>
        <script src="{{ asset('assets/js/loader.js') }}"></script>
        <script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
        <script>
          function fetchAdminCampaign()
          {
                               jQuery.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: "GET",
                                url:get_data_url,
                                cache: false,
                                dataType: "json",
                                 beforeSend: function() {
                                    showLoader();
                                },
                                success: function (result) {
                                   hideLoader();
                                   jQuery(".show_records").html(result.data);
                                   //jQuery('#'+dataTableId).DataTable({"ordering": false});
                                },
                                complete: function() {
                                 hideLoader();
                                },
                                error: function (xhr) {
                                   hideLoader();
                                   //console.log(xhr);
                                }
                    });
          }
          fetchAdminCampaign();
            
        </script>
@endsection
@endsection
