@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-globe"></i> View Details</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home'?>"><i class="fa fa-home fa-lg"></a></i></li>
                <?php if( $urlType == 'users' ){?>
                    <li class="breadcrumb-item"><a href="<?php echo $url.'/users/view-business-list/'.$id?>">Business </a>/ View Details</li>
                <?php } else {?>
                    <li class="breadcrumb-item"><a href="<?php echo $url.'/customers/view-business-list/'.$id?>">Business </a>/ View Details</li>
                <?php } ?>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group text-right">
                            <a href="#" data-type="<?php echo $urlType;?>" class="btn btn-primary comp_user_details">User Details</a>
                            <?php if( $urlType == 'users' ){?>
                                <a href="{!! $url.'/users/view-business-list/'.$id!!}" class="btn btn-primary">Back</a>
                            <?php } else {?>
                                <a href="{!! $url.'/customers/view-business-list/'.$id!!}" class="btn btn-primary">Back</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <h4>Business Name:{{ $business_data->business_name }}</h4>
                <div class="tile row">

                    <div class="col-md-6">
                        <h2>{{ ucfirst($userData->firstName) }} {{ ucfirst($userData->lastName)  }}</h2>
                          <div class="">
                <table>
                                @if(isset($userData->email))
                 <tr>
                    <td><span class="details-user"><b>Email :</b></span></td>
                   <td><span class="details_now"> {{ $userData->email }}</span></td>
                </tr>
                @endif
                @if(isset($userData->phoneNo))
                  <tr>
                    <td><span class="details-user"><b>Phone :</b></span></td>
                    <td><span class="details_now"> {{ $userData->phoneNo }}</span></td>
                  </tr>
                @endif
                @if(isset($userData->companyName))
                  <tr>
                    <td><span class="details-user"><b>Company Name :</b></span></td>
                    <td><span class="details_now"> {{ $userData->companyName }}</span></td>
                  </tr>
                @endif
                @if(isset($userData->companyUrl))
                  <tr>
                     <td><span class="details-user"><b>Company URL :</b></span></td>
                     <td><span class="details_now"> {{ $userData->companyUrl }}</span></td>
                  </tr>
                @endif
                @if(isset($userData->region->name))
                  <tr>
                   <td><span class="details-user"><b>Country :</b></span></td>
                   <td><span class="details_now"> {{ $userData->region->name }}</span></td>
                  </tr>
                @endif
                @if(isset($userData->region->name))
                  <tr>
                   <td><span class="details-user"><b>Created At :</b></span></td>
                   <td><span class="details_now"> {{date('d/m/Y', strtotime($userData->created_at))}}</span></td>
                  </tr>
                @endif
                </table>
                          </div>
                    </div>
                    <div class="col-md-6">
                        <h2>Plan Details</h2>
                        <div class="">
                            <?php 
                             if($userData->is_group==1)
                             {
                                                 $UserGroupAccount=App\Model\UserGroupAccount::where('is_active',1)->where('user_id',$userData->id);
                                                 if($UserGroupAccount->count()!=0)
                                                 { 
                                                   $plan_name = $UserGroupAccount->first()->plans->name;
                                                   $creditaoumt = $UserGroupAccount->first()->plan_credit;
                                                 }
                             }
                             else
                             {
                                  if(isset($business_data->user_plan->plans->name))
                                  { 
                                       $plan_name=$business_data->user_plan->plans->name;
                                      
                                  }
                             }
                             ?>
             <table>
                          
              <tr>
                <td><span class="details-user"><b>Plan Name :</b></span></td>
                <td><span class="details_now"> {{ $plan_name }}</span></td>
              </tr>
                       
                         <?php
                         if(!empty($creditaoumt))
                         {
                             
                         }
                         else
                         {
                                 if(isset($userData->is_usernew) && $userData->is_usernew==1)
                                 {
                                    if(isset($userData->plan_country_id_region->currency) && isset($business_data->user_plan->plan_id))
                                    $PlanPrice=\App\Model\PlanPrice::where('plan_id',$business_data->user_plan->plan_id)->where('currency_id',$userData->plan_country_id_region->currency);
        
                                 }else{
                                     if(isset($userData->region->currency) && isset($business_data->user_plan->plan_id))
                                    $PlanPrice=\App\Model\PlanPrice::where('plan_id',$business_data->user_plan->plan_id)->where('currency_id',$userData->region->currency);
                                 }
                                 if($PlanPrice->count()!=0)
                                 {
                                     $creditaoumt=$PlanPrice->first()->credit;
                                 }
                         }

                        ?>

                        
              <tr>
                              <td><span class="details-user"><b>Credits Amount :</b></span></td>
                <td><span class="details_now"> {{ $creditaoumt }}</span></td>
              </tr>
                        

            </table>
                      </div>
                    </div>
                    <div class="col-12">
                        <h4>Payment Details</h4>
                        <br>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="table-responsive purchase-table">
                              <div class="table table-bordered table-hover">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Plan</th>
                                      <th scope="col">Date</th>
                                      <th scope="col">Valid upto</th>
                                      <th scope="col">Plan Account</th>
                                        <th scope="col">Plan Type</th>
                                      <th scope="col">Status</th>
                                      <th scope="col">Cancelled By</th>
                                       <th scope="col">Cancel Reason</th>
                                        <th scope="col">Cancel Option</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $i = 0;
                                    // echo "<pre>";print_r($purchaseHistory);die;
                                    // echo "<pre>";print_r($business_data->user_paymentsy);die;
                                     ?>
                                    @forelse ($purchaseHistory as $value)
                                    <?php
                                     $i++;
                                             $startDate= gmdate("d/m/Y", ($value->startDate));
                                             $endDate=gmdate("d/m/Y", ($value->endDate));
                                             $countryShort="";
                                             $currencySymbol="";
                                             if($value->users->is_usernew==1)
                                             {
                                               $countryShort=isset($value->users->plan_country_id_region->countries->countryShort) ?$value->users->plan_country_id_region->countries->countryShort :'';
                                               $currencySymbol=$value->users->plan_country_id_region->countries->currencySymbol;

                                             }
                                             else
                                             {
                                               $countryShort=isset($value->users->region->countries->countryShort) ?$value->users->region->countries->countryShort :'';
                                               $currencySymbol=$value->users->region->countries->currencySymbol;

                                             }
                                             if(!empty($countryShort) && $value->type!='promo_code')
			    	 	 	     	         {
			    	 	 	     	            $timezone=App\Helpers\GlobalFunctions::getRegion($countryShort);
			    	 	 	     	            reset($timezone);
						                        $region = key($timezone);
						                        $timezone = App\Helpers\GlobalFunctions::get_time_zone($countryShort,$region);
						                        $startDate=\Carbon\Carbon::createFromTimestamp($value->startDate)->timezone($timezone)->format('d/m/Y');
						                        $endDate=\Carbon\Carbon::createFromTimestamp($value->endDate)->timezone($timezone)->format('d/m/Y');

			    	 	 	     	         }
                                    ?>
                                    <tr class="tr{{$value->subscr_id}}">
                                      <td>{{$i}}</td>
                                      <?php if ($value->usersPlans): ?>
                                          <td>{{$value->usersPlans->name}}</td>
                                      <?php else: ?>
                                          <td>Credit balance</td>
                                    <?php endif; ?>
                                      <td>{{ $startDate }}</td>
                                <td>{{ $endDate  }}</td>

                                      <td>  {{ $currencySymbol  }} {{$value->amount_paid}}</td>
                                        <td>@if( $value->type == 'promo_code' ) Promo Plan @elseif($value->type == 'charge') Charge @else Subscription Plan @endif</td>
                                      @if( gmdate("Y-m-d", ($value->endDate)) >  date('Y-m-d') )
                                      <td>
                                         <span class="badge badge-secondary text-uppercase">{{$value->status}}</span>
                                      </td>
                                      @else
                                      <td>@if( $value->status == 'Active' ) <span class="badge badge-secondary text-uppercase">Completed</span>  @else  <span class="badge badge-secondary text-uppercase">{{$value->status}}</span>  @endif</td>
                                      @endif

                                      <td>@if($value->status!='Active') @if($value->cancel_by==1) Admin @elseif($value->cancel_by==0) Customer @elseif($value->cancel_by==2) Cancelled By Promo  @else No Action @endif @else No Action @endif</td>
                                      <td>@if(!empty($value->message)) {{$value->message}} @else N/A @endif</td>
                                      <td>@if($value->type!='promo_code') @if($value->cancel_option==2) Cancel at the end of the current period @elseif($value->cancel_option==1) Cancel immediately @else N/A  @endif @else N/A @endif</td>
                                    </tr>
                                    @empty
                                    <tr>
                                      <td colspan="5">No Record Found !</td>
                                    </tr>
                                    @endforelse
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                        </div>
                    </div>

                      <div class="col-12">
                        <h4>Current Top Up Information</h4>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive purchase-table">
                                    <div class="table table-bordered table-hover">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Top Up Name</th>
                                                    <th scope="col">Credit</th>
                                                    <th scope="col">Amount</th>

                                                </tr>
                                            </thead>
                                              <tbody>
                                                @if(!empty($TopUpDataSet))
                                               <tr>
                                                    <td>Top Up</td>
                                                    <td>@if(isset($TopUpDataSet->amount)) {{$currencySymbol}}{{$TopUpDataSet->amount}} @else N/A @endif</td>
                                                    <td>@if(isset($TopUpDataSet->credits)) {{$TopUpDataSet->credits}} @else N/A @endif</td>

                                               </tr>
                                               @else
                                              <tr><td colspan="3"><div class="alert alert-danger">Sorry no current top up found !. </div></td></tr>

                                               @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <h4>Additional Services</h4>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive purchase-table">
                                    <div class="table table-bordered table-hover">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Promo</th>
                                                    <th scope="col">Credit</th>
                                                    <th scope="col">Plan</th>
                                                    <th scope="col">No of months</th>
                                                    <th scope="col">Promo Code Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 0; ?>
                                                @forelse ($promoHistory as $value)
                                                    <?php $i++; ?>
                                                    <tr class="tr{{$value->id}}">
                                                        <td>{{$i}}</td>
                                                        <td>{{  ucfirst($value->title) }}</td>
                                                        <td>{{  $value->credit }}</td>
                                                        <td>{{  ucfirst($value->plan_name)}}</td>
                                                        <td>{{  $value->month }}</td>
                                                        <td>@if(isset($value->promocode->agency_check_box) && $value->promocode->agency_check_box=='on') Agency Promo Code @else N/A @endif</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="5">No Record Found !</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Delete Modal -->
    <div id="comp_user_details_model" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">User Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                     <table class="table">
                                  <thead>
                                    <tr>
                                        <th scope="col">Sno.</th>
                                        <th scope="col">User Name</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                  </thead>
                                  <tbody id="user_details_data">


                                  </tbody>
                     </table>

                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->

    @section('js')
    <script type="text/javascript">
      $(".comp_user_details").click(function(){
            var url="<?php echo $url?>";
            var dataType = $(this).attr('data-type');
            if( dataType == 'users' )
                var urlParam = 'get-user-details';
            else
                var urlParam = 'get-customer-details';
            var tableData="<tr><td colspan='3'>No User Available.</td></tr>";
            jQuery("#comp_user_details_model").modal('show');
                      jQuery.ajax({
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          type: "GET",
                          url:url+'/'+urlParam+'/'+"<?php echo $enc_business_id?>"+'/'+"<?php echo $id?>",
                          cache: false,
                          dataType: "json",
                           beforeSend: function() {
                              //showLoader();
                          },
                          success: function (result) {
                            // hideLoader();
                            var data="";
                             //jQuery(".show_records").html(result);
                             if(result=="")
                             {
                                 $("#user_details_data").html(tableData);
                             }
                             else
                             {
                               $.each(result,function(x,c){
                                    data+="<tr>";
                                    data+="<td>"+c.s_no+"</td>";
                                    data+="<td>"+c.name+"</td>";
                                    data+="<td><a data-toggle='tooltip' title='' data-original-title='View Business' href='"+c.url+"'><span><i class='fa fa-eye'></i> </span></a></td>";
                                    data+="</tr>";

                               });
                              $("#user_details_data").html(data);
                             }
                             //jQuery('#'+dataTableId).DataTable({"ordering": false});
                          },
                          complete: function() {
                          // hideLoader();
                          },
                          error: function (xhr) {
                            // hideLoader();
                             //console.log(xhr);
                          }
                   });

      });
    </script>
    @endsection
@endsection
