<div class="form-group col-md-12">
    @if($customer->is_group==1)
          <h4>Apply  Promo code for group account</h4>
    @else
    <div class="form-group">
        <label for="">Select Business</label>
        {!! Form::select('business_id',$business,null, ['class'=>'form-control','id'=>'customerbusiness']) !!}
      
    </div>
    @endif
</div>
<div class="form-group col-md-12">
    <div class="form-group">
        <label for="">Promo Code</label>
        {{ Form::text('promocode',null,['class'=>'form-control']) }}
          {!! Form::hidden('user_id',\Crypt::encrypt($customer->id)) !!}
    </div>
</div>
<div class="col-md-12">
    <input type="submit" class="btn btn-primary pull-right" value="Submit">
</div>
