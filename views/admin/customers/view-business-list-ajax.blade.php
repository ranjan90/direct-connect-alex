<table class="table  table-hover table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Business Name</th>
            <th>Plan Name</th>
            <th>Date</th>
            <th>Valid till</th>
            <th>Credits Remaining</th>
            <th>Campaigns</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody class="show_records">
    <?php $i=0; ?>
    @forelse($business as $business_value)
        <?php
            $stripe_cust_id = $plan_end_date = $plan_start_date = '';
            $BusinessOwner  = \App\Model\BusinessProfile::find($business_value->id)->get_users()->wherePivot('is_owner','1');
            if($BusinessOwner->count()!=0)
            {
                $BusinessOwner  = $BusinessOwner->first();
                $stripe_cust_id = $BusinessOwner->stripe_cust_id;
            }
            $UserPayment             = \App\Model\UserPayment::where('customer_id',$stripe_cust_id)->whereIn('type',['subscription','promo_code'])->where('business_id',$business_value->id);
            if($UserPayment->count()!=0)
            {
                $userPlan                = $UserPayment->orderBy('id','DESC')->first();
                $plan_start_date         = gmdate("d/m/Y", ($userPlan->startDate));
                $plan_end_date           = gmdate("d/m/Y", ($userPlan->endDate));
            }
            $remaining_mints         = 0;
            $totalActiveCampaigns    = $totalInActiveCampaigns = 0;
            $totalActiveCampaigns    = DB::select('select COUNT(title) as toalcount from campaigns where status = 1 and business_id='.$business_value->id);
            $totalInActiveCampaigns  = DB::select('select COUNT(title) as toalcount from campaigns where status = 0 and business_id='.$business_value->id);
            if( !empty($totalActiveCampaigns) )
                $totalActiveCampaigns = $totalActiveCampaigns[0]->toalcount;
            if( !empty($totalInActiveCampaigns) )
                $totalInActiveCampaigns = $totalInActiveCampaigns[0]->toalcount;

            $isSubscriptionActive = 1;
            $plan_name            = '';
            if(isset($business_value->business_plans) && isset($business_value->business_plans->isSubscriptionActive) )
                $isSubscriptionActive = $business_value->business_plans->isSubscriptionActive;

            if(isset($business_value->business_plans) && isset($business_value->business_plans->plans) )
                $plan_name = $business_value->business_plans->plans->name;
            if(isset($business_value->business_plans) && isset($business_value->business_plans->balanceAmount) )
                $remaining_mints = $business_value->business_plans->balanceAmount;

         $i++; ?>
        <tr>
            <td>{{ $page + $i }}</td>
            <td>@if(isset($business_value->business_name)){{ $business_value->business_name }} @endif</td>
            <td>{{ $plan_name }}</td>
            <td>{{ $plan_start_date }}</td>
            <td>{{ $plan_end_date }}</td>
            <td>{{ $remaining_mints }}</td>
            <td style="text-align:center"> <span class="badge badge-pill badge-success" style="font-size:14px;cursor:pointer" data-toggle="tooltip" title="Active Campaigns">{{ $totalActiveCampaigns }}</span>&nbsp;&nbsp;&nbsp; <span class="badge badge-pill badge-danger" style="font-size:14px;cursor:pointer" data-toggle="tooltip" title="Inactive Campaigns">{{$totalInActiveCampaigns}}</span></td>
            <td>@if($business_value->is_deleted==1) <span class="badge badge-danger badge-secondary text-uppercase">Deleted</span> @elseif($isSubscriptionActive != 2) <span class="badge badge-danger badge-secondary text-uppercase">Inactive</span> @else <span class="badge badge-success badge-secondary text-uppercase">{{$userPlan->status}}</span> @endif</td>
            <td>
                <?php if( $urlType == 'users' ){?>
                    <a href="{{ $url.'/view-user/'.$id.'/'.Crypt::encrypt($business_value->id) }}" data-toggle="tooltip" title="View Details"><span><i class="fa fa-eye"></i> </span></a>
                <?php } else {?>
                    <a href="{{ $url.'/view-customer/'.$id.'/'.Crypt::encrypt($business_value->id) }}" data-toggle="tooltip" title="View Details"><span><i class="fa fa-eye"></i> </span></a>
                <?php } ?>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="7">No Records Found !</td>
        </tr>
    @endforelse
    </tbody>
</table>
{{$business->links()}}
