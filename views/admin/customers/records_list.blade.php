<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <?php if($urlType == 'users' ){ ?>
                <th>User Name</th>
                <th>Role</th>
            <?php } else {?>
                <th>Customer Name</th>
            <?php } ?>
            <th>Email</th>
            <th>Phone</th>
            <th>Business</th>
            <th>Remaining Credit</th>
            <th>Promo Code</th>
            <th>Region</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody class="show_records">
    <?php $i=0; ?>
    @forelse($customers as $cust)
        <?php $i++;
        $remaining_mints = 0;
        if( isset($cust->business) && count($cust->business) > 0 )
        {
            if($cust->is_group==1)
                                        {
                                             $UserGroupAccount=App\Model\UserGroupAccount::where('is_active',1)->where('user_id',$cust->id);
                                             if($UserGroupAccount->count()!=0)
                                             {
                                               $remaining_mints= $UserGroupAccount->first()->balanceAmount;
                                             }
                                        }
                                        else{
                                                foreach( $cust->business as $keey=>$vallue)
                                                {
                                                    $uPlan = App\Model\UserPlan::where('business_id',$vallue['id']);
                                                    if($uPlan->count()!=0)
                                                    {
                                                        $remaining_mints1 = $uPlan->first()->balanceAmount;
                                                        $remaining_mints = $remaining_mints + $remaining_mints1;
                                                    }
                                                }
                                      }
        }

        ?>
        <tr>
            <td>{{ $page + $i }}</td>
            <td class='customer_name'>@if(isset($cust->firstName)){{ ucfirst($cust->firstName) }} @endif @if(isset($cust->lastName)){{ ucfirst($cust->lastName) }} @endif</td>
            <?php if($urlType == 'users' ){ ?>
                <td>@if(isset($cust->roles->first()->name))
                        @if( $cust->roles->first()->name == 'business-admin')
                            {{'Admin'}}
                        @else
                            {{ $cust->roles->first()->name }}
                        @endif
                    @endif
                </td>
            <?php } ?>
            <td>@if(isset($cust->email)){{ $cust->email }} @endif</td>
            <td>@if(isset($cust->phoneNo)){{ $cust->phoneNo }} @endif</td>
            <td>
                <?php
                    if(isset($cust->businessArr))
                    {
                        $cust->businessArr = array_values($cust->businessArr);
                        if( count($cust->businessArr) > 2 )
                        {
                            $businessNameString = $businessNameStringTooltip = '';
                            $keyb = 0;
                            foreach ($cust->businessArr as $key123 => $value123)
                            {
                                $keyb = $keyb+1;
                                $businessNameStringTooltip.= $keyb.') '.$value123.'&#013;';
                                if($keyb < 3)
                                    $businessNameString.=$value123.' <br>';
                            } ?>
                            <span style="cursor:pointer" data-html="true" data-toggle="tooltip" title="<?php echo $businessNameStringTooltip;?>">
                                <?php
                                    $moreBusiness = count($cust->businessArr) - 2;
                                    echo $businessNameString." + ".$moreBusiness." more";
                                ?>
                            </span>
                        <?php } else { ?>
                            <span>
                                <?php foreach ($cust->businessArr as $keyl => $valuel) {
                                    echo  $valuel."<br>";
                                } ?>
                            </span>
                        <?php }

                    } ?>
            </td>
            <td>{{$remaining_mints}}</td>
            <td>{{ $cust->promo_code }}</td>
            <td>@if(isset($cust->region->name)){{ $cust->region->name }} @endif</td>
            <td>
                <?php if($urlType == 'users' ){ ?>
                    <a href="{{ $url.'/users/view-business-list/'.Crypt::encrypt($cust->id) }}" data-toggle="tooltip" title="View"><span><i class="fa fa-eye"></i> </span></a>
                <?php } else {?>
                    <a href="{{ $url.'/customers/view-business-list/'.Crypt::encrypt($cust->id) }}" data-toggle="tooltip" title="View"><span><i class="fa fa-eye"></i> </span></a>
                <?php } ?>
                @if(count($cust->user_campaigns) > 0)
                <!--<a href="{{ $url.'/customer-campaign/'.Crypt::encrypt($cust->id) }}"data-toggle="tooltip" title="Campaign"><span><i class="fa fa-newspaper-o"></i> </span></a>-->
                @endif
                <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="existingCustomerPromoCodeBtn"><span><i class="fa fa-tag" data-toggle="tooltip" title="Promo Code"></i> </span></a>
                  <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="customerLogin"><span><i class="fa fa-sign-in" data-toggle="tooltip" title="Login as user"></i> </span></a>

                   <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="cancelBusinessPlanBtn"><span>           <i class="fa fa-list" data-toggle="tooltip" title="Cancel Business Subscription Plan" aria-hidden="true"></i> </span></a>
                   <a href="{{ $url.'/customers/apply-group-plan/'.Crypt::encrypt($cust->id) }}"><span><i class="fa fa-list" data-toggle="tooltip" title="Apply Group Plan" aria-hidden="true"></i> </span></a>

                   <a href="javascript:;" data-ss=data-id="{{$cust->id}}"  data-id="{{Crypt::encrypt($cust->id)}}" class="leadConciergePermission"><span>           <i class="fa fa-lock" data-toggle="tooltip" title="Lead Concierge Announcement Permission" aria-hidden="true"></i> </span></a>

                    @if(isset($cust->verified) && $cust->verified==0)

                                            <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="userVerifyLogin"><span><i class="fa fa-unlock" data-toggle="tooltip" title="Verfiy User"></i> </span></a>
                                         @endif
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="9">No Records Found !</td>
        </tr>
    @endforelse
    </tbody>
</table>
{{$customers->links()}}
