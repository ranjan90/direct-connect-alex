@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Business Name</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <?php if( $urlType == 'users' ){?>
                    <li class="breadcrumb-item"><a href="<?php echo $url.'/users' ?>">Users </a>/Business Name</li>
                <?php } else {?>
                    <li class="breadcrumb-item"><a href="<?php echo $url.'/customers' ?>">Customers </a>/Business Name</li>
                <?php } ?>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="row">
                    <div class="col-md-12">
                        <div class="form-group text-right">
                            <?php if( $urlType == 'users' ){?>
                                <a href="<?php echo $url.'/users' ?>" class="btn btn-primary">Back</a>
                            <?php } else {?>
                                <a href="<?php echo $url.'/customers' ?>" class="btn btn-primary">Back</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

<!--                         <div class="form-group text-right">
                            <a href="{{ URL(config('app.admintemplatename').'/add-new-plan') }}" class="btn btn-primary">Add Plan</a>
                        </div> -->
                    </div>
                </div>
                <div class="tile">
                            <div class="tile-body table-responsive recordsTable">
                                               <div class="form-group text-left">
                                               <h4>{{$customer->firstName.''.$customer->lastName}}</h4>
                        </div>
                                <table class="table  table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Business Name</th>
                                            <th>Plan Name</th>
                                            <th>Date</th>
                                            <th>Valid till</th>
                                            <th>Credits Remaining</th>
                                            <th>Campaigns</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="show_records">
                                    <?php $i=0; ?>
                                    @forelse($business as $business_value)
                                        <?php
                                            $stripe_cust_id = $plan_end_date = $plan_start_date = '';
                                            $BusinessOwner  = \App\Model\BusinessProfile::find($business_value->id)->get_users()->wherePivot('is_owner','1');
                                            if($BusinessOwner->count()!=0)
                                            {
                                                $BusinessOwner  = $BusinessOwner->first();
                                                $stripe_cust_id = $BusinessOwner->stripe_cust_id;
                                            }
                                            if($BusinessOwner->is_group==1)
                                            {
                                                 $UserPayment             = \App\Model\UserPayment::where('customer_id',$stripe_cust_id)->whereIn('type',['subscription'])->where('group_plan',1)->where('status','=','Active');

                                            }
                                            else{

                                                 $UserPayment             = \App\Model\UserPayment::where('customer_id',$stripe_cust_id)->whereIn('type',['subscription','promo_code'])->where('business_id',$business_value->id);

                                            }
                                            if($UserPayment->count()!=0)
                                            {
                                                $userPlan                = $UserPayment->orderBy('id','DESC')->first();
                                                $plan_start_date         = gmdate("d/m/Y", ($userPlan->startDate));
                                                $plan_end_date           = gmdate("d/m/Y", ($userPlan->endDate));
                                            }
                                            $remaining_mints         = 0;
                                            $totalActiveCampaigns    = $totalInActiveCampaigns = 0;
                                            $totalActiveCampaigns    = DB::select('select COUNT(title) as toalcount from campaigns where status = 1 and business_id='.$business_value->id);
                                            $totalInActiveCampaigns  = DB::select('select COUNT(title) as toalcount from campaigns where status = 0 and business_id='.$business_value->id);
                                            if( !empty($totalActiveCampaigns) )
                                                $totalActiveCampaigns = $totalActiveCampaigns[0]->toalcount;
                                            if( !empty($totalInActiveCampaigns) )
                                                $totalInActiveCampaigns = $totalInActiveCampaigns[0]->toalcount;

                                            $isSubscriptionActive = 1;
                                            $plan_name            = '';
                                            if(isset($business_value->business_plans) && isset($business_value->business_plans->isSubscriptionActive) )
                                                $isSubscriptionActive = $business_value->business_plans->isSubscriptionActive;

                                            if($BusinessOwner->is_group==1)
                                            {
                                                 $UserGroupAccount=App\Model\UserGroupAccount::where('is_active',1)->where('user_id',$BusinessOwner->id);
                                                 if($UserGroupAccount->count()!=0)
                                                 {
                                                  $plan_name = $UserGroupAccount->first()->plans->name;
                                                   $remaining_mints = $UserGroupAccount->first()->balanceAmount;
                                                 }
                                            }
                                            else
                                            {
                                                if(isset($business_value->business_plans) && isset($business_value->business_plans->plans) )
                                                    $plan_name = $business_value->business_plans->plans->name;
                                                if(isset($business_value->business_plans) && isset($business_value->business_plans->balanceAmount) )
                                                    $remaining_mints = $business_value->business_plans->balanceAmount;
                                            }

                                         $i++; ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>@if(isset($business_value->business_name)){{ $business_value->business_name }} @endif</td>
                                            <td>{{ $plan_name }}</td>
                                            <td>{{ $plan_start_date }}</td>
                                            <td>{{ $plan_end_date }}</td>
                                            <td>{{ $remaining_mints }}</td>
                                            <td style="text-align:center"> <span class="badge badge-pill badge-success" style="font-size:14px;cursor:pointer" data-toggle="tooltip" title="Active Campaigns">{{ $totalActiveCampaigns }}</span>&nbsp;&nbsp;&nbsp; <span class="badge badge-pill badge-danger" style="font-size:14px;cursor:pointer" data-toggle="tooltip" title="Inactive Campaigns">{{$totalInActiveCampaigns}}</span></td>
                                            <td>@if($business_value->is_deleted==1) <span class="badge badge-danger badge-secondary text-uppercase">Deleted</span> @elseif($isSubscriptionActive != 2) <span class="badge badge-danger badge-secondary text-uppercase">Inactive</span> @else <span class="badge badge-success badge-secondary text-uppercase">Active</span> @endif</td>
                                            <td>
                                                <?php if( $urlType == 'users' ){?>
                                                    <a href="{{ $url.'/view-user/'.$id.'/'.Crypt::encrypt($business_value->id) }}" data-toggle="tooltip" title="View Details"><span><i class="fa fa-eye"></i> </span></a>
                                                <?php } else {?>
                                                    <a href="{{ $url.'/view-customer/'.$id.'/'.Crypt::encrypt($business_value->id) }}" data-toggle="tooltip" title="View Details"><span><i class="fa fa-eye"></i> </span></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">No Records Found !</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                {{$business->links()}}
                            </div>
                </div>
            </div>
        </div>
    </main>
@endsection
