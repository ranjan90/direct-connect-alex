@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><img class="img-responsive" src="{{ asset('assets/admin/images/lead_user.png') }}" alt="" />
<!--                    <i class="fa fa-users"></i>-->
                    <?php echo $title;?></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $url.'/home';?>"><i class="fa fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item"><?php echo $title;?></li>
            </ul>
        </div>
        <div class="row">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group pull-left col-md-6">
                            <?php if($urlType == 'users' ){ ?>
                                <input type="text" data-url="{{url('ctwdr_dmlogin/users')}}" class="form-control" id="searchKey" placeholder="Search User by Name">
                            <?php } else{?>
                                <input type="text" data-url="{{url('ctwdr_dmlogin/customers')}}" class="form-control" id="searchKey" placeholder="Search Customer by Name">
                            <?php } ?>
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_from',null,['class'=>'form-control datepicker', 'id'=>'fromDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_to',null,['class'=>'form-control datepicker', 'id'=>'toDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-2">
                            <a href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
                        </div>
<!--                         <div class="form-group text-right">
                            <a href="{{ URL(config('app.admintemplatename').'/add-new-plan') }}" class="btn btn-primary">Add Plan</a>
                        </div> -->
                    <div class="col-md-12">
                <div class="tile">
                    <b>User Front Url: </b><a href="<?php echo url('') ?>" target="_blank"><?php echo url('') ?></a>
                    <?php if($urlType != 'users' ){ ?>
                        <div class="text-right admincustomersbtn">
                            <a href="javascript:void(0)" class="btn btn-success btn-arrow btn-arrow-bottom">Active Customer </a>
                            <a href="{{ URL('ctwdr_dmlogin/inactive-customers') }}" class="btn btn-danger active">Inactive Customer</a>
                            <a href="{{ URL('ctwdr_dmlogin/incomplete-customers') }}" class="btn btn-orange active">Incomplete signup</a>
                        </div>
                    <?php }?>
                    <div class="tile-body table-responsive recordsTable">
                        <table class="table  table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <?php if($urlType == 'users' ){ ?>
                                        <th>User Name</th>
                                        <th>Role</th>
                                    <?php } else{?>
                                        <th>Customer Name</th>
                                    <?php } ?>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Business</th>
                                    <th>Remaining Credits</th>
                                    <th>Promo Code</th>
                                    <th>Region</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="show_records">
                            <?php $i=0; ?>
                            @forelse($customers as $cust)
                                <?php $i++;
                                $remaining_mints = 0;
                                if( isset($cust->business) && count($cust->business) > 0 )
                                {

                                        if($cust->is_group==1)
                                        {
                                             $UserGroupAccount=App\Model\UserGroupAccount::where('is_active',1)->where('user_id',$cust->id);
                                             if($UserGroupAccount->count()!=0)
                                             {
                                               $remaining_mints= $UserGroupAccount->first()->balanceAmount;
                                             }
                                        }
                                        else{
                                                foreach( $cust->business as $keey=>$vallue)
                                                {
                                                    $uPlan = App\Model\UserPlan::where('business_id',$vallue['id']);
                                                    if($uPlan->count()!=0)
                                                    {
                                                        $remaining_mints1 = $uPlan->first()->balanceAmount;
                                                        $remaining_mints = $remaining_mints + $remaining_mints1;
                                                    }
                                                }
                                      }
                                }
                                ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td class="customer_name">@if(isset($cust->firstName)){{ ucfirst($cust->firstName) }} @endif @if(isset($cust->lastName)){{ ucfirst($cust->lastName) }} @endif</td>
                                    <?php if($urlType == 'users' ){ ?>
                                        <td>@if(isset($cust->roles->first()->name))
                                                @if( $cust->roles->first()->name == 'business-admin')
                                                    {{'Admin'}}
                                                @else
                                                    {{ $cust->roles->first()->name }}
                                                @endif
                                            @endif
                                        </td>
                                    <?php } ?>
                                    <td>@if(isset($cust->email)){{ $cust->email }} @endif</td>
                                    <td>@if(isset($cust->phoneNo)){{ $cust->phoneNo }} @endif</td>
                                    <td>
                                        <?php
                                            if(isset($cust->businessArr))
                                            {
                                                $cust->businessArr = array_values($cust->businessArr);
                                                if( count($cust->businessArr) > 2 )
                                                {
                                                    $businessNameString = $businessNameStringTooltip = '';
                                                    $keyb = 0;
                                                    foreach ($cust->businessArr as $key123 => $value123)
                                                    {
                                                        $keyb = $keyb+1;
                                                        $businessNameStringTooltip.= $keyb.') '.$value123.'<br>';
                                                        if($keyb < 3)
                                                            $businessNameString.=$value123.' <br>';
                                                    } ?>
                                                    <span style="cursor:pointer" data-html="true" data-toggle="tooltip" title="<?php echo $businessNameStringTooltip;?>">
                                                        <?php
                                                            $moreBusiness = count($cust->businessArr) - 2;
                                                            echo $businessNameString." + ".$moreBusiness." more";
                                                        ?>
                                                    </span>
                                                <?php } else { ?>
                                                    <span>
                                                        <?php foreach ($cust->businessArr as $keyl => $valuel) {
                                                            echo  $valuel."<br>";
                                                        } ?>
                                                    </span>
                                                <?php }

                                            } ?>
                                    </td>
                                    <td>{{$remaining_mints}}</td>
                                    <td>{{ $cust->promo_code }}</td>
                                    <td>@if(isset($cust->region->name)){{ $cust->region->name }} @endif</td>
                                    <td>
                                        <!--<a href="{{ $url.'/view-customer/'.Crypt::encrypt($cust->id) }}" data-toggle="tooltip" title="View"><span><i class="fa fa-eye"></i> </span></a>
                                        @if(count($cust->user_campaigns) > 0)
                                        <a href="{{ $url.'/customer-campaign/'.Crypt::encrypt($cust->id) }}"data-toggle="tooltip" title="Campaign"><span><i class="fa fa-newspaper-o"></i> </span></a>
                                        @endif-->
                                        <?php if($urlType == 'users' ){ ?>
                                            <a href="{{ $url.'/users/view-business-list/'.Crypt::encrypt($cust->id) }}"><span><i class="fa fa-eye" data-toggle="tooltip" title="View Business"></i> </span></a>
                                        <?php } else {?>
                                            <a href="{{ $url.'/customers/view-business-list/'.Crypt::encrypt($cust->id) }}"><span><i class="fa fa-eye" data-toggle="tooltip" title="View Business"></i> </span></a>
                                        <?php } ?>
                                        <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="existingCustomerPromoCodeBtn"><span><i class="fa fa-tag" data-toggle="tooltip" title="Promo Code"></i> </span></a>

                                        <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="customerLogin"><span><i class="fa fa-sign-in" data-toggle="tooltip" title="Login as user"></i> </span></a>

                                         <a href="javascript:;" data-ss=data-id="{{$cust->id}}"  data-id="{{Crypt::encrypt($cust->id)}}" class="cancelBusinessPlanBtn"><span>           <i class="fa fa-list" data-toggle="tooltip" title="Cancel Business Subscription Plan" aria-hidden="true"></i> </span></a>
                                         <a href="{{ $url.'/customers/apply-group-plan/'.Crypt::encrypt($cust->id) }}"><span><i class="fa fa-list" data-toggle="tooltip" title="Apply Group Plan" aria-hidden="true"></i> </span></a>


                                           <a href="javascript:;" data-ss=data-id="{{$cust->id}}"  data-id="{{Crypt::encrypt($cust->id)}}" class="leadConciergePermission"><span>           <i class="fa fa-lock" data-toggle="tooltip" title="Lead Concierge Announcement Permission" aria-hidden="true"></i> </span></a>


                                         @if(isset($cust->verified) && $cust->verified==0)

                                            <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="userVerifyLogin"><span><i class="fa fa-unlock" data-toggle="tooltip" title="Verfiy User"></i> </span></a>
                                         @endif



                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No Records Found !</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{$customers->links()}}
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->

    <!-- Promo code Modal -->
    <div id="customerLogin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Login as user</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            Are you sure you want to login <span class="display_customer_name"></span> user?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="modal-btn-si">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>

              </div>
            </div>
        </div>
    </div>




<div id="leadConciergePermission" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Lead Concierge Announcement Permission</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    {{ Form::open(array('id'=>'leadConciergePermissionForm','url'=>URL('ctwdr_dmlogin/save-lead-permission'))) }}
                <div class="modal-body" id="leadConciergePermissionBody">

                </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" id="save_lead_permission">Save Permission</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                  </div>
               {{ Form::close() }}
            </div>
        </div>
    </div>





     <!-- Promo code Modal -->
    <div id="userVerifyLogin" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Verify User</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            Are you sure you want to verify <span class="verfiy_display_customer_name"></span> user?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="verify_user_yes">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>

              </div>
            </div>
        </div>
    </div>



    <div id="existingCustomersPromoCode" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Promo Code</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::open(array('id'=>'existingCustomerPromoCodeForm','url'=>URL('ctwdr_dmlogin/apply-promocode'))) }}
                                <div id="existingCustomerBody">
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="existingCancelBusinessPlan" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cancel Business  Subscription Plan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::open(array('id'=>'cancelBusinessPlanForm','url'=>URL('ctwdr_dmlogin/cancel-business-plan'))) }}
                                <div id="existingCancelBusinessPlanBody">
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @section('js')
    <link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('assets/js/loader.js') }}"></script>

    <script type="text/javascript">
        var url         = "<?php echo $url?>";
        var customerId =0;
        $(function() {
            $( ".datepicker" ).datepicker();
        });
        $(document).on('change', '#fromDate', function()
        {
            var date1 = $(this).val();
            $("#toDate").datepicker("option","minDate",date1);
            if($("#toDate").val()!="")
                $("#searchKey").trigger('keyup');
        });
        $(document).on('change', '#toDate', function()
        {
            if($("#fromDate").val()!="")
                $("#searchKey").trigger('keyup');
        });

        $(".reset_filter").click(function()
        {
            if($(".form-control").val(""))
            {
                $("#searchKey").trigger('keyup');
            }
        });
          $(document).on('click', '.anchorID', function(event)
        {
            //window.open('https://www.codexworld.com', '_blank');
        });

      //modalConfirm
      var modalConfirm = function(callback){

         $(document).on("click",'.customerLogin',function(){
               $("#customerLogin").modal('show');
               $("#modal-btn-si").attr('data-id',$(this).attr('data-id'));
               $(".display_customer_name").text($(this).closest('tr').find('.customer_name').text());
          });



$(document).on("click",'.leadConciergePermission',function(){
                 customerId  = $(this).attr('data-id');
                businessFunc(customerId,'leadConciergePermission','leadConciergePermissionBody',0,'customer-permission-check');



          });



          $(document).on("click",'#modal-btn-si',function(){
             callback($(this).attr('data-id'));
             $("#customerLogin").modal('hide');
          });




        };

         var verifyModalConfirm = function(callback){
                 $(document).on("click",'.userVerifyLogin',function(){
                       $("#userVerifyLogin").modal('show');
                       $("#verify_user_yes").attr('data-id',$(this).attr('data-id'));
                       $(".verfiy_display_customer_name").text($(this).closest('tr').find('.customer_name').text());
                  });

                $(document).on("click",'#verify_user_yes',function(){
                     callback($(this).attr('data-id'));
                     $("#userVerifyLogin").modal('hide');
                  });

             }


           //modalConfirm
        verifyModalConfirm(function(confirm){
                  if(confirm){
                        jQuery.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type        : "POST",
                                url         : url+'/admin-customer-verify',
                                cache       : false,
                                data        : {login_id:confirm,"_token": "{{ csrf_token() }}"},
                                dataType    : "json",
                                beforeSend  : function() {
                                    $('.loader_div').show();
                                },
                                success: function (response)
                                { $('.loader_div').hide();
                                    if(response.success)
                                    {

                                          $.toast({
                                                heading             : 'Success',
                                                text                : response.success_message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'success',
                                                hideAfter           :  2000,
                                                position            : 'top-right'
                                            });
                                            if(response.url)
                                            {
                                                            setTimeout(function() {
                                                                window.location.href=response.url;

                                                             },1000);

                                            }
                                    }
                                    else{

                                         $.toast({
                                                heading             : 'Error',
                                                text                : response.error_message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'error',
                                                hideAfter           : response.delayTime,
                                                position            : 'top-right'
                                            });

                                    }


                                },
                                complete: function() {
                                },
                                error: function (xhr) {
                                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                                         tostrerror(message);
                                    });
                                }
                            });

                  }else{
                    //Acciones si el usuario no confirma
                    console.log("false");
                  }
        });






        //modalConfirm
        modalConfirm(function(confirm){
                  if(confirm){
                        jQuery.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type        : "POST",
                                url         : url+'/admin-customer-login',
                                cache       : false,
                                data        : {login_id:confirm,"_token": "{{ csrf_token() }}"},
                                dataType    : "json",
                                beforeSend  : function() {
                                    $('.loader_div').show();
                                },
                                success: function (response)
                                { $('.loader_div').hide();
                                    if(response.success)
                                    {

                                          $.toast({
                                                heading             : 'Success',
                                                text                : response.success_message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'success',
                                                hideAfter           : 2000,
                                                position            : 'top-right'
                                            });
                                            if(response.url)
                                            {
                                                        if(response.delayTime)
                                                            setTimeout(function() {
                                                                var win = window.open(response.url, '_blank');
                                                                win.focus();
                                                               //$('.anchorID').trigger('click');

                                                             }, 1000);
                                                        else
                                                            {
                                                              window.location.href=response.url;
                                                            }
                                            }
                                    }
                                    else{

                                         $.toast({
                                                heading             : 'Error',
                                                text                : response.error_message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'error',
                                                hideAfter           : response.delayTime,
                                                position            : 'top-right'
                                            });

                                    }


                                },
                                complete: function() {
                                },
                                error: function (xhr) {
                                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                                         tostrerror(message);
                                    });
                                }
                            });

                  }else{
                    //Acciones si el usuario no confirma
                    console.log("false");
                  }
        });


        //businessFunc
        function businessFunc(customerId,Id,htmlId,fieldData=0,action_url,$check=0)
        {
            //var customerId  = $(this).attr('data-id');
            if( customerId == '' || customerId == undefined )
                return false;


            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type        : "GET",
                url         :url+'/'+action_url+'/'+customerId,
                cache       : false,
                dataType    : "json",
                beforeSend  : function() {
                    $('.loader_div').show();
                },
                success: function (result)
                {
                    $('.loader_div').hide();
                    if(result.success)
                    {
                        if($check==0)
                        {
                         jQuery("#"+Id).modal('show');
                        }
                        $("#"+htmlId).html(result.html);
                    }
                    else
                    {
                        jjQuery("#"+Id).modal('hide');
                        $("#"+htmlId).html('');
                    }
                },
                complete: function() {
                },
                error: function (xhr) {
                    $('.loader_div').hide();
                    //console.log(xhr);
                }
            });
        }

        $(document).on('click', '.existingCustomerPromoCodeBtn', function(event)
        {
        	  customerId  = $(this).attr('data-id');
        	 businessFunc(customerId,'existingCustomersPromoCode','existingCustomerBody',0,'customer-business-list');

        });

        $(document).on('click', '.cancelBusinessPlanBtn', function(event)
        {
        	  customerId  = $(this).attr('data-id');
        	 businessFunc(customerId,'existingCancelBusinessPlan','existingCancelBusinessPlanBody',1,'customer-cancel-business-plan');

        });


        $("#leadConciergePermissionForm").validate({
     errorClass   : "has-error",
          highlight    : function(element, errorClass) {
              $(element).parents('.form-group').addClass(errorClass);
          },
          unhighlight  : function(element, errorClass, validClass) {
              $(element).parents('.form-group').removeClass(errorClass);
          },
          submitHandler: function (form)
          {
                                jQuery.ajax({
                                    url         : form.action,
                                    type        : form.method,
                                    //data        : $(form).serialize(),
                                    data        : new FormData(form),
                                    headers     : {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    contentType : false,
                                    cache       : false,
                                    processData : false,
                                    dataType    : "json",
                                    beforeSend  : function() {
                                        $('.loader_div').show();
                                    },
                                    success: function (result)
                                    {
                                        $('.loader_div').hide();
                                         $.toast({
                                                heading             : 'Success',
                                                text                : 'Permission plan have been saved',
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'success',
                                                position            : 'top-right'
                                            });



                                    },
                                    complete: function() {

                                    },
                                    error: function (xhr) {
                                        $('.loader_div').hide();
                                        //console.log(xhr);
                                        jQuery.each(xhr.responseJSON.errors,function(k,message){
                                         $.toast({
                                                heading             : 'Error',
                                                text                : message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'error',
                                                position            : 'top-right'
                                            });
                                         });

                                    }
                                });
                             return false;
            },

      });

 $("#cancelBusinessPlanForm").validate({
     errorClass   : "has-error",
          highlight    : function(element, errorClass) {
              $(element).parents('.form-group').addClass(errorClass);
          },
          unhighlight  : function(element, errorClass, validClass) {
              $(element).parents('.form-group').removeClass(errorClass);
          },
          rules:
          {
              business_id:
              {
                required: true,
              }
          },
          messages:
          {
              business_id: {
                  required: "This field is required.",
              }
          },
          submitHandler: function (form)
          {
                                jQuery.ajax({
                                    url         : form.action,
                                    type        : form.method,
                                    //data        : $(form).serialize(),
                                    data        : new FormData(form),
                                    headers     : {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    contentType : false,
                                    cache       : false,
                                    processData : false,
                                    dataType    : "json",
                                    beforeSend  : function() {
                                        $('.loader_div').show();
                                    },
                                    success: function (result)
                                    {
                                        $('.loader_div').hide();
                                         $.toast({
                                                heading             : 'Success',
                                                text                : 'Business plan have been activated',
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'success',
                                                position            : 'top-right'
                                            });

                                        businessFunc(customerId,'existingCancelBusinessPlan','existingCancelBusinessPlanBody',1,'customer-cancel-business-plan',1);
                                    },
                                    complete: function() {

                                    },
                                    error: function (xhr) {
                                        $('.loader_div').hide();
                                        //console.log(xhr);
                                        jQuery.each(xhr.responseJSON.errors,function(k,message){
                                         $.toast({
                                                heading             : 'Error',
                                                text                : message,
                                                loader              : true,
                                                loaderBg            : '#fff',
                                                showHideTransition  : 'fade',
                                                icon                : 'error',
                                                position            : 'top-right'
                                            });
                                         });

                                    }
                                });
                             return false;
            },

      });


      $("#existingCustomerPromoCodeForm").validate({
          errorClass   : "has-error",
          highlight    : function(element, errorClass) {
              $(element).parents('.form-group').addClass(errorClass);
          },
          unhighlight  : function(element, errorClass, validClass) {
              $(element).parents('.form-group').removeClass(errorClass);
          },
          rules:
          {
              business_id:
              {
                required: true,
              },
              promocode:{
                   required: true,
              }
          },
          messages:
          {
              business_id: {
                  required: "This field is required.",
              },
              promocode: {
                  required: "This field is required.",
              },
          },
          submitHandler: function (form)
          {
              formSubmit(form);
          }
      });
    </script>
    @endsection
    <!-- Promo code Ends -->
@endsection
