<table class="table table-responsive">
                            <thead>
                                <tr><td colspan="5" class="text-center">
                                        Cancel Plan History
                                      </td></tr>
                                <tr>
                                    <th>Plan Name</th>
                                    <th>Business Name</th>
                                     <th>Business Status</th>
                                    <th>Cancelled By</th>
                                    <th>Cancelled Date</th>


                                </tr>
                            </thead>
                             <tbody class="show_records--">

                                  @if($user_plan_list_data->count()==0)
                                  <tr>
                                      <td colspan="5" class="text-center">
                                        No Record Found.
                                      </td>
                                  </tr>
                                  @else
                                  @foreach($user_plan_list_data as $key=>$value)
                                      <tr>
                                           <td>
                                           @if(isset($value->usersPlans->name)){{ $value->usersPlans->name}}@else N/A @endif
                                           </td>
                                            <td>
                                           @if(isset($value->business->business_name)){{ $value->business->business_name}}@else N/A @endif
                                           </td>
                                            <td>
                                           @if($value->business->is_deleted==1) Deleted @else Active @endif
                                           </td>
                                           <td>
                                            @if(isset($value->cancel_by) && $value->cancel_by==0) Customer @else Admin @endif
                                           </td>
                                           <td>
                                            @if(isset($value->updated_at)){!! \Carbon\Carbon::parse($value->updated_at)->format('d/m/Y H:i:s') !!} @else N/A @endif
                                           </td>
                                     </tr>
                                  @endforeach
                                  @endif

                            </tbody>
                        </table>


<div class="form-group col-md-12">
    <div class="form-group">
        <label for="">Please Select Active Business</label>
        {!! Form::select('business_id',$business,null, ['class'=>'form-control','id'=>'customerbusiness']) !!}
    </div>
    <div class="col-md-12">
    <input type="submit" class="btn btn-primary pull-right" value="Cancel Business Plan">
</div>
</div>
