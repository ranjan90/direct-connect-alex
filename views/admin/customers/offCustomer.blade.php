@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Customers</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Customers</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group pull-left  col-md-4 nopadding">
                            <input type="text" data-url="{{url('ctwdr_dmlogin/incomplete-customers')}}" class="form-control" id="searchKey" placeholder="Search Customer by Name">
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_from',null,['class'=>'form-control datepicker', 'id'=>'fromDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_to',null,['class'=>'form-control datepicker', 'id'=>'toDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-2">
                            <a href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
                        </div>
<!--                         <div class="form-group text-right">
                            <a href="{{ URL(config('app.admintemplatename').'/add-new-plan') }}" class="btn btn-primary">Add Plan</a>
                        </div> -->
                    </div>
                </div>
                <div class="tile">
                    <div class="text-right admincustomersbtn">
                        <a href="{{ URL('ctwdr_dmlogin/customers') }}" class="btn btn-success">Active Customer </a>
                        <a href="{{ URL('ctwdr_dmlogin/inactive-customers') }}" class="btn btn-danger active">Inactive Customer</a>
                        <a href="javascript:void(0)" class="btn btn-orange active btn-arrow btn-arrow-bottom">Incomplete signup</a>
                    </div>
                    <div class="tile-body table-responsive recordsTable">
                        <table class="table  table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Region</th>
                                    <th>Topup Amount</th>
                                    <th>Promo Code</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="show_records">
                            <?php $i=0; ?>
                            @forelse($customers as $cust)
                                <?php $i++; ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ ucfirst($cust->firstName) }} {{ ucfirst($cust->lastName)  }}</td>
                                    <td>{{ $cust->email }}</td>
                                    <td>{{ $cust->phoneNo }}</td>
                                    <td>{{ $cust->region->name }}</td>
                                    <td>{{ $cust->top_up_recharge_amount }}</td>
                                    <td>{{ $cust->promo_code }}</td>
                                    <td>
                                        <a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $cust->id }}" data-table="{{ Crypt::encrypt('users') }}"><i class="fa fa-trash" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No Records Found !</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{$customers->links()}}
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->
@endsection
@section('js')
<link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript">
    $(function() {
        $( ".datepicker" ).datepicker();
    });
    $(document).on('change', '#fromDate', function()
    {
        var date1 = $(this).val();
        $("#toDate").datepicker("option","minDate",date1);
        if($("#toDate").val()!="")
            $("#searchKey").trigger('keyup');
    });
    $(document).on('change', '#toDate', function()
    {
        if($("#fromDate").val()!="")
            $("#searchKey").trigger('keyup');
    });

    $(".reset_filter").click(function()
    {
        if($(".form-control").val(""))
        {
            $("#searchKey").trigger('keyup');
        }
    });
</script>
@endsection
