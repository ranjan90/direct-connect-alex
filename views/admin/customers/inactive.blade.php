@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Customers</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Customers</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('flash_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="form-group pull-left  col-md-4 nopadding">
                            <input type="text" data-url="{{url('ctwdr_dmlogin/inactive-customers')}}" class="form-control" id="searchKey" placeholder="Search Customer by Name">
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_from',null,['class'=>'form-control datepicker', 'id'=>'fromDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-3">
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_to',null,['class'=>'form-control datepicker', 'id'=>'toDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group pull-left  col-md-2">
                            <a href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
                        </div>
<!--                         <div class="form-group text-right">
                            <a href="{{ URL(config('app.admintemplatename').'/add-new-plan') }}" class="btn btn-primary">Add Plan</a>
                        </div> -->
                    </div>
                </div>
                <div class="tile">
                    <div class="text-right admincustomersbtn">
                        <a href="{{ URL('ctwdr_dmlogin/customers') }}" class="btn btn-success">Active Customer </a>
                        <a href="javascript:void(0)" class="btn btn-danger active btn-arrow btn-arrow-bottom">Inactive Customer</a>
                        <a href="{{ URL('ctwdr_dmlogin/incomplete-customers') }}" class="btn btn-orange active">Incomplete signup</a>
                    </div>
                    <div class="tile-body table-responsive recordsTable">
                        <table class="table  table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Business</th>
                                    <th>Remaining Credits</th>
                                    <th>Promo Code</th>
                                    <th>Region</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="show_records">
                            <?php $i=0; ?>
                            @forelse($customers as $cust)
                                <?php $i++;
                                $remaining_mints = 0;
                                if( isset($cust->business) && count($cust->business) > 0 )
                                {
                                    foreach( $cust->business as $keey=>$vallue)
                                    {
                                        $uPlan = App\Model\UserPlan::where('business_id',$vallue['id']);
                                        if($uPlan->count()!=0)
                                        {
                                            $remaining_mints1 = $uPlan->first()->balanceAmount;
                                            $remaining_mints = $remaining_mints + $remaining_mints1;
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>@if(isset($cust->firstName)){{ ucfirst($cust->firstName) }} @endif @if(isset($cust->lastName)){{ ucfirst($cust->lastName) }} @endif</td>
                                    <td>@if(isset($cust->email)){{ $cust->email }} @endif</td>
                                    <td>@if(isset($cust->phoneNo)){{ $cust->phoneNo }} @endif</td>
                                    <td>
                                        <?php
                                            if(isset($cust->businessArr))
                                            {
                                                $cust->businessArr = array_values($cust->businessArr);
                                                if( count($cust->businessArr) > 2 )
                                                {
                                                    $businessNameString = $businessNameStringTooltip = '';
                                                    $keyb = 0;
                                                    foreach ($cust->businessArr as $key123 => $value123)
                                                    {
                                                        $keyb = $keyb+1;
                                                        $businessNameStringTooltip.= $keyb.') '.$value123.'<br>';
                                                        if($keyb < 3)
                                                            $businessNameString.=$value123.' <br>';
                                                    } ?>
                                                    <span style="cursor:pointer" data-html="true" data-toggle="tooltip" title="<?php echo $businessNameStringTooltip;?>">
                                                        <?php
                                                            $moreBusiness = count($cust->businessArr) - 2;
                                                            echo $businessNameString." + ".$moreBusiness." more";
                                                        ?>
                                                    </span>
                                                <?php } else { ?>
                                                    <span>
                                                        <?php foreach ($cust->businessArr as $keyl => $valuel) {
                                                            echo  $valuel."<br>";
                                                        } ?>
                                                    </span>
                                                <?php }

                                            } ?>
                                    </td>
                                    <td>{{$remaining_mints}}</td>
                                    <td>{{ $cust->promo_code }}</td>
                                    <td>@if(isset($cust->region->name)){{ $cust->region->name }} @endif</td>
                                    <td>
                                        <!--<a href="{{ $url.'/view-customer/'.Crypt::encrypt($cust->id) }}" data-toggle="tooltip" title="View"><span><i class="fa fa-eye"></i> </span></a>
                                        @if(count($cust->user_campaigns) > 0)
                                        <a href="{{ $url.'/customer-campaign/'.Crypt::encrypt($cust->id) }}"data-toggle="tooltip" title="Campaign"><span><i class="fa fa-newspaper-o"></i> </span></a>
                                        @endif-->
                                        <a href="{{ $url.'/customers/view-business-list/'.Crypt::encrypt($cust->id) }}"><span><i class="fa fa-eye" data-toggle="tooltip" title="View Business"></i> </span></a>
                                        <a href="javascript:;" data-id="{{Crypt::encrypt($cust->id)}}" class="existingCustomerPromoCodeBtn"><span><i class="fa fa-tag" data-toggle="tooltip" title="Promo Code"></i> </span></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No Records Found !</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{$customers->links()}}
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->

    <!-- Promo code Modal -->
    <div id="existingCustomersPromoCode" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Promo Code</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::open(array('id'=>'existingCustomerPromoCodeForm','url'=>URL('ctwdr_dmlogin/apply-promocode'))) }}
                                <div id="existingCustomerBody">
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('js')
    <link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        $(function() {
            $( ".datepicker" ).datepicker();
        });
        $(document).on('change', '#fromDate', function()
        {
            var date1 = $(this).val();
            $("#toDate").datepicker("option","minDate",date1);
            if($("#toDate").val()!="")
                $("#searchKey").trigger('keyup');
        });
        $(document).on('change', '#toDate', function()
        {
            if($("#fromDate").val()!="")
                $("#searchKey").trigger('keyup');
        });

        $(".reset_filter").click(function()
        {
            if($(".form-control").val(""))
            {
                $("#searchKey").trigger('keyup');
            }
        });

        $(document).on('click', '.existingCustomerPromoCodeBtn', function(event)
        {
            var customerId  = $(this).attr('data-id');
            if( customerId == '' || customerId == undefined )
                return false;
            var url         = "<?php echo $url?>";

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type        : "GET",
                url         :url+'/customer-business-list/'+customerId,
                cache       : false,
                dataType    : "json",
                beforeSend  : function() {
                    $('.loader_div').show();
                },
                success: function (result)
                {
                    $('.loader_div').hide();
                    if(result.success)
                    {
                        jQuery("#existingCustomersPromoCode").modal('show');
                        $("#existingCustomerBody").html(result.html);
                    }
                    else
                    {
                        jQuery("#existingCustomersPromoCode").modal('hide');
                        $("#existingCustomerBody").html('');
                    }
                },
                complete: function() {
                },
                error: function (xhr) {
                    $('.loader_div').hide();
                    //console.log(xhr);
                }
            });
      });
      $("#existingCustomerPromoCodeForm").validate({
          errorClass   : "has-error",
          highlight    : function(element, errorClass) {
              $(element).parents('.form-group').addClass(errorClass);
          },
          unhighlight  : function(element, errorClass, validClass) {
              $(element).parents('.form-group').removeClass(errorClass);
          },
          rules:
          {
              business_id:
              {
                required: true,
              },
              promocode:{
                   required: true,
              }
          },
          messages:
          {
              business_id: {
                  required: "This field is required.",
              },
              promocode: {
                  required: "This field is required.",
              },
          },
          submitHandler: function (form)
          {
              formSubmit(form);
          }
      });
    </script>
    @endsection
    <!-- Promo code Ends -->
@endsection
