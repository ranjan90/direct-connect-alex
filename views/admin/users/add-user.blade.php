@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-list"></i>Add User</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/user-management') }}">User Management</a></li>
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">Add User</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Add New User</h3>
                <div class="tile">
                {{ Form::model($planData['plan_detail'],array('url' => $planData['form_action'],'id' => 'addUserForm','class' => 'plan-form','autocomplete' => 'off')) }}
                <div class="row">
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">First Name<span class="star">*</span></label>
                            {!!  Form::text('firstname',null,['placeholder'=>"First Name",'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Last Name<span class="star">*</span></label>
                            {!!  Form::text('lastname',null,['placeholder'=>"Last Name",'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Email<span class="star">*</span></label>
                            {!!  Form::text('email',null,['placeholder'=>"Email",'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <!-- <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Password<span class="star">*</span></label>
                            <input name="password" type="password" value="" class="form-control"/>
                        </div>
                    </div> -->
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Phone<span class="star">*</span></label>
                            {!!  Form::text('phone',null,['placeholder'=>"Phone Number",'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Country<span class="star">*</span></label>
                            <select name="Country" Class="form-control">
                                 <option value ="">Select Country</option>
                                 @foreach($planData['country'] as $country)
                                    <option value="{{$country->id}}">{{ucfirst($country->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Select Role<span class="star">*</span></label>
                            <!-- {{Form::select('Role', $planData['role'],null,['class'=>'form-control']) }} -->
                            <select name="Role" Class="form-control">
                            <option value ="">Select Role</option>
                            @foreach($planData['role'] as $role)
                               <option value="{{$role->id}}">{{ucfirst($role->name)}}</option>
                           @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="col-md-4 plans">
                        <div class="form-group">
                            <label for="">Select Customer<span class="star">*</span></label>
                            <select name="customer_id" id="addUserCustomerId" Class="form-control">
                            <option value ="">Select Customer</option>
                            @foreach($customers as $customer)
                               <option value="{{$customer['id']}}">{{ucfirst($customer['name'])}}</option>
                           @endforeach
                           </select>
                        </div>
                    </div>

                    <div class="col-md-12 text-right">
                        <button type="submit" id="add-user" class="btn btn-primary submitCustomerBtn">Save</button>
                    </div>


            </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
@endsection
