<table class="table  table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Region</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="show_records">
                            <?php $i=0; ?>
                            @forelse($users as $cust)
                                <?php $i++; ?>
                                <tr>
                                    <td>{{ $page + $i }}</td>
                                    <td>@if(isset($cust->firstName)){{ ucfirst($cust->firstName) }} @endif @if(isset($cust->lastName)){{ ucfirst($cust->lastName) }} @endif</td>
                                    <td>@if(isset($cust->email)){{ $cust->email }} @endif</td>
                                    <td>@if(isset($cust->phoneNo)){{ $cust->phoneNo }} @endif</td>
                                    <td>@if(isset($cust->region->name)){{ $cust->region->name }} @endif</td>

                                    <td>{{  ucfirst($cust->roles()->pluck('name')->implode(' ')) }}</td>
                                    <td>
                                        <a href="{{ $url.'/assign-business/'.Crypt::encrypt($cust->id)}}" data-toggle="tooltip" title="Assign Business" data-placement="bottom"><i class="fa fa-tasks" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">No Records Found !</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{$users->links()}}
