@extends('layouts.admin')
@section('content')
<main class="app-content">
    <div class="app-title">
        <div>
            <?php if(!empty($business) && isset($business[0]) ) {?>
                <h1 style="color:#212529"><i class="fa fa-list"></i> <?php echo ucfirst($business[0]->business_name);?></h1>
            <?php } else {?>
                <h1><i class="fa fa-list"></i> Permissions</h1>
            <?php }?>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/user-management') }}">User Management</a></li>
            <li class="breadcrumb-item"><a  href = "javascript:void(0) ">Permission</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Set Permission</h3>
            <div class="tile">
            {{ Form::model($user,array('url' => $userData['form_action'],'id' => 'permission-form','class' => 'plan-form','autocomplete' => 'off')) }}
            <div class="row">
                <div class="col-md-4 plans">
                    <div class="form-group">
                         <b>User Name : {{ucfirst($user->firstName).' '.ucfirst($user->lastName)}}</b>
                         {{ Form::hidden('name', $user->firstName, array('class' => 'form-control','readonly'=>'true')) }}
                         {!!  Form::hidden('user_id',$userData['record_id']) !!}
                    </div>
                </div>
            </div>
            <style>
            .form-group.has-error label {
                color: #dd4b39;
            }
            .permission-from-group.has-error label.has-error{
              display: none;
            }
            </style>
           @foreach ($permissions as $permission_key=>$permission_value)
              <h4>{{$permission_key}}</h4>
              @foreach($permission_value as $sub_permission_key =>$sub_permission_value)
                <div class="form-group permission-from-group">
                  {{Form::checkbox('permissions[]',  $sub_permission_value->id, in_array($sub_permission_value->id, $userPermissions) ) }}
                  {{Form::label($sub_permission_value->title, $sub_permission_value->title) }}<br>
                </div>
             @endforeach
             @endforeach
              <div class="row">
                <div class="col-md-12 text-right">
                 @if(isset($userData['record_id']) and $userData['record_id']!="")
                    <button type="submit" class="btn btn-primary submitBtn">Update</button>
                 @else
                    <button type="submit" class="btn btn-primary submitBtn">Save</button>
                 @endif
                </div>
              </div>
              {{ Form::close() }}
            </div>
        </div>
    </div>
</main>
@endsection
