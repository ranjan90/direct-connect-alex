@extends('layouts.admin')
@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-list"></i><?php echo ucfirst($output['data']->firstName).' '.ucfirst($output['data']->lastName);?></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item"><a href="{{ URL(config('app.admintemplatename').'/user-management') }}">User Management</a></li>
                <li class="breadcrumb-item"><a  href = " javascript:void(0) ">Assign Business</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <!-- Icon Cards-->
                    <div class="row">
                      <div class="col-md-12">
                        {{ Form::open(array('url'=>$url.'/assign-business-form','id'=>'assignBusinessForm','autocomplete' => 'off')) }}
                          <div class="row">
                            <div class="col-md-4 plans">
                                <div class="form-group"></div>
                            </div>
                            <div class="col-md-4 plans">
                                <div class="form-group">
                                    <label for="">Business<span class="star">*</span></label>
                                    <select name="business_id" Class="form-control">
                                         <option value ="">Select Business</option>
                                         @foreach($output['ownersBusinesss'] as $business)
                                            <?php if( isset($output['data']) && !empty($output['data'])){?>
                                                <option <?php if( $business->business_id == $output['businessId'] ){ ?>selected<?php } ?> value="{{$business->businessDropdown->id}}">{{ucfirst($business->businessDropdown->business_name)}}</option>
                                            <?php } else {?>
                                                <option value="{{$business->businessDropdown->id}}">{{ucfirst($business->businessDropdown->business_name)}}</option>
                                            <?php } ?>
                                        @endforeach
                                    </select>
                                    @if(isset($userId))
                                    {!! Form::hidden('userId',$userId) !!}
                                    @else
                                    {!! Form::hidden('userId',null) !!}
                                    @endif
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 text-right">
                                {!!  Form::submit('Save',['class'=>'btn btn-primary']) !!}
                            </div>
                          </div>
                          {{ Form::close() }}
                      </div>
                   </div>
                    <div class="latest-champaign-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Sno.</th>
                                        <th scope="col">Business</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @can(config('permissions.data.view-contact.name'))
                                        <?php $i=0;?>
                                        @forelse($output['assignedBusinesss'] as $assignedBusiness)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ ucfirst($assignedBusiness['business_dropdown']['business_name']) }}</td>
                                                <td>
                                                    @can(config('permissions.data.delete-contact.name'))
                                                        <a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $assignedBusiness['id'] }}" data-table="{{ Crypt::encrypt('business_users') }}"><span  data-toggle="tooltip" title="Unassgin Business" data-placement="bottom"><span><i class="fa fa-trash"></i> </span></a>
                                                    @endcan
                                                    @can(config('permissions.data.set-user-permission.name'))
                                                        <a href="{{ $url.'/set-permissions/'.Crypt::encrypt($userId.'---'.$assignedBusiness['business_id']) }}" data-toggle="tooltip" title="Set Permissions"><span><i class="fa fa-ban"></i> </span></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="3" align="center">No business available.!</td>
                                            </tr>
                                        @endforelse
                                    @endcan
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Unassgin</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to unassign this business ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Unassgin</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->
@endsection
