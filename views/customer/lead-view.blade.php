<style>
.pullRightBtn{
    float: right;
}
.callDataTable tr td,th {
	font-size: 12px !important;
}
</style>
<?php
$getBusinessId = \Auth::user()->getBusinessId();
// $callsettings  = \App\Model\CallSetting::select('*')->where('business_id',$getBusinessId);
// if( $callsettings->count() > 0 )
// {
//     $callsettings = $callsettings->first();
//     $recDisplay = $callsettings->call_recording_display;
// }
// else
// {
//     $callsettings = array();
//     $recDisplay = '';
// }
?>
 <div class="modal-content sdfsdfsd">
    <div class="modal-header">
       <h4 class="modal-title">Call Info</h4>
       <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <div class="row">
          <div class="table-responsive ">
              <div class="table">
                  <table class="table" width="100%" border="1" borderColor="#e9ecef">
                      <tbody>
                          <?php $camp = App\Model\Campaign::where('id',$data[0]->campaign_id)->first(); if(!empty($data) && isset($data[0])) {?>
                          <tr><td style="width:200px;">Call Time</td><td><?php
                          echo App\Helpers\GlobalFunctions::getTimeZoneDateTime($camp,$data[0]->startdate);
                          //echo date('d/m/Y h:i:s', strtotime());
                           ?></td></tr>
                          <tr><td width="100px">Agent</td><td><?php echo ucfirst($data[0]->agent);?></td></tr>
                          <tr><td width="100px">Department</td><td><?php echo ucfirst($data[0]->department);?></td></tr>
                          <tr><td width="100px">Answered By</td>
                              <td>
                                  <?php //echo ucfirst($data[0]->answered_by);?>
                                   <?php
                                                      $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($data[0]->parse_emails);
                                                      extract($return_data);

                                                    ?>
                                  <?php
                                    if($data[0]->answered_by != '' )
                                    {
                                        echo ucfirst($data[0]->answered_by);
                                    }
                                    else if( !empty($callAttempt) && isset($callAttempt[0]) && $callAttempt[0]->contact_name != '' )
                                    {
                                        echo ucfirst($callAttempt[0]->contact_name.'('.$callAttempt[0]->number.")");
                                    } ?>
                                    
                                     @if(isset($business_btn_color))

                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo isset($business_btn_message) ?  $business_btn_message :  'N/A'; ?></button>

                                                    @endif
                                 
                              </td>
                          </tr>
                          <tr><td width="100px">Visitor</td>
                              <td>
                                  <?php echo ucfirst($data[0]->lead_contact);?>
                                  @if(isset($customer_btn_color))

                                                     <button type="button" class="call-status-button btn <?php  echo $customer_btn_color;?> set_btn_width"><?php echo isset($customer_btn_message) ?  $customer_btn_message : 'N/A'; ?></button>

                                                    @endif
                                  
                              </td>
                          </tr>
                          <tr><td width="100px">Status</td><td>
                            <?php
                               $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($data[0]->parse_emails);
                               extract($return_data);
                               if(!empty($status_message))
                               {
                                      echo $status_message;
                               }
                               else{
                                echo 'N/A';
                               }
                            ?>
                          </td></tr>
                          <tr><td width="100px">Whisper</td><td><?php echo ucfirst($data[0]->whisper);?></td></tr>
                          <?php if($data[0]->recording != '' && $data[0]->call_recording_display==1 ) {?>
                            @if($data[0]->call_recording_display==1)
                            <tr><td width="100px">Recording URL</td>
                                <td><a style="font-size:13px" href="<?php echo $data[0]->recording;?>" target="_blank"><?php echo $data[0]->recording;?></a>
                                </td>
                            </tr>
                            @endif
                          <?php }
                                if($data[0]->call_recording_display==1)
                                { ?>
                                  <!--tr>
                                      <td colspan="2" class="showCallTrnascription">
                                          <label for="collapsible" class="lbl-toggle">Show Call Transcription</label>
                                          <div class="collapsible-content">
                                              <div class="content-inner">
                                              <?php if( $keywords != '' ) {
                                                  $matchedKeywords = App\Helpers\GlobalFunctions::findKeywordsInTranscription($data[0]->call_transcription, $keywords );
                                                  $data[0]->call_transcription = App\Helpers\GlobalFunctions::hightlightKeywordsInTranscription($data[0]->call_transcription, $keywords );
                                                  if(!empty($matchedKeywords))
                                                  { ?>
                                                      <p><span><strong>Tags : </strong></span>
                                                      <?php
                                                      foreach ($matchedKeywords as $mkey => $mvalue) {
                                                          if($mkey > 0 )
                                                              echo ', '.$mvalue;
                                                          else
                                                              echo $mvalue;
                                                      } ?>
                                                      </p>
                                              <?php    }
                                                  ?>

                                              <?php } ?>
                                              <p <?php if( $data[0]->call_transcription == '' ){?>style="text-align:center"<?php } ?>>
                                                  <?php
                                                    if( $data[0]->call_transcription != '' )
                                                        echo $data[0]->call_transcription;
                                                    else
                                                        echo 'No transcription.';
                                                  ?>
                                              </p>
                                              </div>
                                          </div>
                                      </td>
                                  </tr-->
                          <?php }  ?>
                      <?php } else {?>
                          <tr align="center"><td colspan="5">No data found.</td></tr>
                      <?php } ?>
                      </tbody>
                  </table>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="table-responsive ">
              <div class="table">
                  <table class="callDataTable table" width="100%" border="1" borderColor="#e9ecef">
                      <thead>
                          <tr>
                              <th>Member</th>
                              <th>Pickup time</th>
                              <?php if(!empty($callAttempt) && isset($callAttempt[0]) && $callAttempt[0]->recording != '' && $data[0]->call_recording_display==1 ) {?>
                              <th>Recording</th>
                              <?php } ?>
                              <th>Disposition</th>
                              <th>Call Length</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php if(!empty($callAttempt) && isset($callAttempt[0])){?>
                          <tr>
                               <td><?php if($callAttempt[0]->api_lead->answered_by != '' ) { echo ucfirst($callAttempt[0]->api_lead->answered_by); } else if( $callAttempt[0]->contact_name != '' ) { echo ucfirst($callAttempt[0]->contact_name.'('.$callAttempt[0]->number.")"); } ?></td>
                               <td><?php
                                    echo App\Helpers\GlobalFunctions::getTimeZoneDateTime($camp,$callAttempt[0]->sc1_answered);

                                    //reset($timezone);
                                    //echo date('d/m/Y h:i:s', strtotime($callAttempt[0]->sc1_answered));
                               ?></td>
                               <?php if($callAttempt[0]->recording != '' && $data[0]->call_recording_display==1 ) {?>
                               <td>

                                      <audio controls controlsList="nodownload">
                                          <source src="<?php echo $callAttempt[0]->recording;?>" type="audio/wav">
                                      </audio>

                              </td>
                              <?php } ?>
                              <td><?php echo ucfirst($callAttempt[0]->disposition);?></td>
                              <td><?php echo $callAttempt[0]->duration;?></td>
                         </tr>
                      <?php } else { ?>
                          <tr align="center"><td colspan="5">No data found.</td></tr>
                      <?php } ?>
                      </tbody>
                  </table>
            </div>
        </div>
      </div>
      <br>
       <div class="text-right">
          <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Close</button>
       </div>
    </div>

 </div>
 <script>
     $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
     
 </script>
