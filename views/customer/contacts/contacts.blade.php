<div class="table-responsive latest-champaign-table">
    <table class="table contact-table">
    	  <thead>
    	  <tr>
    		  <th>Name</th>
            <th>Email</th>
    		   <th>Contact Number</th>
    			<th></th>
    	  </tr>
    	  </thead>
    	  <tbody>
          <?php $i = true; ?>
        @forelse($contacts as $contact)
        <tr>
            <td>{{$contact->name}}</td>
            <td>{{$contact->email}}</td>
            <td>{{$contact->contact}}</td>
            <td>
              <div class="campaign-checkbox">
                <div class="form-group">
                    <input type="checkbox" id="check{{$contact->id}}" value="{{$contact->id}}" name="contactid[]">
                    <label for="check{{$contact->id}}"><span class="circle-checkbox"></span></label>
                </div>
              </div>
        </tr>
        @empty
        <?php $i= false; ?>
        <tr>
            <td colspan="3">No Contacts Available.</td>
        </tr>
        @endforelse
    	  </tbody>
	</table>
</div>
</div>
<div class="modal-footer">
      <div class="text-right">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <?php if ($i): ?>
          <button type="submit" class="btn btn-primary">Save</button>
        <?php endif; ?>
      </div>
