@extends('layouts.customer')
@section('content')
<div class="row">
<h2 class="main_title_head">Company Contacts</h2>
</div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->

<div class="row">
	<div class="col-md-12">
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

	<div class="heading-campaigns">
	  <h2 class="campaign_name">You can add or edit team members at any time.</h2>
	  <div class="create-new-campaign-btn">
      @can(config('permissions.data.add-contact.name'))
      @if(empty($userId))
		    <a href="{{$url.'/add-contact'}}" class="btn btn-warning create-latest-campaign"><i class="fa fa-user"></i> Create a New Contact</a>
      @else
        <a href="{{ $url.'/add-contact/'.$userId}}" class="btn btn-warning create-latest-campaign"><i class="fa fa-user"></i> Create a New Contact</a>
      @endif
      @endcan
	  </div>
	</div>
  <div class="main-up"><p class="up-tip">Once you reach the limit of team members included in your plan, you will need to delete members or <a href="javascript:void(0)" class="text-orange upgrate-take-tour">UPGRADE</a> your account to add new team members.
        </p>
        <div class="tol-tip cstm-tol d-none upgrate-take-tour-show">
            <h4>Company Contacts Limit Reached!</h4>
            <p>You have reached the company contacts limit included in your current plan.  </p>
            <p>Simply Upgrade your monthly subscription to add more team members, or just replace one of the existing members that you no longer need. </p>
            <div class="limit-btn d-inline-block w-100">
                <a class="btn btn-default tour-gotit-upgrate">Okay! got it!</a>
                <a class="btn btn-warning float-right upgrate-now-button">Upgrade now!</a>
            </div>
        </div>
        </div>
	</div>
	</div>
    <!-- Icon Cards-->
    <div class="row">

      <div class="col-md-12 contact_listing">

      </div>
    </div>
</div>
@section('js')
<script type="text/javascript">
$('.upgrate-take-tour').click(function(){
  $('.upgrate-take-tour-show').removeClass('d-none');
})
$(document).on('click','.tour-gotit-upgrate',function(){
    $('.upgrate-take-tour-show').addClass('d-none');
})
$(document).on('click','.upgrate-now-button',function(){
  localStorage.setItem("upgratenow", 1);
  window.location.replace(prefixUrl+'/plans');
})
//getContactData
   var sendData={uid:"<?php echo $uid ?>"};
   //console.log(localStorage.getItem('business_data_id'));

/* Method :getContactData */
function getContactData()
     {
             $.ajax({
                url                : prefixUrl + '/fetch-all-contacts?business_id='+localStorage.getItem('business_data_id'),
                type               : 'GET',
                dataType           : 'html',
                data:              sendData,
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function() {
                      showLoader();
                },
                complete   : function() {
                    hideLoader();
                },
                success    : function(response) {
                    $('.contact_listing').html(response);
                     hideLoader();

                }
            });
}
if(storge_customer_id=="")
{
  getContactData();
}
var paginationClass="contact_listing";
</script>
@endsection
@endsection
