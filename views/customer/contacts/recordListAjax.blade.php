<div class="latest-champaign-table"><div class="table-responsive"><table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Country Code</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
                @can(config('permissions.data.view-contact.name'))
                <?php $i=0; ?>
                @forelse($contacts as $contact)
                <?php $i++; ?>
                <tr>
                   <td>{{ $page+$i }}</td>
                  <td>{{ $contact->name }}</td>
                  <td>{{ $contact->country->countryPhoneCode }} ({{ $contact->country->countryName }})</td>
                  <td>{{ $contact->contact }}</td>
                  <td>
                    @can(config('permissions.data.edit-contact.name'))
                    <a href="{{$url.'/edit-contact/'.Crypt::encrypt($contact->id)}}" data-toggle="tooltip" title="Edit" data-placement="bottom"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a>
                    @endcan
                    @can(config('permissions.data.delete-contact.name'))
                    <a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $contact->id }}" data-table="{{ Crypt::encrypt('contacts') }}"><span  data-toggle="tooltip" title="Delete" data-placement="bottom"><img src="{{asset('assets/customer/img/delete.png')}}"></span></a>
                    @endcan
                  </td>

                    <!-- <a href="{{URL('delete-record')}}"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a> -->

                </tr>
                @empty
                <tr>
                  <td colspan="5">No Contacts Available.</td>
                </tr>
                @endforelse
                  @endcan
              </tbody>
</table>
</div>
</div>
{!! $contacts->appends(['business_id'=>$business_id,'uid'=>$uid])->links()!!}
