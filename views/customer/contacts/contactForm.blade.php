@extends('layouts.customer')
@section('content')
<div class="row">
<h2 class="main_title_head">Create Company Contacts</h2>
</div>
  <div class="container-fluid">
    <!-- Icon Cards-->
    <div class="row">

      <div class="col-md-12">
          @if(isset($data))
            {{ Form::model($data,array('url'=>$url.'/save-contact','id'=>'contact_form','autocomplete' => 'off')) }}
          @else
            {{ Form::open(array('url'=>$url.'/save-contact','id'=>'contact_form','autocomplete' => 'off')) }}
          @endif
          <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Contact Name <span class="star">*</span></label>
                    {!! Form::text('name',null,['placeholder'=>"Name",'class'=>'form-control']) !!}

                    @if(isset($recordId))
                    {!! Form::hidden('recordId',$recordId) !!}
                    @else
                    {!! Form::hidden('recordId',null) !!}
                    @endif

                    @if(isset($userId) && !empty($userId))
                    {!! Form::hidden('userId',$userId) !!}
                    @else
                    {!! Form::hidden('userId',$uid) !!}
                    @endif

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Contact Email <span class="star">*</span></label>
                    {!! Form::text('email',null,['placeholder'=>"Contact Email",'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Role/Title </label>
                    {!! Form::text('role',null,['placeholder'=>"Role/Title",'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Department/Location </label>
                    {!! Form::text('dept',null,['placeholder'=>"Department/Location",'class'=>'form-control']) !!}
                    @if(isset($recordId))
                    {!! Form::hidden('recordId',$recordId) !!}
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Country Code <span class="star">*</span></label>
                    {!! Form::select('code',$countryCode,Null,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Phone Number <span class="star">*</span></label>
                    {!!  Form::text('number',null,['placeholder'=>"Phone Number",'class'=>'form-control','id'=>'phonenum']) !!}
                </div>
            </div>
          </div>
          <?php
          $daysArr = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
          // echo '<pre>';
          // print_r($data['schedule']);
          ?>
          <div class="row" style="display: none;">
            <div class="col-md-12">
              <div class="form-group">
                    <label for="">Schedule <span class="star">*</span></label>
              </div>
              <div class="">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Days</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @for($i=0;$i<=6;$i++)
                    <tr>
                      <td>{{ $daysArr[$i] }}</td>
                      <td>
                        <input class="form-control validate timepicker" type="text" name="dayFrom[]" value="@if(isset($data['schedule'][$i]->from)) {{$data['schedule'][$i]->from}} @endif" id="dayF{{$i}}">
                        </td>
                      <td><input class="form-control validate timepicker" type="text" name="dayTo[]" value="@if(isset($data['schedule'][$i]->to)) {{$data['schedule'][$i]->to}} @endif" id="dayT{{$i}}"></td>

                      <td><span class="daySts" style="font-size: 30px; color: #007bff;" data-val="@if(isset($data['schedule'][$i]->status)) {{ $data['schedule'][$i]->status }} @else 0 @endif"><i class="fa @if(isset($data['schedule'][$i]->status) && $data['schedule'][$i]->status!=0) fa-toggle-on @else fa-toggle-off @endif" data-toggle="tooltip" title="" data-original-title="Change Status"></i></span><input class="sts-input" type="hidden" name="dayStatus[]" value="@if(isset($data['schedule'][$i]->status)) {{ $data['schedule'][$i]->status }} @else 0 @endif"></td>
                    </tr>
                    @endfor
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-right">
              <a class="float-left backStepBtn back-now" href="{{$url.'/contacts/'.$uid}}"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
              @if(isset($recordId))
                {!!  Form::submit('Update',['class'=>'btn btn-warning']) !!}
              @else
                {!!  Form::submit('Save Changes',['class'=>'btn btn-warning']) !!}
              @endif

            </div>
          </div>
          {{ Form::close() }}
        </div>
    </div>
</div>
@section('js')

    <script type="text/javascript">


			$(document).ready(function(){

					$('#phonenum').keyup(function(){
            var value = $('#phonenum').val();
            if(value != '' || value == undefined){
              $('#phonenum').val(parseInt(value));
            }else{
              $('#phonenum').val('');
            }
				   });
			 });
    </script>
@endsection
@endsection
