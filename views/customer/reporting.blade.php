@php
$consumedMin = 0;
if(isset($data->objects)){
  foreach($data->objects as $camps){
    $consumedMin += $camps->total_called_seconds/60;
  }
}
@endphp
@extends('layouts.customer')
@section('content')
<div class="row">
<h2 class="main_title_head">Reporting</h2>
</div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->

  </div>
<div class="col-md-12 dashboard_report_data">
    </div>


<link href="{{ asset('assets/customer/css/bootstrap-multiselect.css') }}" rel="stylesheet">
@section('js')
<script type="text/javascript" src="{{ asset('assets/customer/js/bootstrap-multiselect.js') }}"></script>
<?php
if(Auth::check()) {
  $roleName = Auth::User()->roles()->first()->name;
  if( $roleName == 'business-admin')
    $roleName = 'admin';
} else {
   $roleName = 'customer';
}
?>
<script type="text/javascript">
    var tab="<?php echo $tab?>";
    var filter="<?php echo $filter?>";
    var campId="<?php echo $campId?>";
    var dateFrom="<?php echo $dateFrom?>";
    var dateTo="<?php echo $dateTo?>";
    var searchkey="<?php echo $searchkey?>";
    var roleName="<?php echo $roleName?>";
    var call_keywords="<?php echo $call_keywords?>";
    /* getFetchReportData() */
    function getFetchReportData(tab="",filter="",campId="",dateFrom="",dateTo="",call_keywords="")
       {
             $.ajax({
                url                : siteroot + '/'+roleName+'/fetch-report-data?business_id='+localStorage.getItem('business_data_id'),
                type               : 'GET',
                dataType           : 'html',
                data               : {tab:tab,filter:filter,campaign:campId,date_from:dateFrom,date_to:dateTo,searchkey:searchkey,call_keywords:call_keywords},
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function() {
                    $(".loader_div").show();
                },
                complete   : function() {
                    $(".loader_div").hide();
                },
                success    : function(response) {
                    $('.dashboard_report_data').html(response);
                    $(".loader_div").hide();

                }
            });
    }
  if(localStorage.getItem('business_data_id')!=null || localStorage.getItem('business_data_id')!="")
  getFetchReportData(tab,filter,campId,dateFrom,dateTo,call_keywords);
  var paginationClass="dashboard_report_data";
</script>
@endsection
@endsection
