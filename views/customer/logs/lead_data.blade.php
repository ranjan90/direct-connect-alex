<?php
$callsettings  = \App\Model\CallSetting::select('*')->where('business_id',$business_id);
if( $callsettings->count() > 0 )
{
    $callsettings = $callsettings->first();
    $recDisplay = $callsettings->call_recording_display;
}
else
{
    $recDisplay = '';
}
?>

<div class="latest-champaign-table">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Lead #</th>
                    <!--<th scope="col">Lead Name</th>-->
                    <!--<th scope="col">Lead Type</th>-->
                    <!--<th scope="col">Campaign Email Id</th>-->
                    <!--<th scope="col">Phone No</th>-->
                    <!--<th scope="col">Message</th>-->
                    <!--<th scope="col">Status</th>-->
                    <!--<th scope="col">Created At</th>-->
                    <!--<th>Lead Id</th>
                    <th>Lead Name</th>
                    <th>Lead Type</th>-->
                    <!-- <th>Busines Name</th>
                     --><!--<th>Campaign Email Id</th>-->
                    <th>Campaign Name</th>
                    <!--<th>Phone No</th>-->
                    <th>Credits Used</th>
                    <th>Customer Number</th>
                    <?php if($recDisplay==1){?>
                        <th>Tags</th>
                    <?php } ?>
                    <th>Business</th>
                   <th>Customer</th>

                    <th>Status</th>

                    <th style="min-width: 175px;">Created At</th>
                    @can(config('permissions.data.log-detail.name'))
                            <th>More info</th>

                    @endcan
                </tr>
            </thead>
             <tbody class="show_records">
                 @can(config('permissions.data.log-list.name'))
                  @if($data->count()==0)
                    <tr><td style="text-align:center !important" colspan="9" class="text-center">No Record Found.</td></tr>
                  @else
                      @foreach($data as $key=>$value)
                          <tr>
                              <td data-id="<?php echo $value->id;?>">@if(!empty($value->lead_id)){{$value->lead_id}} @else N/A @endif</td>
                              <!--<td>@if(!empty($value->leadName)){{$value->leadName}} @else N/A @endif</td>-->
                              <!--<td>@if(!empty($value->campaignEmail))@php @endphp @if (filter_var($value->campaignEmail, FILTER_VALIDATE_EMAIL)) Email @else Api @endif @else N/A @endif</td>-->
                              <!--<td> @if(!empty($value->campaignEmail)){{$value->campaignEmail}} @else N/A @endif</td>-->
                              <!--<td>@if(!empty($value->mobileNo)){{$value->mobileNo}} @else N/A @endif</td>-->
                              <!-- <td> @if(!empty($value->campaign_details->business->business_name)){{$value->campaign_details->business->business_name}} @else N/A @endif</td> -->
                              <td> @if(!empty($value->campaign_details->title)){{$value->campaign_details->title}} @else N/A @endif</td>
                              <td>@if(isset($value->api_leads->credit_amount)){{ $value->api_leads->credit_amount }} @else 0 @endif</td>
                             <?php /*
                              <td>
                                  <?php
                                    if(!empty($value->api_response))
                                    {
                                        $api_response=json_decode($value->api_response);
                                        if(!empty($api_response) && isset($api_response->content))
                                        {
                                             if($value->status==1)
                                              {
                                                  if(isset($api_response->content->message)){echo $api_response->content->message;}else{ echo 'N/A'; }
                                              }
                                              else
                                              {
                                                  if(isset($api_response->content->error))
                                                  {
                                                     echo $api_response->content->error;
                                                  }
                                                  else if(isset($api_response->content->error_message))
                                                  {
                                                      echo $api_response->content->error_message;
                                                  }
                                                  else if(isset($api_response->content->message))
                                                  {
                                                      echo $api_response->content->message;
                                                  }
                                                  else{
                                                    echo 'N/A';
                                                  }

                                              }
                                          }
                                          else
                                          {
                                              echo "N/A";
                                          }
                                      }
                                      else{
                                           echo "N/A";
                                      }
                                    ?>
                             </td>
                             */?>
                             <td>@if(!empty($value->mobileNo)){{$value->mobileNo}} @else N/A @endif</td>
                             <?php if($recDisplay==1){?>
                                 <td>
                                     <?php
                                     if($value->api_leads['call_recording_display'] == 1)
                                     {
                                        if(isset($call_keywords))
                                        {
                                             $matchedKeywords = App\Helpers\GlobalFunctions::findKeywordsInTranscription($value->api_leads['call_transcription'], $call_keywords );
                                             if(!empty($matchedKeywords))
                                             {
                                                 foreach ($matchedKeywords as $mkey => $mvalue) {
                                                     if($mkey > 0 )
                                                         echo ', '.$mvalue;
                                                     else
                                                         echo $mvalue;
                                                 }
                                             }
                                         }
                                     }
                                     ?>
                                 </td>
                             <?php } ?>

                                                 <?php
                                                      $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($value);
                                                      extract($return_data);

                                                    ?>

                                                  <td>
                                                    @if(isset($business_btn_color))

                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif"  class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo isset($business_btn_message) ?  $business_btn_message :  'N/A'; ?></button>

                                                    @endif
                                                   </td>
                                                   <td>
                                                    @if(isset($customer_btn_color))

                                                     <button type="button" class="call-status-button btn <?php  echo $customer_btn_color;?> set_btn_width"><?php echo isset($customer_btn_message) ?  $customer_btn_message : 'N/A'; ?></button>

                                                    @endif
                                                   </td>

                            <td>
                                <?php
                                    //if($value->status==1)
                                    //{ ?>
                                        <!-- <a href="javascript:void(0)" class="call-status-button btn btn-success">Success</a> -->
                                    <?php
                                    //}
                                    //else
                                    //{ ?>
                                       <!--  <a href="javascript:void(0)" class="call-status-button btn btn-danger">Failed</a> -->
                            <?php

                            //} ?>
                              <?php
                                                         if(isset($status_message) && !empty($status_message))
                                                         {
                                                                echo $status_message;
                                                         }
                                                         else
                                                         {
                                                                 echo App\Helpers\GlobalFunctions::returnCallStatus($value);
                                                         }

                                                        ?>
                            </td>
                            <td> @if(!empty($value->expectedCall) && isset($value->campaign_details->coutry_details) && $value->campaign_details->coutry_details->countryPhoneCode==""){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($value->campaign_details,$value->expectedCall) !!} @else {!! date('d/m/Y h:i:s A', strtotime($value->expectedCall))  !!} @endif
                            </td>
                            @can(config('permissions.data.log-detail.name'))
                                <td>
                                    <a class="confirm_view_pop_up" href="javascript:void(0)" data-target="#confirm-view-modal" data-toggle="modal" data-id="{!! Crypt::encrypt($value->campaignEmail) !!}" data-lead-id ="{!! Crypt::encrypt($value->id) !!}" ><i class="fa fa-eye" title="More information" data-original-title="More information" data-toggle="tooltip"></i></a>
                                </td>
                            @endcan
                        </tr>
                        @endforeach
                    @endif
                    @endcan
                </tbody>
            </table>
        </div>
        </div>
        @can(config('permissions.data.log-list.name'))
            {!! $data->appends(['searchKey'=>$searchKey,'statusBox'=>$statusBox,'fromDate'=>$dateFrom,'toDate'=>$dateTo])->links()!!}
        @endcan
<script type="text/javascript">
    $( window ).on("load", function()
    {
        setTimeout(function(){
            $('#call_keywords_multiselect').multiselect('refresh');
            $('#call_keywords_multiselect').multiselect('rebuild');
        }, 200);
        setTimeout(function(){
            $('#call_keywords_multiselect').multiselect('refresh');
            $('#call_keywords_multiselect').multiselect('rebuild');
        }, 500);
    });
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
