    @extends('layouts.customer')
@section('content')
<style>
.multiselect-container>li>a>label>input[type=checkbox]{
    vertical-align: middle;
    margin-bottom: 0px !important;
}
.multiselect-container > li > a > label {
	padding: 3px 20px 3px 5px !important;
}
.btn-group .multiselect{
	height: 45px;
}
</style>

    <div class="row">
        <h2 class="main_title_head">Lead Logs</h2>
    </div>
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <div class="row check">
            @if ($message = Session::get('flash_message'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="col-md-12">
                <div class="heading-campaigns">
                    <div class="row">
                        <div class="col-md-3" style="float:left">
                            <label>Search</label>
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <input type="text" data-url="<?php echo url(request()->route()->getPrefix()).'/logs'?>" class="form-control" id="searchKey" placeholder="Search by Campaign Name, Lead ID">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="float:left">
                            <label>Status</label>
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!!Form::select('statusBox', [''=>'Filter By Status','success'=>'Success','failed'=>'Failed'], null, ['class' => 'form-control filterCheckBox filterSelectBox','id'=>'statusBox'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2" style="float:left">
                            <label>Date From</label>
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_from',null,['class'=>'form-control datepicker', 'id'=>'fromDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2" style="float:left">
                            <label>Date To</label>
                            <div class="input-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    {!! Form::text('date_to',null,['class'=>'form-control datepicker', 'id'=>'toDate','autocomplete'=>'off'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 plans business-multiple-select">
                            <label>Choose Keyword</label>
    						<div class="form-group">
    							{!!Form::select('call_keywords', $BusinessCallKeywords, null, ['multiple'=>'multiple','class' => 'form-control','id'=>'call_keywords_multiselect','data-page'=>'logs'])!!}
                                <input id="call-keywords-hidden" class="" type="hidden" name="call_keywords" value="@if(isset($call_keywords)) {{ $call_keywords }} @endif">
    						</div>
    					</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <a  href="javascript:void(0);" class="btn btn-primary reset_filter">Reset Filter</a>
                            <button class="btn btn-primary" type="button" id="addCallKeywordBtn">Add Tag <i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-6" id="call-keywords-container">
    	            		<div class="bootstrap-tagsinput">
    							<?php
    								if(isset($selectedKeywordsids) && $selectedKeywordsids != '')
    								{
    									$selectedKeywordsidsArr = explode(',',$selectedKeywordsids);
    									foreach ($selectedKeywordsidsArr as $keyid => $valueid)
    									{
    										$keywordName = isset($BusinessCallKeywords[$valueid]) ? $BusinessCallKeywords[$valueid] : '';
    										if($keywordName !='' )
    										{
    											echo '<span data-id="'.$valueid.'" class="callKeywordLabel tag label label-info"><b>'.$keywordName.'</b><span data-id="'.$valueid.'" class="callKeywordsCloseBtn" data-role="remove"></span></span>';
    										}
    									}
    								}
    							?>
    						</div>
    					</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 contact_listing recordsTable">
                @include('customer.logs.lead_data',['data'=>$data])
            </div>
        </div>
    </div>

    <link href="{{ asset('assets/customer/css/bootstrap-multiselect.css') }}" rel="stylesheet">
    @section('js')
    <script type="text/javascript" src="{{ asset('assets/customer/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript">
        var paginationClass = 'contact_listing';
        var route_url       = "<?php echo url(request()->route()->getPrefix())?>";
        var get_data_url    = route_url+'/fetch-lead-log-list';
        var camp            = route_url+'/view-campaign-detail';

        $('#call_keywords_multiselect').multiselect({
           nonSelectedText:'Choose Keyword',
           numberDisplayed: 1
       });

        $('button[class="multiselect dropdown-toggle btn btn-default"]').removeAttr('title');

        jQuery(document).on('click','.confirm_view_pop_up',function(e)
        {
            var promotr = localStorage.getItem("promotr");
            var searchKeywords = $('#call-keywords-hidden').val();
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:camp+'/'+jQuery(this).attr('data-id')+'?lead_id='+jQuery(this).attr('data-lead-id')+'&keywords='+searchKeywords,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    // showLoader();
                },
                success: function (res)
                {
                    hideLoader();
                    $(".view_campaign_data").html(res.data)
                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                     hideLoader();
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        //tostrerror(message);
                         $(".view_campaign_data").html(message)
                    });
                }
            });
        });

        //#statusBox
        $("#statusBox").change(function(){
            $("#searchKey").trigger('keyup');
        });
        $(function() {
            $( ".datepicker" ).datepicker();
        });

        $(document).on('change', '#fromDate', function()
        {
            var date1 = $(this).val();
            $("#toDate").datepicker("option","minDate",date1);
            if($("#toDate").val()!="")
                $("#searchKey").trigger('keyup');
        });

        $(document).on('change', '#toDate', function()
        {
            if($("#fromDate").val()!="")
                $("#searchKey").trigger('keyup');
        });

        $(".reset_filter").click(function()
        {
            $('#call-keywords-hidden').val('');
            $("#call_keywords_multiselect").multiselect("deselectAll", false);
            $('#call_keywords_multiselect').multiselect('refresh');
            $('#call_keywords_multiselect').multiselect('rebuild');
            $('#call-keywords-container').find('.bootstrap-tagsinput').html('');
            if($(".form-control").val(""))
            {
                 $("#searchKey").trigger('keyup');
            }
        });
        $( window ).on("load", function() {
                $(".form-control").val("");
                $('#call-keywords-hidden').val('');
        });
    </script>
    @endsection
    @endsection

 <!-- View Modal -->
    <div id="confirm-view-modal" class="modal fade"  role="dialog" style="display:none">
        <div class="modal-dialog modal-lg" style="max-width:80%;">>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">View  Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body view_campaign_data">

                </div>
            </div>
        </div>
    </div>
    <!-- View Enddds -->
