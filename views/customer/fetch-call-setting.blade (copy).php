<?php
  $testUrl = explode('/',$url);
  array_pop($testUrl);
  $testUrl = implode('/', $testUrl);

?>
      @php
       $currency="";
       $user="";
      if(!empty($businessOwnerId))
      {
      $currency = \App\User::where('id',$businessOwnerId)->first()->region->currency;
      $user = \App\User::with(['user_plan.plans.plan_prices'=>function($q) use($currency) {
        $q->where('currency_id',$currency);
      }])->where('id',$businessOwnerId)->first();
    }
      @endphp
      <?php
      if(!empty($user) && isset($user->user_plan) && !empty($user))
        $currencySymbol = $user->user_plan->plans->plan_prices[0]->plan_currency->currencySymbol;
      else
       $currencySymbol = '';
      ?>
      <link href="{{ asset('assets/customer/css/wickedpicker.min.css') }}" rel="stylesheet">
      <div class="col-md-12">
        <div class="setting-tabs">
          <ul class="nav nav-tabs reporting-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#call-setting">CALL SETTINGS</a>
            </li>

          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <!-- Call Setting Tab -->
            <div id="call-setting" class="tab-pane active
            "><br>
              <div class="update-password">
                <div class="">

                @if(isset($callSetting) && !empty($callSetting))
                  {{ Form::model($callSetting,array('id'=>'call_setting_form','files' => true,'url'=>URL('customer/call-setting'))) }}
                @else
                  {{ Form::open(array('id'=>'call_setting_form','files' => true,'url'=>URL('customer/call-setting'))) }}
                @endif
                    <div class="row">
                      <div class="col-md-12 d-none">
                        <div class="form-group">
                          <label for="call-transfer-hours">Call Transfer Hours</label>
                          {{ Form::textarea('call_transfer_hours',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group call-recording">
                            <label for="call_recording_display">Call Company Contacts</label>
                            </br>
                            {{ Form::radio('call_method',3)}} <span class="yes-text">In Order - We will call contacts in the order they are provided for each Campaign.</span>
                              </br>
                            {{ Form::radio('call_method',4)}} <span class="yes-text">Random Order - We will call the Campaign contacts in random order each time.  Ideal for Sales teams!</span>
                          </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_recording_display">Call Recording Display</label>
                          {{ Form::radio('call_recording_display',1,false,array('class'=>'recDisp'))}} <span class="yes-text">Yes</span>
                          {{ Form::radio('call_recording_display',2,false,array('class'=>'recDisp'))}} <span class="yes-text">No</span>
                        </div>
                      </div>


                          <!-- <div class="col-md-6">
                                <div class="form-group">
                                  <label for="email_subject">Company Name</label>
                                  {{ Form::text('company_name',$company_name,['class'=>'form-control']) }}
                                </div>

                                <input type="submit" id="call_setting_submit" class="btn btn-primary" value="Update">
                          </div>-->




                      <div class="col-md-12 callAnn @if(!empty($callSetting) && isset($callSetting['call_recording_display']) && $callSetting['call_recording_display']!=1) d-none @endif">
                        <div class="form-group">
                          <label for="call_announcement">Call Recording – Announcement This message is played when the customer answers the call.(use this tag {business_name} for business name)</label>
                          {{ Form::textarea('call_announcement',old('message'),['class'=>'form-control col-md-12','cols'=>10,'rows'=>2])}}
                          </br>
                           <label for="call_announcement">Call Recording Announcement – Legal Disclaimer This message must be delivered if the call recording is turned ON.</label>

                           {{ Form::textarea('call_announcement_2',old('message'),['class'=>'form-control col-md-12','cols'=>10,'rows'=>2,'readonly'=>true])}}
                        </div>
                    <p><strong>Note 1:</strong> We recommend adding your company name to the call announcement. For example: "Hello, this is your requested call back from ABC Company. Please wait while I connect you with one of our consultants. This call may be recorded for quality and training purposes." </p>
                    <p class="mb-0"><strong>Note 2:</strong> The legal disclaimer "This call may be recorded for quality and training purposes." cannot be edited or deleted.</p>
                      </div>
                      <div class="col-md-12">
                          <div class="main-welcome-message-radio" style="margin-top:20px">
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="containerOuter">
                                              <div class="containerradio">
                                                  {{ Form::radio('welcome_message_type',1,false,array('class'=>'welcome_message_type_radio hidden','id'=>'input1')) }}
                                                  <label class="entry" for="input1"><div class="circle"></div><div class="entry-label">Use default welcome message</div></label>
                                                  {{ Form::radio('welcome_message_type',2,false,array('class'=>'welcome_message_type_radio hidden','id'=>'input2')) }}
                                                  <label class="entry" for="input2"><div class="circle"></div><div class="entry-label">Use custom call recording</div></label>
                                                  <div class="highlight"></div>
                                                  <div class="overlay"></div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6"></div>
                                  </div>
                                  <div class="row cstm-uplod" @if(!empty($callSetting) && isset($callSetting['welcome_message_type']) && $callSetting['welcome_message_type'] == 1) style="display:none" @endif id="customCallRecordingCantainer" >
                                      <div class="col-md-12">
                                          <?php if( $paymentDone == 'yes' ){
                                              if($fileStatus == '' || $showFileUpload == 1){
                                                  if($totalApprovedAudioFiles == 10){ ?>
                                                  <div class="">You can upload maximum 10 audio files. To upload new file you have to remove from the existing files.</div>
                                                  <br />
                                              <?php } else {?>
                                                  <div class="data">Upload file, .wav file</div>

                                                  <div class="custom-file">
                                                      <i class="fas fa-file-upload"></i>
                                                      <input type="file" name="audiofile" accept=".wav" class="custom-file-input" id="customAudioFile">
                                                      <label class="custom-file-label" for="customFile">Choose file</label>
                                                  </div>
                                                  <span> <strong>Note* </strong>a) The legal disclaimer must be included in your audio recording and cannot be changed or removed from the recording or the recording will not be approved.<br/>
                                                      b) The recording will be submitted for approval and that approval is usually done within 2 business days.
                                                  </span>
                                              <?php }
                                                } else if($fileStatus == 'Approved' || $fileStatus == 'Pending') {?>
                                                  <br/>
                                                  <div class="col-md-2" style="float:left">
                                                      <i class="fa fa-file-audio fa-4x"></i>
                                                  </div>
                                                  <div class="col-md-10">
                                                      <?php if($fileStatus == 'Pending') {?>
                                                          <div class="">File is waiting to be approved.</div>
                                                          <div><button class="btn btn-warning" href="javascript:;" id="welcomeFileRemove" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none;	background-color: #ffa300;	padding: 5px;margin: 5px -1px 0 0px;"><b>Remove</b></button></div>
                                                          <span> <strong>Note* </strong>  : a) You can change the file any time that is waiting for approval. You don't have to pay again.</span>
                                                      <?php }  else if($fileStatus == 'Approved') {?>
                                                          <div class="">This file is approved. If you want to upload new file then you <br>have to pay again after that you can upload new file.</div>
                                                          <div><button class="btn btn-warning" type="button" href="javascript:;" data-id="<?php echo Crypt::encrypt($audioId);?>" id="makeCurrentFileInactive" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none;	background-color: #ffa300;	padding: 5px;margin: 5px -1px 0 0px;"><b>Upload new file</b></button></div>
                                                    <?php } ?>
                                                      <?php if($audiofile != ''){?>
                                                          <br />
                                                          <audio style="width:64%" controls controlsList="nodownload">
                                                              <source id="welcomeAudioSrc" src="<?php echo $testUrl.'/get_welcome_recording?file='.$audiofile.'&bb='.$business_id;?>" type="audio/wav">
                                                          </audio>
                                                     <?php } ?>
                                                  </div>
                                              <?php } else if( $fileStatus == 'Rejected' ){ ?>
                                                  <div class="col-md-2" style="float:left">
                                                      <i class="fa fa-file-audio fa-4x"></i>
                                                  </div>
                                                  <div class="col-md-10">
                                                      <div class="">File Rejected.</div>
                                                      <div><button class="btn btn-warning" href="javascript:;" id="welcomeFileRemove" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none;	background-color: #ffa300;	padding: 5px;margin: 5px -1px 0 0px;"><b>Remove</b></button></div>
                                                      <?php
                                                      if($audiofile != ''){?>
                                                          <br />
                                                          <audio style="width:64%" controls controlsList="nodownload">
                                                              <source id="welcomeAudioSrc" src="<?php echo $testUrl.'/get_welcome_recording?file='.$audiofile.'&bb='.$business_id;?>" type="audio/wav">
                                                          </audio>
                                                      <?php }?>
                                                      <div class="error-upload" style="width:64%">
                                                          <span><b>Reason : </b></span>
                                                          <div class="border-error text-left">
                                                              <p class="mb-0"><?php echo ucfirst($rejectionReason);?></p>
                                                          </div>
                                                      </div>
                                                      <br />

                                                  </div>
                                            <?php  }
                                            }else {?>
                                                <div class="row">
                                                    <div class="col-md-2 text-center">
                                                        <button type="button" class="btn btn-primary" id="audioFilePaymentBtn" >Pay Now</button>
                                                    </div>
                                                    <div class="col-md-2 text-center"><span class="OR"><em>OR</em></span></div>
                                                    <div class="col-md-8">
                                                        <div class="row">

                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                    <input id="callAudioPromoCodeInput" type="text" class="input-lg form-control" placeholder="Have Promo Code"/>
                                                                    <label id="callAudioPromoCode-error" class="has-error" style="display:none;width:92%" for="callAudioPromoCode">This field is required.</label>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                    <button id="callAudioPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <span> <strong>Note* </strong>  : You will be charged $25 to upload a new file. After payment you have only 3 attempts to change the rejected files after that you have to pay again. If your file is approved and you want to upload a new file then you have to pay $25.</span>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                          <div class="text-left mt-2">
                                              <br />
                                              <audio id="welcomeAudioTagHidden" style="width:41%;display:none" controls controlsList="nodownload">
                                                  <source id="welcomeAudioSrcHidden" src="" type="audio/wav">
                                              </audio>
                                          </div>

                                      </div>
                                      <div class="col-md-4"></div>
                                  </div>
                              </div>
                      </div>


                      <div class="col-md-12">
                        <div class="form-group">
                            </br>
                          <label for="welcome_message">Call Welcome Message This will announce the call to your Company contacts.</label>
                          {{ Form::textarea('welcome_message',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="customer_wait_message">Call Instructions for the Company Contact.</label>
                          {{ Form::textarea('customer_wait_message',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                        <div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_announcement_email">Call Summary Email</label>
                          {{ Form::radio('call_announcement_email',1,false,array('class'=>'annEmail')) }} <span class="yes-text">Yes</span>
                          {{ Form::radio('call_announcement_email',2,false,array('class'=>'annEmail')) }} <span class="yes-text">No</span>
                        </div>
                      </div>

                      <div class="col-md-12 emailDiv @if(!empty($callSetting) && isset($callSetting['call_announcement_email']) && $callSetting['call_announcement_email']!=1) d-none @endif">
                        <div class="form-group">
                          <!--<label for="email_subject">Email Subject</label>
                           {{ Form::text('email_subject',null,['class'=>'form-control']) }}
                          -->
                          {{ Form::hidden('email_subject',null,['class'=>'form-control']) }}
                        </div>
                      </div>
                      <div class="col-md-12 emailDiv @if(!empty($callSetting) && isset($callSetting['call_announcement_email']) && $callSetting['call_announcement_email']!=1) d-none @endif">
                        <div class="form-group">
                          <!--<label for="email_body">Email Body</label>  {{ Form::textarea('email_body',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}} -->
                          {{ Form::hidden('email_body',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                      </div>
                      {{ Form::hidden('retry_time',0,['class'=>'form-control','min'=>'1','max'=>'120'])}}
                       {{ Form::hidden('retry_delay_second',0,['class'=>'form-control','min'=>'1','max'=>'60'])}}
                      <!--<div class="col-md-6">
                        <div class="form-group">
                          <label for="retry_time">Retry Time</label>
                              {{ Form::number('retry_time',null,['class'=>'form-control','min'=>'1','max'=>'120'])}}
                        </div>
                      </div>-->
                      <!--<div class="col-md-6">
                        <div class="form-group">
                          <label for="retry_delay_second">Retry Delay Time (in mintues)</label>
                         {{ Form::number('retry_delay_second',null,['class'=>'form-control','min'=>'1','max'=>'60'])}}
                        </div>
                      </div>-->
                          <div class="col-md-12">
                        <div class="form-group call-recording">
                          <label for="call_recording_display">Business Caller ID</label>
                          </br>
                          {{ Form::text('callerID_sc1',null,['class'=>'form-control'])}}
                          </br>
                           <label for="call_recording_display">Customer Caller ID</label>
                          {{ Form::text('callerID_sc2',null,['class'=>'form-control'])}}
                        </div>
                      </div>
                      <?php
                        if(\Auth()->user()->isCustomer() || \Auth()->user()->isEditior() || \Auth()->user()->isAgency() || \Auth()->user()->isBusinessAdmin() )
                        { ?>
                          <div class="col-md-12">
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          <?php } ?>
                    </div>
                    <br />
                    <div class="row">
                        <h2 class="main_title_head">Approved files</h2>
                        <p><strong>Note*  : </strong>You can switch between approved files here at any time. </p>
                    </div>
                    <div class="latest-champaign-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">File</th>
                                        <th scope="col">Approved at</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i=0;
                                        $testUrl = explode('/',$url);
                                        array_pop($testUrl);
                                        $testUrl = implode('/', $testUrl);
                                        if( !empty($approvedAudioFiles))
                                        {
                                            foreach($approvedAudioFiles as $value)
                                            {
                                                $i++;?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>
                                                    <?php if( $value->file != '' ){?>
                                                        <audio controls controlsList="nodownload">
                                                            <source src="<?php echo $testUrl.'/get_welcome_recording?file='.$value->file.'&bb='.$value->business_id;?>" type="audio/wav">
                                                        </audio>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php echo date('d/m/Y H:i:s',strtotime($value->approvedAt));?>
                                                </td>
                                                <td>
                                                    <div class="off-on-button" style="float:left">
                                                        <label class="switch">
                                                            <input data-toggle="tooltip" title="Make this file active" data-toggle="modal" data-target="#confirm-make-audiofile-active-modal" id="{{ Crypt::encrypt($value->id) }}" data-status="{{ $value->isActive }}" type="checkbox"  class="audioFileStatusChange"  checked="checked">
                                                            <span class="slider <?php if($value->isActive == 'yes'){?>fa-toggle-on<?php } else {?>fa-toggle-off<?php }?>">
                                                                <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                                                <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <a id="deleteApprovedAudioFile" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-remove-audiofile-modal" data-file="{{ $value->file }}">
                                                        <span  data-toggle="tooltip" title="Delete" data-placement="bottom">
                                                            <img src="{{asset('assets/customer/img/delete.png')}}" style="margin-top:10px;">
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php }
                                            } else { ?>
                                            <tr>
                                                <td colspan="7" align="center">No files available.</td>
                                            </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                {{ Form::close() }}
                </div>
              </div>
            </div>

            <!-- Credit Recharge Tab -->
            <div id="credit-recharge" class="tab-pane">
              <br>
              @php
              $rechargeArr = array(
              '0'=>'Select Amount',
              '25'=>25,
              '50'=>50,
              '100'=>100,
              '150'=>150,
              '200'=>200,
              '300'=>300,
              '400'=>400,
              '500'=>500,
              '1000'=>1000,
              '2000'=>2000
              );
              foreach($rechargeArr as $k => $v){
              if($k!=0){
              $rArr[$k] = $currencySymbol.' '.$v;
              }else{
              $rArr[$k] = $v;
              }
              }
              @endphp
              <div class="update-password">
                {{ Form::open(array('url'=>URL('recharge-credit'), 'method'=>'POST', 'id'=>'recharge_form')) }}
                <div class="row col-md-4">
                  <div class="form-group">
                    <label for="recharge Amount">Select Recharge Amount
                    </label>
                    {{ Form::select('rechargeAmt',$rArr,null, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="row col-md-2">
                  <div class="form-group">
                    {{ Form::submit('Recharge',['class'=>'btn btn-primary']) }}
                  </div>
                </div>
                {{ Form::close() }}
              </div>
            </div>
              <!-- Credit Recharge Tab -->


            <!-- Change Password Tab -->
            <div id="password-setting" class="tab-pane"><br>
              <div class="update-password">
                {{ Form::open(array('url'=>URL($url.'/change-password'), 'id'=>'change_password')) }}
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="pass">Enter Old Password <span class="star">*</span></label>
                      {!! Form::password('old_pass',['class'=>'form-control']) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="pass">Enter New Password <span class="star">*</span></label>
                      {!! Form::password('new_pass',['class'=>'form-control']) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="pass">Enter Confirm Password <span class="star">*</span></label>
                      {!! Form::password('new_pass2',['class'=>'form-control']) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                  </div>
                </div>
                {{ Form::close() }}
              </div>
            </div>
            <!-- Cards Tab ->
            <div id="cards-setting" class="tab-pane">
              <br>
              <div class="update-password">
                {{ Form::open(array('url'=>URL('customer/save-card'), 'id'=>'save-card')) }}
                <?php $card = false; ?>
                <div class="row">
                  <div class="formArea">
                    @if(!empty($output['data']))
                    <?php foreach ($output['data'] as $key => $value){ ?>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="cc-number" class="control-label">Card Holder Name
                          <span class="star">*
                          </span>
                        </label>
                        <input type="text" class="input-lg form-control" name="cardHolderName" value="{{$value['name']}}" placeholder="Card Holder Name" required="">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="cc-number" class="control-label">Card number
                          <span class="star">*
                          </span>
                        </label>
                        <input id="cc-number" type="tel" class="input-lg form-control cc-number mastercard identified" name="cardNumber" value="{{'XXXX XXXX XXXX ' .$value['last4']}}" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" readonly="">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="cc-exp" class="control-label">Card expiry
                          <span class="star">*
                          </span>
                        </label>
                        <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" name="ccExpiryMonth" autocomplete="cc-exp" value="{{$value['exp_month'].'/'.$value['exp_year']  }}" placeholder="mm / yy" readonly="">
                      </div>
                      {!! Form::hidden('card',$value['id']) !!}
                      <?php $card=true; } ?>
                      @else
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="newCardHolderName" class="control-label">Card Holder Name
                            <span class="star">*
                            </span>
                          </label>
                          <input type="text" class="input-lg form-control" id="newCardHolderName" name="newCardHolderName" placeholder="Card Holder Name" required="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cc-number" class="control-label">Card number
                            <span class="star">*
                            </span>
                          </label>
                          <input id="cc-number" type="tel" class="input-lg form-control cc-number mastercard identified" name="newcardNumber" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cc-exp" class="control-label">Card expiry
                            <span class="star">*
                            </span>
                          </label>
                          <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" name="newccExpiryMonth" autocomplete="cc-exp" placeholder="mm / yy" required="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cc-cvc" class="control-label">Card CVC
                            <span class="star">*
                            </span>
                          </label>
                          <input id="cc-cvc" type="tel" class="input-lg form-control cc-cvc" name="newcvvNumber" autocomplete="off" placeholder="•••" required="">
                        </div>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="btn-main">
                      <a
                         <?php if(!$card) {?> style="display:none"
                      <?php }?> href="javascript:void(0)" class="btn btn-primary deleteRecord">Delete
                      </a>
                    <div class="setting-save">
                      {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                    </div>
                  </div>
                </div>
              </div>
              {{ Form::close() }}
            </div>
          </div>
          <!-- Cards Tab -->
        </div>
      </div>
    </div>
    <script src="{{ asset('assets/customer/js/wickedpicker.js')}}"></script>
     <script type="text/javascript">
            $('.timepicker').wickedpicker({twentyFour: true});
            //timepickericon
            $('.timepickericon').click(function(){

                 $('.timepicker').trigger('click');
            });
            //call_setting_submit
            $("#call_setting_submit").click(function(e){
                $('#call_setting_form').trigger('submit');
            });
            $(document).on("change", "#customAudioFile", function(evt)
            {
                $('#welcomeAudioTagHidden').show();
                var $source = $('#welcomeAudioSrcHidden');
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
            });
        </script>
    <script>
         //Call settings
  $("#call_setting_form").validate({
    errorClass   : "has-error",
    highlight    : function(element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight  : function(element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:{
      call_announcement: {
        required: true,
        noSpace: true,
      },
      call_announcement_2: {
        required: true,
        noSpace: true,
      },
      welcome_message: {
        required: true,
        noSpace: true,
      },
       customer_wait_message: {
        required: true,
        noSpace: true,
      },
       callerID_sc1: {
                required: true,
                noSpace: true,
                minlength: 10,
                maxlength: 11,
                number: true
      },
      callerID_sc2: {
                required: true,
                noSpace: true,
                minlength: 10,
                maxlength: 11,
                number: true
      }
    },
    messages:{
      callerID_sc1:{
        'minlength':'Please enter 10 digit business caller id',
        'maxlength':'Please enter 11 digit business caller id'
      },
      callerID_sc2:{
        'minlength':'Please enter 10 digit customer caller id',
        'maxlength':'Please enter 11 digit customer caller id'
      }

    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });


    </script>
