<style>
.call-status-button {
  height: 25px !important;
  width: 50px !important;
}
.btn-warning {
  color: #212529;
  background-color: #f89406;
  border-color: #f89406;
}
.multiselect-container>li>a>label>input[type=checkbox]{
    vertical-align: middle;
    margin-bottom: 0px !important;
}
.multiselect-container > li > a > label {
  padding: 3px 20px 3px 5px !important;
}
.btn-group .multiselect{
  height: 45px;
}
</style>
<?php
$callsettings  = \App\Model\CallSetting::select('*')->where('business_id',$business_id);
if( $callsettings->count() > 0 )
{
    $callsettings = $callsettings->first();
    $recDisplay = $callsettings->call_recording_display;
}
else
{
    $recDisplay = '';
}
?>
      {{ Form::open(array('url'=>$url.'/reporting','method'=>'GET','id'=>'lead_form')) }}
        <input id="tab-input" type="hidden" name="tab" value="@if(isset($tab)) {{$tab}} @endif">
        <input id="filter-input" type="hidden" name="filter" value="all">
        <input id="filter-input" class="campaign" type="hidden" name="campaign" value="@if(isset($currentCamp) && $currentCamp!='') {{ base64_encode($currentCamp) }} @else {{ base64_encode(0) }} @endif">
        <input id="search-key-filter" class="" type="hidden" name="searchkey" value="@if(isset($searchkey)) {{ $searchkey }} @endif">
        <input id="call-keywords-hidden" class="" type="hidden" name="call_keywords" value="@if(isset($call_keywords)) {{ $call_keywords }} @endif">
        <?php
        if( !$isBusinessOwner )
        {
               if( $campaignpermissiontype == 4 )
               {
                        if( !empty($campIds))
                        {
                           $cmp = $cp = App\Model\Campaign::where('business_id',$business_id)->whereIn('id',$campIds);
                        }
                        else
                        {
                           $cmp = $cp = App\Model\Campaign::where('business_id',$business_id)->where('id',0);
                        }
               }
                else if( $campaignpermissiontype == 3 )
               {
                         $cmp = $cp = App\Model\Campaign::where('business_id',$business_id)->where('id',0);
               }
                else if( $campaignpermissiontype == 1 || $campaignpermissiontype == 2 )
               {
                         $cmp = $cp = App\Model\Campaign::where('business_id',$business_id);
               }
               else
               {
                        $cmp  = $cp = App\Model\Campaign::where('business_id',$business_id)->where('id',0);
               }
        }
        else
        {
           $cmp = $cp = App\Model\Campaign::where('business_id',$business_id);
        }
        ?>
        @php
                $total_leads          = 0;
                $cmpCount           = 0;
                $call_leads           = 0;
                $convert_lead         = 0;
                $outsideavailablehours_leads  = 0;
                $Triggered_as_Lead_Concierge_Leads=0;
                $Out_Of_Hours=0;
				$dateFromV = '';
				$dateToV = '';
                $campaignEmailID="";
                $cmpCount=0;
                if(!empty($dateFrom))
                    {
                        $dateFromV=explode("-",$dateFrom);
                        if(is_array($dateFromV) && isset($dateFromV[0]) && isset($dateFromV[1]) && isset($dateFromV[2]))
                        {

                           $dateFromV=$dateFrom=$dateFromV[2].'-'.$dateFromV[0].'-'.$dateFromV[1];

                        }

                    }
                    if(!empty($dateTo))
                    {
                        $dateToV=explode("-",$dateTo);
              if(is_array($dateToV) && isset($dateToV[0]) && isset($dateToV[1]) && isset($dateToV[2]))
                        {

                            $dateToV=$dateTo=$dateToV[2].'-'.$dateToV[0].'-'.$dateToV[1];
                        }
                    }
                if($cmp->count()!=0)
				 {
	                    $total_leads        = $parse = App\Model\ParseEmail::whereIn('campaignEmail',$cp->pluck('email'));
						$outsideavailablehours_leads = App\Model\ParseEmail::whereIn('campaignEmail',$cp->pluck('email'))->whereIn('outside_available_hours',[2,3,4,10,11,12]);
	                    $Triggered_as_Lead_Concierge_Leads_Query=App\Model\ParseEmail::whereIn('campaignEmail',$cp->pluck('email'))->whereIn('outside_available_hours',[10]);
	                    $call_leads          = App\Model\ApiLeads::whereIn('lead_id',$parse->pluck('lead_id'))->where('sc1','success');
                      $call_leads_data          = App\Model\ApiLeads::whereIn('lead_id',$parse->pluck('lead_id'));
	                    if(!empty($dateFrom))
	                    {

	                           //$cmp->whereDate('created_at','>=',$dateFrom);
	                           $cmp->whereDate('updated_at','>=',$dateFrom);
	                           $total_leads->whereDate('expectedCall','>=',$dateFrom);
	                           $outsideavailablehours_leads->whereDate('expectedCall','>=',$dateFrom);
	                           $call_leads->whereDate('startdate','>=',$dateFrom);
	                           $call_leads_data->whereDate('startdate','>=',$dateFrom);
	                           $Triggered_as_Lead_Concierge_Leads_Query->whereDate('expectedCall','>=',$dateFrom);
	                    }
	                    if(!empty($dateTo))
	                    {

	                            //$cmp->whereDate('created_at','<=',$dateTo);
	                            $cmp->whereDate('updated_at','<=',$dateTo);
	                            $total_leads->whereDate('expectedCall','<=',$dateTo);
	                            $Triggered_as_Lead_Concierge_Leads_Query->whereDate('expectedCall','<=',$dateTo);
	                            $outsideavailablehours_leads->whereDate('expectedCall','<=',$dateTo);
	                            $call_leads->whereDate('startdate','<=',$dateTo);
	                            $call_leads_data->whereDate('startdate','<=',$dateTo);

	                    }
	                    //$call_leads->whereHas('parse_emails',function($p)use($dateFrom,$dateTo){
			            // if(!empty($dateFrom))
			            //{
			                 // $dtf    = explode("-", $dateFrom);
			                  //$date1  = $dtf[2]."-".$dtf[0]."-".$dtf[1];
			                  //$l->whereDate('created_at','>=',$date1);
			                  // $p->whereDate('expectedCall','>=',$date1);
			             //}
			             //if(!empty($dateTo))
			             // {
			                 // $dtt      = explode("-", $dateTo);
			                  //$date2    = $dtt[2]."-".$dtt[0]."-".$dtt[1];
			                  //$l->whereDate('created_at','<=',$date2);
			                  // $p->whereDate('expectedCall','<=',$date2);
			             // }

			             //});
	                     if($total_leads->count()!=0)
	                     {
	                    	 $campaignEmail=$total_leads->pluck('campaignEmail');
	                    	 $cmpCount=App\Model\Campaign::whereIn('email',$campaignEmail)->count();
	                    	 //$campaignEmailID=App\Model\Campaign::whereIn('email',$campaignEmail)->get();
	                     }
	                     if($call_leads_data->count()!=0)
	                     {
	                    	 $campaignEmail=$call_leads_data->pluck('campaign_id');
	                    	 //$cmpCount=App\Model\Campaign::whereIn('id',$campaignEmail)->count();
	                    	 $campaignEmailID=App\Model\Campaign::whereIn('id',$campaignEmail)->get();
	                     }
	                    $total_leads         = $total_leads->count();
	                    $outsideavailablehours_leads = $Out_Of_Hours=$outsideavailablehours_leads->count();
	                    $Triggered_as_Lead_Concierge_Leads=$Triggered_as_Lead_Concierge_Leads_Query->count();
	                    $call_leads= $call_leads->count();
	                    $leadsInvalidCount=$leadsInvalidCount;
	                    //if($leadsInvalid->count()!=0)
	                    //{
	                     //$leadsInvalidCount=$leadsInvalid->count();
	                    //}

	                    if(!empty($call_leads) && $total_leads>0)
	                    	$convert_lead=($call_leads*100)/$total_leads;

						if($outsideavailablehours_leads > 0 && $total_leads > 0)
	                        $outsideavailablehours_leads = ($outsideavailablehours_leads/$total_leads) *100 ;
	                    //if($outsideavailablehours_leads > 0 && $Triggered_as_Lead_Concierge_Leads > 0)
	                        //$outsideavailablehours_leads = ($Triggered_as_Lead_Concierge_Leads/$outsideavailablehours_leads) *100 ;
                }
                $comsumed_min = 0;
                $avg      = 0;
                @endphp
                   @if($campaignEmailID != "")
				    	@forelse($campaignEmailID as $camps)
						@php
						    $totalLeadQuery	= App\Model\ApiLeads::where('campaign_id',$camps->id);
						    if(!empty($dateFrom))
					        {

                              $totalLeadQuery->whereDate('startdate','>=',$dateFrom);
					        }
				            if(!empty($dateTo))
				            {

				                $totalLeadQuery->whereDate('startdate','<=',$dateTo);
				            }
                            $totalCredit	= $totalLeadQuery->sum('credit_amount');
                            $comsumed_min+= $totalCredit;
		                @endphp
		            @empty
		            @endforelse
		            @endif
		            @php
		          if(!empty($comsumed_min) && $total_leads>0)
                  	$avg = $comsumed_min/$total_leads;
                  @endphp
                  <?php
                  //$total_leads = $leadsInvalid->count()+$call_leads;
                  ?>
                  <div class="row reports-tabs">
            <div class="col-md-3">
              <label>Date From</label>
              <div class="input-group">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                  </div>
                  {!! Form::text('date_from',$dateFromData,['class'=>'form-control datepicker', 'id'=>'date1'])!!}
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <label>Date To</label>
              <div class="input-group">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
                  </div>
                  {!! Form::text('date_to',$dateToData,['class'=>'form-control datepicker', 'id'=>'date2'])!!}
                </div>
              </div>
            </div>
            <div class="col-md-6">
      <div class="submit-class">
              {!! Form::submit('Submit',['class'=>'btn btn-primary'])!!}
              <a href="{{ $url.'/reporting' }}" class="btn btn-default">Reset</a>
            </div>
            <div class="col-md-6">
            </div>
      </div>
      </div>
          <div class="main_boxs">
            <div class="row">
                <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $cmpCount !!}</h1>
                          <span>Campaigns</span>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $total_leads !!}</h1>
                          <span>Leads</span>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $call_leads !!}</h1>
                          <span>Calls</span>
                      </div>
                  </div>
                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>@if($convert_lead==100) {!! $convert_lead.'%' !!} @else{!! number_format($convert_lead,2).'%' !!}@endif</h1>
                          <span>Leads to Call </span>
                      </div>
                    </div>
                    <div class="col-md-3">
                            <div class="box_details">
                                <h1>
                                <?php
                                      $min=0;
                                      if(!empty($avg))
                                      {
                                            $avg_min=number_format($avg,2);
                                            $avg_min_data=explode(".",$avg_min);
                                            if(isset($avg_min_data[0]) && !empty($avg_min_data[0]))
                                             {
                                              $min=$avg_min_data[0].' Min ';
                                             }
                                             if(isset($avg_min_data[1]) && !empty($avg_min_data[1]) && $avg_min_data[1] > 0)
                                              {
                                               $min.=$avg_min_data[1].' Sec';
                                             }
                                      }
                                      echo $min;
                                ?>
                                </h1>
                                  <span>Avg call duration</span>
                              </div>
                  </div>

                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $Out_Of_Hours !!}</h1>
                          <span>Out Of Hours</span>
                      </div>
                  </div>

                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $Triggered_as_Lead_Concierge_Leads !!}</h1>
                          <span>Triggered as Lead Concierge Leads</span>
                      </div>
                  </div>



                  <div class="col-md-3">
                    <div class="box_details">
                        <h1>{!! $leadsInvalidCount !!}</h1>
                          <span>Unsuccessful Triggers</span>
                      </div>
                  </div>

                  <!--<div class="col-md-3">
                    <div class="box_details">
                        <h1>{!!  $comsumed_min !!}</h1>
                          <span>Minutes Consumed</span>
                      </div>
                  </div>-->


              </div>

          </div>
          <h6>{!! number_format($outsideavailablehours_leads,2) !!}% of your leads were received outside of business hours.</h6>

      {{ Form::close() }}
      <!-- Nav tabs -->
      <ul class="nav nav-tabs reporting-tabs">
        <li class="nav-item">
          <a class="leadTabs nav-link @if(isset($tab)) @if($tab=='min_consumed') active show @endif @else active @endif" data-toggle="tab" href="#min_consumed">CAMPAIGN REPORT</a>
        </li>

        <li class="nav-item">
          <a class="leadSummeryTabs leadTabs nav-link @if(isset($tab) && $tab=='lead_summary') active show @endif" data-id="lead_summery" data-toggle="tab" href="#lead_summary">LEADS SUMMARY</a>
        </li>
         <li class="nav-item">
          <a class="leadOutSideSummery leadTabs nav-link @if(isset($tab) && $tab=='lead_out_side_hour') active show @endif" data-id="lead_out_side_hour" data-toggle="tab" href="#lead_out_side_hour">OUT OF HOURS LEADS</a>
        </li>
        <li class="nav-item">
          <a class="leadSummeryInvalid leadTabs nav-link @if(isset($tab) && $tab=='lead_invalid') active show @endif" data-id="lead_invalid" data-toggle="tab" href="#lead_invalid">LEADS INVALID</a>
        </li>


      </ul>

      <!-- Tab panes -->
  <div class="tab-content">
    <div id="min_consumed" class="tab-pane tab_data_set @if(isset($tab)) @if($tab=='min_consumed') active @endif @else active @endif">
      <div class="leadRecord">
        <div class="row">
          <?php //$userPlan = App\Model\UserPlan::where('business_id',$business_id)->first(); ?>
          <!--<div class="col-md-6">
            @if(!empty($userPlan) && isset($userPlan->plans->minutes_per_month) && $userPlan->plans->minutes_per_month==3)
              <h6 align="left"> <b>Total Minutes Consumed: {{$consumedMin}}</b></h6>
            @endif
          </div-->
          <?php $i=1; ?>
        </div>

        <div class="clearfix"></div>

        <div class="latest-champaign-table">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th scope="col">Campaign Name</th>
                <th scope="col">Credits Consumed</th>
                <th scope="col">No. of Leads</th>
                <th scope="col">Action</th>
              </tr>
              <?php
                   $i=1;
                   $k1=0;

              ?>
					@if($campaignEmailID != "")
				  	@forelse($campaignEmailID as $camps)
								@php
									$total_leads = 0;
									if(true){
										   $totalLeadQuery= App\Model\ApiLeads::where('campaign_id',$camps->id);
                                           if(!empty($dateFrom))
        							        {

                                              $totalLeadQuery->whereDate('startdate','>=',$dateFrom);
        							        }
    							            if(!empty($dateTo))
    							            {

    							                $totalLeadQuery->whereDate('startdate','<=',$dateTo);
    							            }
    								  		$total_leads =  $totalLeadQuery->count();
                                            $totalCredit=$totalLeadQuery->sum('credit_amount');



                                              $totalLeadQuery->whereDate('startdate','>=',$dateFrom);
                              }
                              if(!empty($dateTo))
                              {

                                  $totalLeadQuery->whereDate('startdate','<=',$dateTo);
                              }
                          $total_leads =  $totalLeadQuery->count();
                                            $totalCredit=$totalLeadQuery->sum('credit_amount');



								@endphp
								@if($total_leads)
									@php
										$k1=1;
									@endphp
									<tr>
										<td scope="col">{{ $camps->title }}</td>
										<td scope="col">{{ $totalCredit }}</td>
										<td scope="col">
											<?php if(isset($total_leads)){ echo $total_leads;} else { echo 0; } ?>
										</td>
										<td scope="col">
											<a href="{{$url.'/campaign-leads'}}/{{ Crypt::encrypt(isset($camps->id ) ? $camps->id : 0) }}"><i class="fa fa-list" data-toggle="tooltip" data-placement="top" title="Leads"></i></a>
										</td>
									</tr>
								@else
									@php
										$k1=0;
									@endphp
								@endif
								<?php $i++; ?>
								@empty
								<tr>
									<td colspan="5">No Reports Available.</td>
								</tr>
								@endforelse
								@if($k1==0)
								@endif
								@else
								<tr>
									<td scope="col" colspan="5">No Reports Available.</td>
								</tr>
								@endif
						</table>
					</div>
				</div>
			</div>
		</div>

        <!-- Leads Summary -->
  <div id="lead_summary" class="tab-pane tab_data_set @if(isset($tab) && $tab=='lead_summary') active @endif">
    <div class="recordsTable leadRecord">
      <div class="row">
        <div class="form-group md-form text-right col-md-3 float-left">
          <select class="form-control" id="campID">
            @foreach($campList as $k=>$v)
            <option value="{{ base64_encode($k) }}" @if($currentCamp==$k) selected @endif>{{ $v }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group text-right col-md-3 ">
          <input type="text" class="form-control" name="searchkey" value="<?php echo $searchkey;?>" placeholder="Search by answered by, contact no" id="searchKeyInput" />
        </div>
        <div class="col-md-2 plans business-multiple-select">
            <div class="form-group">
              {!!Form::select('call_keywords', $BusinessCallKeywords, null, ['multiple'=>'multiple','class' => 'form-control','id'=>'call_keywords_multiselect','data-page'=>'reports'])!!}
            </div>
          </div>
          <div class="form-group col-md-4">
                <form action="{{$url.'/fetch-report-data'}}" id="export-lead-csv-form" method="get" target="_blank">
                  <input id="tab-input" type="hidden" name="tab" value="">
                  <input id="filter-input" type="hidden" name="filter" value="">
                  <input id="filter-input" type="hidden" name="campaign" value="">
                  <input id="search-key-filter" type="hidden" name="searchkey" value="">
                  <input id="date1" name="date_from" type="hidden" value="">
                  <input id="date2" name="date_to" type="hidden" value="">
                  <input id="date2" name="download" type="hidden" value="csv">
            <input id="call-keywords-hidden1" class="" type="hidden" name="call_keywords" value="@if(isset($call_keywords)) {{ $call_keywords }} @endif">
                </form>
            <button class="btn btn-primary" type="button" id="addCallKeywordBtn">Add Tag <i class="fa fa-plus"></i></button>
                <button class="btn btn-primary export-lead-csv" type="button">Download CSV</button>
          </div>

        </div>
        <div class="row">
          <div class="col-md-12" id="call-keywords-container">
                  <div class="bootstrap-tagsinput">
              <?php
                if($selectedKeywordsids != '')
                {
                  $selectedKeywordsidsArr = explode(',',$selectedKeywordsids);
                  foreach ($selectedKeywordsidsArr as $keyid => $valueid)
                  {
                    $keywordName = isset($BusinessCallKeywords[$valueid]) ? $BusinessCallKeywords[$valueid] : '';
                    if($keywordName !='' )
                    {
                      echo '<span data-id="'.$valueid.'" class="callKeywordLabel tag label label-info"><b>'.$keywordName.'</b><span data-id="'.$valueid.'" class="callKeywordsCloseBtn" data-role="remove"></span></span>';
                    }
                  }
                }
              ?>
            </div>
          </div>
        </div>
            <div class="table-responsive recordsTable latest-champaign-table">
                <div class="table">
                    <table class="table" width="100%">
                        <thead>
                            <tr>
                                <!-- <th scope="col" width="15%">Date</th> -->
                                <th scope="col" width="5%">Lead #</th>
                                <th scope="col" width="15%">Campaign Name</th>
                                <th scope="col" width="15%">Answered By</th>
                                <th scope="col" width="15%">Contact No</th>
                                <th scope="col" width="15%">Credits</th>
                                       <th scope="col" width="10%">Original Lead Time</th>
                                <th scope="col" width="10%">Call Triggered Time</th>
                <?php if($recDisplay==1){?>
                                  <th scope="col" width="15%">Tags</th>
                <?php } ?>
                                <th scope="col" width="15%">Business</th>
                                <th scope="col" width="10%">Customer</th>

                                <th scope="col" width="10%">Status</th>
                                <th scope="col" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @if(isset($leads))
                                @forelse($leads as $lead)
                                    @if(!empty($lead->parse_emails))
                                    <tr>
                                        <!-- <td scope="col">{!!  App\Helpers\GlobalFunctions::getTimeZoneDateTime($lead->campaign_details,$lead->startdate);  !!}</td> -->
                                        <td scope="col">{{ $lead->lead_id }}</td>
                                        <td scope="col">{{ $lead->campaign_details->title }}</td>
                                        <td scope="col">{{ $lead->answered_by }}</td>
                                        <td scope="col">{{ $lead->lead_contact }}</td>
                                        <td scope="col">{{ $lead->credit_amount }}</td>
										<?php
											$return_data=App\Helpers\GlobalFunctions::returnCallBusiness($lead->parse_emails);
											extract($return_data);
											?>
											<td scope="col">

											@if(!empty($lead->parse_emails->expectedCall) && isset($lead->parse_emails->campaign_details->coutry_details) && $lead->parse_emails->campaign_details->coutry_details->countryPhoneCode==""){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($lead->parse_emails->campaign_details,$lead->parse_emails->expectedCall) !!} @else {!! date('d/m/Y h:i:s A', strtotime($lead->parse_emails->expectedCall))  !!} @endif


											</td>

											<td scope="col">
											@if(isset($lead->parse_emails->triggerAt)){!!  date('d/m/Y h:i:s A',strtotime($lead->parse_emails->triggerAt)) !!}@endif</td>
										<?php if($recDisplay==1){?>
											<td scope="col">
												<?php
													if($lead->call_recording_display == 1)
													{
														$matchedKeywords = App\Helpers\GlobalFunctions::findKeywordsInTranscription($lead->call_transcription, $call_keywords );
														if(!empty($matchedKeywords))
														{
															foreach ($matchedKeywords as $mkey => $mvalue) {
																if($mkey > 0 )
																	echo ', '.$mvalue;
																else
																	echo $mvalue;
															}
														}
													}
												?>
											</td>
										<?php } ?>


                                                  <td scope="col">
                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo $business_btn_message; ?></button>
                                                   </td>
                                                   <td scope="col">

                                                     <button type="button" class="call-status-button btn <?php echo  $customer_btn_color;?> set_btn_width"><?php echo $customer_btn_message; ?></button>
                                                   </td>


                                                   <td>
                                                          <?php
                                                         if(!empty($status_message))
                                                         {
                                                              echo $status_message;
                                                         }
                                                         else
                                                         {
                                                                 echo App\Helpers\GlobalFunctions::returnCallStatus($lead->parse_emails);
                                                         }

                                                        ?>


                                                   </td>
                                        <?php /*
                                        <td scope="col">
                                            <?php if($lead->sc1 == 'success' ){ ?><button type="button" class="call-status-button btn btn-success">Connected!</button><?php } else if($lead->sc1 == 'calling' ){?> <button type="button" class="call-status-button btn btn-warning"></button><?php } else {?> <button type="button" class="call-status-button btn btn-danger">Not Connected</button><?php } ?>
                                        </td>




                                        <td scope="col">
                                            <?php if($lead->sc2 == 'success' ){ ?><button type="button" class="call-status-button btn btn-success">Connected!</button><?php } else if($lead->sc2 == 'calling' ){?> <button type="button" class="call-status-button btn btn-warning"></button><?php } else {?> <button type="button" class="call-status-button btn btn-danger">Not Connected</button><?php } ?>
                                        </td>
                                        */
                                        ?>
                                        <td><a class="viewReportLeadDetail" id="{{Crypt::encrypt($lead->id)}}" href="javascript:void(0)"><span data-toggle="tooltip" title="View Detail" data-placement="bottom" ><img src="{{asset('assets/customer/img/view.png')}}"></span></a></td>
                                    </tr>
                                    @endif
                                <?php $i++; ?>
                                @empty
                                @endforelse
                            @else
                                <tr>
                                    <td scope="col" colspan="5">No Leads Available.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            @if(isset($leads))
              {!! $leads->appends(['pagination'=>'true','business_id'=>$business_id,'tab'=>$tab,'filter'=>'all','campaign'=>$currentCamp,'searchKey'=>$searchkey,'call_keywords'=>$call_keywords,'date_from'=>$dateFromData,'date_to'=>$dateToData])->links()!!}

            @endif
            </div>
      </div>
    </div>



  <!-- Leads invalid -->
  <div id="lead_invalid" class="tab_data_set tab-pane @if(isset($tab) && $tab=='lead_invalid') active @endif">
    <div class="recordsTable leadRecord">
      <div class="table-responsive recordsTable latest-champaign-table">
        <div class="table">
          <table class="table" width="100%">
            <thead>
              <tr>
                <th scope="col" width="15%">Campaign</th>
                <th scope="col" width="15%">Name</th>
                <th scope="col" width="10%">Phone</th>
                <th scope="col" width="25%">Message</th>
                <th scope="col" width="10%">Time</th>
                <th scope="col" width="10%">Day of week</th>
                <th scope="col" width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
                @if(isset($leadsInvalid) && !empty($leadsInvalid[0]))
                  @forelse($leadsInvalid as $lead)
                    <tr>
                      <td scope="col">@if(isset($lead->campaign_details)){{ ucfirst($lead->campaign_details->title) }}@endif</td>
                                <td scope="col">@if(isset($lead->leadName)){{ ucfirst($lead->leadName) }}@endif</td>
                                <td scope="col">@if(isset($lead->mobileNo)){{ $lead->mobileNo }}@endif</td>
                                <td scope="col">
                      <?php
                        if(!empty($lead->api_response))
                        {
                          $api_response = json_decode($lead->api_response);
                          if(!empty($api_response) && isset($api_response->content))
                          {
                            if( $lead->status == 1 )
                            {
                              if( isset($api_response->content->message))
                              {
                                echo $api_response->content->message;
                              }
                              else
                              {
                                echo 'N/A';
                              }
                            }
                            else
                            {
                              if(isset($api_response->content->error))
                              {
                                echo $api_response->content->error;
                              }
                              else if(isset($api_response->content->error_message))
                              {
                                echo $api_response->content->error_message;
                              }
                              else if(isset($api_response->content->message))
                              {
                                echo $api_response->content->message;
                              }
                              else
                              {
                                echo 'N/A';
                              }
                            }
                          }
                          else
                          {
                            echo "N/A";
                          }
                        }
                        else
                        {
                          echo "N/A";
                        }
                      ?>
                      </td>
                                <td scope="col">
                                    @if(!empty($lead->expectedCall) && isset($lead->campaign_details->coutry_details) && $lead->campaign_details->coutry_details->countryPhoneCode==""){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($lead->campaign_details,$lead->expectedCall) !!} @else {!! date('d/m/Y h:i:s A', strtotime($lead->expectedCall))  !!} @endif
                                </td>
                                <td scope="col">@if(isset($lead->created_at)){{ date('l', strtotime($lead->created_at)) }}@endif</td>
                      <td><a class="viewInvalidLeadDetail" id="{{Crypt::encrypt($lead->id)}}" href="javascript:void(0)"><span data-toggle="tooltip" title="View Detail" data-placement="bottom" ><img src="{{asset('assets/customer/img/view.png')}}"></span></a></td>
                    </tr>
                @empty
                  @endforelse
                @else
                  <tr>
                    <td scope="col" colspan="7">No Leads Available.</td>
                  </tr>
                @endif

            </tbody>
          </table>
        </div>
        @if(isset($leadsInvalid) && $leadsInvalid->count()!=0)
        {!! $leadsInvalid->appends(['business_id'=>$business_id,'tab'=>$tab,'filter'=>'all','campaign'=>$currentCamp,'searchKey'=>$searchkey,'date_from'=>$dateFromData,'date_to'=>$dateToData])->links()!!}
        @endif
      </div>
    </div>
  </div>

    <!-- <br>
    <br>
    <h6>Leads Outside Available Hours</h6> -->
   <!-- Leads invalid -->
  <div id="lead_out_side_hour" class="tab_data_set tab-pane @if(isset($tab) && $tab=='lead_out_side_hour') active @endif">
    <!--   <h6>Leads Outside Available Hours</h6> -->
    <div class="table-responsive recordsTable latest-champaign-table">
      <div class="table">
        <table class="table" width="100%">
          <thead>
            <tr>
              <!--<th scope="col">Sr.</th>-->
              <th scope="col" width="15%">Campaign</th>
              <th scope="col" width="10%">Name</th>
              <th scope="col" width="10%">Phone</th>
             <!--  <th scope="col" width="10%">Email</th> -->
              <th scope="col" width="7%">Original Lead Time</th>
              <th scope="col" width="7%">Call Triggered Time</th>
              <th scope="col" width="15%">Day of week</th>
              <th scope="col" width="10%">Triggered ?</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; ?>
            @if(isset($outsideleadsData))
                @forelse($outsideleadsData as $outsidelead)
                <tr>
                  <td scope="col">@if(isset($outsidelead->campaign_details)){{ ucfirst($outsidelead->campaign_details->title) }}@endif</td>
                  <td scope="col">@if(isset($outsidelead->leadName)){{ ucfirst($outsidelead->leadName) }}@endif</td>
                  <td scope="col">@if(isset($outsidelead->mobileNo)){{ $outsidelead->mobileNo }}@endif</td>
                  <!-- <td scope="col"> -->
                        <?php
                          //if(isset($outsidelead->original_lead))
                           //echo 'N/A';
                           //echo $parserOutput = Helper::getLeadEmail($outsidelead->original_lead, 1);
                        ?>
                 <!--  </td> -->
                   <td scope="col">

                            @if(!empty($outsidelead->expectedCall) && isset($outsidelead->campaign_details->coutry_details) && $outsidelead->campaign_details->coutry_details->countryPhoneCode==""){!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($outsidelead->campaign_details,$outsidelead->expectedCall) !!} @else {!! date('d/m/Y h:i:s A', strtotime($outsidelead->expectedCall))  !!} @endif


                     </td>

                      <td scope="col">
                      @if(isset($outsidelead->triggerAt)){!!  date('d/m/Y h:i:s A',strtotime($outsidelead->triggerAt)) !!} @endif</td>


                  <td scope="col">
                      @if(isset($outsidelead->expectedCall)){{ date('l', strtotime($outsidelead->expectedCall)) }}@endif</td>

                      <td scope="col">
                        @if($outsidelead->outside_available_hours==3)
                        Dropped
                        @else
                        @if(isset($outsidelead->campaign_details) && $outsidelead->campaign_details->triggerOutsideHoursLeads=='yes' && !empty($outsidelead->triggerAt))
                         Yes
                          @else
                          No
                         @endif
                        @endif

                      </td>
                </tr>
                <?php $i++; ?>
                @empty
                @endforelse
              @else
              <tr>
                <td scope="col" colspan="6">No Leads Available.</td>
              </tr>
              @endif
          </tbody>
        </table>
      </div>
      @if(isset($outsideleadsData) && $outsideleadsData->count()!=0)
        {!! $outsideleadsData->appends(['business_id'=>$business_id,'tab'=>$tab,'filter'=>'all','campaign'=>$currentCamp,'searchKey'=>$searchkey,'date_from'=>$dateFromData,'date_to'=>$dateToData])->links()!!}
        @endif
    </div>

  </div>
  </div>
      <script type="text/javascript">
      $(function() {
      $('#call_keywords_multiselect').multiselect({
       nonSelectedText:'Choose Keyword',
       numberDisplayed: 1
     });
     var selectedKeywordsids = '<?php echo $selectedKeywordsids;?>';
     setTimeout(function(){
       var selectedKeywordsidsArr = selectedKeywordsids.split(',');
       $( selectedKeywordsidsArr ).each(function( index, element ) {
         $('#call_keywords_multiselect option[value="'+element+'"]').attr('selected',true);
       });
       $('#call_keywords_multiselect').multiselect('refresh');
       $('#call_keywords_multiselect').multiselect('rebuild');
     }, 500);
      $('button[class="multiselect dropdown-toggle btn btn-default"]').removeAttr('title');

          $( ".datepicker" ).datepicker();
          if(reporting_tabs_li=='lead_summery')
          {
               $(document).find(".leadTabs").removeClass('active');
               $(document).find(".tab_data_set").removeClass('active');
               $(document).find("#lead_summary").addClass('active');
               $(document).find(".leadSummeryTabs").addClass('active');
          }

           if(reporting_tabs_li=='lead_invalid')
          {
               $(document).find(".leadTabs").removeClass('active');
               $(document).find(".tab_data_set").removeClass('active');
               $(document).find("#lead_invalid").addClass('active');
               $(document).find(".leadSummeryInvalid").addClass('active');
          }
             if(reporting_tabs_li=='lead_out_side_hour')
          {
               $(document).find(".leadTabs").removeClass('active');
               $(document).find(".tab_data_set").removeClass('active');
               $(document).find("#lead_out_side_hour").addClass('active');
               $(document).find(".leadOutSideSummery").addClass('active');
          }





      });

      $(document).on('click','.leadTabs',function(){
             reporting_tabs_li=$(this).data('id');
      });
      $(document).on('change', '#date1', function(){
          var date1 = $(this).val();
          $("#date2").datepicker("option","minDate",date1);
      });
      $(document).on('click','.calIcon',function(){
  $(this).parent().next().trigger('click').datepicker('show');
});
      $(document).on('click','.export-lead-csv',function(){
        var a= $("form#lead_form");
        var b= $("form#export-lead-csv-form");
        var tab = a.find('input[name="tab"]').val();
        var filter = a.find('input[name="filter"]').val();
        var campaign = a.find('input[name="campaign"]').val();
        var searchkey = a.find('input[name="searchkey"]').val();
        var date_from = a.find('input[name="date_from"]').val();
        var date_to = a.find('input[name="date_to"]').val();
        var call_keywords = a.find('input[name="call_keywords"]').val();

        b.find('input[name="tab"]').val(tab);
        b.find('input[name="filter"]').val(filter);
        b.find('input[name="campaign"]').val(campaign);
        b.find('input[name="searchkey"]').val(searchkey);
        b.find('input[name="date_from"]').val(date_from);
        b.find('input[name="date_to"]').val(date_to);
        b.find('input[name="call_keywords"]').val(call_keywords);

        $(b).submit();
      })


    //   $("#lead_form").validate({
    //       errorClass   : "has-error",
    //       highlight    : function(element, errorClass) {
    //         $(element).parents('.form-group').addClass(errorClass);
    //       },
    //       unhighlight  : function(element, errorClass, validClass) {
    //         $(element).parents('.form-group').removeClass(errorClass);
    //       },
    //       rules:
    //       {
    //         date_from:
    //         {
    //           required: true
    //         },
    //         date_to:
    //         {
    //           required: true
    //         }
    //       },
    //       messages:
    //       {
    //         date_from: {
    //           required: "Date From is required."
    //         },
    //         date_to: {
    //           required: "Date To is required."
    //         },
    //       },
    //       submitHandler: function (form)
    //       {
    //        return true;
    //          //formSubmit(form);
    //       }
    //     });

    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
      </script>

<div id="leadInfoModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" id="leadInfoModalDialog" style="max-width:950px" >
    </div>
</div>
