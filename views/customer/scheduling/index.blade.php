@extends('layouts.customer')
@section('content')
<link href="{{ asset('assets/customer/css/wickedpicker.min.css') }}" rel="stylesheet">
<div class="row">
<h2 class="main_title_head">Report Schedule</h2>
</div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <!-- Icon Cards-->
   <div class="row call_seting_data">
          <div class="col-md-12">
        <div class="setting-tabs">
          <ul class="nav nav-tabs reporting-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#call-setting">Report Schedule</a>
            </li>

          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <!-- Call Setting Tab -->
            <div id="call-setting" class="tab-pane active
            "><br>
              <div class="update-password">
                <div class="">

               
                  {{ Form::model($reportArray,array('id'=>'report_schedule','url'=>$url.'/save-schedule-report')) }}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="call-transfer-hours">Set mail received schedule</label>
                          {!!Form::select('schedule_id',$data, $schedule_id, ['class' => 'form-control'])!!}
                        </div>
                      </div>

                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="call-transfer-hours">Mail received time</label>

                          {!!Form::time('time', null, ['class' => 'form-control'])!!}
                        </div>
                      </div>
                    
                      <div class="col-md-12">
                        <input type="submit" class="btn btn-primary" value="Submit">
                      </div>
                    </div>

                {{ Form::close() }}
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
   </div>
@section('js')
<script type="text/javascript">

/* Method :getFetchScheduling */
function getFetchSchedulingReport()
     {
             $.ajax({
                url                : prefixUrl + '/fetch-scheduling-report',
                type               : 'GET',
                dataType           : 'html',
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function() {
                      showLoader();
                },
                complete   : function() {
                    hideLoader();
                },
                success    : function(response) {
                    $('.call_seting_data').html(response);
                     hideLoader();

                }
            });
}
//getFetchSchedulingReport();

</script>

@endsection
@endsection
