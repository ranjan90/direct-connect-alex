<div class="col-md-12">
        <div class="setting-tabs">
          <ul class="nav nav-tabs reporting-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#call-setting">Report Scheduling</a>
            </li>

          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <!-- Call Setting Tab -->
            <div id="call-setting" class="tab-pane active
            "><br>
              <div class="update-password">
                <div class="">

               
                  {{ Form::open(array('id'=>'report_schedule','url'=>$url.'/save-scheduling-report')) }}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="call-transfer-hours">Select Day</label>
                          {!!Form::select('schedule_id', $data, null, ['class' => 'form-control'])!!}
                        </div>
                      </div>

                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="call-transfer-hours">Select Time</label>

                          {!!Form::time('time', null, ['class' => 'form-control'])!!}
                        </div>
                      </div>
                    
                      <div class="col-md-12">
                        <input type="submit" class="btn btn-primary" value="Submit">
                      </div>
                    </div>

                {{ Form::close() }}
                </div>
              </div>
            </div>

         


         
        </div>
      </div>
    </div>

