@extends('layouts.customer')
@section('content')
<link href="{{ asset('assets/customer/css/wickedpicker.min.css') }}" rel="stylesheet">
<div class="row">
<h2 class="main_title_head">Call Settings</h2>
</div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <!-- Icon Cards-->
   <div class="row call_seting_data">

   </div>
    <div class="modal fade" id="confirm_delete" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title">Delete Card</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                      <h6>As you cannot recover it, Are you sure you want to delete this card ?.</h6>
                      <div class="text-right">
                          <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-primary deleteCard">Yes</button>
                      </div>
                  </div>
              </div>
          </div>
    </div>
@section('js')
<script type="text/javascript">

/* Method :getCallSetting */
function getCallSetting()
     {
         $.ajax({
            url                : prefixUrl + '/setting?business_id='+localStorage.getItem('business_data_id'),
            type               : 'GET',
            dataType           : 'html',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            beforeSend : function() {
                  showLoader();
            },
            complete   : function() {
                hideLoader();
            },
            success    : function(response) {
                $('.call_seting_data').html(response);
                 hideLoader();

            }
        });
}
if(storge_customer_id=="")
{
   getCallSetting();
}
var paginationClass="call_seting_data";
</script>
@endsection
<!-- <script src="{{ asset('assets/'.config("app.frontendtemplatename").'/js/jquery.payment.js') }}"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });

            });
              </script>

<script type="text/javascript">

jQuery(function($) {
      $(document).find('[data-numeric]').payment('restrictNumeric');
      $(document).find('.cc-number').payment('formatCardNumber');
      $(document).find('.cc-exp').payment('formatCardExpiry');
      $(document).find('.cc-cvc').payment('formatCardCVC');
      $.fn.toggleInputError = function(erred) {
        this.parent('.form-group').toggleClass('has-error', erred);
        return this;
      };
});
</script> -->
@endsection
