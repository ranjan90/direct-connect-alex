<?php
  $dayArr = array(
       'mon'=>'mon',
       'tue'=>'tue',
       'wed'=>'wed',
       'thu'=>'thu',
       'fri'=>'fri'
    );
?>
<div class="table-responsive">
    <div clas="table">
                            <table class="table table-responsive">

                            	@if(isset($user_data->firstName))
									<tr>
										<td><span class="details-user"><b>Customer Name :</b></span></td>
										<td><span class="details_now"> {{ $user_data->firstName.' '.$user_data->lastName }}</span></td>
									</tr>
								@endif
								@if(isset($user_data->email))
									<tr>
										<td><span class="details-user"><b>Customer Email :</b></span></td>
										<td><span class="details_now"> {{ $user_data->email}}</span></td>
									</tr>
								@endif
								@if(isset($leadData->campaign_details->business->business_name))
									<tr>
										<td><span class="details-user"><b>Business Name :</b></span></td>
										<td><span class="details_now"> {{ $leadData->campaign_details->business->business_name}}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->title))
									<tr>
										<td><span class="details-user"><b>Campaign Title :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->title }}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->email))
									<tr>
										<td><span class="details-user"><b>Campaign Email Address :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->email }}</span></td>
									</tr>
								@endif
								@if($leadData->status==11)
    								<tr>
                                        <td><span class="details-user"><b>Template Mismatched :</b></span></td>
                                        <td>
                                            <?php
                                                if(!empty($leadData->api_response))
                                                {
                                                    $api_response = json_decode($leadData->api_response);
                                                    if(!empty($api_response) && isset($api_response->content))
                                                    {
                                                        if(isset($api_response->content->error_description))
                                                        {
                                                            echo $api_response->content->error_description;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo "N/A";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "N/A";
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($leadData->campaign_details->testMail))
                                    <tr>
                                        <td style="width:50%">
                                            <span ><b>Original Lead Format :</b></span></br>
                                            <span class="details_now"> {!! $leadData->campaign_details->testMail !!}</span>
                                        </td>
                                        <td style="width:50%">
                                            <span ><b>NEW lead format received :</b></span></br>
                                            <span class="details_now">
                                                <?php
                                                    if( isset($leadData->original_lead) )
                                                    {
                                                        $lines = explode(PHP_EOL, $leadData->original_lead);
                                                        if( is_array($lines))
                                                        {
                                                            foreach ($lines as $key => $value)
                                                            {
                                                                if( trim($value) != '' )
                                                                    echo '<p class="testMail">'.$value."</p>";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            echo $leadData->original_lead;
                                                        }
                                                    }
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                @endif
								@if(isset($userCamp->template))
									<tr>
										<td><span class="details-user"><b>Campaign Template :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->template }}</span></td>
									</tr>
								@endif
								@if(isset($userCamp->country_id) && $userCamp->country_id!=0)
									<tr>
										<td><span class="details-user"><b>Campaign Country :</b></span></td>
										<td><span class="details_now"> {{ $userCamp->coutry_details->countryName }}</span></td>
									</tr>
								@endif

							<!-- 	@if(isset($userCamp->available_hours))

									<?php  $availHours = json_decode($userCamp->available_hours); ?>

    									<tr class="mb-2">
    										<td><span class="details-user"><b>Available Hours :</b></span></td>
    										<td>
    										     <p class="details_now">


                   								 <p class="details_now"> @if(isset($availHours->to->working))

                   								  @if(isset($userCamp->available_days))
                                              <?php  $days = json_decode($userCamp->available_days);
                                                 ?>
                                                @foreach($days as $day)
                                                     @if(in_array($day,$dayArr))
                                                        {{ ucfirst($day.' ') }}
                                                     @endif
                                                @endforeach
                                                 @endif
                   								 {{ $availHours->from->working }} - {{ $availHours->to->working }} @else N/A @endif</p>
    											 <p class="details_now"> @if(isset($availHours->from->sat))Sat {{ $availHours->from->sat }} - {{ $availHours->to->sat }}@else N/A @endif</p>
    											 <p class="details_now"> @if(isset($availHours->from->sun)) Sun {{ $availHours->from->sun }} - {{ $availHours->to->sun }} @else N/A @endif</p>
    										</td>
    									</tr>
								@endif -->
                <?php
                    $availHours="";
                    $days="";
                ?>
                @if(isset($leadData->lead_data_history->available_hours) && !empty($leadData->lead_data_history->available_hours))
                     <?php  $availHours = json_decode($leadData->lead_data_history->available_hours); ?>
                     <?php  $days = json_decode($leadData->lead_data_history->available_days);?>
                @else
                  @if(isset($userCamp->available_hours))
                         <?php  $availHours = json_decode($userCamp->available_hours); ?>
                         <?php  $days = json_decode($userCamp->available_days);?>
                  @endif
                @endif

                @if(!empty($availHours))
                              <tr class="mb-2">
                                      <td><span ><b>Available Hours :</b></span></td>
                                      <td>
                                          <p class="details_now">
                                         @if(isset($availHours->from->working))
                                              @if(!empty($days))
                                                  @foreach($days as $day)
                                                       @if(in_array($day,$dayArr))
                                                          {{ ucfirst($day.' ') }}
                                                       @endif
                                                  @endforeach
                                          @endif
                                           {{ $availHours->from->working }} - {{ $availHours->to->working }} @else N/A @endif</p>
                                           <p class="details_now">@if(isset($availHours->from->sat) && !empty($availHours->from->sat)) Sat {{ $availHours->from->sat }} - {{ $availHours->to->sat }} @else N/A @endif</p>
                                           <p class="details_now">@if(isset($availHours->from->sun) && !empty($availHours->from->sun)) Sun {{ $availHours->from->sun }} - {{ $availHours->to->sun }} @else N/A @endif</p>
                                      </td>
                                </tr>
                @endif
								<?php $breakHours = json_decode($userCamp->break_hours); ?>
								@if(isset($userCamp->break_hours))
									<tr class="d-none">
										<td><span class="details-user"><b>Break Hours :</b></span></td>
										<td><span class="details_now"> {{ $breakHours->from }} - {{ $breakHours->to }}</span></td>
									</tr>
								@endif

								<tr><td><span class="details-user"><b>Company Contacts</b></span></td><td></td></tr>
							</table>
							</div>
</div>
                           <div class="table-responsive">
							<table class="table table-responsive">
								<thead>
									<tr>
										<th>Name</th>
										<th>Phone Number</th>
									</tr>
								</thead>
								<tbody>
								    @if(isset($leadData->lead_contacts))
									@forelse($leadData->lead_contacts as $ct)
									<tr>
										<td>@if(isset($ct->name)){{ $ct->name }}@else N/A @endif</td>
										<td>@if(isset($ct->contact)){{ $ct->contact }}@else N/A @endif</td>
									</tr>
									@empty
									<tr><td colspan="2">No Record Found !</td></tr>
									@endforelse
									@endif
								</tbody>

							</table>
							</div>







                            <script type="text/javascript">
                            jQuery(document).on('click','.testMail',function(event) {
                                  event.preventDefault();
                                  $('.popover').hide();
                                  return false;
                            });
                            function hightLight (element, start, end,tagName , id, isCustomTag) {
                                var str = element[0].innerHTML;
                                str = `${ str.substr(0, start) }<mark data-name="${tagName.replace("\"", "")}" class="campaignTag" data-custom-tag="${isCustomTag}" data-id="${id}">${str.substr(start, end - start)}</mark>${str.substr(end)}`;
                                element[0].innerHTML = str.replace("\"", "");

                            }
                            <?php
                            if( !empty($campTags))
                            {
                                foreach ($campTags as $key => $value) { ?>
                                    jQuery('.testMail').each(function(index)
                                    {
                                        var thiss = $(this);
                                        var tagnamee = '<?php echo $value->tagName;?>';
                                        if( tagnamee != '' )
                                        {
                                            if (index == '<?php echo $value->indexRow?>') {
                                                hightLight(thiss, '<?php echo $value->positionStart?>', '<?php echo $value->positionEnd?>','<?php echo $value->tagName?>','<?php echo $value->id?>','<?php echo $value->isCustomTag; ?>')
                                            }
                                        }
                                    });
                            <?php }
                            } ?>
                            </script>
                            <style>
                            .testMail .mark, mark {
                            	padding: .2em;
                            	background-color: #CCEFFC;
                            }
                            </style>
