<?php
$campaignpermissiontype = '';
if( !empty($CampaignPermissions) )
{
    if( isset($CampaignPermissions[0]) )
    {
        $campaignpermissiontype = $CampaignPermissions[0]->permission_type;
    }
}
?>
<!--2nd   tour tip-->
<div class="dash-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Dashboard Menu</h4>
        <p>On the dashboard menu you will find everything that you need to manage your projects.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">2</a></li><li>of 12</li></ul>
            <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div>
        </div>
    </div>
</div>
<!--End of 2nd step-->

<!--3rd   tour tip-->
<div class="dash-pop step-3-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Campaigns</h4>
        <p>Once you have set up your campaign you will be able to go here to select which one you want manage,edit or even listernto the individual lead recordings</p>
        <div class="content">
            <ul class="step"><li>Step</li><li><a class="circle-cstm">3</a></li><li>of 12</li></ul>
            <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div>
        </div>
    </div>
</div>
<!-- End       3rd   tour tip-->

<!-- 4th   tour tip-->
<div class="dash-pop step-4-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Reports</h4>
        <p>Track your performance and share maningful insights with your team about customers and campaigns using out easy-to-create reports.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">4</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       4th   tour tip-->
<!--  5th   tour tip-->
<div class="dash-pop step-5-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Leads Contacts</h4>
        <p>Manage the hours and settings, of the team members who receive calls and leads in your organisation.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">5</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       5th   tour tip-->
<!-- 6th   tour tip-->
<div class="dash-pop step-6-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Users</h4>
        <p>Manage user priveleges and who you wan to have access to your magical dashboard and powerful new campaigns.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">6</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       6th   tour tip-->

<!-- 7th   tour tip-->
<div class="dash-pop step-7-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Plans & Billing</h4>
        <p>View your current Plan and remaining credits. Switch Plans, Top Up levels or change your payment details, its all here.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">7</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       7th   tour tip-->
<!-- 8th   tour tip-->
<div class="dash-pop step-8-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Purchase History</h4>
        <p>Get a clear overview of the history of what you have purchased so far.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">8</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!--End       8th   tour tip-->
<!--9th   tour tip-->
<div class="dash-pop step-9-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Call Settings</h4>
        <p>This is where you can edit and change your whisper or call user messages.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">9</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!--  End 9th   tour tip-->

<!-- 10th   tour tip-->
<div class="dash-pop step-10-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Need Help?</h4>
        <p>You have questions? We have answers! Come here any time to search our frequently asked questions, or take this tour again.</p>
        <p>Look for our help icon on each page, if you require additional information. Or contact our expert team directly via our chat support button above left.</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">10</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       10th   tour tip-->
<!--11th   tour tip-->
<div class="dash-pop step-11-pop d-none">
    <div class="text-right"><button type="button" class="close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
    <div class="modal-btn text-left">
        <h4>Create a New Campaign</h4>
        <p>Bring your campaigns to life! configure feature-rich,tailored campaigns to super-charge your business. Follow the simple steps and you will be Direct Connecting in no time...</p>
        <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">11</a></li><li>of 12</li></ul>
        <div class="text-right"><a class="btn-warning btn">Okay! Got it!</a></div></div>
    </div>
</div>
<!-- End       10th   tour tip-->
<div class="campaign-buttons campFilter">
    <a href="JavaScript:;" data-value="0" @if($filter==0) class="button-active" @endif>All Campaigns</a>
    <a href="JavaScript:;" data-value="1" @if($filter==1) class="button-active" @endif>Active Campaigns</a>
    <a href="JavaScript:;" data-value="2" @if($filter==2) class="button-active" @endif>Inactive Campaigns</a>
</div>
<!-- Icon Cards-->
<!-- Area Chart Example-->
<div class="row">
    <div class="col-md-12">
        <div class="recordsTableStop">
            <div class="table-responsive latest-champaign-table">
                <table class="table">
                    <tr>
                        <th></th>
                        <th style="width: 210px;">Campaign Name</th>
                        <th> Integration</th>
                        <th>Key</th>
                        <th style="width:257px;">Campaign Status</th>
                        <th style="width:150px;">Actions</th>
                    </tr>
                    @forelse($camps as $camp)
                        <tr <?php if( $camp->isResetTemplate == 'no' && $camp->template_mismatch == 'yes' ){?>data-toggle="tooltip" title="we have found a template mismatch. Please reset template from edit page." data-placement="bottom"  style="background: #fff3cd;color: #856404;"<?php }?> >
                            <td>
                                @can(config('permissions.data.change-campaign-status.name'))
                                    <div class="off-on-button">
                                        <label class="switch">
                                            <input data-toggle="tooltip" title="Change Status" data-toggle="modal" data-target="#confirm-status-modal" id="{{ $camp->id }}" data-status="{{ $camp->status }}" data-table="{{ Crypt::encrypt('campaigns') }}" class="statusRecord" @if($camp->step==6 && $camp->status==1) checked="checked" @endif >
                                            <span class="slider @if($camp->step==6 && $camp->status==1) fa-toggle-on @else fa-toggle-off @endif">
                                                <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                                <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                            </span>
                                        </label>
                                    </div>
                                @endcan
                            </td>
                            <td>
                                <span class="latest-campaign-name">
                                    <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 2 ) ){?>
                                        <a href="@if($camp->step==6) {{$url.'/view-campaign'}}/{{Crypt::encrypt($camp->id)}} @else javascript:void(0) @endif">{{ $camp->title }}</a>
                                    <?php } else if(  $campaignpermissiontype == 4 ) {
                                        $isPermissionExist = false;
                                        $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'view');
                                        if($isPermissionExist){ ?>
                                            <a href="@if($camp->step==6) {{$url.'/view-campaign'}}/{{Crypt::encrypt($camp->id)}} @else javascript:void(0) @endif">{{ $camp->title }}</a>
                                    <?php } else {?>
                                        <a href="javascript:void(0)">{{ $camp->title }}</a>
                                    <?php }
                                    } else { ?>
                                        <a href="javascript:void(0)">{{ $camp->title }}</a>
                                    <?php } ?>
                                </span>
                                <span class="date-campaign">{{date('d/m/Y', strtotime($camp->updated_at))}}</span>
                            </td>
                            <td>{{ $camp->type==1 ? 'Email' : 'Api' }}</td>
                            <td>{{ $camp->email }}</td>
                            <td>
                                <ul class="home-camp-list">
                                    @if($camp->step < 6 || $camp->invalid_step > 0)
                                        <li class="camp-inactive"><i class="fa fa-circle"></i> Campaign Incomplete</li>
                                        <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 3 ) ){?>
                                            <li><a href="{{$url.'/setup-campaign'}}/{{Crypt::encrypt($camp->id)}}">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                        <?php } else if(  $campaignpermissiontype == 4 ) {
                                            $isPermissionExist = false;
                                            $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'edit');
                                            if($isPermissionExist){ ?>
                                                <li><a href="{{$url.'/setup-campaign'}}/{{Crypt::encrypt($camp->id)}}">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                        <?php }else {?>
                                            <li><a href="javascript:;">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                        <?php }
                                            } else {?>
                                            <li><a href="javascript:;">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                        <?php } ?>
                                    @else
                                        <li class="camp-active"><i class="fa fa-circle"></i>Campaign Complete</li>
                                    @endif
                                </ul>
                            </td>
                            <td>
                                <ul class="icon-social">
                                    <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 3 ) ){?>
                                        <li class="comon-d"><a href="{{$url.'/setup-campaign/'.Crypt::encrypt($camp->id)}}" data-toggle="tooltip" title="Edit" data-placement="bottom"> <img src="{{asset('assets/customer/img/edi-d.png')}}"></a></li>
                                    <?php } else if(  $campaignpermissiontype == 4 ) {
                                        $isPermissionExist = false;
                                        $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'edit');
                                        if($isPermissionExist){ ?>
                                            <li class="comon-d"><a href="{{$url.'/setup-campaign/'.Crypt::encrypt($camp->id)}}" data-toggle="tooltip" title="Edit" data-placement="bottom"> <img src="{{asset('assets/customer/img/edi-d.png')}}"></a></li>
                                        <?php }
                                        }
                                        if($camp->step==6)
                                        { ?>
                                            @can(config('permissions.data.reporting-list.name'))
                                                <li class="lead-d"><a href="{{$url.'/campaign-leads'}}/{{ Crypt::encrypt($camp->id) }}" data-toggle="tooltip" title="Lead" data-placement="bottom"><img src="{{asset('assets/customer/img/lead-d.png')}}"></li>
                                            @endcan
                                <?php   }?>
                                    @can(config('permissions.data.delete-campaign.name'))
                                        <li class="comon-d"><a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $camp->id }}" data-table="{{ Crypt::encrypt('campaigns') }}"> <span  data-toggle="tooltip" title="Delete" data-placement="bottom"><img src="{{asset('assets/customer/img/delete.png')}}"></span></a></li>
                                    @endcan
                                </ul>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="4"> No Campaign Available!</td></tr>
                    @endforelse
                </table>
            </div>
            {{$camps->appends(['campFilter'=>$filter,'business_id'=>$business_id])->links()}}
        </div>
    </div>
<!-- Example DataTables Card-->
</div>
<?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 2  || $campaignpermissiontype == 4 ) ){?>
<div class="graph">
    <div class="graph-latest row">
        <div class="col-md-6">
            <div class="tower-graph">
                <!--              <img src="{{ asset('assets/customer/img/campaigns_chart.svg') }}  ">-->
                <div class="d-flex c-m-t">
                    <h3>Campaign Chart</h3>
                    <h6>TOTAL CAMPAIGNS</h6>
                </div>
                <div class="dnt-chart">
                    @if(count($chartData['camp_chart']) > 0)
                    <canvas id="myChart2"></canvas>
                    <h2><strong>{{count($chartData['camp_chart'])}}</strong><br>Campaigns</h2>
                    @else
                    <img class="img-fluid" src="{{asset('assets/customer/img/dash-camp.png')}}">
                    @endif
                </div>
            </div>
        </div>        
        <div class="col-md-6">
            <div class="tower-graph">
                <!--               <img src="{{ asset('assets/customer/img/Leads_chart.svg') }}  ">-->
                <div class="d-flex c-m-t">
                    <h3>Leads Chart</h3>
                    <h6>MONTHLY ACTIVITIES</h6>
                </div>
                <div class="dnt-chart">
                    @if(count($chartData['camp_chart']) > 0)
                        <canvas id="myChart"></canvas>
                    @else
                        <img class="img-fluid" src="{{asset('assets/customer/img/tab.png')}}">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

    <div class="row">
        <div class="col-md-12">
            <div class="latest-champaign-table main-latest-champaign-table">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Report Name</th>
                            <th>Credits Consumed</th>
                            <th>Leads</th>
                            <th style="width:200px;">Actions</th>
                        </tr>
                        <?php
            				if( !$isBusinessOwner )
            				{
            					if( $campaignpermissiontype == 4 )
            					{
                                    $campIds    = Helper::getViewPermissionCampIds( $CampaignPermissions );
            						if( !empty($campIds))
                                        $cmp = App\Model\Campaign::where('business_id',$business_id)->whereIn('id',$campIds)->Has('api_leads');
            						else
            	                		$cmp = App\Model\Campaign::where('business_id',$business_id)->where('id',0)->Has('api_leads');
            					}
            					else if( $campaignpermissiontype == 1 || $campaignpermissiontype == 2 )
            	                	$cmp = App\Model\Campaign::where('business_id',$business_id)->Has('api_leads');
                                else
                                    $cmp = App\Model\Campaign::where('business_id',$business_id)->where('id',0)->Has('api_leads');
            				}
            				else
                                $cmp = App\Model\Campaign::where('business_id',$business_id)->Has('api_leads');
            			?>
                        @if($cmp->count()!=0)
                            @forelse($cmp->get() as $camps)
                                @php
                                    $totalLeadQuery= App\Model\ApiLeads::where('campaign_id',$camps->id);
                                @endphp
                                @if(true)
                                @php
                                    $total_leads =  $totalLeadQuery->count();
                                    $totalCredit=$totalLeadQuery->sum('credit_amount');
                                @endphp
                                <tr>
                                    <td><span class="latest-campaign-name">{{$camps->title}}</span></td>
                                    <td>{{$totalCredit}}</td>
                                    <td>
                                        <?php    if(isset($total_leads)){ echo $total_leads;} else { echo 0; }    ?>
                                    </td>
                                    <td>
                                        <ul class="icon-social">
                                            <li>
                                                @can(config('permissions.data.reporting-list.name'))
                                                    <a href="{{$url.'/campaign-leads'}}/{{ Crypt::encrypt(isset($camps->id ) ? $camps->id : 0) }}" data-toggle="tooltip" title="Leads" data-placement="bottom"><img src="{{asset('assets/customer/img/phone-p.png')}}"></a>
                                                @endcan
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @else
                                @endif
                                @empty
                                <tr>
                                    <td colspan="4">No records found !</td>
                                </tr>
                            @endforelse
                        @else
                            <tr> <td colspan="4">No records found !</td>  </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@if(count($chartData['camp_chart']) > 0)
    <script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    });
    var business_id="<?php echo $business_id ?>";
    $(document).ready(function(){
    let startDate = $('input[name="startDate"]').val();
    let endate =  $('input[name="endDate"]').val();
    if($.trim(startDate).length > 0 && $.trim(endate).length == 0){
    tost('Please select Date to and submit request again.','Error',1000);
    return false;
    }
    $.ajax({
    url: "<?php echo $url;?>/get-charts",
    method: "POST",
    data : {startDate : startDate , endDate : endate ,business_id:business_id},
    beforeSend : function () {
    $('.loader_div').show();
    if (typeof(myLineChart) != "undefined" && myLineChart !== null)
    { myLineChart.destroy(); myPieChart.destroy();}
    },
    complete : function () {
    $('.loader_div').hide();
    },
    success: function(data) {
    var month = [];
    var count = [];
    var pieCampName = []
    var pieCount = []
    $('.totalLeads').html(data.lead_count);
    if (data.lead_count !=0 )
    {
    for (var i = 0; i < data.camp_chart.length; i++) {
    month.push(data.camp_chart[i].monthname);
    count.push(data.camp_chart[i].lead_count);
    }
    for (var i = 0; i < data.campChart.length; i++) {
    pieCampName.push(data.campChart[i].title);
    pieCount.push(data.campChart[i].count);
    }
    }else{
    month = ['No Record'];
    count = [0];
    pieCampName = ['No Record'];
    pieCount = [0];
    }
    //Bar Chart
    new Chart(document.getElementById("myChart"), {
    type: 'bar',
    data: {
    labels: month,
    datasets: [
    {
    label: "Total Leads",
    backgroundColor: ['#00adee','#00aef4','#4ed0fb','#1b6ea0','#00aef4','#4ed0fb'],
    data: count
    }
    ]
    },
    options: {
    legend: { display: false },
    title: {
    display: true,
    }
    }
    });
    //Bar Chart Ends
    //Doughnut Chart
    var ctx = document.getElementById("myChart2").getContext('2d');
    new Chart(document.getElementById("myChart2"), {
    type: 'doughnut',
    data: {
    labels: pieCampName,
    datasets: [
    {
    backgroundColor: ['#00adee','#1b6ea0','#ccc'],
    data: pieCount
    }
    ]
    },
    options: {
    legend: {
    display: false
    }
    }
    });

    },
    error: function(data) {
    console.log(data);
    }
    });
    });

    </script>
@endif
