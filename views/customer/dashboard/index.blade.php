@extends('layouts.customer')
@section('content')
 <!--@if(!empty($user_status) && isset($user_status->status) && $user_status->status == 9)-->
 <!--   <div class="row alert alert-error alertPopups">-->
 <!--     <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>-->
 <!--   </div>-->
 <!-- @endif-->
 <!-- @if(!empty($user_status) && isset($user_status->status) && $user_status->status == 12)-->
 <!--   <div class="row alert alert-error alertPopups">-->
 <!--     <div class="col-md-12">{!! \Config::get('message.stripe_payment_fail') !!}</div>-->
 <!--   </div>-->
 <!-- @endif-->
 <!-- @if(!empty($user_status) && isset($user_status->status) && $user_status->status == 11)-->
 <!--   <div class="row alert alert-error alertPopups">-->
 <!--     <div class="col-md-12">{!! \Config::get('message.cancel_renewal_payment') !!}</div>-->
 <!--   </div>-->
 <!-- @endif-->


<?php

   $BusinessOwner= App\Model\BusinessProfile::find(Auth::user()->getBusinessId())->get_users()->wherePivot('is_owner','1');
    if($BusinessOwner->where('register_step',3)->count()!=0)
    {
              $user_data_check= $BusinessOwner->first();
    }
    if(isset($user_data_check->is_group) && $user_data_check->is_group==1)
    {
            if(\App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->count()==0)
            {
                ?>
                <div class="row alert alert-error alertPopups">
      <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>
    </div>

                <?php
            }
    }
else
{
    ?>

  @if(!empty($user_status) && isset($user_status->isSubscriptionActive) && $user_status->isSubscriptionActive == 1)
    <div class="row alert alert-error alertPopups">
      <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>
    </div>
     @if(isset($user_status->message) && !empty($user_status->message))

    <div class="row alert alert-error alertPopups">
      <div class="col-md-12">Business plan cancel reason is: {{$user_status->message}}</div>
    </div>
    @endif
  @endif
<?php
    }
?>  
  <div class="container-fluid">
     <h1 class="dashtitle">DASHBOARD</h1>
     <h3 class="dashsubtitle">LATEST CAMPAIGNS
         <div class="create-campaign">
             @can(config('permissions.data.add-campaign.name'))
                 @if(!empty($user_status) && isset($user_status->isSubscriptionActive) && $user_status->isSubscriptionActive == 2)
                     <a href="{{$url.'/add-new-campaign'}}" class="btn btn-primary create-latest-campaign take-tour-campaign">CREATE A NEW CAMPAIGN</a>
                 @endif
             @endcan
         </div>
     </h3>
  </div>
  <div class="clearfix"></div>
 <div class="container-fluid dashboard_data">

 </div>
  <br>
       <form id="out_hour_form" method="POST">

  <div class="table-responsive offHourRecordsTable latest-champaign-table">

  </div>

     </form>

 <!-- Delete Modal -->
    <div id="confirm_off_hour_change_status_id" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title trigger_title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6 class="trigger_message"></h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary offHourYesBtn">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->





 <!-- View Modal -->
    <div id="out_hour_more_info_pop_up" class="modal fade"  role="dialog" style="display:none">
        <div class="modal-dialog modal-lg" style="max-width:80%;">>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">View  Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body out_hour_more_data">

                </div>
            </div>
        </div>
    </div>
    <!-- View Enddds -->




<?php
 $f=0;
  if(isset($filter))
  {
      $f=$filter;
  }
?>
@section('js')
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script type="text/javascript">
var filter= {!!  $f !!};
var select_status="";
/* getDashboardCampaign() it will be show records accroding to business*/
function getDashboardCampaign(filter=null)
{
   $.ajax({
      url                : prefixUrl + '/fetch-dashboard?business_id='+localStorage.getItem('business_data_id')+"&campFilter="+filter,
      type               : 'GET',
      dataType           : 'html',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      },
      beforeSend : function() {
           // showLoader();
      },
      complete   : function() {
          hideLoader();
      },
      success    : function(response) {
          $('.dashboard_data').html(response);
         hideLoader();

      }
  });
}

function fetchOffHourRecords()
{
             $.ajax({
              url                : prefixUrl + '/fetch-off-hour-records?status='+select_status,
              type               : 'GET',
              dataType           : 'json',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              },
              beforeSend : function() {
                   showLoader();
              },
              complete   : function() {
                  hideLoader();
              },
              success    : function(response) {
                 $('.offHourRecordsTable').html(response.result);
                 hideLoader();

              }
          });

}

  var modalConfirm = function(callback,data_status){


          $(document).on("click",'.confirm_off_hour_change_status',function(){
               $('#confirm_off_hour_change_status_id').modal('show');
               $(".trigger_title").text($(this).attr('data-title'));
               $(".trigger_message").text($(this).attr('data-message'));
               $(".offHourYesBtn").attr('data-id',$(this).attr('data-id'));
               $(".offHourYesBtn").attr('data-status',$(this).attr('data-status'));
          });

          $(document).on("click",'.offHourYesBtn',function(){
                  select_status=$(".leadTriggerSelect").val();
                  callback($(this).attr('data-id'),$(this).attr('data-status'));
          });

 };


  modalConfirm(function(confirm,data_status)
    {
        if(confirm)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:prefixUrl+'/change-off-hour-status/'+confirm+'/'+data_status,
                dataType: "json",
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                        $('#confirm_off_hour_change_status_id').modal('hide');
                         tostrsuccess(data.message);

                },
                complete: function() {
                    hideLoader();
                    fetchOffHourRecords();
                },
                error: function (xhr) {
                    $('#confirm_off_hour_change_status_id').modal('hide');
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                    });
                }
            });
        }
    });

  //leadTriggerSelect

  $(document).on('change','.leadTriggerSelect',function($c){

            select_status=$(this).val();
            fetchOffHourRecords();

  });



 jQuery(document).on('click','.out_hour_more_info_pop_up',function(e)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'GET',
                url:prefixUrl+'/get-out-hour-info/'+jQuery(this).attr('data-id'),
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (res)
                {
                    hideLoader();
                    $(".out_hour_more_data").html(res.data)
                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                     hideLoader();
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        //tostrerror(message);
                         $(".out_hour_more_data").html(message)
                    });
                }
            });
        });

   $(document).on('click','.submitleadTriggerSelect',function($c){

             jQuery.ajax({

                url:prefixUrl+'/change-hour',
                type: 'post',
                data:$("#out_hour_form").serialize(),
                dataType : 'json',
                headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                        $('#confirm_off_hour_change_status_id').modal('hide');
                         tostrsuccess(data.message);

                },
                complete: function() {
                    hideLoader();
                    fetchOffHourRecords();
                },
                error: function (xhr) {
                    $('#confirm_off_hour_change_status_id').modal('hide');
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                    });
                }
            });


  });


 $(document).on("change",".all_outside_hour_checkbox",function(){

        if ($(this).is(':checked')) {
             $(document).find('.outside_hour_checkbox').prop('checked',true);
        } else {
            $(document).find('.outside_hour_checkbox').prop('checked',false);
        }
});





if(localStorage.getItem('business_data_id')!=null || localStorage.getItem('business_data_id')!="")
getDashboardCampaign(filter);
fetchOffHourRecords();
var paginationClass="dashboard_data";
</script>
@endsection
@endsection
