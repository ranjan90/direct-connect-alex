<div class="row mt-3 px-2">
  <div class="col-md-12"><!-- <h6>Leads Outside Available Hours: {{$outsideOnleadstotal+$outsideOffleadstotal}} </h6> -->
    <h6>Lead Concierge - Calls scheduled to be Direct Connected soon!</h6>
  </div>
</div>
<div class="table-responsive latest-champaign-table">
   <table class="table">
       <tbody>
            <tr>
               <th scope="col" width="10%">Direct Connect to be triggered at</th>
                <th scope="col" width="15%">Campaign <br/>Name</th>
                <th scope="col" width="10%">Lead <br/>Name</th>
                <th scope="col" width="8%">Phone <br> Number</th>
                <th scope="col" width="6%">Original Lead Time</th>
                <th scope="col" width="6%">Day of <br/>week</th>
                  <th scope="col" width="6%">DO NOT </br/>Direct Connect</th>
                <th scope="col" width="5%">More Info</th>
            </tr>
    <?php
        $i                          = 1;
        $default_retry_delay_second = 0;
        $dayArr = array(
            0 => 'mon',
            1 => 'tue',
            2 => 'wed',
            3 => 'thu',
            4 => 'fri'
        );
        $dateArr = array(
            0 => 'monday',
            1 => 'tuesday',
            2 => 'wednesday',
            3 => 'thursday',
            4 => 'friday'
        );
        $check_camp_array   = [];
    ?>
        @if(isset($outsideleadsData) && $outsideleadsData->count() > 0)
            @forelse($outsideleadsData as $outsidelead)
            @if($outsidelead->campaign_details->step==6)
                    <tr>
                         <!-- <td>@if(!empty($selected_day)){!! date('m-d-Y', strtotime($selected_date)).' '.$get_last_value !!} @else Sorry Time not match  @endif</td> -->
                        <td>@if(isset($outsidelead->triggerAt)){!! date('d/m/Y', strtotime($outsidelead->triggerAt)) !!} <br> {!! date('h:i:s A', strtotime($outsidelead->triggerAt)) !!} @else N/A @endif</td>
                        <td>@if(isset($outsidelead->campaign_details)){{ ucfirst($outsidelead->campaign_details->title) }}@endif</td>
                        <td>@if(isset($outsidelead->leadName)){{ ucfirst($outsidelead->leadName) }}@endif</td>
                        <td>@if(isset($outsidelead->mobileNo)){{ $outsidelead->mobileNo }}@endif</td>
                        <td>
                            @if(!empty($outsidelead->expectedCall) && isset($outsidelead->campaign_details->coutry_details) && $outsidelead->campaign_details->coutry_details->countryPhoneCode=="")
                                {!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($outsidelead->campaign_details,$outsidelead->expectedCall,true) !!}
                            @else
                                {!! date('d/m/Y', strtotime($outsidelead->expectedCall))  !!}<br>{!! date('h:i:s A', strtotime($outsidelead->expectedCall))  !!}
                            @endif
                        </td>
                        <td>@if(isset($outsidelead->expectedCall)){{ date('l', strtotime($outsidelead->expectedCall)) }}@endif</td>

                        <td>
                            @if($outsidelead->outside_available_hours==2)
                                <div class="off-on-button">
                                    <label class="switch">
                                        <input data-title="No Lead Trigger"  data-message="Are you sure you don't want to trigger this lead ?" data-toggle="tooltip" title="Change No Lead Trigger" data-toggle="modal" data-target="#confirm_off_hour_change_status_id" id="{!! Crypt::encrypt($outsidelead->id) !!}" data-status="{!! Crypt::encrypt($outsidelead->outside_available_hours) !!}""  data-id="{!! Crypt::encrypt($outsidelead->id) !!}" type="checkbox" @if( $outsidelead->outside_available_hours ==3) class="" @else  class="confirm_off_hour_change_status" checked="checked" @endif>
                                        <span class="slider @if($outsidelead->outside_available_hours==2) fa-toggle-on @else fa-toggle-off @endif">
                                            <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                            <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                        </span>
                                    </label>
                                </div>
                             @else
                             @endif
                        </td>
                        <td><a class="out_hour_more_info_pop_up" href="javascript:void(0)" data-target="#out_hour_more_info_pop_up" data-toggle="modal"  data-id ="{!! Crypt::encrypt($outsidelead->id) !!}" ><img  title="More information" data-original-title="More information" data-toggle="tooltip" src="{{asset('assets/customer/img/info.jpg')}}"></a></td>
                    </tr>
                    <?php $i++; ?>
                    @endif
                    @empty
                    @endforelse

                  @else
                  <tr>
                    <td scope="col" colspan="8" align="center">No Leads Available.</td>
                  </tr>
                  @endif
                </tbody>
            </table>
      </div>
      <?php echo $outsideleadsData->links();?>
