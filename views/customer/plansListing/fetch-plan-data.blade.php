<?php
$existingPlanActive = $creditRechargeActive = $cardsActive = $plan_active = '';
if(\Auth()->user()->can('plans-list') && \Auth()->user()->can('credit-recharge') && \Auth()->user()->can('cards'))
    $existingPlanActive     = " active";
else if(\Auth()->user()->can('plans-list'))
    $existingPlanActive     = " active";
else if(\Auth()->user()->can('credit-recharge'))
    $creditRechargeActive   = " active";
else if(\Auth()->user()->can('cards'))
    $cardsActive    = " active";
else if(\Auth()->user()->can('switch-plan'))
    $plan_active    = " active";


if( !empty($_GET['plan_active']) )
{
    $existingPlanActive = '';
    $plan_active        = 'active';
    $cardsActive        = '';
}

if( isset($_GET['top_active']) && $_GET['top_active'] == 'active' ) {
    //$creditRechargeActive   = " active";
}
?>
@php
    $consumedMin = 0;

@endphp
<style type="text/css">
.green{
    color: green;
}
.view_promo_plan{
    color:#007bff !important;
}

</style>
<div class="row"><h2 class="main_title_head">Plans & Billing</h2></div>
<div class="container-fluid">
    @php $sub_id = ''; $currentPlanName=""; @endphp
    <!-- Icon Cards-->
    <div class="row">
        @php
            $currency   = \App\User::where('id',$user_id)->first()->region->currency;
            $user       = \App\User::with(['user_plan.plans.plan_prices'=>function($q) use($currency)
            {
                $q->where('currency_id',$currency);
            }])->where('id',$user_id)->first();
            if(isset($user->user_plan->plans->plan_prices[0]) && !empty($user->user_plan->plans->plan_prices[0]))
                $currencySymbol = $user->user_plan->plans->plan_prices[0]->plan_currency->currencySymbol;
            else
                $currencySymbol = '';
        @endphp
        @if (session('plan_status'))
            <div class="alert alert-warning message_show">{{ session('plan_status') }}</div>
        @endif
        <div class="col-md-12" id="PlanBilling">
            <div class="tabbable-panel Campaigns-tabs">
                <ul class="nav nav-tabs reporting-tabs">
                @can(config('permissions.data.plans-list.name'))
                    <li class="nav-item">
                        <a class="leadTabs active_prev <?php echo $existingPlanActive;?>   " data-toggle="tab" href="#existing_plan">CURRENT PLAN & Top Up</a>
                    </li>
                @endcan
                {{--@if(isset($userPlan) && $userPlan!='')--}}
                @can(config('permissions.data.switch-plan.name'))
                    <li class="nav-item">
                        <a class=" swith-plan-trigger leadTabs <?php echo $plan_active ?>" data-toggle="tab" href="#new_plan">SWITCH PLANS</a>
                    </li>
                @endcan
                @can(config('permissions.data.credit-recharge.name'))
                    <li class="nav-item">
                        <a class=" <?php echo $creditRechargeActive;?>  " data-toggle="tab" id="tab-recharge" href="#credit-recharge">CREDIT RECHARGE </a>
                    </li>
                @endcan
                @can(config('permissions.data.cards.name'))
                    <li class="nav-item">
                        <a class=" <?php echo $cardsActive;?>  " data-toggle="tab" href="#cards-setting">SAVED CARDS</a>
                    </li>
                @endcan
                </ul>
                <?php
                    $remaining_mints    = 0;
                    $plan_credits       = 0;
                    $plan_type          = 0;
                    $sub_plan_name      = "";
                    if(!empty($group_plan_credit) && isset($group_plan_credit->balanceAmount) && isset($group_plan_credit->plan_credit))
                    {
                        $plan_credits= $group_plan_credit->plan_credit;
                        $remaining_mints=$group_plan_credit->balanceAmount;
                    }
                     else if(isset($business_id))
                    {
                        $uPlan = App\Model\UserPlan::where('business_id',$business_id);
                        if($uPlan->count()!=0)
                        {
                            //$remaining_mints = $uPlan->first()->plans->minutes_per_month-$consumed_mints;
                            // $remaining_mints = $uPlan->first()->plans->minutes_per_month-$uPlan->first()->minuteConsumed;
                            $remaining_mints    = $uPlan->first()->balanceAmount;
                            $plan_type          = $uPlan->first()->plan_type;
                           $d_plan_data= App\Model\Plan::where('id',$uPlan->first()->plan_id);
                           if($d_plan_data->count()!=0)
                           {
                              $sub_plan_name= $d_plan_data->first()->name;
                           }
                            if($plan_type==2)
                            {
                             $plan_credits       = $uPlan->first()->promo_plan_credits;
                            }else{
                              $plan_credits       = $uPlan->first()->plan_credits;

                            }

                        }
                    }
                ?>
            </div>

            <div class="tab-content">
                @can(config('permissions.data.plans-list.name'))
                    <div id="existing_plan" class="tab-pane  <?php echo $existingPlanActive;?>  ">
                        <h2 class="campaign_name">
                            {{--@if(isset($userPlan) && $userPlan!='' && $user_data->status==1)--}}
                                @if($remaining_mints < 50)
                                    <a href="#" class="alert-danger">You have {{ $remaining_mints }} credit remaining.</a>
                                @else
                                    <span class="alert-success">You have {{ $remaining_mints }} credit remaining. </span>
                                @endif
                           {{-- @endif--}}
                        </h2>
                        @if(empty($group_plan_credit))
                        <p>You can <a href="#" class="text-orange upgrate-take-tour">UPGRADE</a> or cancel your plan at any time. Call credits do not expire as long as you have an active plan. </p>
                        @endif
                        <div class="tol-tip cstm-tol d-none upgrate-take-tour-show">
                            <h4>Company Contacts Limit Reached!</h4>
                            <p>You have reached the company contacts limit included in your current plan.  </p>
                            <p>Simply Upgrade your monthly subscription to add more team members, or just replace one of the existing members that you no longer need. </p>
                            <div class="limit-btn d-inline-block w-100">
                                <a class="btn btn-default tour-gotit-upgrate">Okay! got it!</a>
                                <a class="btn btn-warning float-right upgrate-now-button">Upgrade now!</a>
                            </div>
                        </div>
                        <div class="table-responsive recordsTable latest-champaign-table">
                              <h2 class="campaign_name">Current Plan</h2>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width:13%;">Plan name</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Valid till</th>
                                        <th scope="col" style="width:16%;">Included Credits</th>
                                        <!--th scope="col" style="width:12%;">Leads per month</th-->
                                        <th scope="col">Amount</th>
                                        <!--<th scope="col">info</th>-->
                                        <th scope="col">Status</th>
                                        @if(empty($group_plan_credit))
                                        <th scope="col">Action</th>
                                        @endif
                                        <th scope="col">Plan Type</th>
                                    @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                                        @if(isset($userPlan) && $userPlan->type=='promo_code')
                                        <!-- <th scope="col">Subscription Detail</th> -->
                                        @endif
                                    @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $currecnySymbol="";$i=0; $i++; ?>
                                    @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                                        @if(isset($userPlan) && $userPlan!='')
                                           @php
                                             $sub_id = $userPlan->sub_id;
                                             $startDate= gmdate("d/m/Y", ($userPlan->startDate));
                                             $endDate=gmdate("d/m/Y", ($userPlan->endDate));
                                             $countryShort=isset($user_data->region->countries->countryShort) ?$user_data->region->countries->countryShort :'';
                                             $currecnySymbol=App\Helpers\GlobalFunctions::userCurrencySymbol($userPlan->users->id);
                                             if(!empty($countryShort) && $plan_type!=2)
              			    	 	 	     	         {
              			    	 	 	     	             $getUserStartEndDate=App\Helpers\GlobalFunctions::getUserStartEndDate($countryShort,$userPlan->startDate,($userPlan->endDate));
              			    	 	 	     	             $startDate=$getUserStartEndDate['startDate'];
              			    	 	 	     	             $endDate=$getUserStartEndDate['endDate'];

              			    	 	 	     	         }
                                            @endphp
                                            <tr id="tr{{$userPlan->sub_id}}">
                                                <td>@if(isset($userPlan->usersPlans->name)){{ $userPlan->usersPlans->name }}@else N/A @endif</td>
                                               <td>{{ $startDate }}</td>
                                                <td class="valid_up_to" data-ref="{{ $endDate }}">{{ $endDate }}</td>
                                                <td>{{ $plan_credits }}</td>
                                                <td>
                                                    <span class="text-right"></span> {{$currecnySymbol}}{{$userPlan->amount_paid}}
                                                </td>
                                                @php $canceled_at = 'Active' @endphp
                                                    @if($userPlan->canceled_at == 1 )
                                                        @php $canceled_at = 'Subscrition cancelled at '.gmdate('d/m/Y', ($userPlan->cancelDate)) @endphp
                                                    @endif
                                                    @if($userPlan->canceled_at == 2 )
                                                        @php $canceled_at = 'Subscription will be cancelled at the end of current period on '.gmdate('d/m/Y', ($userPlan->cancelDate)) @endphp
                                                    @endif
                                                    <!--<td><i class="fa fa-info-circle green" data-toggle="tooltip" title="{{$canceled_at}}"></i></td>-->
                                                <td class="active-status">
                                                    <span class="badge badge-secondary text-uppercase">
                                                        @if($userPlan->status=='Active')
                                                            <span style="color:#94d500">{{ $userPlan->status}}</span>
                                                            <span class="active-ic"><i class="fa fa-circle"></i></span>
                                                        @else
                                                            <span style="color:#ff0000">Inactive</span>
                                                            <span class="inactive-ic"><i class="fa fa-circle"></i></span>
                                                        @endif
                                                    </span>
                                                </td>
                                                 @if(empty($group_plan_credit))
                                                @if(isset($userPlan->endDate))
                                                    @if( gmdate('Y-m-d', ($userPlan->endDate)) >= date('Y-m-d') )
                                                        <td>
                                                            {{--@if($userPlan->status != 'Cancelled') --}}
                                                            @if($user_id==\Auth::user()->id)
                                                                @if($userPlan->status=='Active')
                                                                    <a class="unsubscribePlan btn btn-danger btn-small" data-type="<?php if($plan_type==2){echo 'promo_code';}else{ echo 'subscription';}?>"  href="javascript:void(0)" data-ref="{{$userPlan->sub_id}}" data-reason="{{$userPlan->message}}" data-cancel-option="{{$userPlan->cancel_option}}">Cancel</a>
                                                                @else
                                                                    <span class="badge badge-secondary text-uppercase">{{ $userPlan->status}}</span>
                                                                @endif
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td> <span class="badge badge-secondary text-uppercase">{{ $userPlan->status}}</span> </td>
                                                    @endif
                                                @endif
                                                @endif
                                                <td>@if($plan_type==2) Promo Plan @else Subscription Plan @endif</td>
                                                  @if($plan_type==2)
                                                 <!--   <td>
                                                        <a href="javascript:;" style="text-decoration:none" data-toggle="modal" data-target="#data_plan_confirm_payment_modal" class='view_promo_plan'>View</a>
                                                   </td> -->
                                                    @endif
                                            </tr>
                                        @else
                                            <!--<tr><td colspan="10"><div class="alert alert-danger">We encountered an issue when trying to access your payment information.
                                            Please try again shortly. </div></td></tr>-->
                                            <tr><td colspan="10"><div class="alert alert-danger">Sorry no current plan found !. </div></td></tr>
                                        @endif
                                    @else
                                    @endif
                                </tbody>
                            </table>
                            </br>
                            <h2 class="campaign_name">Current Top Up</h2>
                            <p>Top Up triggers when your balance reaches <?php if(isset($group_plan_credit->top_recharge_credit)) echo $group_plan_credit->top_recharge_credit; else echo '25';?> credits.</p>
                             <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width:13%;">Top Up Name</th>
                                        <th scope="col">Top Up Amount</th>
                                        <th scope="col">Top Up Credit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                      if(!empty($group_plan_credit))
                                      {
                                        ?>
                                          <tr>
                                        <td>Group Top Up</td>
                                        <td>@if(isset($group_plan_credit->top_price)) {{$currecnySymbol}}{{$group_plan_credit->top_price}} @else N/A @endif</td>
                                        <td>@if(isset($group_plan_credit->top_credit)) {{$group_plan_credit->top_credit}} @else N/A @endif</td>

                                   </tr>

                                        <?php

                                      }
                                      else
                                      { 
                                    ?>
                                   @if(isset($TopUpDataSet) && !empty($TopUpDataSet))
                                   <tr>
                                        <td>Top Up</td>
                                        <td>@if(isset($TopUpDataSet->amount)) {{$currecnySymbol}}{{$TopUpDataSet->amount}} @else N/A @endif</td>
                                        <td>@if(isset($TopUpDataSet->credits)) {{$TopUpDataSet->credits}} @else N/A @endif</td>

                                   </tr>
                                   @else
                                  <tr><td colspan="3"><div class="alert alert-danger">Sorry no current top up found !. </div></td></tr>

                                   @endif
                                   <?php
                                    } 
                                   ?>

                                </tbody>
                             </table>
                        </div>
                    </div>
                @endcan

                {{-- @if(isset($userPlan) && $userPlan!='') --}}
                @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                    @can(config('permissions.data.switch-plan.name'))
                    <!-- Leads Summary -->
                        <div id="new_plan" class="tab-pane <?php echo $plan_active?>">
                            <div class="plains_main_div">
                                @if(isset($group_plan_credit->top_recharge_credit) && !empty($group_plan_credit->top_recharge_credit))
                                <h2 class="campaign_name">Current Group Plan.</h2>
                                @else
                                <h2 class="campaign_name">Choose your new plan to upgrade.</h2>
                                @endif
                                <div class="plan-section">
                                    {{ Form::open(array('url' => url('/sufflePlan'),'id' => 'sufflePlan-form','class' => 'plan-form','autocomplete' => 'off')) }}
                                    <div class="planPanel">
                                        <div class="row">
                                            @php
                                                $choosePlanId = '';
                                                $planplan_id  = '';
                                                $Class="first-plan";
                                            @endphp
                                            @if(!$plansData->isEmpty())
                                                @foreach($plansData as $plan)
                                                <?php
                                                if ($plan->plans->billingType == 2) {
                                                    $span = '<span class="planspan">Pay Per Minute</span>';
                                                    $Class = 'first-plan';
                                                } else if($plan->plans->billingType == 3) {
                                                    $span = '<span class="planspan">Trail Plan</span>';
                                                    $Class = 'first-plan second-plan';
                                                }
                                                $btnSelect              = 'Buy Now';
                                                $color                  = '#2961af';
                                                $acClass                = '';
                                                $selectedCurrentClass   = "checkPlainPrc";
                                                if(isset($userPlan->plan_id) && $plan->stripe_plan_id == $userPlan->plan_id && $userPlan->status == 'Active')
                                                {
                                                    $choosePlanId           = $plan->stripe_plan_id;
                                                    $btnSelect              = 'Current';
                                                    $acClass                = '';
                                                    $planplan_id            = $plan->plan_id;
                                                    $vClass                 = true;
                                                    $color                  = '#999';
                                                    $selectedCurrentClass   = "";
                                                    $currentPlanName        = $plan->plans->name.' '.$plan->plan_currency->currencySymbol.' '.$plan->amount;
                                                }
                                                $noc         = $plan->plans->no_of_contacts;
                                                $contactNow  = isset(\Auth::user()->user_plan->plans->no_of_contacts) ? \Auth::user()->user_plan->plans->no_of_contacts :0;
                                                $userContact = isset(\Auth::user()->user_contacts) ?\Auth::user()->user_contacts->count() : 0;
                                                $check       = 0;
                                                if ($userContact > 0 )
                                                {
                                                    if( $noc < $contactNow)
                                                    {
                                                        if ($userContact > $noc)
                                                        {
                                                            $check = 1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        $check = 0;
                                                    }
                                                }
                                                 $number_of_business=0; 
                                                if(isset($group_plan_credit->number_of_business))
                                                {
                                                    $number_of_business=$group_plan_credit->number_of_business;
                                                }
                                             $patterns             = array('/{contactNumber}/','/{creditNumber}/','/{businesNumber}/');
                                                $replacements         = array($plan->plans->no_of_contacts,$plan->credit,$number_of_business);
                                                $emailstring          = (new \App\Helpers\GlobalFunctions)->emailReplacement($patterns,$replacements,$plan->plans->description);
                                                $emailstring          = $emailstring['content'];
                                            ?>
                                                <div class="col-md-4">
                                                    <div class="{{$Class}}">
                                                        <h5>{{$plan->plans->name}}<br>
                                                        <?php if( $plan->amount == 99 ){?>
                                                            <span style="font-size:12px;display: block;text-transform: initial;">Most popular</span>
                                                        <?php } ?>
                                                        </h5>
                                                        <div class="price-dollar"  id="<?php echo $plan->plans->name.' '.$plan->plan_currency->currencySymbol.' '.$plan->amount?>">
                                                            @if($selectedCurrentClass == 'checkPlainPrc')
                                                                <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</span>
                                                                <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}/month</b></p>
                                                            @else
                                                                <br><br><br>
                                                                <h2 class="text-center">CURRENT PLAN</h2>
                                                                <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</span>
                                                                <p class="text-center"><b>{{ $plan->plan_currency->currencyCode }}/month</b></p>
                                                                <br><br>
                                                            @endif
                                                        </div>
                                                        <div class="plan-text">
                                                            <ul>
                                                                {!!html_entity_decode($emailstring)!!}
                                                            </ul>
                                                            @if($selectedCurrentClass == 'checkPlainPrc')
                                                                <div class="plan-btn">
                                                                    <a href="javascript:void(0)"  style="background:<?php echo $color; ?>" data-payId="{{$plan->stripe_plan_id}}" data-plainid="{{$plan->plan_id}}" data-mod="{{$check}}" class="<?php echo $selectedCurrentClass; ?> btn btn-primary btn-block {{$acClass}}">{{$btnSelect}}</a>
                                                                </div>
                                                            @else
                                                                <div class="plan-btn">
                                                                    <br><br>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($plansData) }}" />
                                            @else
                                                <p>No plans found..</p>
                                            @endif
                                            <input id="modelData" type="hidden" name="modelData" class="modelToken" value="0">
                                            <input type="hidden" name="choosePlanId" class="usedPlainId" value="{{trim($planplan_id)}}">
                                            <input type="hidden" name="payId" class="payId" value="{{trim($choosePlanId)}}"/>
                                            <input type="hidden" id="sub_id" name="sub_id" value="{{trim($sub_id)}}">
                                            <input type="hidden" id="currentPlanName" value="<?php echo $currentPlanName?>"/>
                                        </div>
                                          @if(empty($group_plan_credit))
                                        <div class="Bottom_button text-right">
                                            <button class="btn btn-primary btn-rounded nextBtn sufflePlan subPlanData" type="button">Save Changes</button>
                                        </div>
                                        @endif
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endcan
                @else
                    <tr><td colspan="10"><div class="alert alert-danger">Please set your card details first.</div></td></tr>
                @endif

                @if(isset($user_data->stripe_cust_id) && $user_data->stripe_cust_id!="")
                    <!-- Credit Recharge Tab -->
                    @can(config('permissions.data.credit-recharge.name'))
                        <div id="credit-recharge" class="tab-pane <?php echo $creditRechargeActive;?> ">
                            <?php if($selectTopupLevel){?>
                                <h3>Top-Up to supercharge your leads.</h3>
                           <p>An active plan includes an alert when call costs are running low. Your top-ups will ensure you never miss a lead.</p>
                            <?php } else {?>
                                <h2 class="campaign_name">Supercharge your leads with top-ups! Never miss an opportunity.</h2>
                                <p>Your top-up package is automatically triggered when your credit balance drops to <?php if(isset($group_plan_credit->top_recharge_credit)) echo $group_plan_credit->top_recharge_credit; else echo '25';?> credits. There's no need to manually top-up, once you have choosen top up level, your top-ups are automatic triggered as needed. Call credits do not expire and can be used at any time as long as you have an active monthly plan.</p>
                            <?php } ?>
                                {{--@if(!empty($planId)) --}}
                                    @if(isset($userPlan->status) && $userPlan->status!='Cancelled')
                                        <div class="plains_main_div">
                                            <div class="plan-section">
                                                {{ Form::open(array('url' => url('/recharge-credit'),'id' => 'recharge_form','class' => 'plan-form','autocomplete' => 'off')) }}
                                                    <div class="container">
                                                        <div class="row">
                                                            @php
                                                                $choosePlanId = '';
                                                            @endphp
                                                            @foreach($topupData as $plan)
                                                                <?php
                                                                    $btnSelect      = 'Select';
                                                                    $acClass        = '';
                                                                    $check          = 0;
                                                                    $patterns       = array('/{Credit_Amount}/');
                                                                    $replacements   = array($plan->credit);
                                                                    $emailstring    = preg_replace($patterns, $replacements,$plan->tops->description);
                                                                    $Class          = 'first-plan';
                                                                    $color          = '#2961af';
                                                                    $clickClass     = "checkTopUpPrc";
                                                                    if($plan->top_id==$TopUpId)
                                                                    {
                                                                        $btnSelect  = 'Current';
                                                                        $color      = '#999';
                                                                        $clickClass = "";
                                                                    }
                                                                ?>
                                                                <div class="col-md-4 plan_row">
                                                                    <div class="{{$Class}}">
                                                                        <h5>Top Up<br></h5>
                                                                        <div class="price-dollar" id="{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}">
                                                                            <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}</span>
                                                                        </div>
                                                                        <div class="plan-text">
                                                                            <ul>
                                                                                {!!html_entity_decode($emailstring)!!}
                                                                            </ul>
                                                                            <div class="plan-btn">
                                                                                <a href="javascript:void(0)"  style="background:<?php echo $color; ?>" data-price="{{ $plan->price }}" data-credit="{{ $plan->credit }}" data-plainid="{!! \Crypt::encrypt($plan->top_id)!!}" data-mod="{{$check}}" class="<?php echo $clickClass ?> btn btn-primary btn-block {{$acClass}} topPlanClass">{{$btnSelect}}</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                                <input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($topupData) }}" />
                                                        </div>
                                                        @if(empty($group_plan_credit))
                                                        <div class="Bottom_button text-right">
                                                            <button class="btn btn-rounded subTopUpPlanData" disabled="disabled"  target="#top-up-confirm-payment-modal" type="button">Save Changes</button>
                                                        </div>
                                                        @endif
                                                    </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-danger">Sorry you have cancelled a subscription.</div>
                                    @endif
                                {{-- @else --}}
                                    <!--<div class="alert alert-danger">Firstly purchase a plan.</div>-->
                                {{-- @endif --}}

                        </div>
                    @endcan
                @endif
                <!-- Credit Recharge Tab -->

                <!-- Cards Tab -->
                @can(config('permissions.data.cards.name'))

                    <div id="cards-setting" class="tab-pane <?php echo $cardsActive;?>">
                          <div class="row">
                    <div class="col-md-12">
                        <div class="tile">
                            <div class="latest-champaign-table">
                                <div class="tile-body   table-responsive recordsTable">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Business Name</th>
                                                <th>Card Name</th>
                                                <th>Card Last Digit </th>
                                                <th>Card Exp Month</th>
                                                <th>Card Exp Year</th>
                                                <th>Brand</th>
                                                <th>Default</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="show_record_card" class="show_records">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        <br>
                        <div class="update-password">
                            {{ Form::open(array('url'=>$url.'/save-card', 'id'=>'save-card')) }}
                            <?php $card = false; ?>
                            <div class="row">
                                <div class="formArea">
                                    @if(!empty($saveCardData) && isset($saveCardData))
                                        <?php
                                              $value=$saveCardData;
                                        //foreach ($output['data'] as $key => $value){ ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="cc-number" class="control-label">Card Holder Name
                                                        <span class="star">*</span>
                                                    </label>
                                                    <input type="text" class="input-lg form-control" name="newCardHolderName" value="{{$value->name}}" placeholder="Card Holder Name" required="">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label">Card expiry
                                                        <span class="star">*</span>
                                                    </label>
                                                    <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" name="newccExpiryMonth" autocomplete="cc-exp" value="{{$value->exp_month.'/'.$value->exp_year  }}" placeholder="mm / yy" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label">Default
                                                        <span class="star">*</span>
                                                    </label>

                                                    <?php
                                                        $is_default_zero=false;
                                                        $is_default_one=false;

                                                        if($value->is_default==0 || $value->is_default==null)
                                                        {
                                                            $is_default_zero=true;

                                                        }
                                                        else{

                                                            $is_default_one=true;

                                                        }
                                                    ?>

                                          {{ Form::radio('is_default',1,$is_default_one,array('class'=>'annEmail')) }} <span class="yes-text">Yes</span>
                                          {{ Form::radio('is_default',0,$is_default_zero,array('class'=>'annEmail')) }} <span class="yes-text">No</span>

                                                </div>
                                            </div>
                                            {!! Form::hidden('card',\Crypt::encrypt($value->id)) !!}
                                            {!! Form::hidden('business_id_data',\Crypt::encrypt($value->business_id)) !!}
                                            <?php
                                             //$card=true;
                                        //} ?>
                                    @else
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="newCardHolderName" class="control-label">Card Holder Name
                                                    <span class="star">*</span>
                                                </label>
                                                <input type="text" class="input-lg form-control" id="newCardHolderName" name="newCardHolderName" placeholder="Card Holder Name" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cc-number" class="control-label">Card number
                                                    <span class="star">*</span>
                                                </label>
                                                <input id="cc-number" type="tel" class="input-lg form-control cc-number mastercard identified" name="newcardNumber" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cc-exp" class="control-label">Card expiry
                                                    <span class="star">*</span>
                                                </label>
                                                <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" name="newccExpiryMonth" autocomplete="cc-exp" placeholder="mm / yy" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cc-cvc" class="control-label">Card CVC
                                                    <span class="star">*</span>
                                                </label>
                                                <input id="cc-cvc" type="tel" class="input-lg form-control cc-cvc" name="newcvvNumber" autocomplete="off" placeholder="•••" required="">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            @if($user_id==\Auth::user()->id)
                                <div class="col-md-12">
                                    <div class="btn-main">
                                       <!--  <a <?php if(!$card) {?> style="display:none"<?php }?> href="javascript:void(0)" class="btn btn-danger deleteRecord">Delete</a> -->
                                        <div class="setting-save">
                                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{ Form::close() }}
                    </div>
                @endcan
                <!--CARD END-->
            </div>
        </div>
    </div>


<!-- The Modal -->
<div class="modal" id="planDegrade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Important Instructions</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                {{ Form::open(array('url'=>$url.'/remove-contacts', 'id'=>'downgradeContacts')) }}
                <input type="hidden" name="planId" class="usedPlainId" value="">
                <h6 style="font-size: 14px"> When downgrading an account you are now able to have only 5 contact numbers. This may cause some of your existing campaigns to be PAUSED if you haven't already removed the excess phone numbers.
                    <br>Please visit the "Campaigns" tab and edit the campaigns shown in RED (if any)
                </h6>
                <div class="campaigns-modal-table">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Contact Name</th>
                                    <th>Contact Number</th>
                                    <th>Email</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=0; @endphp
                                @forelse(\Auth::user()->user_contacts as $contact)
                                    @php $i++; @endphp
                                    <tr @if(in_array($contact->id, $campContact)) class="green" @endif>
                                        <td>{{ $i }}</td>
                                        <td>{{ $contact->name }}</td>
                                        <td>{{ $contact->contact }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td><input type="checkbox" name="removeContact[]" value="{{ $contact->id }}" autocomplete="off"><span class="glyphicon glyphicon-ok"></span></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No record found !</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!-- Confirm Payment Modal -->
<div id="data_plan_confirm_payment_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Subscription Plan Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Your subscription plan is: <?php echo $sub_plan_name;?> and it will be activated after expried promo plan.
                <div class="text-right">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Confirm Payment Ends -->

<!-- Confirm Payment Modal -->
<div id="top-up-confirm-payment-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm top up change</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php if($selectTopupLevel){?>
                    <h6>Your top up plan amount is <span id="confirmTopUpAmount"></span>. Are you sure you <br>want to buy this plan when your credits reach 25 or <br>less ?</h6>
                <?php } else {?>
                    <h6>Your top up plan amount is <span id="confirmTopUpAmount"></span>.<br>Are you sure you want to buy your top up plan ?</h6>
                <?php } ?>
                <div class="text-right">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary confirmTopBtn">Confirm</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Confirm Payment Ends -->


<div class="modal fade" id="business_plan_pop_up" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h6>You need to purchase a plan for access to your business.</h6>
                <div class="text-right">
                    <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="business_top_pop_up" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h6>You need to choose a top up to get automated recharge when your credits goes below 25.</h6>
                <div class="text-right">
                    <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm_delete" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Card</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>As you cannot recover it, Are you sure you want to delete this card ?.</h6>
                <div class="text-right">
                    <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary deleteCard">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($_GET['plan_active'])) { ?>
    <script>
        $("#business_plan_pop_up").modal('show');
        $("#business_plan_pop_up").on("hidden.bs.modal", function () {
            $('.modal-backdrop').remove();
        });
    </script>
<?php } ?>
<?php if( isset($_GET['top_active']) && $_GET['top_active'] == 'active' ) { ?>
    <script>

         setTimeout(function(){
            $("#tab-recharge").trigger('click');
         }, 1000);
        $("#business_top_pop_up").modal('show');
        $("#business_top_pop_up").on("hidden.bs.modal", function () {
            $('.modal-backdrop').remove();
        });
    </script>
<?php } ?>
<script type="text/javascript">
    var route_url="<?php echo $url?>";
    var get_data_url=route_url+'/fetch-business-card-list';
    var confirm_delete=route_url+'/delete-business-card';
    var default_confirm_delete=route_url+'/set-default-business-card';
</script>
<script src="{{ asset('assets/js/fetch.js') }}"></script>
<script type="text/javascript">

    function fetchAllRecordData()
    {
              jQuery.ajax({
                                  headers: {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                                  type: "GET",
                                  url:get_data_url,
                                  cache: false,
                                  dataType: "json",
                                   beforeSend: function() {
                                      showLoader();
                                  },
                                  success: function (result) {
                                     hideLoader();
                                     jQuery(".show_records").html(result.data);
                                     //jQuery('#'+dataTableId).DataTable({"ordering": false});
                                  },
                                  complete: function() {
                                   hideLoader();
                                  },
                                  error: function (xhr) {
                                     hideLoader();
                                     //console.log(xhr);
                                  }
                      });
     }

    var modalConfirm = function(callback){

			          $(document).on("click",'.business_confirm_delete_pop_up',function(){
			               $('#business-confirm-delete-modal').modal('show');
			               $("#business_yes").attr('data-id',$(this).attr('data-id'));
			          });
			          $(document).on("click",'#business_yes',function(){
			             callback($(this).attr('data-id'));
			             $("#business-confirm-delete-modal").modal('hide');
			          });

     };
    modalConfirm(function(confirm)
    {


        if(confirm)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'get',
                url:confirm_delete+'/'+confirm,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                      fetchAllRecordData();


                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                    $("#show_record_card").html("");
                    $('#confirm-delete-modal').modal('hide');
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                    });
                }
            });
        }
    });



     var modalDefaultConfirm = function(callback){

			          $(document).on("click",'.default_business_confirm_pop_up',function(){
			               $('#default_business_confirm_pop_up_model_data').modal('show');
			               $("#business_default_yes").attr('data-id',$(this).attr('data-id'));
			          });

			          $(document).on("click",'#business_default_yes',function(){
			             callback($(this).attr('data-id'));
			             $("#default_business_confirm_pop_up_model_data").modal('hide');
			          });

     };
    modalDefaultConfirm(function(setconfirm)
    {



        if(confirm)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'get',
                url:default_confirm_delete+'/'+setconfirm,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                    tostrsuccess(data.message);
                      fetchAllRecordData();

                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                    // $("#show_record_card").html("");
                    $('#confirm-delete-modal').modal('hide');
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                    });
                }
            });
        }
    });




    $('li.nav-item').on('click',function()
    {
        $(this).nextAll().find('a').removeClass('active_prev');
        $(this).prevAll().find('a').addClass('active_prev');
    })

    // save-card
    $("#save-card").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            cardHolderName:
            {
                required: true,
                noSpace: true,
            },
            newCardHolderName:
            {
                required: true,
                noSpace: true,
            }
        },
        messages:{
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
</script>

<script>
    $('.upgrate-take-tour').click(function(){
        $('.upgrate-take-tour-show').removeClass('d-none');
    })

    $(document).on('click','.tour-gotit-upgrate',function(){
        $('.upgrate-take-tour-show').addClass('d-none');
    })

    $(document).on('click','.upgrate-now-button',function()
    {
        $('.swith-plan-trigger').trigger('click');
        $('.upgrate-take-tour-show').addClass('d-none');
    })
</script>
