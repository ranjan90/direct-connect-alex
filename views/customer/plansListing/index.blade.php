@extends('layouts.customer')
@section('content')

<?php 

$business_card_edit="";
 if(isset($_GET['business_card_edit']) && !empty($_GET['business_card_edit']))
              {
                  $business_card_edit=$_GET['business_card_edit'];

              }
?>
<div class="fetch_plan_data">

</div>
<?php
 $plan_active=isset($_GET['plan']) ? 'active':0;
 $top_active = isset($_GET['topup']) ? 'active' : 0;
?>
@section('js')

<script src="{{ asset('assets/'.config("app.frontendtemplatename").'/js/jquery.payment.js') }}"></script>
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>

<script type="text/javascript">

  var plan_active="<?php echo $plan_active?>";
  var top_active="<?php echo $top_active?>";
   var business_card_edit="<?php echo $business_card_edit ?>";
  
  /* Method :getContactData */
function fetchPlanData()
     {
             $.ajax({
                url                : prefixUrl + '/fetch-plan-data?business_id='+localStorage.getItem('business_data_id')+'&plan_active='+plan_active+'&top_active='+top_active+'&business_card_edit='+business_card_edit,
                type               : 'POST',
                dataType           : 'html',
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function() {
                      showLoader();
                },
                complete   : function() {
                    hideLoader();
                },
                success    : function(response) {
                    $('.fetch_plan_data').html(response);
                     hideLoader();
                     if(localStorage.getItem("upgratenow") != null && localStorage.getItem("upgratenow")==1) {
                          localStorage.setItem("upgratenow", 0);
                          $(document).find('.swith-plan-trigger').trigger('click');
                      }
                }
            });
}
if(storge_customer_id=="")
{
   fetchPlanData();
}
</script>

<script type="text/javascript">
$('a.alert-danger').on('click',function(){ $('#tab-recharge').trigger('click');})

jQuery(function($) {
  $(document).find('[data-numeric]').payment('restrictNumeric');
  $(document).find('.cc-number').payment('formatCardNumber');
  $(document).find('.cc-exp').payment('formatCardExpiry');
  $(document).find('.cc-cvc').payment('formatCardCVC');
  $.fn.toggleInputError = function(erred) {
    this.parent('.form-group').toggleClass('has-error', erred);
    return this;
  };
});
$(document).on('click','.checkTopUpPrc',function(){
        var Obj         =   $(this);
        $(".checkTopUpPrc").removeClass('activeCls');
        $(".checkTopUpPrc").text('Select');
        // $('.checkTopUpPrc').css('background','#fff');
        // $('.checkTopUpPrc').css('color','#2961af');
        $('.checkTopUpPrc').css('background','#223c4b');
        Obj.addClass('activeCls');
        Obj.text('Selected');
        Obj.css('background','#2961af');
        Obj.css('color','#fff');
        $(".subTopUpPlanData"). prop("disabled", false);
  });

$(document).on('click','.subTopUpPlanData', function(){

        var price = $(document).find(".activeCls").closest('.plan_row').find('.price-dollar').attr('id');
        $("#confirmTopUpAmount").text(price);
        $("#top-up-confirm-payment-modal").modal('show');
});

$(document).on('click','.confirmTopBtn', function(){
  $.ajax({
          url                : siteroot + '/recharge-credit',
          type               : 'post',
          data               : { rechargeAmt:$(document).find(".activeCls").attr('data-credit'),rechargeStripeAmt:$(document).find(".activeCls").attr('data-price'),top_id:$(document).find(".activeCls").attr('data-plainid'),'business_id':localStorage.getItem('business_data_id')},
          headers        : {
              'X-CSRF-Token': $('meta[name="_token"]').attr('content')
          },
          dataType     : "json",
          beforeSend : function() {
              $(".loader_div").show();
          },
          complete   : function() {
              $(".loader_div").hide();
          },
          success    : function(response) {
                 $(".loader_div").hide();
             //tostrsuccess(response.success_message);
             if(response.success){

                     $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                      });
                       window.location.href =prefixUrl +'/plans';
            }
             else if (response.success==false)
                                  {
                                       $.toast({
                                        heading             : 'Error',
                                        text                : response.error_message,
                                        loader              : true,
                                        loaderBg            : '#fff',
                                        showHideTransition  : 'fade',
                                        icon                : 'error',
                                        hideAfter           : response.delayTime,
                                        position            : 'top-right'
                                      });
                                  }
            else{
              $.toast({
                heading             : 'Error',
                text                : response.error_message,
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'error',
                hideAfter           : response.delayTime,
                position            : 'top-right'
              });
           }
              $("#top-up-confirm-payment-modal").modal('hide');
           

          },
          complete: function() {
             
                 hideLoader();
         },
          error: function (xhr) {
             hideLoader();
             $.toast({
                heading             : 'Error',
                text                : response.error_message,
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'error',
                hideAfter           : response.delayTime,
                position            : 'top-right'
              });
             //console.log(xhr);
          }
    });
 });

</script>

@endsection
@endsection
