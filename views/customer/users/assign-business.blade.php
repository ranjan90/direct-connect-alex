@extends('layouts.customer')
@section('content')

  <div class="container-fluid">

	<div class="row">
	<div class="col-md-12">
	<div class="heading-campaigns">
	  <h2><?php echo ucfirst($output['data']->firstName).' '.ucfirst($output['data']->lastName);?></h2>
	  <div class="create-new-campaign-btn">
	      <a class="btn btn-primary back-now" href="{{$url.'/user-management'}}">Back</a>
	  </div>
	</div>
	</div>
	</div>
    <!-- Icon Cards-->
    <div class="row">
      <div class="col-md-12">
        {{ Form::open(array('url'=>$url.'/assign-business-form','id'=>'assignBusinessForm','autocomplete' => 'off')) }}
          <div class="row">
            <div class="col-md-4 plans">
                <div class="form-group"></div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <label for="">Business<span class="star">*</span></label>
                    <select name="business_id" Class="form-control">
                         <option value ="">Select Business</option>
                         @foreach($output['ownersBusinesss'] as $business)
                            <?php if( isset($business->businessDropdown)) {
                                if( isset($output['data']) && !empty($output['data'])){?>
                                <option <?php if( $business->business_id == $output['businessId'] ){ ?>selected<?php } ?> value="{{$business->businessDropdown->id}}">{{ucfirst($business->businessDropdown->business_name)}}</option>
                            <?php } else {?>
                                <option value="{{$business->businessDropdown->id}}">{{ucfirst($business->businessDropdown->business_name)}}</option>
                            <?php } } ?>
                        @endforeach
                    </select>
                    @if(isset($userId))
                    {!! Form::hidden('userId',$userId) !!}
                    @else
                    {!! Form::hidden('userId',null) !!}
                    @endif
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-right">
                {!!  Form::submit('Save',['class'=>'btn btn-primary']) !!}
            </div>
          </div>
          {{ Form::close() }}
    </div>
</div>
<br/>
<div class="latest-champaign-table">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Business</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @can(config('permissions.data.assign-business.name'))
                    <?php $i=0;?>
                    @forelse($output['assignedBusinesss'] as $assignedBusiness)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ ucfirst($assignedBusiness['business_dropdown']['business_name']) }}</td>
                            <td>
                                 @can(config('permissions.data.set-user-permission.name'))
                                    <a href="{{ $url.'/set-permissions/'.Crypt::encrypt($userId.'---'.$assignedBusiness['business_id']) }}" data-toggle="tooltip" title="Set Permissions"><span><i class="fa fa-lock"></i> </span></a>
                                @endcan
                                @can(config('permissions.data.delete-assigned-business.name'))
                                    <a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-Unassgin-business-modal" id="{{ $assignedBusiness['id'] }}" data-table="{{ Crypt::encrypt('business_users') }}"><span  data-toggle="tooltip" title="Unassgin Business" data-placement="bottom"><span><i class="fa fa-trash"></i> </span></a>
                                @endcan

                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3" align="center">No business assigned.</td>
                        </tr>
                    @endforelse
                @endcan
            </tbody>
        </table>
    </div>
</div>

@endsection
