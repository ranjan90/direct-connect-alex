@extends('layouts.customer')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="heading-campaigns">
                <h2>Permissions</h2>

                <?php if(!empty($business) && isset($business[0]) ) {?>

                <div class="create-new-campaign-btn">
                    <a class="btn btn-primary back-now" href="{{ $url.'/assign-business/'.Crypt::encrypt($user->id)}}">Back</a>
                </div>
            </div>
        </div>
    </div>
<!-- Icon Cards-->
    <div class="row">
        <div class="col-md-6">
              <b> Business :  <?php echo ucfirst($business[0]->business_name);?></b>
          </div>
           <div class="col-md-6 text-right">
               <?php if($user->firstName == '' && $user->customer_id == 0 ){?>
                  <b>User Email : {{$user->email}}</b>
                <?php } else {?>
                  <b>User Name : {{ucfirst($user->firstName).' '.ucfirst($user->lastName)}}</b>
                 <?php } ?>
         </div>
              </div>

            <?php } ?>
            {{ Form::model($user,array('url' => $userData['form_action'],'id' => 'permission-form','class' => 'plan-form','autocomplete' => 'off')) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="row">
                            <div class="col-md-12 plans">
                                <div class="form-group">
                                    <?php if($user->firstName == '' && $user->customer_id == 0 ){?>
                                        {{ Form::hidden('name', $user->email, array('class' => 'form-control','readonly'=>'true')) }}
                                    <?php } else {?>
                                        {{ Form::hidden('name', $user->firstName, array('class' => 'form-control','readonly'=>'true')) }}
                                    <?php } ?>
                                    {!!  Form::hidden('user_id',$userData['record_id']) !!}
                                </div>
                            </div>
                        </div>
                        <style>
                        .form-group.has-error label {
                            color: #dd4b39;
                        }
                        .permission-from-group.has-error label.has-error{
                            display: none !important;
                        }
                        .campaignpermissiontype{
                            margin-right: 10px;
                            cursor: pointer;
                        }
                        </style>
                        <?php $campPermission   = true;
                        $campaignpermissiontype = '';
                        if( !empty($CampaignPermissions) )
                        {
                            if( isset($CampaignPermissions[0]) )
                            {
                                $campaignpermissiontype = $CampaignPermissions[0]->permission_type;
                            }
                        }
                        ?>
                        <div class="form-group permission-from-group">
                            @foreach ($permissions as $permission_key=>$permission_value)
                                <?php if( $permission_key == 'Company Contacts' && $campPermission == true ){ ?>
                                    <p>
                                        <input type="radio" class="campPermissionTypeRadio" value="1" <?php if($campaignpermissiontype == '1'){ echo 'checked'; } ?> name="campaignpermissiontype" id="vieweditcamps" />&nbsp;<label for="vieweditcamps" class="campaignpermissiontype" >View/Edit all campaigns</label>
                                        <input type="radio" class="campPermissionTypeRadio" value="2" <?php if($campaignpermissiontype == '2'){ echo 'checked'; } ?> name="campaignpermissiontype" id="viewcamps" />&nbsp;<label for="viewcamps" class="campaignpermissiontype" >View all campaigns</label>
                                        <input type="radio" class="campPermissionTypeRadio" value="3" <?php if($campaignpermissiontype == '3'){ echo 'checked'; } ?> name="campaignpermissiontype" id="editcamps" />&nbsp;<label for="editcamps" class="campaignpermissiontype" >Edit all campaigns</label>
                                        <input type="radio" class="campPermissionTypeRadio" value="4" <?php if($campaignpermissiontype == '4'){ echo 'checked'; } ?> name="campaignpermissiontype" id="choosecamps" />&nbsp;<label for="choosecamps" class="campaignpermissiontype" >Choose specific</label>
                                    </p>
                                    <div class="form-group permission-from-group" id="customCampPermissionContainer" <?php if($campaignpermissiontype != '4'){?> style="display:none" <?php } ?>>
                                        <?php if( !empty($camps) ){?>
                                            <fieldset style="border:1px solid;padding:10px;">
                                            <legend style="width:auto;font-weight: initial;">Custom permissions</legend>
                                            <?php
                                                foreach ($camps as $key => $value) { ?>
                                                    <div class="col-md-3" style="float:left">
                                                        <p style="margin-bottom:5px;"><?php echo ucfirst($value['title']);?></p>
                                                        <p>
                                                            <?php
                                                                $campFoundKeys = array();
                                                                $editChecked   = $viewChecked = false;
                                                                if( !empty($CampaignPermissions) )
                                                                {
                                                                    $campidsarr     = array_column($CampaignPermissions, 'camp_id');
                                                                    $campFoundKeys  = array_keys($campidsarr, $value['id']);
                                                                    if( !empty($campFoundKeys) )
                                                                    {
                                                                        foreach ($campFoundKeys as $keyc => $valuec)
                                                                        {
                                                                            $current        = $CampaignPermissions[$valuec];
                                                                            $action_type    = $current->action_type;
                                                                            if( $action_type == 'view' )
                                                                                $viewChecked = true;
                                                                            else if( $action_type == 'edit' )
                                                                                $editChecked = true;
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            <input type="checkbox" <?php if($viewChecked){ echo "checked"; } ?> name="choosedViewCamp[]" value="<?php echo $value['id'];?>" id="choosedViewCamp_<?php echo $value['id'];?>" />&nbsp;<label for="choosedViewCamp_<?php echo $value['id'];?>" class="campaignpermissiontype" >View</label>
                                                            <input type="checkbox" <?php if($editChecked){ echo "checked"; } ?> name="choosedEditCamp[]" value="<?php echo $value['id'];?>" id="choosedEditCamp_<?php echo $value['id'];?>" />&nbsp;<label for="choosedEditCamp_<?php echo $value['id'];?>" class="campaignpermissiontype" >Edit</label>
                                                        </p>
                                                    </div>
                                                <?php } ?>
                                            </fieldset>
                                            <?php } ?>
                                    </div>
                                    <div class="clearfix" style="margin-bottom:15px;"></div>
                                <?php $campPermission = false;
                                } ?>
                                <h4>{{$permission_key}}</h4>
                                <hr style="border-top: 1px solid #a5a5a6;margin-top:0px;margin-bottom:10px;" />
                                <div class="row">
                                    @foreach($permission_value as $sub_permission_key =>$sub_permission_value)
                                        <div class="form-group permission-from-group col-md-3">
                                            {{Form::checkbox('permissions[]',  $sub_permission_value->id, in_array($sub_permission_value->id, $userPermissions) ) }}
                                            {{Form::label($sub_permission_value->title, $sub_permission_value->title) }}<br>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            @if(isset($recordId))
                {!!  Form::submit('Update',['class'=>'btn btn-primary']) !!}
            @else
                {!!  Form::submit('Submit',['class'=>'btn btn-primary']) !!}
            @endif
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
