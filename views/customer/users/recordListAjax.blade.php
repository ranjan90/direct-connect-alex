<div class="latest-champaign-table"><div class="table-responsive"><table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">User Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Role</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
                @can(config('permissions.data.view-user.name'))
                <?php $i=0; ?>
                @forelse($users as $user)
                <?php $i++;
                    $permissionType  = '';
                    $permissionType  = Helper::checkCustomPermissionBusinessWise( $user->user_id, $user->role_id, $business_id);
                 ?>
                <tr>
                   <td>{{ $page+$i }}</td>
                  <td>@if(isset($user->firstName)){{ ucfirst($user->firstName) }} @endif @if(isset($user->lastName)){{ ucfirst($user->lastName) }} @endif</td>
                  <td>@if(isset($user->email)){{ $user->email }} @endif</td>
                  <td>@if(isset($user->phoneNo)){{ $user->phoneNo }} @endif</td>
                  <td>
                      <?php
                        if( $user->name == 'business-admin' ){
                            echo 'Admin';
                            if( $permissionType != '' )
                                echo $permissionType;
                        } else {
                            echo ucfirst($user->name);
                            if( $permissionType != '' )
                                echo $permissionType;
                        } ?>
                  </td>
                <td>
                    @can(config('permissions.data.edit-user.name'))
                    @if($user->customer_id != 0)
                        <a href="{{ $url.'/edit-user/'.Crypt::encrypt($user->user_id)}}" data-toggle="tooltip" title="Edit" data-placement="bottom"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a>
                    @endif
                    @endcan
                    @can(config('permissions.data.delete-user.name'))
                        <a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $user->user_id }}" data-table="{{ Crypt::encrypt('users') }}"><span  data-toggle="tooltip" title="Delete" data-placement="bottom"><img src="{{asset('assets/customer/img/delete.png')}}"></span></a>
                    @endcan
                    @can(config('permissions.data.assign-business.name'))
                        <a href="{{ $url.'/assign-business/'.Crypt::encrypt($user->user_id)}}" data-toggle="tooltip" title="Assign Business" data-placement="bottom"><i class="fa fa-tasks" aria-hidden="true"></i></a>
                    @endcan
                </td>
                </tr>
                @empty
                <tr>
                  <td colspan="7" align="center">No users available.</td>
                </tr>
                @endforelse
                  @endcan
              </tbody>
</table>
</div>
</div>
{{$users->links()}}
