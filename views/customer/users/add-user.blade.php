@extends('layouts.customer')
@section('content')
<style>
.multiselect-container>li>a>label>input[type=checkbox]{
    vertical-align: middle;
    margin-bottom: 0px !important;
}
</style>
<div class="row">
<h2 class="main_title_head">Create Users</h2>
</div>
  <div class="container-fluid">
    <!-- Icon Cards-->
    <div class="row">
      <div class="col-md-12">
          @if(isset($output['data']))
            {{ Form::model($output['data'],array('url'=>$url.'/add-user','id'=>'addUserForm','autocomplete' => 'off')) }}
          @else
            {{ Form::open(array('url'=>$url.'/add-user','id'=>'addUserForm','autocomplete' => 'off')) }}
          @endif
          <div class="row">
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!--label for="">First Name<span class="star">*</span></label-->
                    {!!  Form::text('firstName',null,['placeholder'=>"First Name",'class'=>'form-control']) !!}
                    @if(isset($recordId))
                    {!! Form::hidden('recordId',$recordId) !!}
                    @else
                    {!! Form::hidden('recordId',null) !!}
                    @endif
                    {!! Form::hidden('customer_id',$userId) !!}
                </div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!--label for="">Last Name<span class="star">*</span></label-->
                    {!!  Form::text('lastName',null,['placeholder'=>"Last Name",'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!--label for="">Email<span class="star">*</span></label-->
                    {!!  Form::text('email',null,['placeholder'=>"Email",'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!-- label for="">Phone<span class="star">*</span></label-->
                    {!!  Form::text('phoneNo',null,['placeholder'=>"Phone",'class'=>'form-control','id'=>'phoneNo']) !!}
                </div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!--label for="">Country<span class="star">*</span></label-->
                    <select name="country" Class="form-control">
                         <option value ="">Select Country</option>
                         @foreach($output['country'] as $country)
                            <?php if( isset($output['data']) && !empty($output['data'])){?>
                                <option <?php if( $country->id == $output['data']->country ){ ?>selected<?php } ?> value="{{$country->id}}">{{ucfirst($country->name)}}</option>
                            <?php } else {?>
                                <option value="{{$country->id}}">{{ucfirst($country->name)}}</option>
                            <?php } ?>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4 plans">
                <div class="form-group">
                    <!--label for="">Select Role<span class="star">*</span></label-->
                    <select name="Role" Class="form-control">
                    <option value ="">Select Role</option>
                    @foreach($output['role'] as $role)
                        <?php if( isset($output['data']) && !empty($output['data']['roles'])){?>
                            <option <?php if( $role->id == $output['data']['roles']['0']->id ){ ?>selected<?php } ?> value="{{$role->id}}">
                                <?php if( $role->name == 'business-admin' ){ echo 'Admin'; } else { echo ucfirst($role->name); } ?>
                            </option>
                        <?php } else {?>
                            <option value="{{$role->id}}"><?php if( $role->name == 'business-admin' ){ echo 'Admin'; } else { echo ucfirst($role->name); } ?></option>
                        <?php } ?>
                   @endforeach
                   @if(isset($userId))
                   {!! Form::hidden('userId',$userId) !!}
                   @else
                   {!! Form::hidden('userId',null) !!}
                   @endif
                   </select>
                </div>
            </div>
            @if(!isset($recordId))
            <div class="col-md-4 plans business-multiple-select">
                <div class="form-group">
                    <select name="business_idd[]" id="business_id_multiselect" multiple="multiple" Class="form-control">
                         @foreach($ownersBusinesss as $business)
                            <?php if( isset($business->businessDropdown)) {?>
                                <option value="{{$business->businessDropdown->id}}">{{ucfirst($business->businessDropdown->business_name)}}</option>
                            <?php }  ?>
                        @endforeach
                    </select>

                </div>
            </div>
            @endif
            </div>

          <div class="row">
            <div class="col-md-12 text-right">
              <a class="float-left backStepBtn back-now" href="{{$url.'/user-management'}}"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
              @if(isset($recordId))
                {!!  Form::submit('Update',['class'=>'btn btn-warning']) !!}
              @else
                {!!  Form::submit('Save Changes',['class'=>'btn btn-warning']) !!}
              @endif
            </div>
          </div>
          {{ Form::close() }}
        </div>






</div>
<link href="{{ asset('assets/customer/css/bootstrap-multiselect.css') }}" rel="stylesheet">
@section('js')
<script type="text/javascript" src="{{ asset('assets/customer/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript">
            $(document).ready(function(){
                    $('#phoneNo').keyup(function(){
                        var value = $('#phoneNo').val();
                        if(value != '' || value == undefined){
                            $('#phoneNo').val(parseInt(value));
                        }else{
                            $('#phoneNo').val('');
                        }
                 });

                 $('#business_id_multiselect').multiselect({
                     nonSelectedText:'Select Business',
                     numberDisplayed: 1
                 });
                  $('button[class="multiselect dropdown-toggle btn btn-default"]').removeAttr('title');
             });
    </script>
@endsection
@endsection
