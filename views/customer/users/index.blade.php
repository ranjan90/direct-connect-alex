@extends('layouts.customer')
@section('content')
<div class="row">
<h2 class="main_title_head">Users <i class="fas fa-info-circle" id="permissionsInfo" style="float:right;cursor:pointer" data-toggle="tooltip" title="View default permissions" ></i></h2>
</div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->
<div class="row check">
	<div class="col-md-12">
	<div class="heading-campaigns">
	  <h2 class="campaign_name">View and manage permissions of your team members.</h2>
	  <div class="create-new-campaign-btn">
      @can(config('permissions.data.add-user.name'))
      @if(empty($userId))
		    <a href="{{$url.'/add-new-user'}}" class="btn btn-primary create-latest-campaign">Create new user</a>
      @else
        <a href="{{ $url.'/add-new-user/'.$userId}}" class="btn btn-primary create-latest-campaign">Create new user</a>
      @endif
      @endcan

	  </div>
	</div>
  <p>You can control which people can access your leads by customising their settings. </p>
  <p>Everyone who works in team can have a different role depending on what they need to work on. Only an admin can assign roles and change others’ roles. We recommend you review access settings regularly. </p>
	</div>
	</div>
    <!-- Icon Cards-->
    <!--  -->
    <div class="row">

      <div class="col-md-12 contact_listing">

      </div>
    </div>
</div>
@section('js')
<script type="text/javascript">
   var sendData={uid:"<?php echo $uid ?>"};
   /* Method :getUers */
   function getUsersList()
   {
       $.ajax({
          url                : prefixUrl + '/fetch-all-users?business_id='+localStorage.getItem('business_data_id'),
          type               : 'GET',
          dataType           : 'html',
          data:              sendData,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          },
          beforeSend : function() {
                showLoader();
          },
          complete   : function() {
              hideLoader();
          },
          success    : function(response) {
              $('.contact_listing').html(response);
               hideLoader();

          }
       });
    }
    if(storge_customer_id=="")
    {
        getUsersList();
    }
    var paginationClass='contact_listing';
</script>
@endsection
@endsection
