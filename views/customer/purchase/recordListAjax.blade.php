<div id="plan-history" class="tab-pane  @if(isset($_GET['purchase'])) active @endif">
   <div class="table-responsive latest-champaign-table">

         <table class="table">
           <thead>
             <tr>
               <!-- <th scope="col">Sno.</th> -->
               <th scope="col" style="width:18%;">Plan name</th>
               <th scope="col">Date</th>
               <th scope="col" style="width:16%;">Valid Till</th>
               <th scope="col" style="width:17%;">Minutes per month</th>
               <th scope="col" style="width:17%;">Leads per month</th>
               <th scope="col" style="width:17%;">No of contacts</th>
               <th scope="col" style="width:16%;">Amount</th>
               <!-- <th scope="col"></th> -->
             </tr>
           </thead>
           <tbody>

             <!-- <?php $i=0; ?> -->
             @forelse($purchaseHistory as $value)
             @if(isset($value->users->user_plan->plans) )
             <?php $i++; ?>
             <tr class="tr{{$value->subscr_id}}">
                 <td>{{  $i +$purchase }}</td>
                 <?php if ($value->usersPlans): ?>
                   <td>{{  $value->usersPlans->name}}</td>
                   <td>{{  gmdate("d/m/Y", ($value->startDate))}}</td>
                   <td>{{  gmdate("d/m/Y", ($value->endDate)) }}</td>
                   <td>{{$value->usersPlans->minutes_per_month}}</td>
                   <td>{{$value->usersPlans->leads_per_month}}</td>
                 <?php else: ?>
                   <td>Credit balance</td>
                   <td>{{  gmdate("d/m/Y", ($value->startDate))}}</td>
                   <td>{{  gmdate("d/m/Y", ($value->endDate)) }}</td>
                   <td>NA</td>
                   <td>NA</td>
                 <?php endif; ?>
                 <td>{{$value->usersPlans->no_of_contacts}}</td>
                 <td><span class="dollar"></span>{{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}}</td>
             </tr>
             @else
             <tr>
               <td colspan="7">No Purchase History Available !</td>
             </tr>
             @endif

             @empty
             <tr>
               <td colspan="7">No Purchase History Available !</td>
             </tr>
             @endforelse
           </tbody>


         </table>
         @if(count($purchaseHistory) > 0 )
         {{$purchaseHistory->links()}}
         @endif
     </div>
</div>
<!-- Leads Summary -->
<div id="balance-history" class="tab-pane @if(isset ($_GET['charge']) ) active @endif">
      <div class="table-responsive recordsTable">
           <div class="table table-striped table-hover">
             <table class="table">
               <thead>
                 <tr>
                   <!-- <th scope="col">Sno.</th> -->
                   <th scope="col">TopUp Amount</th>
                   <th scope="col">TopUp Credit</th>
                   <th scope="col">Date</th>
                 </tr>
               </thead>
               <tbody>

                 <?php $i=0; ?>
                 @forelse($charge as $value)
                 @if(isset($value->users->user_plan->plans) )
                 <?php $i++; ?>
                 <tr>
                   <!-- <td>{{ $i + $charges}}</td> -->
                   <td><span class="dollar"></span>{{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}}</td>
                   <td>{{ $value->credit_amt}}</td>
                   <td>{{  gmdate("d/m/Y", ($value->startDate))}}</td>
                 </tr>

                 @endif

                 @empty
                 <tr>
                   <td colspan="3">No record found !</td>
                 </tr>
                 @endforelse
               </tbody>


             </table>
             @if(count($charge) > 0 )
             {{$charge->links()}}
             @endif
           </div>
         </div>
   </div>
