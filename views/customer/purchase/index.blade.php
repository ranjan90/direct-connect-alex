@extends('layouts.customer')
@section('content')
<div class="fetch_purchase_data recordsTable"></div>
@section('js')
<script type="text/javascript">
  /* Method :getContactData */
function fetchPurchaseData()
     {
             $.ajax({
                url                : prefixUrl + '/fetch-purchase-history?business_id='+localStorage.getItem('business_data_id'),
                type               : 'POST',
                dataType           : 'html',
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function() {
                      showLoader();
                },
                complete   : function() {
                    hideLoader();
                },
                success    : function(response) {
                    $('.fetch_purchase_data').html(response);
                     hideLoader();

                }
            });
}
if(storge_customer_id=="")
{
   fetchPurchaseData();
}
</script>
@endsection
@endsection
