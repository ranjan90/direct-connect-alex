<?php $purchaseActive = $balanceActive = '';
if(\Auth()->user()->can('plan-history-list') && \Auth()->user()->can('balance-history-list'))
    $purchaseActive = " active";
else if(\Auth()->user()->can('plan-history-list'))
    $purchaseActive = " active";
else if(\Auth()->user()->can('balance-history-list'))
    $balanceActive = " active";
?>
<div class="row">
<h2 class="main_title_head">Purchase History</h2>
</div>
<div class="container-fluid">
<!-- Breadcrumbs-->

	<ul class="nav nav-tabs reporting-tabs" style="display:none;">
                    @can(config('permissions.data.plan-history-list.name'))
                   <li class="nav-item">
                       <a class="leadTabs nav-link  <?php echo $purchaseActive;?>  " data-toggle="tab" href="#plan-history">PLAN HISTORY </a>
                   </li>
                   @endcan
                   @can(config('permissions.data.balance-history-list.name'))
                   <li class="nav-item">
                       <a class="leadTabs nav-link <?php echo $balanceActive;?> " data-toggle="tab" href="#balance-history"> BALANCE HISTORY</a>
                   </li>
                   @endcan
               </ul>
			    <?php /* <div class="tab-content recordsTable" style="display:none;">
                    @can(config('permissions.data.plan-history-list.name'))
                   <div id="plan-history" class="tab-pane   <?php echo $purchaseActive;?> ">
        				      <div class="table-responsive latest-champaign-table">

                            <table class="table">
                              <thead>
                                <tr>
                                  <!-- <th scope="col">Sr.</th> -->
                                  <th scope="col" style="width:18%;">Plan name</th>
                                  <th scope="col" class="Date_box">Date</th>
                                  <th scope="col" class="Date_box" style="width:16%;">Valid Till</th>
                                  <th scope="col" style="width:17%;">Minutes per month</th>
                                  <!-- <th scope="col" style="width:17%;">Leads per month</th> -->
                                  <th scope="col" style="width:17%;">No of contacts</th>
                                  <th scope="col" style="width:16%;">Amount</th>
                                   <th scope="col" style="width:16%;">Plan Type</th>
                                </tr>
                              </thead>
                              <tbody>
                               <!-- <?php $i=0; ?> -->

                                    @forelse($purchaseHistory as $value)

                                    @if(isset($value->business->user_plan->plans) )
                                    <?php $i++; ?>
                                    <tr class="tr{{$value->subscr_id}}">
                                        <!-- <td>{{ $i }}</td> -->
                                        <?php if ($value->usersPlans): ?>
                                          <td>{{$value->usersPlans->name}}</td>
                                          <td>{{gmdate("d/m/Y", ($value->startDate))}}</td>
                                          <td>{{gmdate("d/m/Y", ($value->endDate)) }}</td>
                                          <td>{{$value->usersPlans->minutes_per_month}}</td>
                                          <!-- <td>{{$value->usersPlans->leads_per_month}}</td> -->
                                        <?php else: ?>
                                          <td>Credit balance</td>
                                          <td>{{gmdate("d/m/Y", ($value->startDate))}}</td>
                                          <td>{{gmdate("d/m/Y", ($value->endDate)) }}</td>
                                          <td>NA</td>
                                          <td>NA</td>
                                        <?php endif; ?>
                                        <td>{{$value->usersPlans->no_of_contacts}}</td>
                                        <td><span class="dollar"></span>@if(isset($value->users->region->countries->currencySymbol)){{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}} @else N/A @endif</td>
                                         <td>@if($value->startDate=='promo_code') Promo Plan @else Subscription Plan @endif</td>
                                    </tr>
                                    @else
                                    <tr>
                                      <td colspan="7">No Purchase History Available !</td>
                                    </tr>
                                    @endif

                                    @empty
                                    <tr>
                                      <td colspan="7">No Purchase History Available !</td>
                                    </tr>
                                    @endforelse

                              </tbody>


                            </table>
                            @if(!empty($purchaseHistory))
                            {{$purchaseHistory->links()}}
                            @endif

                        </div>
                   </div>
                   @endcan
                   <!-- Leads Summary -->
                   @can(config('permissions.data.balance-history-list.name'))
                   <div id="balance-history" class="tab-pane <?php echo $balanceActive;?>">
				                 <div class="table-responsive recordsTable latest-champaign-table">

                                <table class="table">
                                  <thead>
                                    <tr>
                                      <!-- <th scope="col">Sno.</th> -->
                        			        <th scope="col">TopUp Amount</th>
                                      <th scope="col">TopUp Credit</th>
                                      <th scope="col">Date</th>
                                    </tr>
                                  </thead>
                                  <tbody>

                                    <?php $i=0; ?>
                                    @forelse($charge as $value)
                                    @if(isset($value->business->user_plan->plans) )
                                    <?php

                                    $i++; ?>
                                    <tr>
                                      <!-- <td>{{$i}}</td> -->
                                      <td><span class="dollar"></span>@if(isset($value->users->region->countries->currencySymbol)){{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}}@else N/A @endif</td>
                                      <td>@if(isset($value->credit_amt) && $value->credit_amt!="")@if(isset($value->users->region->countries->currencySymbol)){{ $value->credit_amt}} @else N/A @endif @else N/A @endif</td>
                                      <td>{{  gmdate("d/m/Y", ($value->startDate))}} </td>
                                    </tr>

                                    @endif

                                    @empty
                                    <tr>
                                      <td colspan="3">No record found !</td>
                                    </tr>
                                    @endforelse
                                  </tbody>


                                </table>
                                @if(count($charge) > 0 )
                                {{$charge->links()}}
                                @endif

                            </div>
                      </div>
                     @endcan
          </div>*/?>
          <div class="heading-campaigns">
            <h2 class="campaign_name">Plan History</h2>
          </div>
          <div class="clearfix"></div>
          @can(config('permissions.data.plan-history-list.name'))
         <div id="plan-history" class="tab-pane   <?php echo $purchaseActive;?> ">
            <div class="table-responsive latest-champaign-table">

                  <table class="table">
                    <thead>
                      <tr>
                        <!-- <th scope="col">Sr.</th> -->
                        <th scope="col" style="width:18%;">Plan name</th>
                        <th scope="col" class="Date_box">Date</th>
                        <th scope="col" class="Date_box" style="width:16%;">Valid Till</th>
                        <!-- <th scope="col" style="width:17%;">Leads per month</th> -->
                        <th scope="col" style="width:17%;">No of contacts</th>
                        <th scope="col" style="width:16%;">Amount</th>
                         <th scope="col" style="width:16%;">Plan Type</th>
                         <th scope="col" style="width:16%;">Status</th>
                          <th scope="col" style="width:16%;">Cancelled By</th>
                           <th scope="col" style="width:16%;">Cancel Reason</th>
                            <th scope="col" style="width:16%;">Cancel Option</th>
                      </tr>
                    </thead>
                    <tbody>
                     <!-- <?php $i=0; ?> -->

                          @forelse($purchaseHistory as $value)
                        
                          <?php
                            $i++;

                                             $startDate= gmdate("d/m/Y", ($value->startDate));
                                             $endDate=gmdate("d/m/Y", ($value->endDate));
                                             $countryShort=isset($value->users->region->countries->countryShort) ?$value->users->region->countries->countryShort :'';
                                             $currecnySymbol=App\Helpers\GlobalFunctions::userCurrencySymbol($value->users->id);
                                             if(!empty($countryShort) && $value->type!='promo_code')
			    	 	 	     	         {

			    	 	 	     	             $getUserStartEndDate=App\Helpers\GlobalFunctions::getUserStartEndDate($countryShort,$value->startDate,($value->endDate));
			    	 	 	     	             $startDate=$getUserStartEndDate['startDate'];
			    	 	 	     	             $endDate=$getUserStartEndDate['endDate'];


			    	 	 	     	         }

                          ?>
                          <tr class="tr{{$value->subscr_id}}">
                              <!-- <td>{{ $i }}</td> -->
                              <?php if ($value->usersPlans): ?>
                                <td>{{$value->usersPlans->name}}</td>
                                <td>{{ $startDate }}</td>
                                <td>{{ $endDate  }}</td>
                                <!-- <td>{{$value->usersPlans->leads_per_month}}</td> -->
                              <?php else: ?>
                                <td>Credit balance</td>
                                <td>{{ $startDate }}</td>
                                <td>{{ $endDate }}</td>
                                <td>NA</td>
                                <td>NA</td>
                              <?php endif; ?>
                              <td>@if(isset($value->usersPlans->no_of_contacts)){{$value->usersPlans->no_of_contacts}} @else N/A @endif</td>
                              <td><span class="dollar"></span>{{$currecnySymbol}} {{$value->amount_paid}}</td>
                              <td>@if($value->type=='promo_code') Promo Plan @else Subscription Plan @endif</td>
                              <td>@if($value->status=='Active') Active @else Cancelled @endif </td>
                              <td>@if($value->status!='Active') @if($value->cancel_by==1) Admin @elseif($value->cancel_by==0) Customer @elseif($value->cancel_by==2) Cancelled By Promo  @else No Action @endif @else No Action @endif</td>

                               <td>@if(!empty($value->message)) {{$value->message}} @else N/A @endif</td>
                              <!--<td>@if($value->type!='promo_code') @if($value->cancel_option==1) Cancel at the end of the current period @else Cancel immediately @endif @else N/A @endif</td>-->
                              <td>@if($value->type!='promo_code') @if($value->cancel_option==2) Cancel at the end of the current period @elseif($value->cancel_option==1) Cancel immediately @else N/A  @endif @else N/A @endif</td>
                          </tr>
                         

                          @empty
                          <tr>
                            <td colspan="7">No Purchase History Available !</td>
                          </tr>
                          @endforelse

                    </tbody>


                  </table>
                  @if(!empty($purchaseHistory))
                  {{$purchaseHistory->links()}}
                  @endif

              </div>
         </div>
         @endcan

          <div class="heading-campaigns">
            <h2 class="campaign_name">Top Up History</h2>
          </div>
          <div class="clearfix"></div>
          @can(config('permissions.data.balance-history-list.name'))
          <div id="balance-history" class="tab-pane <?php echo $balanceActive;?>">
                <div class="table-responsive recordsTable latest-champaign-table">

                       <table class="table">
                         <thead>
                           <tr>
                             <!-- <th scope="col">Sno.</th> -->
                             <th scope="col">Top Up Amount</th>
                             <th scope="col">Top Up Credit</th>
                             <th scope="col">Date</th>
                           </tr>
                         </thead>
                         <tbody>

                           <?php $i=0; ?>
                           @forelse($charge as $value)
                           {{--@if(isset($value->business->user_plan->plans) )--}}
                           <?php

                           $i++;


                                             $startDate= gmdate("d/m/Y", ($value->startDate));
                                             $countryShort=isset($value->users->region->countries->countryShort) ?$value->users->region->countries->countryShort :'';
                                             if(!empty($countryShort))
			    	 	 	     	         {
			    	 	 	     	            $getUserStartEndDate=App\Helpers\GlobalFunctions::getUserStartEndDate($countryShort,$value->startDate);
			    	 	 	     	            $startDate=$getUserStartEndDate['startDate'];

			    	 	 	     	         }
                           ?>
                           <tr>
                             <!-- <td>{{$i}}</td> -->
                             <!--<td><span class="dollar"></span>@if(isset($value->users->region->countries->currencySymbol)){{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}}@else N/A @endif</td>-->
                             <!--<td>@if(isset($value->credit_amt) && $value->credit_amt!=""){{ $value->credit_amt}}  @else N/A @endif</td>-->
                             <!--<td>{{  gmdate("d/m/Y", ($value->startDate))}} </td>-->

                             <!--<td><span class="dollar"></span>@if(isset($value->users->region->countries->currencySymbol)){{$value->users->region->countries->currencySymbol}} {{$value->amount_paid}}@else N/A @endif</td>-->
                             <td><span class="dollar"></span>  {{$currecnySymbol}}@if(isset($value->amount_paid)){{$value->amount_paid}}@else N/A @endif</td>

                             <td>@if(isset($value->credit_amt) && $value->credit_amt!=""){{ $value->credit_amt}}  @else N/A @endif</td>
                             <td>{{  $startDate }} </td>

                           </tr>

                         {{--  @endif--}}

                           @empty
                           <tr>
                             <td colspan="3">No record found !</td>
                           </tr>
                           @endforelse
                         </tbody>


                       </table>
                       @if(!empty($charge))
                       {{$charge->links()}}
                       @endif

                   </div>
             </div>
            @endcan

    <br />
    <div class="heading-campaigns">
        <!--<h2 class="campaign_name">Additional</h2>-->
         <h2 class="campaign_name">Promo History</h2>
    </div>
    <div class="clearfix"></div>
    @can(config('permissions.data.balance-history-list.name'))
        <div id="balance-history" class="tab-pane <?php echo $balanceActive;?>">
            <div class="table-responsive recordsTable latest-champaign-table">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Promo</th>
                            <th scope="col">Credit</th>
                            <th scope="col">Plan</th>
                            <th scope="col">No of months</th>
                            <th scope="col">Promo Code Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($promoHistory as $value)
                            <tr>
                                <td>{{ucfirst($value->title)}}</td>
                                <td>{{$value->credit}}</td>
                                <td>{{ucfirst($value->plan_name)}}</td>
                                <td>{{$value->month}}</td>
                                 <td>@if(isset($value->promocode->agency_check_box) && $value->promocode->agency_check_box=='on') Agency Promo Code @else N/A @endif</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No record found !</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                @if(!empty($promoHistory))
                    {{$promoHistory->links()}}
                @endif
            </div>
        </div>
    @endcan

     <br />
    <div class="heading-campaigns">
        <!--<h2 class="campaign_name">Additional</h2>-->
         <h2 class="campaign_name">Audio File History</h2>
    </div>
    <div class="clearfix"></div>
    @can(config('permissions.data.balance-history-list.name'))
        <div id="balance-history" class="tab-pane <?php echo $balanceActive;?>">
            <div class="table-responsive recordsTable latest-champaign-table">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">File No</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;?>
                        @forelse($AudioPayment as $value)
                            <tr>
                                <td>{{ $i}}</td>
                                <td>{{$currencyCode->currencySymbol}} {{$value->paidAmount}}</td>
                            </tr>
                            <?php $i++; ?>
                        @empty
                            <tr>
                                <td colspan="4">No record found !</td>
                            </tr>                        
                        @endforelse
                    </tbody>
                </table>
                @if(!empty($AudioPayment))
                    {{$AudioPayment->links()}}
                @endif
            </div>
        </div>
    @endcan



</div>
</div>
