<?php
   $testUrl = explode('/',$url);
   array_pop($testUrl);
   $testUrl = implode('/', $testUrl);

   ?>
@php
$currency="";
$user="";
if(!empty($businessOwnerId))
{
$currency = \App\User::where('id',$businessOwnerId)->first()->region->currency;
$user = \App\User::with(['user_plan.plans.plan_prices'=>function($q) use($currency) {
$q->where('currency_id',$currency);
}])->where('id',$businessOwnerId)->first();
}
@endphp
<?php
   if(!empty($user) && isset($user->user_plan) && !empty($user))
     $currencySymbol = $user->user_plan->plans->plan_prices[0]->plan_currency->currencySymbol;
   else
    $currencySymbol = '';
   ?>
<link href="{{ asset('assets/customer/css/wickedpicker.min.css') }}" rel="stylesheet">
<style>
.main-welcome-message-radio .custom-file-input {
    height: 100%;
    cursor: pointer;
}
/*    .data a {
    cursor: pointer;
}
 .cstm-uplod .fa-file-upload {
    z-index: 9;
}
.custom-file input {
    z-index: 0;
    position: relative;
}*/
</style>
<div class="col-md-12">
   <div class="setting-tabs">
      <ul class="nav nav-tabs reporting-tabs" role="tablist">
         <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#call-setting">CALL SETTINGS</a>
         </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
         <!-- Call Setting Tab -->
         <div id="call-setting" class="tab-pane active
            ">
            <br>
            <div class="update-password">
               <div class="">
                  <?php //echo "<pre>";print_r($callSetting->call_summary_webhook);die();?>
                  @if(isset($callSetting) && !empty($callSetting))
                  {{ Form::model($callSetting,array('id'=>'call_setting_form','files' => true,'url'=>URL($role.'/call-setting'))) }}
                  @else
                  {{ Form::open(array('id'=>'call_setting_form','files' => true,'url'=>URL($role.'/call-setting'))) }}
                  @endif
                  <div class="row">
                     <div class="col-md-12 d-none">
                        <div class="form-group">
                           <label for="call-transfer-hours">Call Transfer Hours</label>
                           {{ Form::textarea('call_transfer_hours',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group call-recording">
                           <label for="call_recording_display">Call Company Contacts</label>
                           </br>
                           {{ Form::radio('call_method',3)}} <span class="yes-text">In Order - We will call contacts in the order they are provided for each Campaign.</span>
                           </br>
                           {{ Form::radio('call_method',4)}} <span class="yes-text">Random Order - We will call the Campaign contacts in random order each time.  Ideal for Sales teams!</span>
                        </div>
                     </div>

                   


                     <div class="col-md-12">
                        <div class="form-group">
                           </br>
                           <label for="call_announcement">Call Announcement – This message will announce the call to your company contacts..</label>

                           {{ Form::textarea('call_announcement',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2,'readonly'=>true])}}
                        </div>
                     </div>
                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="welcome_message">Lead Concierge Announcement</label>
                           <?php
                            $rd=true; 
                            ?>
                           @can(config('permissions.data.lead-concierge-announcement.name'))
                            <?php 
                            $rd=false;
                            ?>
                           @endcan
                          {{ Form::textarea('lead_concierge_announcement',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2,'readonly'=>$rd])}}
                        </div>
                      </div>
                      
                     <div class="col-md-12">
                        <div class="form-group">
                           <label for="call_instructions">Call Instructions – These instructions are given to your Company Contact before dialling the customers phone number.</label>
                           {{ Form::textarea('call_instructions',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                           {{ Form::hidden('call_instructions_type',1,['class'=>'form-control'])}}
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="row">
                           <div class="col-md-12 col-lg-4">
                              <div class="form-group call-recording ">
                                 <label for="call_recording_display">Record All Calls</label>
                                 {{ Form::radio('call_recording_display',1,false,array('class'=>'recDisp','id'=>'radio_call_1'))}} <span class="yes-text">ON</span>
                                 {{ Form::radio('call_recording_display',2,false,array('class'=>'recDisp','id'=>'radio_call_2'))}} <span class="yes-text">OFF</span>
                              </div>
                           </div>
                           <div class="col-md-12 col-lg-4 callsettingpage">
                              <span id="proTipp">
                                 <p><strong style="font-weight:600;">Want to record calls?</strong>  You’ll need to add a legal disclaimer, <a href="javascript:;" style="text-decoration:none"><strong id="CallSettingProTioClickBtn">click here</strong></a> to learn more!</p>
                              </span>
                           </div>
                           <div class="col-md-12">
                              <div class="proTipContainerDiv" id="proTipContainerDiv">
                                 <i id="proTipCloseIcon" class="fas fa-window-close" style="cursor:pointer;float: right;margin-top: -14px;margin-right: -11px;font-size: 30px;padding-left: 10px;"></i>
                                 <p>When you enable the call recording, we will play an introduction message that informs both parties that the call is being recorded (legal disclaimer).</p>
                                 <p>Our standard call introductions use “text to speech” technology to convert the introduction you type in to an audio message that the customer will hear when the call first connects.</p>
                                 <p><strong style="font-weight:600;">Pro tip:</strong> The “text to speech” message sounds good, but using your own audio recording to introduce the call will always have the best result!</p>
                                 <p>A friendly and professional audio introduction will help inform the customer that you are calling in response to their lead/enquiry, and it allows you to deliver the legal disclaimer (legally required if you are going to record the call!) in a way that sounds natural and friendly.</p>
                                 <p>Instructions:  You can record the introduction on your computer, or on your phone using almost any (free) voice recording app. The file needs to be in .wav format, and must include the legal disclaimer: <strong style="font-weight:600;">This call may be recorded for quality and training purposes.</strong></p>
                                 <p>Sample script:</p>
                                 <p style="padding: 0px 10rem; ">“Hi! This is the call you requested from {{$business_profile->business_name}}.<br> <strong style="font-weight:600;">This call may be recorded for training and quality purposes.</strong><br> Connecting you now…”</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 callAnn
                     @if(!empty($callSetting) && isset($callSetting['call_recording_display']) && $callSetting['call_recording_display']!=1) d-none @endif
                     ">
                        <div class="call_rec_intro_text_wrapper @if(isset($callSetting['call_recording_introduction_type']) && $callSetting['call_recording_introduction_type']==2 || $paymentDone == 'yes') d-none @endif">
                           <div class="form-group">
                              <label for="call_recording_introduction">Call recording Introduction – This message is played when the customer answers the call.  It will introduce the call before the legal disclaimer is played to inform both parties that the call is being recorded.</label>
                              {{ Form::textarea('call_recording_introduction',old('message'),['class'=>'form-control col-md-12','cols'=>10,'rows'=>2])}}
                              </br>
                              <label for="call_recording_introduction">Call recording legal disclaimer - THIS MESSAGE MUST BE DELIVERED IF THE CALL RECORDING IS TURNED ON.</label>
                              {{ Form::textarea('call_recording_introduction_2',old('message'),['class'=>'form-control col-md-12','cols'=>10,'rows'=>2,'readonly'=>true])}}
                           </div>
                           <p><strong>Note 1: </strong> We recommend adding your company name to the call announcement. For example: "Hello, this is your requested call back from ABC Company. Please wait while I connect you with one of our consultants. This call may be recorded for quality and training purposes." </p>
                           <p class="mb-0"><strong>Note 2: </strong> The legal disclaimer "This call may be recorded for quality and training purposes." cannot be edited or deleted.</p>
                        </div>
                        <div class="">
                           <div class="main-welcome-message-radio" style="margin-top:20px">
                              <div class="row">
                                 <div class="col-md-12 col-lg-6">
                                    <div class="containerOuter">
                                       <div class="containerradio">
                                          {{ Form::radio('call_recording_introduction_type',1,false,array('class'=>'call_recording_introduction_type_radio hidden','id'=>'input1')) }}
                                          <label class="entry" for="input1">
                                             <div class="circle btn-warning" style="background-color: #ffa300;color:#fff;border:none">Use "Text to Speech"</div>
                                             <div class="entry-label"></div>
                                          </label>
                                          <div class="highlight"></div>
                                          <div class="overlay"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12 col-lg-6">
                                    <div class="containerOuter">
                                       <div class="containerradio">
                                          {{ Form::radio('call_recording_introduction_type',2,false,array('class'=>'call_recording_introduction_type_radio hidden','id'=>'input2')) }}
                                          <label class="entry" for="input2">
                                             <div id="customRecordingBtn" class="circle btn-warning @if($paymentDone=='no') audioFilePaymentBtn @endif" style="background-color: #ffa300;color:#fff;border:none">Upload an Audio <br>(Pay $25 or Use Promocode)</div>
                                             <div class="entry-label"></div>
                                          </label>
                                          <div class="highlight"></div>
                                          <div class="overlay"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php if( \Auth()->user()->isreporter() != 1 ){?>
                              <div class="row cstm-uplod mt-5" @if(!empty($callSetting) && isset($callSetting['call_recording_introduction_type']) && $callSetting['call_recording_introduction_type'] == 1) style="display:none;margin-top:20px;" @endif id="customCallRecordingCantainer" style="margin-top:20px;" >
                              <div class="col-md-12" >
                                 <?php
                                 if( $paymentDone == 'yes' ){
                                    if($fileStatus == '' || $showFileUpload == 1){
                                       if($totalApprovedAudioFiles >= 10){ ?>
                                          <div class="">You can upload maximum 10 audio files. To upload new file you have to remove from the existing files.</div><br />
                                       <?php
                                       } else {
                                       ?>
                                          <div class="row">
                                             <div class="col-md-6">
                                                <div class="data"><a href="javascript:void(0)">Click here</a> to upload your .wav file</div>
                                                <div class="custom-file">
                                                   <i class="fas fa-file-upload"></i>
                                                   <input type="file" name="audiofile" accept=".wav" class="custom-file-input" id="customAudioFile">
                                                   <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                             </div>
                                             <div class="col-md-6">
                                                <p>Sample script: </p>
                                                <p>“Hi! This is the call you requested from {{$business_profile->business_name}}.<br> <strong style="font-weight:600;">This call may be recorded for training and quality purposes.</strong><br> Connecting you now…”</p>
                                             </div>
                                          </div>

                                           <div class="clearfix"></div>
                                           <div id="welcomeAudioTagHidden" class="text-left mt-2" style="display: none;">
                                             <div class="float-left">
                                                <audio style="margin-bottom: 15px; width: 500px;" controls controlsList="nodownload">
                                                   <source id="welcomeAudioSrcHidden" src="" type="audio/wav">
                                                </audio>
                                             </div>
                                             <div class="float-left mt-2 pl-3">
                                                <button id="upload_audio_btn" class="btn btn-primary" type="button">Upload</button>
                                             </div>
                                             <div class="clearfix"></div>
                                          </div>

                                          <!-- <div class="text-left mt-2">
                                             <audio id="welcomeAudioTagHidden" style="margin-bottom: 15px;width:41%;display:none" controls controlsList="nodownload">
                                                <source id="welcomeAudioSrcHidden" src="" type="audio/wav">
                                             </audio>
                                             <br />
                                          </div> -->

                                          <span> <strong>Instructions: </strong><br>
                                          a) The legal disclaimer must be included in your audio recording.  The wording must be “This call may be recorded for quality and training purposes”.<br/>
                                          b) The recording should be less than 30 seconds.  On average, most client recordings are 8-12 seconds.<br>
                                          c) The recording will be submitted for approval and that approval is usually done within 2 business days.  Once approved your file will automatically be live (in use).</span>

                                       <?php
                                       }
                                    } else if($fileStatus == 'Approved' || $fileStatus == 'Pending') { ?>
                                       <div class="col-md-2 col-lg-1" style="float:left;padding-left:0">
                                          <i class="fa fa-file-audio fa-4x"></i>
                                       </div>
                                       <div class="col-md-10" style="padding-left:0">
                                       <?php
                                       if($fileStatus == 'Pending') {
                                       ?>
                                          <div class="">Your file is pending for approval and will be live within 24-48 hours.</div>
                                          <div><button class="btn btn-warning" href="javascript:;" id="welcomeFileRemove" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none; background-color: #ffa300;  padding: 5px;margin: 5px -1px 0 0px;"><b>Remove</b></button></div>
                                          <br><span> <strong>Note :</strong>  a) You can change the file any time that is waiting for approval. You don't have to pay again.</span>
                                    <?php }  else if($fileStatus == 'Approved') {?>
                                    <?php if($totalApprovedAudioFiles >= 10){ ?>
                                    <div class="">This file is approved. You have reached maximum limit 10 to upload audio files. To upload new file you have to remove from the existing files.</div>
                                    <?php } else {?>
                                    <div class="">This file is approved. If you want to upload new file then you <br>have to pay again after that you can upload new file.</div>
                                    <div><button class="btn btn-warning" type="button" href="javascript:;" data-id="<?php echo Crypt::encrypt($audioId);?>" id="makeCurrentFileInactive" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none;  background-color: #ffa300;  padding: 5px;margin: 5px -1px 0 0px;"><b>Upload new file</b></button></div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php if($audiofile != ''){?>
                                    <br />
                                    <audio style="width:64%;margin-top:10px;" controls controlsList="nodownload">
                                       <source id="welcomeAudioSrc" src="<?php echo $testUrl.'/get_welcome_recording?file='.$audiofile.'&bb='.$business_id;?>" type="audio/wav">
                                    </audio>
                                    <?php } ?>
                                 </div>
                                 <?php } else if( $fileStatus == 'Rejected' ){ ?>
                                 <div class="col-md-2" style="float:left">
                                    <i class="fa fa-file-audio fa-4x"></i>
                                 </div>
                                 <div class="col-md-10">
                                    <div class="">File Rejected.</div>
                                    <div><button class="btn btn-warning" href="javascript:;" id="welcomeFileRemove" data-file="<?php echo $audiofile;?>" style="color: #fffdfde8;text-decoration: none; background-color: #ffa300;  padding: 5px;margin: 5px -1px 0 0px;"><b>Remove</b></button></div>
                                    <?php
                                       if($audiofile != ''){?>
                                    <br />
                                    <audio style="width:64%" controls controlsList="nodownload">
                                       <source id="welcomeAudioSrc" src="<?php echo $testUrl.'/get_welcome_recording?file='.$audiofile.'&bb='.$business_id;?>" type="audio/wav">
                                    </audio>
                                    <?php }?>
                                    <div class="error-upload" style="width:64%">
                                       <span><b>Reason : </b></span>
                                       <div class="border-error text-left">
                                          <p class="mb-0"><?php echo ucfirst($rejectionReason);?></p>
                                       </div>
                                    </div>
                                    <br />
                                 </div>
                                 <?php  }
                                    }else {?>
                                 <!-- <div class="row">
                                    <div class="col-lg-2 col-md-4 text-center">
                                       <button type="button" class="btn btn-primary" id="audioFilePaymentBtn" >Pay Now</button>
                                    </div>
                                    <div class="col-lg-2 col-md-3 text-center"><span class="OR"><em>OR</em></span></div>
                                    <div class="col-lg-8 col-md-5">
                                       <div class="row">
                                          <div class="col-lg-6">
                                             <div class="form-group">
                                                <input id="callAudioPromoCodeInput" type="text" class="input-lg form-control" placeholder="Have Promo Code"/>
                                                <label id="callAudioPromoCode-error" class="has-error" style="display:none;width:92%" for="callAudioPromoCode">This field is required.</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-4">
                                             <div class="form-group txt_promo_middle">
                                                <button id="callAudioPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div> -->
                                 <?php } ?>
                                 <div class="clearfix"></div>
                                 <!-- <div id="welcomeAudioTagHidden" class="text-left mt-2" style="display: none;">
                                    <div class="float-left">
                                       <audio style="margin-bottom: 15px; width: 500px;" controls controlsList="nodownload">
                                          <source id="welcomeAudioSrcHidden" src="" type="audio/wav">
                                       </audio>
                                    </div>
                                    <div class="float-left mt-2 pl-3">
                                       <button id="upload_audio_btn" class="btn btn-primary" type="button">Upload</button>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div> -->
                                <!--  <div class="clearfix"></div>
                                 <div class="text-left mt-2">
                                    <audio id="welcomeAudioTagHidden" style="margin-bottom: 15px;width:41%;display:none" controls controlsList="nodownload">
                                       <source id="welcomeAudioSrcHidden" src="" type="audio/wav">
                                    </audio>
                                    <br />
                                 </div> -->
                              </div>
                              <!-- Approved File Section -->
                              <div class="col-md-12">
                                 <h2 class="main_title_head">Approved files</h2>
                                 <p><strong>Note : </strong>You can switch between approved files here at any time. </p>
                              </div>
                              <div class="col-md-12 latest-champaign-table" style="background: none;">
                                 <div class="table-responsive">
                                    <table class="table">
                                       <thead>
                                          <tr>
                                             <th scope="col">#</th>
                                             <th scope="col">File</th>
                                             <th scope="col">Approved at</th>
                                             <?php if( \Auth()->user()->isreporter() != 1 ){?>
                                             <th scope="col">Actions</th>
                                             <?php } ?>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             $i=0;
                                             $testUrl = explode('/',$url);
                                             array_pop($testUrl);
                                             $testUrl = implode('/', $testUrl);

                                             $showTextPopUp=0;
                                             if( !empty($approvedAudioFiles) && $approvedAudioFiles->count()!=0)
                                             {
                                                 $showTextPopUp=1;
                                                 foreach($approvedAudioFiles as $value)
                                                 {
                                                     $i++;?>
                                          <tr>
                                             <td>{{ $i }}</td>
                                             <td>
                                                <?php if( $value->file != '' ){?>
                                                <audio controls controlsList="nodownload">
                                                   <source src="<?php echo $testUrl.'/get_welcome_recording?file='.$value->file.'&bb='.$value->business_id;?>" type="audio/wav">
                                                </audio>
                                                <?php } ?>
                                             </td>
                                             <td>
                                                <?php echo date('d/m/Y H:i:s a',strtotime($value->approvedAt));?>
                                             </td>
                                             <?php if( \Auth()->user()->isreporter() != 1 ){?>
                                             <td>
                                                <div class="off-on-button" style="float:left">
                                                   <label class="switch">
                                                   <input data-toggle="tooltip" title="Make this file active" data-toggle="modal" data-target="#confirm-make-audiofile-active-modal" id="{{ Crypt::encrypt($value->id) }}" data-status="{{ $value->isActive }}" type="checkbox"  class="audioFileStatusChange"  checked="checked">
                                                   <span class="slider <?php if($value->isActive == 'yes'){?>fa-toggle-on<?php } else {?>fa-toggle-off<?php }?>">
                                                   <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                                   <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                                   </span>
                                                   </label>
                                                </div>
                                                <a id="deleteApprovedAudioFile" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-remove-audiofile-modal" data-file="{{ $value->file }}">
                                                <span  data-toggle="tooltip" title="Delete" data-placement="bottom">
                                                <img src="{{asset('assets/customer/img/delete.png')}}" style="margin-top:10px;">
                                                </span>
                                                </a>
                                             </td>
                                             <?php } ?>
                                          </tr>
                                          <?php }
                                             } else { ?>
                                          <tr>
                                             <td colspan="7" align="center">No files available.</td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <!-- Approved Files Section Ends -->
                              <div class="col-md-4"></div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group call-recording mt-3" style="margin-bottom:0px;">
                        <label for="call_announcement_email">Call Summary Email</label>
                        {{ Form::radio('call_announcement_email',1,false,array('class'=>'annEmail')) }} <span class="yes-text">ON</span>
                        {{ Form::radio('call_announcement_email',2,false,array('class'=>'annEmail')) }} <span class="yes-text">OFF</span>
                     </div>
                     <p><strong>Note : </strong>The Call Summary Email is sent to the Company Contact who answers the Direct Connect call.   The summary contains the lead information, as well as a link to the audio recording of the call.
                        When setting up each campaign, the “Company Contacts” tab will allow you to choose if this email is sent to the person who answered the call, or a another central email address, or both!
                     </p>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group call-recording mt-3" style="margin-bottom:0px;">
                        <label for="call_summary_webhook">Call Summary Webhook</label>
                        {{ Form::radio('call_summary_webhook',1,false,array('class'=>'call_summary_webhookRadio')) }} <span class="yes-text">ON</span>
                        {{ Form::radio('call_summary_webhook',2,false,array('class'=>'call_summary_webhookRadio')) }} <span class="yes-text">OFF</span>
                     </div>
                     <p><strong>Note : </strong>The Call Summary will be sent to the added webhook.   The summary contains the lead information, as well as a link to the audio recording of the call.</p>
                  </div>

                  <?php $actionType = 1;$webhookUrl = '';
                      if( !empty($callSummaryWebhook))
                      {
                          $actionType = 3;
                          $webhookUrl = $callSummaryWebhook->url;
                      }
                  ?>
                  <div class="profile-detail" id="callSummaryWebhookContainerDiv" style="<?php if(isset($callSetting->call_summary_webhook) && $callSetting->call_summary_webhook ==2){?> display: none <?php } ?>">
                      {{ Form::hidden('action_type',$actionType,['class'=>'form-control','id'=>'actionTypeHidden']) }}
                      <div class="row">
                          <div class="col-md-8">
                              <div class="form-group">
                                  <label for="pass">Webhook URL <span style="color: red;display: inline;" class="star">*</span></label>
                                  {{ Form::text('callsummarywebhook',$webhookUrl,['id'=>'callsummarywebhookInput','class'=>'form-control','autocomplete'=>'off','placeholder'=>'http://members.directconnect.com/call-summary']) }}
                                  <p><strong>Note :-</strong> Please add a valid url with post method to receive call summary details in json format.</p>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <?php if($actionType < 3 ){?>
                                 <a class="btn btn-primary testWebhookBtn" href="javascript:;">Test webhook</a>
                              <?php } ?>
                              <a class="btn btn-primary cancelWebhookBtn" style="<?php if($actionType < 3 ){?>display:none;<?php } else {?>display:block;<?php }?>color:#fff"><?php if($actionType < 3 ){?>Cancel<?php } else {?>Delete webhook<?php } ?></a>
                          </div>
                      </div>
                      <div style="margin-top:15px;" class="row">
                          <div class="col-md-4" id="requestBodyContainer">
                              <h5><strong>Request Body</strong></h5>
                              <hr>
                              <pre style="min-height:188px;background: #cccccc2e;padding:15px;"><?php echo '{
                  "startdate"     : "25/03/2020 11:33:43",
                  "department"    : "facebook campaign xyz",
                  "answered_by"   : "Test(912222222222)",
                  "lead_id"       : "12345",
                  "lead_name"     : "alex",
                  "lead_number"   : "913333333333",
                  "lead_message"  : "hi this is lead message",
                  "lead_track_no" : "102030",
                  "business"      : "Success!",
                  "customer"      : "Not Connected",
                  "status"        : "Call to the Customer was attempted but not answered / No voicemail.",
                  "recording"     : "https://members.directconnect.com/business_task/get_recording?business=u20FDSASC5&fileref=2f1zAUjmUE974.wav",
                  "call_length"   : "49 sec"
              }';?></pre>
                          </div>
                          <div class="col-md-4" >
                              <h5><strong>Response format</strong></h5>
                              <hr>
                              <pre style="min-height:188px;background: #cccccc2e;padding:15px;"><span>{</span><span>   "content":{</span><span>   "message":"Data received successfully.",<span>   "createdAt":"2020-03-31 22:14:52"</span><span>   }</span><span>   "status":200,</span><span>   "contentType":"application\/json;"</span><span>}</span></pre>
                          </div>
                          <div class="col-md-4" id="responseBodyContainer">
                              <h5><strong>Response</strong></h5>
                              <hr>
                              <pre style="min-height:188px;background: #cccccc2e;padding:15px;"></pre>
                          </div>
                      </div>

                  </div>


                  <div class="emailDiv @if(!empty($callSetting) && isset($callSetting['call_announcement_email']) && $callSetting['call_announcement_email']!=1) d-none @endif">
                     {{ Form::hidden('email_subject',null,['class'=>'form-control']) }}
                  </div>
                  <div class="emailDiv @if(!empty($callSetting) && isset($callSetting['call_announcement_email']) && $callSetting['call_announcement_email']!=1) d-none @endif">
                     {{ Form::hidden('email_body',old('message'),['class'=>'form-control','cols'=>10,'rows'=>2])}}
                  </div>
                  {{ Form::hidden('retry_time',0,['class'=>'form-control','min'=>'1','max'=>'120'])}}
                  {{ Form::hidden('retry_delay_second',0,['class'=>'form-control','min'=>'1','max'=>'60'])}}
                  <div class="col-md-12">
                     <div class="form-group call-recording">

                        <?php
                            //if(isset($callSetting->status) && $callSetting->status==1)
                            if(isset($callSetting->status))
                            {
                            $countryName = strtolower($countryName);
                            if($countryName == 'united states' || $countryName == 'united kingdom' ){ ?>
                                {{ Form::hidden('callerID_sc1',null,['readonly'=>true,'class'=>'form-control'])}}
                            <?php } else {?>
                                                      <label for="call_recording_display">Business Caller ID</label>
                        </br>
                                {{ Form::text('callerID_sc1',null,['class'=>'form-control'])}}
                            <?php } ?>
                        </br>

                         <?php
                            $countryName = strtolower($countryName);
                            if($countryName == 'united states' || $countryName == 'united kingdom' ){ ?>
                               {{ Form::hidden('callerID_sc2',null,['class'=>'form-control','readonly'=>true])}}
                            <?php } else {?>
                            	  <label for="call_recording_display">Customer Caller ID</label>

                               {{ Form::text('callerID_sc2',null,['class'=>'form-control'])}}
                            <?php }
                             }
                             else
                             {
                            ?>
                             {{ Form::text('callerID_sc1',null,['class'=>'form-control'])}}
                              </br>

                               <label for="call_recording_display">Customer Caller ID</label>

                               {{ Form::text('callerID_sc2',null,['class'=>'form-control'])}}

                            <?php
                             }
                            ?>

   </br>
                                        <label style="font-weight:bold">Call Delay</label>
                                       <p>Set a delay before Direct Connect triggers the call.</p>
                                        <div class="form-group call-recording">
                                            <?php
                                                if( isset($callSetting->call_delay_time) && $callSetting->call_delay_time > 0 )
                                                    $call_delay_time  = $callSetting->call_delay_time;
                                                else
                                                    $call_delay_time  = 0;
                                            ?>
                                            
                                            {!!Form::select('call_delay_time', ['0'=>'0 Mins - RECOMMENDED for Best Result!','1'=>'1 Min - Best Result!','2'=>'2 Mins - Best Result!','3'=>'3 Mins - Strong Result!','4'=>'4 Mins - Strong Result!','5'=>'5 Mins - Strong Result!','6'=>'6 Mins - Strong Result!','7'=>'7 Mins - Good Result','8'=>'8 Mins - Good Result','9'=>'9 Mins - Good Result','10'=>'10 Mins - Good Result','11'=>'11 Mins - Good Result','12'=>'12 Mins - Good Result','13'=>'13 Mins - Good Result','14'=>'14 Mins - Good Result','15'=>'15 Mins - Good Result'], $call_delay_time, ['class' => 'form-control col-md-4'])!!}



                     </div>
                  </div>
                  <?php
                     if(\Auth()->user()->isCustomer() || \Auth()->user()->isEditior() || \Auth()->user()->isAgency() || \Auth()->user()->isBusinessAdmin() )
                     { ?>
                  <div class="col-md-12">
                     <input type="submit" class="btn btn-primary" value="Submit">
                  </div>
                  <?php } ?>
               </div>
               {{ Form::close() }}
            </div>
         </div>
      </div>
      <!-- Credit Recharge Tab
      <div id="credit-recharge" class="tab-pane">
         <br>
         @php
         $rechargeArr = array(
         '0'=>'Select Amount',
         '25'=>25,
         '50'=>50,
         '100'=>100,
         '150'=>150,
         '200'=>200,
         '300'=>300,
         '400'=>400,
         '500'=>500,
         '1000'=>1000,
         '2000'=>2000
         );
         foreach($rechargeArr as $k => $v){
         if($k!=0){
         $rArr[$k] = $currencySymbol.' '.$v;
         }else{
         $rArr[$k] = $v;
         }
         }
         @endphp
         <div class="update-password">
            {{ Form::open(array('url'=>URL('recharge-credit'), 'method'=>'POST', 'id'=>'recharge_form')) }}
            <div class="row col-md-4">
               <div class="form-group">
                  <label for="recharge Amount">Select Recharge Amount
                  </label>
                  {{ Form::select('rechargeAmt',$rArr,null, ['class'=>'form-control']) }}
               </div>
            </div>
            <div class="row col-md-2">
               <div class="form-group">
                  {{ Form::submit('Recharge',['class'=>'btn btn-primary']) }}
               </div>
            </div>
            {{ Form::close() }}
         </div>
      </div>
      <!-- Credit Recharge Tab -->
      <!-- Change Password Tab -->
     
            <?php
               $call_recording_display="";
               if(isset($callSetting['call_recording_display']) && $callSetting['call_recording_display']==2)
               {
                     $call_recording_display=$callSetting['call_recording_display'];
               }
               if(isset($isActiveFile) && !empty($isActiveFile))
               {
                    $isActiveFile=$isActiveFile;
               }
               else{
                    $isActiveFile=0;

               }
               if($totalApprovedAudioFiles >= 1)
               {
                    $showTextPopUp=1;

               }

            ?>
            {{ Form::close() }}
         </div>
      </div>
   </div>
</div>
</div>
<script src="{{ asset('assets/customer/js/wickedpicker.js')}}"></script>
<script type="text/javascript">
   $('.timepicker').wickedpicker({twentyFour: true});
   //timepickericon
   $('.timepickericon').click(function(){

        $('.timepicker').trigger('click');
   });
   //call_setting_submit
   $("#call_setting_submit").click(function(e){
       $('#call_setting_form').trigger('submit');
   });
   $(document).on("change", "#customAudioFile", function(evt)
   {
       $('#welcomeAudioTagHidden').show();
       var $source = $('#welcomeAudioSrcHidden');
       $source[0].src = URL.createObjectURL(this.files[0]);
       $source.parent()[0].load();
   });
</script>
<script>
  //var isActiveFile="<?php echo $isActiveFile;?>";
  var isActiveFile="<?php echo $showTextPopUp;?>";
  if( isActiveFile == 1)
  {
   $(".after_approve_file").show();
  }
   var paymentDone="<?php echo $paymentDone;?>";
   var callSetting="<?php echo $call_recording_display;?>";
   console.log(paymentDone);
   $(document).ready(function() {

       var  callRecordingUploadBtnclick = localStorage.getItem('callRecordingUploadBtnclick');

       if( callRecordingUploadBtnclick == 'yes' && paymentDone=='yes')
       {
           $('#customRecordingBtn').trigger('click');
           $("#radio_call_1").prop("checked", true);
           $('.callAnn').removeClass('d-none');
           localStorage.setItem('callRecordingUploadBtnclick','no');
       }

   });
        //Call settings
   $("#call_setting_form").validate({
      errorClass   : "has-error",
      highlight    : function(element, errorClass) {
        $(element).parents('.form-group').addClass(errorClass);
      },
      unhighlight  : function(element, errorClass, validClass) {
        $(element).parents('.form-group').removeClass(errorClass);
      },
      rules:{
        call_recording_introduction: {
          required: true,
          noSpace: true,
        },
        call_recording_introduction_2: {
          required: true,
          noSpace: true,
        },
        call_announcement: {
          required: true,
          noSpace: true,
        },
         call_instructions: {
          required: true,
          noSpace: true,
        },
         callerID_sc1: {
                  required: true,
                  noSpace: true,
                  minlength: 10,
                  maxlength: 11,
                  number: true
        },
        callerID_sc2: {
                  required: true,
                  noSpace: true,
                  minlength: 10,
                  maxlength: 11,
                  number: true
        },
        callsummarywebhook: {
            required: '.call_summary_webhookRadio[value="1"]:checked',
            url : true
        }
      },
      messages:{
        callerID_sc1:{
          'minlength':'Please enter 10 digit business caller id',
          'maxlength':'Please enter 11 digit business caller id'
        },
        callerID_sc2:{
          'minlength':'Please enter 10 digit customer caller id',
          'maxlength':'Please enter 11 digit customer caller id'
        }

      },
      submitHandler: function (form)
      {
        formSubmit(form);
      }
   });

   $(document).on('click','#upload_audio_btn',function(){
      $(document).find('#call_setting_form').submit();
   });

</script>
<script type="text/javascript">
function ajaxPageCallBack(response)
{
    var action_type = 1;
    if(response.action_type)
        action_type = response.action_type;

    $('#responseBodyContainer').find('pre').html('');
    $('#actionTypeHidden').val(action_type);
    if( action_type == 1 )
    {
        $('.testWebhookBtn').val('Test webhook');
        $('.cancelWebhookBtn').hide();
        $('#responseBodyContainer').find('pre').append(JSON.stringify(response.responseMessage, undefined, 2));
    }
    else if( action_type == 2 )
    {
        $('.testWebhookBtn').val('Save webhook');
        $('.cancelWebhookBtn').show();
        $('#responseBodyContainer').find('pre').append(JSON.stringify(response.responseMessage, undefined, 2));
    }
    else if( action_type == 3 )
    {
        $('.testWebhookBtn').hide();
        $('.cancelWebhookBtn').show();
        $('#responseBodyContainer').find('pre').append(JSON.stringify(response.responseMessage, undefined, 2));
    }

}

</script>
