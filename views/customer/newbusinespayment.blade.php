<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 cstm-col col-md-6">
            <h3>Business Information</h3>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <label for="">Name</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span>{{ucfirst($business_name)}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <label for="">Country</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span>{{ucfirst($countryName)}}</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4 cstm-col col-md-6">
            <h3>Plan Information</h3>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <label for="">Plan Name</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span>
                        @if(isset($checkBusinessCountryPlan[0]['plans']['name']))
                            {{$checkBusinessCountryPlan[0]['plans']['name']}}
                        @else
                            N/A
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <label for="">Monthly Subscription</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span>
                        <?php $creditNumber  = "";$contactNumber = ""; ?>
                        @if(isset($checkBusinessCountryPlan[0]['amount']))
                            <?php $creditNumber = $checkBusinessCountryPlan[0]['credit']; ?>
                            @if(isset($checkBusinessCountryPlan[0]['plan_currency']['currencySymbol']))
                                {{$checkBusinessCountryPlan[0]['plan_currency']['currencySymbol']}}
                            @endif
                            {{$checkBusinessCountryPlan[0]['amount']}}
                        @else
                            N/A
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <label for="">Top Up</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span>
                        @if(isset($checkBusinessCountryTopup) && !empty($checkBusinessCountryTopup))
                            @if(isset($checkBusinessCountryPlan[0]['plan_currency']['currencySymbol']))
                                {{$checkBusinessCountryPlan[0]['plan_currency']['currencySymbol']}} {{$checkBusinessCountryTopup->price}} for {{$checkBusinessCountryTopup->credit}} credits
                            @else
                                {{$checkBusinessCountryTopup->price}} for {{$checkBusinessCountryTopup->credit}} credits
                            @endif
                        @else
                            N/A
                        @endif
                    </span>
                </div>
            </div>

            <div class="row" id="paymentViewPromoCode" style="display:none">
                <div class="col-lg-4 col-md-6">
                    <label for="">Promo Code</label>
                </div>
                <div class="col-lg-8 col-md-6">
                    <span id="paymentViewPromoCodeSpan" style="display:none"></span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="">Description</label>
                </div>
                <div class="col-md-12">
                    <span>
                        @if(isset($checkBusinessCountryPlan[0]['plans']['description']))
                            <?php
                                $patterns             = array('/{contactNumber}/','/{creditNumber}/');
                                $replacements         = array($checkBusinessCountryPlan[0]['plans']['no_of_contacts'],$creditNumber);
                                $emailstring=(new \App\Helpers\GlobalFunctions)->emailReplacement($patterns,$replacements,$checkBusinessCountryPlan[0]['plans']['description']);
                            ?>
                            {!! $emailstring['content'] !!}
                        @else
                            N/A
                        @endif
                    </span>
                </div>
            </div>

        </div>

        <div class="col-lg-4 cstm-col payment-cstm pl-0">
            <div class="row"><h3>Payment Details</h3></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group mb-0 promo pr-3 pl-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="control-label">Promo Code</label>
                            </div>
                            <div class="col-lg-8">
                                <input id="newBusinessPromoCode" type="text" class="input-lg form-control" placeholder="" >
                                <input type="hidden" name="promocode" id="promocodehidden" />
                                <label id="newBusinessPromoCode-error" class="has-error" style="display:none" for="newBusinessPromoCode">This field is required.</label>
                            </div>
                            <div class="col-lg-4 pl-0 text-right">
                                <button id="newBusinessPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 15px; display:none;font-size: 13px;" id="appliedPromoCode">
                            <div class="col-md-12" style="padding-right: 0px;">
                                <div class="alert alert-success" role="alert">
                                    <div class="row vertical-align" style="display: flex;align-items: center;">
                                        <div class="col-md-2 p-0 text-center">
                                            <i class="fas fa-smile"></i>&nbsp;
                                        </div>
                                        <div class="col-md-10 p-0" id="promoSuccessMessage"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="choose-card">
            <div class="row">
                <div class="col-md-12">
                    <h4>Choose previous card</h4>
                    <div class="latest-champaign-table">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <?php if(!empty($businessCards)){
                                        foreach ($businessCards as $key => $value) {?>
                                            <tr style="cursor:pointer" class="previousSavedCard" data-id="<?php echo $value->id;?>" data-key="cardRadio_<?php echo $key;?>" data-cardid="<?php echo Crypt::encrypt($value->card_id);?>" >
                                                <td><input class="previousSavedCardRadio" id="cardRadio_<?php echo $key;?>" type="radio" /></td>
                                                <td>{{ucfirst($value->name)}}</td>
                                                <td>XXXX XXXX XXXX {{ucfirst($value->last4)}}</td>
                                            </tr>
                                    <?php }
                                    } else { ?>
                                        <tr><td style="text-align: center;">No cards found.</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 mb-3">
                <div class="col-lg-12">
                    <div class="or_cstm text-center"><span>OR</span></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center mb-3">
                        <button id="newBUsinessAddNewCardBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Add new card</button>

                </div>
            </div>
            <div class="NewCardContainerDiv" style="display:none">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cardHolderName" class="control-label">Card Holder Name <span class="star">*</span></label>
                            <input id="cardHolderName" type="tel" class="input-lg form-control" name="cardHolderName" placeholder="Card Holder Name" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Card Number <span class="star">*</span></label>
                            <input id="cardNumber" type="tel" class="input-lg form-control cc-number mastercard identified" name="cardNumber" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ccExpiryMonth" class="control-label">Card Expiry <span class="star">*</span></label>
                            <input id="ccExpiryMonth" type="tel" class="input-lg form-control cc-exp" name="ccExpiryMonth" autocomplete="cc-exp" placeholder="mm / yy" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Card CVC <span class="star">*</span></label>
                            <input id="cvvNumber" type="tel" class="input-lg form-control cc-cvc" name="cvvNumber" autocomplete="off" placeholder="•••" required="">
                        </div>
                    </div>
                </div>
            </div>



        </div>
        </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
<script type="text/javascript">
    jQuery(function($)
    {
        $(document).find('[data-numeric]').payment('restrictNumeric');
        $(document).find('.cc-number').payment('formatCardNumber');
        $(document).find('.cc-exp').payment('formatCardExpiry');
        $(document).find('.cc-cvc').payment('formatCardCVC');
        $.fn.toggleInputError = function(erred) {
            this.parent('.form-group').toggleClass('has-error', erred);
            return this;
        };
    });
</script>
