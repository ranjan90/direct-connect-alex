@extends('layouts.customer')
@section('content')
   <div class="container-fluid">
      <!-- Icon Cards-->
      <div class="row">
         <!--div class="create-new-campaign">
            <div class="create-campaign-logo"><img src="{{ asset('assets/customer/img/New_campaign.svg') }}  "></div>
            <h3>Create a New Campaign</h3>
            {{Form::open(array('id'=>'create-camp_form','url'=>$url.'/create-new-campaign'))}}
               <div class="form-group">
                  <label for="campaignsTitle">CAMPAIGN NAME <span class="star">*</span></label>
                  <input type="text" class="form-control" name="campaignsTitle" id="new-camp-name" placeholder="Enter Campaign Name">
               </div>
               <div class="campaign-checkbox">
                  <div class="form-group">
                     <label for="campaignsType">CAMPAIGN SETUP METHOD <span class="star">*</span></label>
                     @if(isset($userId))
                     <input type="hidden" name="userid" value="{{$userId}}">
                     @endif
                     <input type="radio" id="mailCheck" value="1" name="campaignType">
                     <label for="mailCheck"><span class="circle-checkbox"></span><span class="recipient-address">EMAIL</span> </label>
                     <input type="radio" id="apiCheck" value="2" name="campaignType">
                     <label for="apiCheck"><span class="circle-checkbox"></span><span class="recipient-address">API</span> </label>
                  </div>
               </div>



               <div class="create-campaign-btn">
                  <button type="submit" class="btn btn-default btn-new-campaign create_camp_btn" disabled="disabled">CREATE CAMPAIGN</button>
               </div>
            {{Form::close()}}
        </div-->
           <!-- <div class="create-campaign-logo"><img src="{{ asset('assets/customer/img/New_campaign.svg') }}  "></div> -->



        <div class="modal-bg m-auto">
            <div class="" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body pt-4 text-left">
                            <h4>Create a New Campaign</h4>
                            {{Form::open(array('id'=>'create-camp_form','url'=>$url.'/create-new-campaign'))}}
                            <div class="form-group">
                                <input type="text" class="form-control" name="campaignsTitle" id="new-camp-name" placeholder="Enter Campaign Name">
                            </div>
                            <div class="setup">
                                <h2 class="mt-4 mb-3">Create a unique campaign for each lead source, including website forms, social media leads, landing pages, etc.</h2>
                                <div class="active-head head">
                                    <img src="{{asset('assets/customer/img/modal-mail.png')}}"><h3>Email</h3><span> (Recommended for a FAST start!)</span>
                                </div>
                                <div class="radio-sec">
                                    <div class="radio">
                                        @if(isset($userId))
                                        <input type="hidden" name="userid" value="{{$userId}}">
                                        @endif
                                        <input id="radio-1" value="1" name="campaignType" type="radio" checked>
                                        <label for="radio-1" class="radio-label"></label>
                                    </div>
                                    <div class="content-set"><p>This option will generate a unique email address that you can add as a lead recipient to your lead source. When an email is received a call is triggered within seconds.</p>
                                        <p class="mb-0">No coding or API integration required!</p>
                                    </div>
                                </div>
                                <div class="head mt-4">
                                    <img src="{{asset('assets/customer/img/api-icon.png')}}"><h3>API</h3><span></span>
                                </div>
                                <div class="radio-sec">
                                    <div class="radio">
                                        <input id="radio-API" value="2" name="campaignType" type="radio">
                                        <label for="radio-API" class="radio-label"></label>
                                    </div>
                                    <div class="content-set"><p>Trigger Direct Connect via API.</p>
                                        <p class="mb-0">This option is ideal for advanced users who want to trigger calls from a CRM or other software. Calls are triggered within seconds.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-btn mt-4 p-0">
                                <button type="submit" class="btn-warning btn float-left" >Create Campaign</button>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>



      </div>
</div>
@endsection
