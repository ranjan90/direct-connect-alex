<?php
$getBusinessId = \Auth::user()->getBusinessId();
$callsettingsss  = \App\Model\CallSetting::select('*')->where('business_id',$getBusinessId);
if( $callsettingsss->count() > 0 )
{
    $callsettingsss = $callsettingsss->first();
    $recDisplay = $callsettingsss->call_recording_display;
}
else
{
    $callsettingsss = array();
    $recDisplay = '';
}
 ?>
@php
  $ratingArr = array('','Poor','Good','Excellent');
@endphp
<table class="table" width="100%">
  <thead>
    <tr>
      <th scope="col" width="6%">Sr.</th>
      <th scope="col" width="7%">Lead ID</th>
      <th scope="col" width="7%">Answered by</th>
      <th scope="col" width="7%">Lead Name</th>
      <th scope="col" width="10%">Call Length</th>
      <th scope="col" width="20%">Call Date</th>
      @if($recDisplay==1)<th scope="col" width="20%">Call Summary</th>@endif
      <th scope="col" style="min-width: 93px;">Business</th>
      <th scope="col" style="min-width: 93px;">Customer</th>
      <th scope="col" width="10%">Rating</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=0; @endphp
  @forelse($leadsData as $leads)
    @php $i++; $length = $leads->call_length/60; @endphp
    <tr id="{{ $leads->id }}">
      <td>{{ $i }}</td>
      <td>{{ $leads->lead_id }}</td>
      <td>{{ ucfirst($leads->answered_by) }}</td>
      <td>@if(isset($leads->parse_emails->leadName)){{ $leads->parse_emails->leadName }} @else N/A @endif</td>
      <td><span class="blue-color"><i data-toggle="tooltip" data-placement="top" title="Total Call Length: {{ $length }} Minute(s)" class="fa fa-info-circle"></i></span></td>
      <td>{{ date("d/m/Y h:i:s A", strtotime($leads->startdate)) }}</td>
      @if($recDisplay==1)
        <td>
          @if($leads->call_recording_display==1)
              <audio controls controlsList="nodownload">
                  <source src="{{ $leads->recording }}" type="audio/wav">
              </audio>
          @else
          N/A
          @endif
        </td>
      @endif
      <?php 
      $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($leads->parse_emails);
                      extract($return_data);
                      ?>
      <td scope="col">
                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo $business_btn_message; ?></button>
                                                   </td>
                                                   <td scope="col">

                                                     <button type="button" class="call-status-button btn <?php echo  $customer_btn_color;?> set_btn_width"><?php echo $customer_btn_message; ?></button>
                                                   </td>

      
      <td class="rateTD" title="@if($leads->rating!=0) {{$ratingArr[$leads->rating]}} @endif">
        @if($leads->rating==0)
          @for($j=1; $j<=3; $j++)
            <span class="star-rate" id="{{$j}}" title="{{$ratingArr[$j]}}"><i style="font-size: 14px;" class="stars far fa-star"></i></span>
          @endfor
        @else
          @for($j=1; $j<=3; $j++)
            @if($j<=$leads->rating)
              <span><i style="font-size: 14px;" class="stars rate-blue fas fa-star"></i></span>
            @else
              <span><i style="font-size: 14px;" class="stars far fa-star"></i></span>
            @endif
          @endfor
        @endif
      </td>
      <td>
        <a class="blue-color getLeadData" data-url="{{ URL('get-lead-data') }}/{{ $leads->id }}" href="javascript:void(0)" data-toggle="modal" data-target="#leadDetails"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View Lead Details"></i></a>
        &nbsp;
        @if($recDisplay==1)
          @if(strpos($leads->recording , 'recordings/False' ) === false ) <a href="{{$leads->recording}}" download="{{$leads->recording}}"><i data-toggle="tooltip" data-placement="top" title="Download Lead" class="fa fa-download" aria-hidden="true"></i></a>@endif
        @endif
      </td>
  </tr>
  @empty
  <tr>
      <td colspan="8">No Record Found !</td>
  </tr>
  @endforelse
  </tbody>
</table>
{{ $leadsData->links() }}
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
