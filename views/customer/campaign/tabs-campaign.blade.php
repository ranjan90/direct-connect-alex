@extends('layouts.customer')
@section('content')
    <div class="container-fluid">
      <!-- Icon Cards-->
      <div class="row">
		<div class="col-md-12">
			<div class="tabbable-panel Campaigns-tabs">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default_1" data-toggle="tab">
							SEND TEST LEAD</a>
						</li>
						<li>
							<a href="#tab_default_2" data-toggle="tab">
							SET UP CALL SCRIPT </a>
						</li>
						<li>
							<a href="#tab_default_3" data-toggle="tab">
							SELECT CONTACTS </a>
						</li>
						<li>
							<a href="#tab_default_4" data-toggle="tab">
							DEFINE CAMPAIGN HOURS </a>
						</li>
						<li>
							<a href="#tab_default_5" data-toggle="tab">
							REVIEW CAMPAIGN </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_1">
							 <h2>This is the campaign Name</h2>
							   <div class="row">
							     <div class="col-md-4">
							     <form id="create-new-campaign1" class="email-address-new-campaign">
									<div class="form-group">
										<label for="exampleInputEmail1">CAMPAIGN EMAIL ADDRESS</label>
										<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="m3x29ehawy@directconnect.com">
									</div>

                                </form>
								<h4>Set up Check list</h4>
								  <div class="campaign-checkbox">
								    <form>
										<div class="form-group">
										  <input type="checkbox" id="recipient">
										  <label for="recipient"><span class="circle-checkbox"></span><span class="recipient-address">I added the custom email address as recipient to my contact form</span> </label>
										</div>
											<div class="form-group">
										  <input type="checkbox" id="recipient1">
										  <label for="recipient1"><span class="circle-checkbox"></span><span class="recipient-address">I sent a test lead </span> </label>
										</div>
											<div class="form-group">
										  <input type="checkbox" id="recipient2">
										  <label for="recipient2"><span class="circle-checkbox"></span><span class="recipient-address">I waited for one minute </span> </label>
										</div>
	                                </form>
								  </div>
							 </div>
							 <div class="col-md-1">

								<a tabindex="0"
								   role="button"
								   data-html="true"
								   data-toggle="popover"
								   data-placement="right"
								   data-trigger="focus"
								   data-content=' <div class="popover-custom-content">
									  <h2> Add this custom email address as recipient to your form</h2>

								<h6>This email address must be added <strong>as recipient </strong>to receive lead/form submissions from this lead source. </h6>

								<h6>Example: if this campaign is for your website « contact us » page, add this email address to receive a copy of the email when a customer submits a new form. </h6>

								<h5><span class="got-recipient">GOT IT</span></h5>
								</div>'>
								<div class="inner-campaign-circle">
								    <div class="outer-campaign-circle">
									</div>
								  </div>
								</a>

    </div>

							   </div>



							 <div class="col-md-7">
							 </div>


						<div class="row">
						  <div class="col-md-12">
						  <div class="save-campaign">
						    <a href="#" class="btn btn-primary">Next</a>
						  </div>
						  </div>
						</div>
						</div>


						<div class="tab-pane" id="tab_default_2">
						 <h2>Call Script </h2>

							<div class="tags-script">

							 <form id="create-new-campaign1" class="email-address-new-campaign">
							  <div class="row">
							  <div class="col-md-7">

										<div class="form-group">

											<textarea  class="form-control" id="" rows="4" placeholder="">New website enquiry received from [Full_Name].
The message received is [Message]. </textarea>
										</div>
										</div>
										<div class="col-md-1">
											<a tabindex="0"
								   role="button"
								   data-html="true"
								   data-toggle="popover"
								   data-placement="right"
								   data-trigger="focus"
								   data-content=' <div class="popover-custom-content">
									  <h2> Use this standard script for your campaign</h2>

								<h6>You can also edit it and create your own custom script.
Use the tags based on your form fields : double-click on a tag to insert unique information from your lead source. </h6>



								<h5><span class="got-recipient">GOT IT</span></h5>
								</div>'>
								<div class="inner-campaign-circle">
								    <div class="outer-campaign-circle">
									</div>
								  </div>
								</a>



</div>





							</div>


							 </div>
							 </form>


							  	<div class="row clearfix">
									  <div class="col-md-12">
									  <div class="Submit-campaign">
									  <a href="#" class="btn btn-default btn-back"><i class="fa fa-arrow-left" aria-hidden="true"></i>
Back</a>
										<a href="#" class="btn btn-primary next-now">Next</a>
									  </div>
									  </div>
						            </div>
							</div>


						<div class="tab-pane" id="tab_default_3">
						  <div class="campaign-time select-contact">
							   <h2>Select Contacts </h2>
							   <div class="row">
							     <div class="col-md-8">
								   <div class="select-contact-part">

									  <h6>The selected contacts will receive calls for this campaign. </h6>
									     <div class="inner-contact-modal">
										  <a href="#" class="btn btn-primary pull-left" data-toggle="modal" data-target="#campaign-contacts">Add an existing contact</a>
									    <a href="#" class="btn btn-primary new-contact-now" data-toggle="modal" data-target="#add-new-tag">Add a new contact</a>

										 </div>
<div id="campaign-contacts" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
   <form method="POST" action="https://www.directconnect.com/tester/customer/save-contact" accept-charset="UTF-8" id="contact_form" autocomplete="off" novalidate="novalidate"><input name="_token" type="hidden" value="">
        <input type="hidden" name="frontreq" value="">
        <input name="recordId" type="hidden" value="">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Select Contact</h4>
               <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="table-responsive campaign-checkbox select-contact-table">
    <table class="table">
    	  <thead>
    	  <tr>
    		  <th>Name</th>
    		   <th>Contact Number</th>
    			<th></th>
    	  </tr>
    	  </thead>
    	  <tbody>
                          <tr>
            <td>anshu</td>
            <td>9756086199</td>
            <td><form>
										<div class="form-group">
										  <input type="checkbox" id="recipient5">
										  <label for="recipient5"><span class="circle-checkbox"></span> </label>
										</div></form></td>
        </tr>
            	  </tbody>
	</table>
</div>
              </div>
            </div>
			<div class="modal-footer">
      <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
              </div>
</div>

         </div>

      </form>
								   </div>
								    </div>
<div id="add-new-tag" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
   <form method="POST" action="https://www.directconnect.com/tester/customer/save-contact" accept-charset="UTF-8" id="contact_form" autocomplete="off" novalidate="novalidate"><input name="_token" type="hidden" value="">
        <input type="hidden" name="frontreq" value="">
        <input name="recordId" type="hidden" value="">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Add New Contact</h4>
               <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Contact Name <span class="star">*</span></label>
                        <input placeholder="Name" class="form-control" name="name" type="text">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Contact Email <span class="star">*</span></label>
                        <input placeholder="Contact Email" class="form-control" name="email" type="text" value="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Role/Title </label>
                        <input placeholder="Role/Title" class="form-control" name="role" type="text">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Department/Location </label>
                        <input placeholder="Department/Location" class="form-control" name="dept" type="text">
                                            </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Country Code <span class="star">*</span></label>
                        <select class="form-control" name="code"><option value="" selected="selected">Select Country Code</option><option value="157">Afghanistan - (+93)</option><option value="176">Albania - (+355)</option><option value="134">Algeria - (+213)</option><option value="34">American Samoa - (+1684)</option><option value="185">Andorra - (+376)</option><option value="32">Angola - (+244)</option><option value="124">Antigua and Barbuda - (+1268)</option><option value="2">Argentina - (+54)</option><option value="175">Armenia - (+374)</option><option value="226">Australia - (+61)</option><option value="197">Austria - (+43)</option><option value="174">Azerbaijan - (+994)</option><option value="137">Bahamas - (+1242)</option><option value="148">Bahrain - (+973)</option><option value="136">Bangladesh - (+880)</option><option value="106">Barbados - (+1246)</option><option value="210">Belarus - (+375)</option><option value="207">Belgium - (+32)</option><option value="121">Belize - (+501)</option><option value="83">Benin - (+229)</option><option value="160">Bermuda - (+1441)</option><option value="150">Bhutan - (+975)</option><option value="21">Bolivia - (+591)</option><option value="187">Bosnia and Herzegovina - (+387)</option><option value="17">Botswana - (+267)</option><option value="10">Brazil - (+55)</option><option value="132">British Virgin Islands - (+1284)</option><option value="74">Brunei - (+673)</option><option value="183">Bulgaria - (+359)</option><option value="91">Burkina Faso - (+226)</option><option value="52">Burundi - (+257)</option><option value="93">Cambodia - (+855)</option><option value="63">Cameroon - (+237)</option><option value="224">Canada - (+1)</option><option value="116">Cape Verde - (+238)</option><option value="133">Cayman Islands - (+1345)</option><option value="67">Central African Republic - (+236)</option><option value="87">Chad - (+235)</option><option value="4">Chile - (+56)</option><option value="131">China - (+86)</option><option value="51">Colombia - (+57)</option><option value="38">Comoros - (+269)</option><option value="48">Congo - (+242)</option><option value="24">Cook Islands - (+682)</option><option value="89">Costa Rica - (+506)</option><option value="186">Croatia - (+385)</option><option value="135">Cuba - (+53)</option><option value="165">Cyprus - (+357)</option><option value="202">Czech Republic - (+420)</option><option value="213">Denmark - (+45)</option><option value="96">Djibouti - (+253)</option><option value="119">Dominica - (+1767)</option><option value="128">Dominican Republic - (+1809)</option><option value="47">East Timor - (+670)</option><option value="50">Ecuador - (+593)</option><option value="141">Egypt - (+20)</option><option value="109">El Salvador - (+503)</option><option value="56">Equatorial Guinea - (+240)</option><option value="105">Eritrea - (+291)</option><option value="216">Estonia - (+372)</option><option value="68">Ethiopia - (+251)</option><option value="3">Falkland Islands - (+500)</option><option value="220">Faroe Islands - (+298)</option><option value="218">Finland - (+358)</option><option value="182">France - (+33)</option><option value="65">French Guiana - (+594)</option><option value="13">French Polynesia - (+689)</option><option value="53">Gabon - (+241)</option><option value="108">Gambia - (+220)</option><option value="180">Georgia - (+995)</option><option value="200">Germany - (+49)</option><option value="77">Ghana - (+233)</option><option value="166">Greece - (+30)</option><option value="219">Greenland - (+299)</option><option value="99">Grenada - (+1473)</option><option value="120">Guadeloupe - (+590)</option><option value="110">Guam - (+1671)</option><option value="112">Guatemala - (+502)</option><option value="86">Guinea - (+224)</option><option value="97">Guinea-Bissau - (+245)</option><option value="66">Guyana - (+592)</option><option value="130">Haiti - (+509)</option><option value="107">Honduras - (+504)</option><option value="231">Hong Kong - (+852)</option><option value="195">Hungary - (+36)</option><option value="221">Iceland - (+354)</option><option value="88">India - (+91)</option><option value="44">Indonesia - (+62)</option><option value="147">Iran - (+98)</option><option value="156">Iraq - (+964)</option><option value="209">Ireland - (+353)</option><option value="155">Israel - (+972)</option><option value="169">Italy - (+39)</option><option value="73">Ivory Coast - (+225)</option><option value="127">Jamaica - (+1876)</option><option value="145">Japan - (+81)</option><option value="154">Jordan - (+962)</option><option value="178">Kazakhstan - (+76)</option><option value="49">Kenya - (+254)</option><option value="54">Kiribati - (+686)</option><option value="153">Kuwait - (+965)</option><option value="177">Kyrgyzstan - (+996)</option><option value="117">Laos - (+856)</option><option value="215">Latvia - (+371)</option><option value="163">Lebanon - (+961)</option><option value="11">Lesotho - (+266)</option><option value="72">Liberia - (+231)</option><option value="143">Libya - (+218)</option><option value="199">Liechtenstein - (+423)</option><option value="211">Lithuania - (+370)</option><option value="206">Luxembourg - (+352)</option><option value="179">Macedonia - (+389)</option><option value="19">Madagascar - (+261)</option><option value="33">Malawi - (+265)</option><option value="62">Malaysia - (+60)</option><option value="59">Maldives - (+960)</option><option value="95">Mali - (+223)</option><option value="167">Malta - (+356)</option><option value="75">Marshall Islands - (+692)</option><option value="114">Martinique - (+596)</option><option value="118">Mauritania - (+222)</option><option value="27">Mauritius - (+230)</option><option value="37">Mayotte - (+262)</option><option value="115">Mexico - (+52)</option><option value="69">Micronesia - (+691)</option><option value="194">Moldova - (+373)</option><option value="190">Monaco - (+377)</option><option value="188">Mongolia - (+976)</option><option value="229">Montenegro - (+382)</option><option value="152">Morocco - (+212)</option><option value="18">Mozambique - (+258)</option><option value="101">Myanmar - (+95)</option><option value="12">Namibia - (+264)</option><option value="60">Nauru - (+674)</option><option value="149">Nepal - (+977)</option><option value="208">Netherlands - (+31)</option><option value="100">Netherlands Antilles - (+599)</option><option value="22">New Caledonia - (+687)</option><option value="6">New Zealand - (+64)</option><option value="94">Nicaragua - (+505)</option><option value="98">Niger - (+227)</option><option value="71">Nigeria - (+234)</option><option value="113">Northern Mariana Islands - (+1670)</option><option value="217">Norway - (+47)</option><option value="123">Oman - (+968)</option><option value="142">Pakistan - (+92)</option><option value="64">Palau - (+680)</option><option value="85">Panama - (+507)</option><option value="43">Papua New Guinea - (+675)</option><option value="14">Paraguay - (+595)</option><option value="30">Peru - (+51)</option><option value="76">Philippines - (+63)</option><option value="204">Poland - (+48)</option><option value="162">Portugal - (+351)</option><option value="129">Puerto Rico - (+1787)</option><option value="146">Qatar - (+974)</option><option value="25">Reunion - (+262)</option><option value="189">Romania - (+40)</option><option value="181">Russia - (+7)</option><option value="55">Rwanda - (+250)</option><option value="125">Saint Kitts and Nevis - (+1869)</option><option value="111">Saint Lucia - (+1758)</option><option value="198">Saint Pierre and Miquelon - (+508)</option><option value="104">Saint Vincent and The Grenadines - (+1784)</option><option value="36">Samoa - (+685)</option><option value="191">San Marino - (+378)</option><option value="61">Sao Tome and Principe - (+239)</option><option value="122">Saudi Arabia - (+966)</option><option value="102">Senegal - (+221)</option><option value="84">Sierra Leone - (+232)</option><option value="227">Singapore - (+65)</option><option value="201">Slovakia - (+421)</option><option value="193">Slovenia - (+386)</option><option value="41">Solomon Islands - (+677)</option><option value="58">Somalia - (+252)</option><option value="9">South Africa - (+27)</option><option value="151">Spain - (+34)</option><option value="81">Sri Lanka - (+94)</option><option value="70">Sudan - (+249)</option><option value="78">Suriname - (+597)</option><option value="15">Swaziland - (+268)</option><option value="214">Sweden - (+46)</option><option value="196">Switzerland - (+41)</option><option value="161">Syria - (+963)</option><option value="139">Taiwan - (+886)</option><option value="171">Tajikistan - (+992)</option><option value="42">Tanzania - (+255)</option><option value="80">Thailand - (+66)</option><option value="82">Togo - (+228)</option><option value="45">Tokelau - (+690)</option><option value="26">Tonga - (+676)</option><option value="92">Trinidad and Tobago - (+1868)</option><option value="159">Tunisia - (+216)</option><option value="168">Turkey - (+90)</option><option value="172">Turkmenistan - (+993)</option><option value="138">Turks and Caicos Islands - (+1649)</option><option value="46">Tuvalu - (+688)</option><option value="57">Uganda - (+256)</option><option value="192">Ukraine - (+380)</option><option value="144">United Arab Emirates - (+971)</option><option value="225">United Kingdom - (+44)</option><option value="223">United States - (+1)</option><option value="8">Uruguay - (+598)</option><option value="170">Uzbekistan - (+998)</option><option value="28">Vanuatu - (+678)</option><option value="79">Venezuela - (+58)</option><option value="90">Vietnam - (+84)</option><option value="35">Wallis and Futuna - (+681)</option><option value="103">Yemen - (+967)</option><option value="31">Zambia - (+260)</option><option value="23">Zimbabwe - (+263)</option></select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Phone Number <span class="star">*</span></label>
                        <input placeholder="Phone Number" class="form-control" id="phonenumber" name="number" type="text">
                    </div>
                </div>
              </div>
            </div>
					<div class="modal-footer">
      <div class="text-right">
                    <button type="submit" class="btn btn-primary">Yes</button>

              </div>
</div>

         </div>

      </form>
								   </div>
								    </div>


									   </div>
									 <div class="campaign-checkbox select-contact-table">
									   <div class="table-responsive">
									     <table class="table">
										   <tr>
										     <th></th>
											 <th>Name</th>
											 <th>Phone Number</th>
											 <th>Actions</th>
										   </tr>
										   <tr>
										   <td><form>
										<div class="form-group">
										  <input type="checkbox" id="recipient4">
										  <label for="recipient4"><span class="circle-checkbox"></span> </label>
										</div></form></td>
										   <td><a href="#" class="client-name">Angelica Mary Sosa</a> </td>
										   <td>04 1120 8267</td>
										   <td><i class="fa fa-edit"></i><i class="fa fa-trash"></i>
</td>
										   </tr>
										    <tr>
										   <td><form>
										<div class="form-group">
										  <input type="checkbox" id="recipient5">
										  <label for="recipient5"><span class="circle-checkbox"></span> </label>
										</div></form></td>
										   <td><a href="#" class="client-name">Oscar Agosta Franco</a>  </td>
										   <td>04 8176 0192</td>
										   <td><i class="fa fa-edit"></i><i class="fa fa-trash"></i></td>
										   </tr>
										    <tr>
										   <td><form>
										<div class="form-group">
										  <input type="checkbox" id="recipient6">
										  <label for="recipient6"><span class="circle-checkbox"></span> </label>
										</div></form></td>
										   <td><a href="#" class="client-name">Alexander Covach </a> </td>
										   <td>04 5463 6272</td>
										   <td><i class="fa fa-edit"></i><i class="fa fa-trash"></i></td>
										   </tr>
										 </table>
									   </div>


									 </div>
									 </div>

								  <div class="col-md-4">
								 </div>
								 </div>
								   	<div class="row clearfix">
									  <div class="col-md-12">
									  <div class="Submit-campaign">
									  <a href="#" class="btn btn-default btn-back"><i class="fa fa-arrow-left" aria-hidden="true"></i>
Back</a>
										<a href="#" class="btn btn-primary next-now">Next</a>
									  </div>
									  </div>
						            </div>
							   </div>

						  </div>
							<div class="tab-pane" id="tab_default_4">
							   <div class="campaign-time">
							   <h2>Set campaign time </h2>
							       <div class="row">
								    <div class="col-md-4">
										<form id="create-new-campaign1" class="email-address-new-campaign">
										<div class="form-group">
											<label for="exampleInputEmail1">Country <span class="star">*</span></label>
											<select  class="form-control" id="" placeholder="">
											<option>Select your Country</option>
											<option>1</option>
											<option>2</option>
											</select>
										</div>

									  </form>
									</div>
									<div class="col-md-8">
									  <div class="calender-section">
									    <div class="avaliable-days">
										  <form id="avaliable-day" class="email-address-new-campaign">
										   	<label for="exampleInputEmail1">Select Available Days <span class="star">*</span></label>
											<!--<div class="week-days-section">
											  <ul>
											  <li class="all-week">ALL</li>
											  <li>M</li>
											  <li>T</li>
											  <li class="active">W</li>
											  <li>T</li>
											  <li class="active">F</li>
											  <li>S</li>
											  <li>S</li>
											  </ul>
											</div>-->
											<div class="weekDays-selector">
                           <label for="day" class="">Available day</label>
                           <input id="weekday-all" class="weekday checkAllBox" type="checkbox">
                           <label for="weekday-all" style="border-radius: 17px;width: 70px;line-height:41px;margin-left: 10px;">All</label>
                           &nbsp;&nbsp;&nbsp;
                           <input id="weekday-mon" class="weekday" type="checkbox" name="availDays[]" value="M">
                           <label for="weekday-mon">M</label>
                           <input id="weekday-tue" class="weekday" type="checkbox" name="availDays[]" value="T">
                           <label for="weekday-tue">T</label>
                           <input id="weekday-wed" class="weekday" type="checkbox" name="availDays[]" value="W">
                           <label for="weekday-wed">W</label>
                           <input id="weekday-thu" class="weekday" type="checkbox" name="availDays[]" value="T">
                           <label for="weekday-thu">T</label>
                           <input id="weekday-fri" class="weekday" type="checkbox" name="availDays[]" value="F">
                           <label for="weekday-fri">F</label>
                           <input id="weekday-sat" class="weekday" type="checkbox" name="availDays[]" value="S">
                           <label for="weekday-sat">S</label>
                           <input id="weekday-sun" class="weekday" type="checkbox" name="availDays[]" value="S">
                           <label for="weekday-sun">S</label>
                        </div>
										  </form>
										</div>
									   <div class="row">
									   <div class="col-md-6">
									  	<form id="time-from" class="email-address-new-campaign">
										<div class="form-group">
											<label for="exampleInputEmail1">From <span class="star">*</span></label>
											<select  class="form-control" id="" placeholder="">
											<option>Select a Starting time</option>
											<option>1</option>
											<option>2</option>
											</select>
										</div>

									  </form>
									   </div>
									    <div class="col-md-6">
											<form id="time-to" class="email-address-new-campaign">
										<div class="form-group">
											<label for="exampleInputEmail1">To <span class="star">*</span></label>
											<select  class="form-control" id="" placeholder="">
											<option>Select a finishing time</option>
											<option>1</option>
											<option>2</option>
											</select>
										</div>

									  </form>
									   </div>
									   </div>


									  </div>
									</div>
							   </div>
							   			   	<div class="row clearfix">
									  <div class="col-md-12">
									  <div class="Submit-campaign">
									  <a href="#" class="btn btn-default btn-back"><i class="fa fa-arrow-left" aria-hidden="true"></i>
Back</a>
										<a href="#" class="btn btn-primary next-now">Next</a>
									  </div>
									  </div>
						            </div>
						   </div>
						   </div>
							<div class="tab-pane" id="tab_default_5">
						     <h2>Confirm New Campaign </h2>
							   <div class="confirm-campaign-box">
							     <div class="row">
								   <div class="col-md-4">
								     <div class="campaign-new-tick-img">
								       <img src="{{ asset('assets/customer/img/detail-main.png') }}  ">
								   </div>
								   </div>
								     <div class="col-md-5">
									 <div class="gavin-testing-part">
									   <div class="new-campaign-confirm-text">
									     <h4>Campaign Title</h4>
										 <h6>Gavin Testing</h6>
									   </div>
									    <div class="new-campaign-confirm-text">
									     <h4>Campaign Template</h4>
										 <h6>New Website enquiry received from [Name]. The message received is [Message]. </h6>
									   </div>
									    <div class="new-campaign-confirm-text">
									     <h4>Campaign Template</h4>
										 <h6>New Website enquiry received from Gavin Sloan. The message received is Testing Form. </h6>
									   </div>
								   </div>
								   </div>
								     <div class="col-md-3">
									  	 <div class="gavin-testing-address">
									   <div class="new-campaign-confirm-text">
									     <h4>Country</h4>
										 <h6>Australia</h6>
									   </div>
									    <div class="new-campaign-confirm-text">
									     <h4>Available Days</h4>
										 <h6>Monday,Friday</h6>
									   </div>
									    <div class="new-campaign-confirm-text">
									     <h4>Available Hours</h4>
										 <h6>11am - 5pm </h6>
									   </div>
									    <div class="new-campaign-confirm-text">
									     <h4>Company Contacts</h4>
										 <h6>

Gavin Sloan - 04 6578 0987
Angelica Smith - 04 6578 0987 </h6>
									   </div>
								   </div>
								   </div>
								 </div>

							   </div>
							   	<div class="row clearfix">
									  <div class="col-md-12">
									  <div class="Submit-campaign">
									  <a href="#" class="btn btn-default btn-back"><i class="fa fa-arrow-left" aria-hidden="true"></i>
Back</a>
										<a href="#" class="btn btn-primary submit-now">Submit Campaign</a>
									  </div>
									  </div>
						            </div>
						   </div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>





  <script type="text/javascript">
  $(document).ready(function(){


        $('#phonenumber').keyup(function(){
            var value = $('#phonenumber').val();
            if(value == '' || value == undefined){
              $('#phonenumber').val('');
            }else{
              $('#phonenumber').val(parseInt(value));
            }
       });


	$("#login").popover({
		html: true,
		placement : 'bottom',
		content: function() {
          return $('#popover-content').html();
        }
	});
    jQuery(document).on('click','#showChart',function(eve) {
      let startDate = $('input[name="startDate"]').val();
      let endate =  $('input[name="endDate"]').val();
      if($.trim(startDate).length > 0 && $.trim(endate).length == 0){
        tost('Please select Date to and submit request again.','Error',1000);
        return false;
      }
      $.ajax({
        url: "{{url('customer/get-charts')}}",
        method: "POST",
        data : {startDate : startDate , endDate : endate },
        beforeSend : function () {
          $('.loader_div').show();
          if (myLineChart != null) { myLineChart.destroy(); myPieChart.destroy();}
        },
        complete : function () {
          $('.loader_div').hide();
        },
        success: function(data) {
          var month = [];
          var count = [];
          var pieCampName = []
          var pieCount = []

          $('.totalLeads').html(data.lead_count)


          if (data.lead_count !=0 ) {
            for (var i = 0; i < data.camp_chart.length; i++) {
               month.push(data.camp_chart[i].monthname);
               count.push(data.camp_chart[i].lead_count);
            }
            for (var i = 0; i < data.campChart.length; i++) {
               pieCampName.push(data.campChart[i].title);
               pieCount.push(data.campChart[i].count);
            }
          }else{
            month = ['No Record'];
            count = [0];
            pieCampName = ['No Record'];
            pieCount = [0];

            if (eve.originalEvent != undefined) {
                $.toast({
              heading             : 'Warning',
              text                : 'No Data found please try with new entries.',
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'warning',
              hideAfter           : 5000,
              position            : 'top-right'
            });

            }

          }





          var ctx = document.getElementById("myBarChart");
          window.myLineChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: month,
              datasets: [{
                label: "Lead",
                backgroundColor: "rgba(2,117,216,1)",
                borderColor: "rgba(2,117,216,1)",
                data: count,
              }],
            },
            options: {
              scales: {
                xAxes: [{
                  time: {
                    unit: 'month'
                  },
                  gridLines: {
                    display: false
                  },
                  ticks: {
                    maxTicksLimit: 6
                  }
                }],
                yAxes: [{
                  ticks: {
                    min: 0,
                    max: Math.max(...count),
                    maxTicksLimit: 5
                  },
                  gridLines: {
                    display: true
                  }
                }],
              },
              legend: {
                display: false
              }
            }
          });


          var ctx1 = document.getElementById("myPieChart");
          window.myPieChart = new Chart(ctx1, {
            type: 'pie',
            data: {
              labels: pieCampName,
              datasets: [{
                data: pieCount,
                backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#28a745','#FF6633',
                                  '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6','#6666FF',
                            		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                            		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                            		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                            		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                            		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                            		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                            		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                            		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                            		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6',
                                ],
              }],
            },
          });

        },
        error: function(data) {
          console.log(data);
        }
      });
    })
    $('#showChart').trigger('click');
  });

  </script>

  <script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
<script>
$("[data-toggle=popover]").each(function(i, obj) {

$(this).popover({
  html: true,
  content: function() {
    var id = $(this).attr('id')
    return $('#popover-content-' + id).html();
  }
});

});
</script>


  @endsection
