<?php
$campaignpermissiontype = '';
if( !empty($CampaignPermissions) )
{
    if( isset($CampaignPermissions[0]) )
    {
        $campaignpermissiontype = $CampaignPermissions[0]->permission_type;
    }
}
?>
<div class="table-responsive latest-champaign-table recordsTableStop">
    <table class="table">
        <tr>
            <th></th>
            <th>Campaign Name</th>
            <th>Integration</th>
            <th>Key</th>
            <th style="width:257px;">Campaign Status</th>
            <th style="width:100px;">Actions</th>
        </tr>

            @forelse($camps as $camp)
                <tr <?php if( $camp->isResetTemplate == 'no' && $camp->template_mismatch == 'yes' ){?>data-toggle="tooltip" title="we have found a template mismatch. Please reset template from edit page." data-placement="bottom"  style="background: #fff3cd;color: #856404;"<?php }?> >
                    <td>
                        @can(config('permissions.data.change-campaign-status.name'))
                            <div class="off-on-button">
                                <label class="switch">
                                    <input data-toggle="tooltip" title="Change Status" data-toggle="modal" data-target="#confirm-status-modal" id="{{ $camp->id }}" data-status="{{ $camp->status }}" data-table="{{ Crypt::encrypt('campaigns') }}"  type="checkbox" @if($camp->available_days=="" || $camp->step < 6 || $camp->invalid_step > 0) class="incompleteCampData" @else  class="statusRecord" @endif @if($camp->step==6 && $camp->status==1) checked="checked" @endif>
                                    <span class="slider @if($camp->step==6 && $camp->status==1) fa-toggle-on @else fa-toggle-off @endif">
                                        <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                        <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                    </span>
                                </label>
                            </div>
                        @endcan
                    </td>
                    <td>
                        <span class="latest-campaign-name">
                            <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 2 ) ){?>
                                <a href="@if($camp->step==6) {{$url.'/view-campaign'}}/{{Crypt::encrypt($camp->id)}} @else javascript:void(0) @endif">{{ $camp->title }}</a>
                            <?php } else if(  $campaignpermissiontype == 4 ) {
                                $isPermissionExist = false;
                                $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'view');
                                if($isPermissionExist){ ?>
                                    <a href="@if($camp->step==6) {{$url.'/view-campaign'}}/{{Crypt::encrypt($camp->id)}} @else javascript:void(0) @endif">{{ $camp->title }}</a>
                            <?php } else {?>
                                <a href="javascript:void(0)">{{ $camp->title }}</a>
                            <?php }
                            } else { ?>
                                <a href="javascript:void(0)">{{ $camp->title }}</a>
                            <?php } ?>
                        </span>
                        <span class="date-campaign">{{date('m/d/Y', strtotime($camp->updated_at))}}</span>
                    </td>
                    <td>{{ $camp->type==1 ? 'Email' : 'Api'}}</td>
                    <td>{{ $camp->email }}</td>
                    <td>
                        <ul class="home-camp-list">
                            @if($camp->step < 6 || $camp->invalid_step > 0)
                                <li class="camp-inactive"><i class="fa fa-circle"></i> CAMPAIGN INCOMPLETE</li>
                                <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 3 ) ){?>
                                    <li><a href="{!! $url.'/setup-campaign' !!}/{{Crypt::encrypt($camp->id)}}">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                <?php } else if(  $campaignpermissiontype == 4 ) {
                                    $isPermissionExist = false;
                                    $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'edit');
                                    if($isPermissionExist){ ?>
                                        <li><a href="{!! $url.'/setup-campaign' !!}/{{Crypt::encrypt($camp->id)}}">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                <?php }else {?>
                                    <li><a href="javascript:;">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                <?php }
                                    } else {?>
                                    <li><a href="javascript:;">Complete these steps to begin receiving Direct Connect calls!</a></li>
                                <?php } ?>
                            @else
                                <li class="camp-active"><i class="fa fa-circle"></i> CAMPAIGN COMPLETE</li>
                            @endif
                        </ul>
                    </td>
                    <td>
                        <ul class="icon-social">
                            <?php if( $isBusinessOwner || ( $campaignpermissiontype == 1 || $campaignpermissiontype == 3 ) ){?>
                            <li><a href="{!! $url.'/setup-campaign/'.Crypt::encrypt($camp->id)!!}"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a></li>
                        <?php } else if(  $campaignpermissiontype == 4 ) {
                            $isPermissionExist = false;
                            $isPermissionExist = Helper::checkCampaignPermission($CampaignPermissions, $camp->id,'edit');
                            if($isPermissionExist){ ?>
                                <li><a href="{!! $url.'/setup-campaign/'.Crypt::encrypt($camp->id)!!}"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a></li>
                        <?php }
                        }
                            if($camp->step==6)
                            { ?>
                                @can(config('permissions.data.reporting-list.name'))
                                    <li><a href="{{$url.'/campaign-leads'}}/{{ Crypt::encrypt($camp->id) }}"><img src="{{asset('assets/customer/img/lead-d.png')}}"></a></li>
                                @endcan
                            <?php }?>
                            @can(config('permissions.data.delete-campaign.name'))
                                <li><a class="deleteRecord" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" id="{{ $camp->id }}" data-table="{{ Crypt::encrypt('campaigns') }}"><img src="{{asset('assets/customer/img/delete.png')}}"></a></li>
                            @endcan
                        </ul>
                    </td>
                </tr>
            @empty
                <tr><td colspan="5">You have not created any campaigns.</td></tr>
            @endforelse

    </table>
</div>

@if(isset($filter))
    {!! $camps->appends(['campFilter'=>$filter,'business_id'=>$business_id])->links() !!}
@else
    {!! $camps->appends(['business_id'=>$business_id])->links()!!}
@endif

<script>
$(document).ready(function()
{
    $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function()
{
    $(".clicked").click(function()
    {
        var campId  = $(this).attr('id');
        var status  = 0;
        if($(this).is(':checked'))
        {
            status  = 1;
        }
        else
        {
            status  = 0;
        }

        $.ajax({
            url                : siteroot + '/customer/updateCampaignStatus/'+campId+'/'+status,
            type               : 'GET',
            dataType           : 'html',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            beforeSend : function() {
                $(".loader_div").show();
            },
            complete   : function() {
                $(".loader_div").hide();
            },
            success    : function(response) {
                var response    = JSON.parse(response);
                $(".loader_div").hide();
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
            }
        });
    });
});
</script>
