@extends('layouts.customer')
@section('content')
@php
$dayArr = array(
   'mon'=>'Monday',
   'tue'=>'Tuesday',
   'wed'=>'Wednesday',
   'thu'=>'Thursday',
   'fri'=>'Friday',
   'sat'=>'Saturday',
   'sun'=>'Sunday'
);
$d = array();
if (isset($campData['step'])) {
   for ($i=1; $i <= $campData['step']; $i++) {
     $d[$i] = $i;
   }
}
$api_dept_id = '';
if(isset($campData['api_dept_id']) && trim($campData['api_dept_id']) !='' ){
  $api_dept_id = $campData['api_dept_id'];
}
@endphp


<?php
$replacements=[];
 if(isset($campData['campaignTags']))
 {
      foreach($campData['campaignTags'] as $tags)
      {
           if( $tags->tagName == 'phone' || $tags->tagName == 'email' || $tags->tagName == 'lead_track_no' ) {
              continue;
          }
          $replacements[]=$tags->tagName;

    }
}
$namestringdisplay="<p>New Contact Us Page opportunity received from {name}.</p><p>The customer's location is {location}.</p><p>The message received is the following: {message}.</p>";
                $namestringdisplaychange = App\Helpers\GlobalFunctions::parserOut($campData['testMail'], $namestringdisplay,$campData['campId']);
// $locationstring = App\Helpers\GlobalFunctions::emailReplacement($name_arr, $replacements, $locationstringdisplay);
// if(isset($locationstring->content))
// {
//    $locationstringdisplay=$locationstring->content;
// }
// $messagestring = App\Helpers\GlobalFunctions::emailReplacement($name_arr, $replacements, $messagestringdisplay);
// if(isset($messagestring->content))
// {
//    $messagestringdisplay=$messagestring->content;
// }
?>

<style>
#leadEmailReceiver-error{
    margin-top: 78px;
    width: 30%;
    left: 21px;
}
#leadEmailReceiver[]-error{
    margin-top: 78px;
    width: 30%;
    left: 21px;
}

@media (min-width:1025px)
{
#leadEmailReceiver\[\]-error { width: 32%;left: 370px; top: 37px;}
#errorMsgg label.has-error::before {transform: rotate(-90deg); left: -9px; top: 6px;}
#errorMsgg label.has-error::after { transform: rotate(-90deg); left: -8px; top: 7px;}
#termserrorMsgg label.has-error{top: 30px;width: 30%}
}
@media (max-width:1024px)
{
#errorMsgg label.has-error::before, #errorMsgg label.has-error::after { display: none;}
#termserrorMsgg label.has-error::before, #termserrorMsgg label.has-error::after { display: none;}
#termserrorMsgg label.has-error{margin-top: -14px;}
#leadEmailReceiver\[\]-error { position: relative;}
}
</style>
   <div class="container-fluid">
      <!-- Icon Cards-->
  <div class="row">
         <div class="col-md-12">
            <div class="tabbable-panel Campaigns-tabs">
               <div class="tabbable-line">
                  <ul class="nav nav-tabs nav-campaigns">
                     @php
                     $tabArr = array('SEND TEST LEAD','SET UP TAGS','SET UP CALL SCRIPT','COMPANY CONTACTS','SET CAMPAIGN HRS','REVIEW CAMPAIGN');
                     @endphp
                     @php $i = 0; @endphp
                     @foreach($tabArr as $tab)
                     @php $i++; @endphp
                     <li class="nav-item">
                        <a id="{{$campData['campId']}}" class="active_prev next_me{{$i}} @if($i > $campData['step']) disabled @endif @if($campData['step']==1 && $i==1) active @endif" href="#step-{{$i}}" data-toggle="tab">{{$tab}}</a>
                     </li>
                     @endforeach
                  </ul>


                  {{Form::model($campData,array('url' => $url.'/save-campaign','id' => 'add-campaign-steps','class' => 'add-campaign-steps','autocomplete' => 'off'))}}
                  {{Form::hidden('step',$campData['step'],['class'=>'campaignStep'])}}
                  {{Form::hidden('invalid_step',$campData['invalid_step'],['class'=>'campaignInvalidStep'])}}
                  {{Form::hidden('campId',Crypt::encrypt($campData['campId']),['class'=>'campId'])}}
                  <div class="tab-content campTabs">
                    <div class="row alert alert-success alertPopups @if(!(Session::has('message'))) d-none @endif">
                       <div class="col-md-12">{!! Session::get('message') !!}</div>
                    </div>
                    @php Session::forget('message'); @endphp
                     <!-- Tab Step 1 -->
                     <div class="tab-pane active" id="step-1">
                        <h2 class="campaign_name"><span class="campaignNameSpan">{{$campData['campaignsTitle']}}</span> <img class="campaignNameEdit" style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                        <h2 class="hide-email-copy" style="font-size:15px">
                            STEP 1.
                            <?php if( $campData['campType']==1 ){?>
                                Copy your unique email address and add it as an additional recipient to receive a copy of leads sent from your chosen lead source.
                            <?php } else {?>
                                Copy your unique api url and add it as an additional recipient to receive a copy of leads sent from your chosen lead source.
                            <?php } ?>
                        </h2>
                        <div class="row">
                           <div class="col-md-8">
                              <div class="input-group mb-3">
                                 <input type="text" class="form-control" readonly="true" value="{{$campData['campType']==1 ? $campData['email'] : URL('api/api-form-data').'/'.$campData['email']}}" id="emailCopy">
                                 <div class="input-group-append">
                                    <span class="input-group-text email-copy-btn"  id="basic-addon2" onclick="copyViaID('emailCopy')" data-copy="{{$campData['campType']==1 ? 'email' : 'url'}}">Click here to copy the {{$campData['campType']==1 ? 'email address' : 'URL'}}</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row hide-email-copy">
                           <div class="col-md-12">
                              <div class="bd-callout-bd-callout-info">
                                  <h4><span>Your unique {{$campData['campType']==1 ? 'email' : 'API URL'}} is</span> {{$campData['campType']==1 ? $campData['email'] : URL('api/api-form-data').'/'.$campData['email']}}</h4>
                                 @if($campData['campType']==1)
                                    <!-- This email address must be added <strong>as recipient </strong>to receive lead/form submissions from this lead source.<br/><br/> -->
                                    <b>Example:</b> If this campaign is for a website “Contact Us” form, you would add this unique email address as an additional recipient to receive a copy whenever a website visitor submits an enquiry via this form.
                                 @else
                                    <!-- This API URL must be hit to receive lead/form submissions from this lead source while your lead form is submitted.<br/><br/> -->
                                    <b>Example:</b> if this campaign is for your website « contact us » page then submit all the form's data to given api url using a curl request.
                                 @endif
                              </div>
                           </div>
                        </div>

                        <h2 style="margin-top:15px;font-size:15px" class="show-email-copy">STEP 2. After adding the unique {{$campData['campType']==1 ? 'email' : 'API URL'}} to your chosen lead source, please use that lead source to send a test lead/enquiry. You will to send a test so that you can proceed to the next step.</h2>
                        <div class="row show-email-copy" style="display:none">
                           <div class="col-md-12">
                              @if($campData['step'] > 1)
                                 <div class="bd-callout-bd-callout-success">
                                    <h4 style="padding:0;margin:0 0 10px;"><i class="fa fa-check-circle"></i> Enquiry received </h4>
                                 </div>
                              @else
                                 <div class="bd-callout-bd-callout-warning">
                                    <p>Place a test enquiry on your web form and wait 2 minutes</p>
                                    <p>Once we have recieved your enquiry you can proceed to <b>'SET UP TAGS'</b> to map the captured fields</p>
                                    <p>Once your test enquiry has been received, you have to click next to get to next steps.
                                 </div>
                                 <div class="bd-callout-bd-callout-danger">
                                    <b>*If you are having difficulty with these steps or believe you have followed these steps but are still seeing this message please <a href="javascript:void(0);" data-toggle="modal" data-target="#contact-us-modal" >contact us</a> for assistance.</b>
                                 </div>
                              @endif
                           </div>
                        </div>

                        <?php /*div class="row">
                           <div class="col-md-6">
                              <span class="email-add">CAMPAIGN EMAIL ADDRESS</span>
                              <div class="campEmailDiv">
                                 {{ $campData['email'] }}
                              </div>
                              <h4>SET UP CHECK LIST</h4>
                              <div class="campaign-checkbox">
                                 <div class="form-group">
                                    <input disabled @if($campData['step'] > 1) checked @endif class="checkL" type="checkbox" id="check1" value="1" name="checklist[]">
                                    <label for="check1"><span class="circle-checkbox"></span><span class="recipient-address">I added the custom email address as recipient to my contact form</span> </label>
                                 </div>
                                 <div class="form-group">
                                    <input disabled @if($campData['step'] > 1) checked @endif class="checkL" type="checkbox" id="check2" value="2" name="checklist[]">
                                    <label for="check2"><span class="circle-checkbox"></span><span class="recipient-address">I sent a test lead </span> </label>
                                 </div>
                                 <div class="form-group">
                                    <input disabled @if($campData['step'] > 1) checked @endif class="checkL" type="checkbox" id="check3" value="3" name="checklist[]">
                                    <label for="check3"><span class="circle-checkbox"></span><span class="recipient-address">I waited for one minute </span> </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-1">
                              <a class="campPops" id="campStep1Pop" tabindex="0"
                                 role="button"
                                 data-html="true"
                                 data-toggle="popover"
                                 data-placement="right"
                                 data-trigger="focus"
                                 data-content='<div class="popover-custom-content">
                                 <div class="receipt-box-text"><h2> Add this custom email address as recipient to your form</h2><span class="cross-recept"><a href="#" data-dismiss="popover" class="got-recipient"><img src="{{asset('assets/customer/img/cross-pop.png')}}"></a></span></div>
                                 <h6>This email address must be added <strong>as recipient </strong>to receive lead/form submissions from this lead source. </h6>
                                 <h6><i>Example: if this campaign is for your website « contact us » page, add this email address to receive a copy of the email when a customer submits a new form.</i></h6>
                                 <h5><a href="#" data-dismiss="popover" class="got-recipient">GOT IT</a></h5>
                                 </div>'>
                                 <div class="inner-campaign-circle">
                                    <div class="outer-campaign-circle"></div>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-7"></div*/?>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p><input style="width:2%;vertical-align:middle" type="checkbox" @if($campData['step'] > 1 )disabled @endif id="CampaigntermsCheckbox" <?php if($campData['terms'] == 1 ){?>checked<?php } ?> value="1" name="terms" class="" /> <label for="CampaigntermsCheckbox">I confirm that I have added this email address to my chosen lead source and after doing so I have sent a test lead.</label></p>
                                    <p style="margin-bottom:0px;" id="termserrorMsgg"></p>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                           <div class="col-md-12 last-next-preview-btn">
                              <div class="Submit-campaign step1BtnRow text-right">
                                 <button id="step_1_btn" type="button" class="btn add-campaign submitBtnCls">Next</button>
                                 <!-- @if($campData['step']==1)
                                    <button id="" type="button" class="btn disabled">Next</button>
                                 @else
                                    <button id="step_1_btn" type="button" class="btn add-campaign submitBtnCls">Next</button>
                                 @endif -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Tab Step 2 -->
                     <div class="tab-pane" id="step-2">
                       <h2 class="campaign_name"><span class="campaignNameSpan">{{$campData['campaignsTitle']}}</span>  <img class="campaignNameEdit" style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                        <h2>Create Tags To Extract Important Information For Your Call Script</h2>
                        <p>
                        <b>What Are Tags?</b>
                        <p>Setting up tags only takes a few minutes. Once created, these tags help pull out important information from your lead or form submissions and shares it with you over a phone call.</p>

                        <p>In this way, when you receive a Direct Connect call to notify that a new customer has filled out your form, you also get access to the key information you need to connect with the customer right away.</p>
                         </p>


                        <p><b>How To Create Tags?</b>
                        <p>Creating tags is easy and you can always go back and modify or delete your tags at anytime.</p>
                        <p>To get started, you first need to send a test lead or enquiry to the unique email address you set up for this campaign.  When a test has been sent, you should see it appear below.</p>

                        <p>To create a new tag, you will use your mouse to click and highlight key customer details that you’d like to include in your call script.   You will highlight the field answer, and when releasing the mouse button a small window will appear with the option to choose the “Tag Name” that best describes the information that you’ve highlighted.</p>

                        <p>If the information doesn’t fit in any of the categories already mentioned, you can always choose to create a new tag by selecting “Create a custom tag.”</p> </p>

                        <p><b>PRO TIP:</b> Remember to highlight your customer’s phone number and tag it as “Phone.” Our system needs this information to trigger a call to your customer.</p>
                        <!-- <p>You also need to highlight the call summary, and name this tag <b>"Message"</b> Our system uses this tag to read the full call summary to your customer.</p> -->
                        <div class="tags-script">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="form-group receivedCampMail">
                                    @if(isset($campData['testMail']) && $campData['testMail']!='')
                                       <div class="form-group">
                                          <!-- <span>MAIL RECEIVED FROM "{{$campData['email']}}".</span> -->
                                          <span>
                                              @if($campData['campType'] == 2)
                                                API data successfully received.
                                              @else
                                                Email data successfully received. @if($campData['testMail'] != '') <a href="javascript:;" class="btn float-right" data-id="{{Crypt::encrypt($campData['campId'])}}" id="refreshCampaignTemplate">Reset Template</a>@endif
                                              @endif
                                          </span>
                                          <div id="textDescription" class="tagSelect">
                                             {!!$campData['testMail']!!}
                                          </div>
                                           <div class="note-cstm">
                                                 <p class="mb-0 text-center">Our system uses phone tag to trigger the call to your customer.</p>
                                             </div>
                                             <br>
                                           <div class="note-cstm">
                                                 <p class="mb-0 text-center">Use the “Lead/Ticket ID (for CRM tracking)” Tag if your leads include a unique number/code.  When a call is triggered we send a call summary email and this will include the unique number/code (if used) so you can match the call record to the lead within your CRM).</p>
                                             </div>
                                           <div class="note-cstm" style="margin-top:10px;">
                                                 <p class="mb-0 text-center">Use the “Email (for CRM tracking)” Tag if your leads include a unique email.  When a call is triggered we send a call summary email and this will include the unique email (if used) so you can match the call record to the lead within your CRM).</p>
                                             </div>
                                       </div>
                                    @else
                                       <div class="bd-callout bd-callout-warning">
                                           @if($campData['campType']==1)
                                           <h6>Oops! Your sample email hasn’t been received yet.</h6>
                                          @else
                                          <h6>Oops! Your sample email hasn’t been received yet.</h6>
                                          @endif
                                          <span><a id="check_mail_again" href="javascript:void(0)" data-camp="{{$campData['campId']}}">Click here</a> to refresh this page to check again.
If this doesn’t work, please send check that you have added the email address to receive a copy of the lead from your chosen lead source, and send another test lead.</span>
                                          <a href="javascript:void(0)"></a>
                                       </div>
                                    @endif
                                 </div>
                                 <!--div class="step2Msg bd-callout bd-callout-info @if(!(isset($campData['testMail']) && $campData['testMail']!='')) d-none @endif">
                                    <h4>Highlight the customer details that you would like used in your call script.<br/>You will only need to highlight the customer details, not the field name</h4>
                                    After highlighting the customer details categorize it by creating a "Tag Name"<br><br>
                                    <b>Note:</b>
                                    <ul>
                                       <li>Ensure you highlight the customers <u>phone number</u>, and call this tag "Phone"<br/><i>This step is important as our system uses this tag to trigger the call to the customer.</i>
                                       </li>
                                       <li>Ensure you highlight the <u>call summary</u>, and call this tag "Message"<br/><i>This step is important as our system uses this tag to read the full call summary to the customer.</i></li>
                                    </ul>
                                 </div-->
                              </div>
                              <?php /*div class="col-md-1">
                                 <a class="campPops" id="campStep2Pop" tabindex="0"
                                    role="button"
                                    data-html="true"
                                    data-toggle="popover"
                                    data-placement="right"
                                    data-trigger="focus"
                                    data-content=' <div class="popover-custom-content">
                                    <div class="receipt-box-text"><h2>INSTRUCTIONS:</h2><span class="closepop cross-recept"><img src="{{asset('assets/customer/img/cross-pop.png')}}"></span></div>
                                    <h6>1. Please highlight the form responses that you would like used in your call script. You will only need to highlight the response, not the field name.<br>
                                    2. After highlighting the response, please create a "Tag Name" for this field.<br>
                                    3. Please ensure you highlight the customers phone number, and call this tag "Phone" - This step is important as our system uses this tag to trigger the call to the customer.<br>
                                    4. Please ensure you highlight the call summary, and call this tag "Message" - This step is important as our system uses this tag to read the full call summary to the customer.<br>
                                    5. Please highlight all relevant answers, then click "next".</h6>
                                    <h5><a href="#" class="closepop">GOT IT</a></h5>
                                    </div>'>
                                    <div class="inner-campaign-circle">
                                       <div class="outer-campaign-circle">
                                       </div>
                                    </div>
                                 </a>
                              </div*/?>
                           </div>
                        </div>
                        <div class="row clearfix">
                           <div class="col-md-12 last-next-preview-btn">
                              <div class="Submit-campaign">
                                 <a class="float-left backStepBtn" data-step="#step-1" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                 <button id="step_2_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Tab Step 3 -->
                     <div class="tab-pane" id="step-3">
                       <h2 class="campaign_name"><span class="campaignNameSpan">{{$campData['campaignsTitle']}}</span>  <img class="campaignNameEdit" style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                        <h2>Create Your Call Script</h2>
                        <p>This script is the information that will be read out to your team when a Direct Connect call is received.</p>
                        <div class="tags-script">
                           <div class="row">
                              <div class="col-md-12" >
                                  <!-- <div id="ajaxTagContainer">
                                         <div class="clearfix mb-3 tagsme">
                                         @if(isset($campData['campaignTags']))
                                            @foreach($campData['campaignTags'] as $tags)
                                                <?php if( $tags->tagName == 'phone' || $tags->tagName == 'email' || $tags->tagName == 'lead_track_no' ) {
                                                    continue;
                                                } ?>
                                               <span style="cursor:pointer;" class="badge badge-pill badge-primary mr-1 tagSpan" data-custom-tag="<?php echo $tags->isCustomTag; ?>" data-val="{<?php echo $tags->tagName; ?>}">{{ $tags->tagName }}</span>
                                            @endforeach
                                         @endif
                                       </div>
                                   </div> -->
                                   <br/>
                                <!--  <p><b>You can either use this standard script for your campaign, or select edit below to create your own custom script.</b></p>
                                 <p>To set your tags, hightlight the customer details and then drag and drop, your own tag from the campaign tags ablove.</p> -->

                                 <div class="sample_script">
                                   <p><b>Sample Script (Using Tags):</b></p>
                                   {!! html_entity_decode($namestringdisplay) !!}

                                   <br/>
                                   <p><b>Sample Script (How it sounds):</b></p>
                                   <div class="showSampleScript">
                                     {!! html_entity_decode($namestringdisplaychange) !!}

                                   </div>
                                 </div>

                                 <div class="yourScript">
                                   <p><b>PRO TIP 1:</b> Create a script that will provide the key details you need before speaking to a potential customer.
Generally you wont need/want to include the customers Phone or Email address in the script as you will already have received this when the lead was sent to your email or CRM.
Be sure to only include tags for information that you need to know before you speak to the customer.</p>
                                  <div id="ajaxTagContainer">
                                         <div class="clearfix mb-3 tagsme">
                                         @if(isset($campData['campaignTags']))
                                            @foreach($campData['campaignTags'] as $tags)
                                                <?php if( $tags->tagName == 'phone' || $tags->tagName == 'email' || $tags->tagName == 'lead_track_no' ) {
                                                    continue;
                                                } ?>
                                               <span style="cursor:pointer;" class="badge badge-pill badge-primary mr-1 tagSpan" data-custom-tag="<?php echo $tags->isCustomTag; ?>" data-val="{<?php echo $tags->tagName; ?>}">{{ $tags->tagName }}</span>
                                            @endforeach
                                         @endif
                                       </div>
                                   </div>

                                   <p>
                                    <b>PRO TIP 2:</b>
                                    Customise the script below to suit your needs, you can insert your custom Tags by clicking on them, or by typing the name in between a brace:  {example}
                                   </p>
                                   <p><b>Your Campaign Script:</b></p>
                                 <div class="form-group">
                                    <textarea rows="4" class="mention form-control validate campaignTemplate" id="mentions-field" name="campaignTemplate" cols="50" aria-required="true" aria-invalid="false">@if(isset($campData['campaignTemplate']) && !empty($campData['campaignTemplate'])){{ $campData['campaignTemplate'] }} @else New {{$campData['campaignsTitle']}} enquiry received from. {name}.&#013;The message received is {message}. @endif</textarea>
                                    <!-- <textarea rows="4" class="mention form-control validate campaignTemplate" id="mentions-field" name="campaignTemplate" cols="50" aria-required="true" aria-invalid="false"> {{ empty($campData['campaignTemplate'])?'empty':'full' }}</textarea> -->
                                 </div>
                                 </div>
                              </div>
                              <?php /*div class="col-md-1">
                                 <a class="campPops" id="campStep3Pop" tabindex="0"
                                    role="button"
                                    data-html="true"
                                    data-toggle="popover"
                                    data-placement="right"
                                    data-trigger="focus"
                                    data-content=' <div class="popover-custom-content">
                                     <div class="receipt-box-text"><h2> Use this standard script for your campaign</h2><span class="closepop cross-recept"><img src="{{asset('assets/customer/img/cross-pop.png')}}"></span></div>
                                    <h6>You can also edit it and create your own custom script.
                                    Use the tags based on your form fields : double-click on a tag to insert unique information from your lead source. </h6>
                                    <h5><span class="closepop">GOT IT</span></h5>
                                    </div>'>
                                    <div class="inner-campaign-circle">
                                       <div class="outer-campaign-circle">
                                       </div>
                                    </div>
                                 </a>
                              </div*/?>
                           </div>
                        </div>
                        <!--div class="bd-callout bd-callout-info">
                           <h4>Use this standard script for your campaign</h4>
                           After highlighting the customer details categorize it by creating a "Tag Name"<br><br>
                           You can also edit it and create your own custom script.
                           Use the tags based on your form fields.
                        </div-->
                        <div class="row clearfix">
                           <div class="col-md-12 last-next-preview-btn">
                              <div class="Submit-campaign">
                                <p>Once you’ve finalized your script, select save and proceed to the next step.</p>
                                 <a class="float-left backStepBtn" data-step="#step-2" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                 <button id="step_3_btn" type="button" class="btn btn-warning float-right add-campaign submitBtnCls">Save</button>
                              </div>
                           </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <h2>Additional Tags</h2>
                                <div class="additionaltags">
                                    @if(isset($campData['campaignTags']))
                                       @foreach($campData['campaignTags'] as $tags)
                                           <?php if( $tags->tagName == 'phone' || $tags->tagName == 'email' || $tags->tagName == 'lead_track_no' ) { ?>
                                               <span style="cursor:pointer;" class="badge badge-pill badge-primary mr-1 additionaltagSpan" data-custom-tag="<?php echo $tags->isCustomTag; ?>" data-val="{<?php echo $tags->tagName; ?>}">{{ $tags->tagName }}</span>
                                           <?php } ?>
                                       @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                     </div>
                     <!-- Tab Step 4 -->
                     <div class="tab-pane" id="step-4">
                        <div class="campaign-time select-contact">
                          <div class="row">
                            <div class="col-md-8">
                              <h2 class="campaign_name"><span class="campaignNameSpan">{{$campData['campaignsTitle']}}</span>  <img class="campaignNameEdit" style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                              <h2>Add Company Contacts</h2>
                              <p>Select and add team members that can receive calls from this campaign.</p>
                              <p>Use this tab to add or remove a team member to receive calls from this Campaign (lead source).</p>
                              <p>You can also set the priority order by choosing which team member gets to receive the call first, second, third, and so on.</p>
                            </div>
                            <div class="col-md-4">
                              <div class="add-input float-right">
                                 <input type="hidden" class="form-control" id="custId">
                                 <input type="hidden" id="api_dept_id" value="{{$campData['api_dept_id']}}">

                                <?php
                                $no_of_contacts=0;
                                $user_data_check="";
                                $BusinessOwner= App\Model\BusinessProfile::find(Auth::user()->getBusinessId())->get_users()->wherePivot('is_owner','1');
                                if($BusinessOwner->where('register_step',3)->count()!=0)
                                {
                                          $user_data_check= $BusinessOwner->first();
                                }
                                 if(isset($user_data_check->is_group) && $user_data_check->is_group==1)
                                  {
                                      if(\App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->count()!=0)
                                       {
                                            $no_of_contacts = \App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->first()->plans->no_of_contacts;
                                       }

                                  }
                                else
                                {
                                if( isset($userRelation->user_plan->plans->no_of_contacts) ){
                                if($userRelation->user_plan->plan_type==1){
                                  $no_of_contacts=$userRelation->user_plan->plans->no_of_contacts;
                                }else{
                                  if(isset($userRelation->user_plan->promo_plans->no_of_contacts)){
                                    $no_of_contacts=$userRelation->user_plan->promo_plans->no_of_contacts;
                                  }
                                }
                              }
                                ?>
                                <?php }?>
                                <button class="btn btn-warning btn-mdb-color btn-rounded add_Contact add float-right openModal @if(!($campData['contactCount'] < $no_of_contacts)) d-none @endif"  data-target="#add-new-tag" type="button"><i class="fa fa-user"></i> Add new contact </button>
                                 <!-- <button class="btn btn-mdb-color btn-rounded getContacts add @if($campData['exContacts'] == 0) d-none @endif" data-url="{{URL('/customer/get-contacts')}}" type="button">Add an existing contact  </button> -->
                              </div>
                            </div>
                          </div>

						      <div class="row">
   							   <div class="col-md-12">
                              <div class="table-responsive latest-champaign-table">
                                 <table class="table contact-table">
                                    <thead>
                                       <tr>
                                          <th></th>
                                          <th>Name</th>
                                          <th>Email</th>
                                          <th>Phone No.</th>
                                          <th>Priority</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody id="tableRowTr" class="contact_data_listing">
                                         @include('customer.campaign.contact-data-list')
                                 </tbody>
                              </table>
                           </div>
                        </div>
						   </div>
						   <div class="row clearfix">
                        <div class="col-md-12 last-next-preview-btn">
                           {{Form::hidden('camp_contact_remove',null,['class'=>'camp_contact_remove'])}}
                              <div class="Submit-campaign">
                                 <a class="float-left backStepBtn" data-step="#step-3" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                 <button id="step_4_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                              </div>
                           </div>
                     </div>
                         <div class="row">
                             <div class="col-md-12">
                                 <h2 class="campaign_name">Call Summary Email</h2>
                                 <p>After a successful call has concluded, a Call Summary email will be sent containing the lead information, and a link to the call recording (if enabled in the “Call Settings”).</p>
                                 <div class="form-group">
                                     <p style="margin-bottom:0px;">Select who should receive the call alert.  You can choose one of the options below, or select both:</p>
                                     <p style="margin-bottom:0px;" id="errorMsgg"></p>
                                     <input style="width:2%;float:left" type="checkbox" id="contactAnsweredCall" <?php if($campData['leadEmailReceiver'] == 1 || $campData['leadEmailReceiver'] == 3){?>checked<?php } ?> value="1" name="leadEmailReceiver[]" class="" />
                                     <label style="margin-left:10px;margin-top:10px;" for="contactAnsweredCall">The Company Contact who answered the call.</label>

                                 </div>
                             </div>
                         </div>
                          <div class="row">
                             <div class="col-md-12">
	                             <div class="form-group ">
                                     <input style="width:2%;float:left" type="checkbox" id="anotherEmail" value="2" <?php if($campData['leadEmailReceiver'] == 2 || $campData['leadEmailReceiver'] == 3){?>checked<?php } ?> name="leadEmailReceiver[]" class="" />
                                     <label style="margin-left:10px;margin-top:10px;" for="anotherEmail">Another email address: Please select the checkbox if you wish to receive the Call Summary email on secondary Email Addresses</label>
                                     <div class="row" id="secondaryEmailContainer">
                                         <?php if($campData['leadEmailReceiver'] == 2 || $campData['leadEmailReceiver'] == 3){
                                             if(!empty($campData['secondaryEmails'])){
                                                 $emailsCOunt = count($campData['secondaryEmails']);
                                                 foreach ($campData['secondaryEmails'] as $key => $value) { ?>
                                                     <div class="secondaryEmailDiv col-lg-4 col-md-8 col-xs-12">
                                                         <div class="form-group row">
                                                             <label class="col-md-1 secondaryEmailLabel"><?php echo $key+1;?>.</label>
                                                             <div class="col-md-11">
                                                                 <input type="text" id="leadanotheremail" value="<?php echo $value->email;?>" name="leadanotheremail[]" class="form-control" placeholder="Enter the email address here"/>
                                                                 <i class="fa fa-trash secondaryEmailRemoveBtn" title="Remove" <?php if($emailsCOunt == 1){?>style="display:none;" <?php } ?>> </i> <i class="fa fa-plus-circle addMoreSecondaryEmailBtn" id="addMoreSecondaryEmailBtn" title="Additional email recipients" style="<?php if($emailsCOunt == 1){?>right:-10px;  <?php } ?>cursor:pointer;display:<?php if( $emailsCOunt == $key+1){?>block;<?php } else {?>none <?php } ?>"></i>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 <?php  }
                                             } else {?>
                                                 <div class="secondaryEmailDiv col-lg-4 col-md-8 col-xs-12">
                                                     <div class="form-group row">
                                                         <label class="col-md-1 secondaryEmailLabel">1.</label>
                                                         <div class="col-md-11">
                                                             <input type="text" id="leadanotheremail" value="" name="leadanotheremail[]" class="form-control" placeholder="Enter the email address here"/>
                                                             <i class="fa fa-trash secondaryEmailRemoveBtn" title="Remove" style="display:none"></i> <i class="fa fa-plus-circle addMoreSecondaryEmailBtn" id="addMoreSecondaryEmailBtn" title="Additional email recipients" style="right:-10px"></i>
                                                         </div>
                                                     </div>
                                                 </div>
                                        <?php }
                                    }else {?>
                                             <div class="secondaryEmailDiv col-lg-4 col-md-8 col-xs-12">
                                                 <div class="form-group row">
                                                     <label class="col-md-1 secondaryEmailLabel">1.</label>
                                                     <div class="col-md-11">
                                                         <input type="text" id="leadanotheremail" value="" name="leadanotheremail[]" class="form-control" placeholder="Enter the email address here"/>
                                                         <i class="fa fa-trash secondaryEmailRemoveBtn" title="Remove" style="display:none"></i> <i class="fa fa-plus-circle addMoreSecondaryEmailBtn" id="addMoreSecondaryEmailBtn" title="Additional email recipients" style="right:-10px"></i>
                                                     </div>
                                                 </div>
                                             </div>
                                    <?php } ?>
                                     </div>
                                 </div>
                            </div>
                         </div>
                     </div>
                     </div>

                     <!-- Tab Step 5 -->
                     <div class="tab-pane" id="step-5">
                        <div class="campaign-time">
                          <h2 class="campaign_name">{{$campData['campaignsTitle']}} <img style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                           <h2>Set The Campaign Hours to Receive Calls</h2>
                           <p>Never miss an opportunity! Direct Connect lets you set the hours that suit your schedule and hours of operation. Direct Connect calls will only be triggered during the hours you select.</p>
                           <p>Set your phone prefix/region below.</p>
                           <div class="row" id="CAMPAIGN_hrs">
                              <div class="col-md-4">
                                  <div class="col-md-12" style="margin-bottom:10px">
                                        <div class="form-group" style="margin-bottom:10px">
                                          <!--   <input style="width: 5%; float: left;margin-top: -6px;" type="checkbox" id="isFbCampaign" <?php if($campData['isFbCampaign'] == 1){?>checked<?php } ?> value="1" name="isFbCampaign" class="" />
                                            <label style="float: right;width: 93%;font-weight: bold;"> International Prefix already supplied. (Select this option if your customers phone number already have an international prefix applied.  Example:  +61406563549)</label> -->
                                           <!--label for="exampleInputEmail1">Country <span class="star">*</span></label-->
                                           {{Form::select('campaignCountry',$planCountries,null,['id'=>'campaignCountry','class'=>'form-control'])}}
                                        </div>
                                      <div class="d-inline cstm-info">
                                                  <img style="cursor:pointer" src="{{asset('assets/customer/img/u.png')}}">
                                      </div>
                                      <div class="icon-tip"><p class="mb-0">When a lead/enquiry is received, Direct Connect will convert the phone number to the international prefix of either the default country of origin selected, or allow for pre-applied API driven systems.(Such as: Facebook)</p></div>

                                        <div class="row icon-tip">
                                            <!--div class="col-md-6">
                                              <div class="form-group">
                                                    {{ Form::number('retry_time',null,['class'=>'form-control','placeholder'=>'Retry Time','min'=>'0','max'=>'120'])}}
                                              </div>
                                          </div-->

                                        </div>

                                        {{ Form::hidden('retry_time',0,['class'=>'form-control','placeholder'=>'Retry Time','min'=>'0','max'=>'120'])}}
                                         <!--div class="row">
                                            <div class="col-md-12">
                                              <div class="form-group">
                                                <span class="retry_time_p_data">Set the retry time and delay to trigger calls from enquiries which were received outside of your nominated business hours.</span>

                                              </div>
                                            </div>

                                        </div-->

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{Form::select('time_zone',$timezones,null,['id'=>'campaignTimezone','class'=>'form-control'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom:10px">
                                            <input style="width: 5%; float: left;margin-top: -6px;" type="checkbox" id="isFbCampaign" <?php if($campData['isFbCampaign'] == 1){?>checked<?php } ?> value="1" name="isFbCampaign" class="" />
                                            <label style="float: right;width: 93%;font-weight: bold;"> International Prefix already supplied. (Select this option if your customers phone number already have an international prefix applied.  Example:  +61406563549)</label>
                                           <!--label for="exampleInputEmail1">Country <span class="star">*</span></label-->

                                        </div>

                                    </div>
                                    <div class=" clearfix"></div>
                                    <div class="col-md-12" style="float:left;" id="campaignAdvancedSettingsAContainer">
                                        <!-- <a href="javascript:;" style="text-decoration:none" id="campaignAdvancedSettingsA">Advanced settings</a> -->
                                    </div>
                                <!--     <div id="campaignCallTriggerToggleContainer" style="display:none"> -->
                                   <div id="campaignCallTriggerToggleContainer">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="off-on-button">
                                                    <label style="font-weight:bold">After Hours - Lead Concierge</b></label>
                                                   <!--  <div class="d-inline cstm-info afterHourTriggerI" style="float:right;z-index:6;position:relative">
                                                        <img style="cursor:pointer" src="{{asset('assets/customer/img/u.png')}}">
                                                    </div>
                                                    <div class="afterHour-icon-tip"><p class="mb-0">Leads received after hours can be held and set to trigger on your next available campaign hour.<br>For example, a lead received at 11 pm at night can be held and then triggered as a Direct Connect call at 9 am the next morning.</p></div> -->
                                                    <br>
                                                    <label class="switch">
                                                         {{ Form::hidden('triggerOutsideHoursLeads',null,['id'=>'triggerOutsideHoursLeadsInput'])}}
                                                        <span id="triggerOutsideHoursLeadsSpan" class="slider @if( $campData['triggerOutsideHoursLeads'] == 'yes' ) fa-toggle-on @else fa-toggle-off @endif">
                                                            <span class="campaign-cross"><i class="fa fa-times"></i></span>
                                                            <span class="campaign-tik"><i class="fa fa-check"></i></span>
                                                        </span>
                                                    </label>
                                                    <p>
                                                    	When turned ON, our Lead Concierge feature will hold any leads received outside of your approved Campaign hours – These leads will then be triggered at the next available campaign time.
                                                    </p>
                                                    <p>
														For example:  If your campaign hours are 9am to 5pm, a lead received at 8pm will not trigger a Direct Connect call as it’s outside your chosen campaign hours.   If Lead Concierge is activated, this call will be queued and triggered the following morning at 9am.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="campaignOutsideCallTriggerDelayTimeContainer" class="col-md-12" style="float:left;<?php if( $campData['triggerOutsideHoursLeads'] == 'no' ) {?>display: none;<?php }?>">
                                            <p> Set the delay between each lead when triggering stored leads.</p>
                                            <div class="form-group">
                                                <?php
                                                    if( isset($campData['retry_delay_second']) && $campData['retry_delay_second'] > 0 )
                                                        $retry_delay_seconddd  = $campData['retry_delay_second'];
                                                    else
                                                        $retry_delay_seconddd  = 5;
                                                ?>
                                                {!!Form::select('retry_delay_second', ['1'=>'1 Min','2'=>'2 Mins','3'=>'3 Mins','4'=>'4 Mins','5'=>'5 Mins - RECOMMENDED!','6'=>'6 Mins','7'=>'7 Mins','8'=>'8 Mins','9'=>'9 Mins','10'=>'10 Mins','11'=>'11 Mins','12'=>'12 Mins','13'=>'13 Mins','14'=>'14 Mins','15'=>'15 Mins'], $retry_delay_seconddd, ['class' => 'form-control filterCheckBox','id'=>'statusBox'])!!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div id="campaignCallTriggerDelayTimeContainer" class="col-md-12" style="float:left;display: none;"> -->
                                      <div id="campaignCallTriggerDelayTimeContainer" class="col-md-12" style="float:left;">
                                        <!-- <label style="font-weight:bold">Call Delay</label>
                                         --><!-- <p>Set a delay before Direct Connect triggers the call.</p> -->
                                        <div class="form-group">
                                            <?php
                                                if( isset($campData['call_delay_time']) && $campData['call_delay_time'] > 0 )
                                                    $call_delay_time  = $campData['call_delay_time'];
                                                else
                                                    $call_delay_time  = 0;
                                            ?>
                                            {!!Form::hidden('call_delay_time',$call_delay_time, ['class' => 'form-control'])!!}

                                            {{--{!!Form::select('call_delay_time', ['0'=>'0 Mins - RECOMMENDED for Best Result!','1'=>'1 Min - Best Result!','2'=>'2 Mins - Best Result!','3'=>'3 Mins - Strong Result!','4'=>'4 Mins - Strong Result!','5'=>'5 Mins - Strong Result!','6'=>'6 Mins - Strong Result!','7'=>'7 Mins - Good Result','8'=>'8 Mins - Good Result','9'=>'9 Mins - Good Result','10'=>'10 Mins - Good Result','11'=>'11 Mins - Good Result','12'=>'12 Mins - Good Result','13'=>'13 Mins - Good Result','14'=>'14 Mins - Good Result','15'=>'15 Mins - Good Result'], $call_delay_time, ['class' => 'form-control'])!!} --}}
                                        </div>
                                    </div>


                              </div>
                              <div class="col-md-8">
                                 <div class="calender-section">
                                    <div class="weekDays-selector week-days-section">
                                       <div class="row">
                                         <!--Old code -->
                                           <?php /* <div class="col-md-12">
                                               <div class="select_text">
                                                   <label for="day">Select Days  <span class="star">*</span></label>
                                               </div>
                                               <div class="WeekendBox">
                                                 <input  id="weekday-all" class="weekday checkAllBox" type="checkbox" <?php if( isset($campData['available_days']) && (array("mon","tue","wed","thu","fri","sat",'sun') == $campData['available_days'])) echo 'checked="checked"';?> >
                                                 <label class="weekDays allWeek" for="weekday-all">All</label>
                                                 <input id="weekday-mon" class="weekday" type="checkbox" name="availDays[]" value="mon" <?php if( isset($campData['available_days']) && in_array('mon', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-mon">M</label>
                                                 <input id="weekday-tue" class="weekday" type="checkbox" name="availDays[]" value="tue" <?php if( isset($campData['available_days']) && in_array('tue', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-tue">T</label>
                                                 <input id="weekday-wed" class="weekday" type="checkbox" name="availDays[]" value="wed" <?php if( isset($campData['available_days']) && in_array('wed', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-wed">W</label>
                                                 <input id="weekday-thu" class="weekday" type="checkbox" name="availDays[]" value="thu" <?php if( isset($campData['available_days']) && in_array('thu', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-thu">T</label>
                                                 <input id="weekday-fri" class="weekday" type="checkbox" name="availDays[]" value="fri" <?php if( isset($campData['available_days']) && in_array('fri', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-fri">F</label>
                                                 <input id="weekday-sat" class="weekday" type="checkbox" name="availDays[]" value="sat" <?php if( isset($campData['available_days']) && in_array('sat', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-sat">S</label>
                                                 <input id="weekday-sun" class="weekday" type="checkbox" name="availDays[]" value="sun" <?php if( isset($campData['available_days']) && in_array('sun', $campData['available_days'])) echo 'checked="checked"';?>>
                                                 <label class="weekDays" for="weekday-sun">S</label>
                                              </div>
                                            </div>
                                            */?>
                                         <!--end-->
                                                      <!-- new code -->
                                                    <div class="select_text">
                                                               <label for="day">Select Days  <span class="star">*</span></label>
                                                           </div>
                                                     <div class="col-md-12">

                                                           <div class="WeekendBox">
                                                                <?php /*<input  id="weekday-all" class="weekday checkAllBox" type="checkbox" <?php if( isset($campData['available_days']) && (array("mon","tue","wed","thu","fri","sat",'sun') == $campData['available_days'])) echo 'checked="checked"';?> >
                                                                <label class="weekDays allWeek" for="weekday-all">All</label>
                                                                */?>
                                                                <input id="weekday-mon" class="weekday" type="checkbox" name="availDays[]" value="mon" <?php if( isset($campData['available_days']) && in_array('mon', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-mon">Mon</label>
                                                                <input id="weekday-tue" class="weekday" type="checkbox" name="availDays[]" value="tue" <?php if( isset($campData['available_days']) && in_array('tue', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-tue">Tue</label>
                                                                <input id="weekday-wed" class="weekday" type="checkbox" name="availDays[]" value="wed" <?php if( isset($campData['available_days']) && in_array('wed', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-wed">Wed</label>
                                                                <input id="weekday-thu" class="weekday" type="checkbox" name="availDays[]" value="thu" <?php if( isset($campData['available_days']) && in_array('thu', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-thu">Thu</label>
                                                                <input id="weekday-fri" class="weekday" type="checkbox" name="availDays[]" value="fri" <?php if( isset($campData['available_days']) && in_array('fri', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-fri">Fri</label>

                                                          </div>
                                                          <div class="row">
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours From <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursFrom[working]',isset($campData['availHoursFromData']->working) ? $campData['availHoursFromData']->working:null,['class'=>'form-control validate','placeholder'=>'Hours available from', 'id'=>'availTimeFrom']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours To <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursTo[working]',isset($campData['availHoursToData']->working) ? $campData['availHoursToData']->working:null,['class'=>'form-control validate','placeholder'=>'Hours available to','id'=>'availTimeTo']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                             </div>
                                                     </div>
                                                     <!--Sat-->
                                                     <div class="col-md-12">

                                                        <div class="WeekendBox">

                                                                <input id="weekday-sat" class="weekday" type="checkbox" name="availDays[]" value="sat" <?php if( isset($campData['available_days']) && in_array('sat', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                <label class="weekDays" for="weekday-sat">Sat</label>

                                                          </div>
                                                          <div class="row">

                                                <?php
                                                      $availHoursFromDataSat=null;
                                                      $availHoursToDataSat=null;
                                                      if(isset($campData['availHoursFromData']->sat) && isset($campData['available_days']) && in_array('sat', $campData['available_days']))
                                                      {
                                                            $availHoursFromDataSat=$campData['availHoursFromData']->sat;

                                                      }
                                                      if(isset($campData['availHoursToData']->sat) && isset($campData['available_days']) && in_array('sat', $campData['available_days']))
                                                      {
                                                            $availHoursToDataSat=$campData['availHoursToData']->sat;

                                                      }
                                                ?>
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours From <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursFrom[sat]',$availHoursFromDataSat,['class'=>'form-control validate','placeholder'=>'Hours available from', 'id'=>'availTimeFromSat']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours To <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursTo[sat]',$availHoursToDataSat,['class'=>'form-control validate','placeholder'=>'Hours available to','id'=>'availTimeToSat']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                             </div>

                                                     </div>
                                                        <!--Sun-->
                                                     <div class="col-md-12">

                                                        <div class="WeekendBox">


                                                                <input id="weekday-sun" class="weekday" type="checkbox" name="availDays[]" value="sun" <?php if( isset($campData['available_days']) && in_array('sun', $campData['available_days'])) echo 'checked="checked"';?>>
                                                                  <label class="weekDays" for="weekday-sun">Sun</label>

                                                          </div>
                                                          <div class="row">
                                                 <?php
                                                      $availHoursFromDataSun=null;
                                                      $availHoursToDataSun=null;
                                                      if(isset($campData['availHoursFromData']->sun) && isset($campData['available_days']) && in_array('sun', $campData['available_days']))
                                                      {
                                                            $availHoursFromDataSun=$campData['availHoursFromData']->sun;

                                                      }
                                                      if(isset($campData['availHoursToData']->sun) && isset($campData['available_days']) && in_array('sun', $campData['available_days']))
                                                      {
                                                            $availHoursToDataSun=$campData['availHoursToData']->sun;

                                                      }
                                                ?>
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours From <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursFrom[sun]',$availHoursFromDataSun,['class'=>'form-control validate','placeholder'=>'Hours available from', 'id'=>'availTimeFromSun']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                                <div class="form-group md-form col-md-6">
                                                   <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours To <span class="star">*</span></label-->
                                                   {!!  Form::text('availHoursTo[sun]',$availHoursToDataSun,['class'=>'form-control validate','placeholder'=>'Hours available to','id'=>'availTimeToSun']) !!}
                                                   <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                             </div>

                                                     </div>

                                                     <!-- end code -->
									        </div>
									</div>

                                    <?php /* old code
                                    <div class="row">
                                       <div class="form-group md-form col-md-6">
                                          <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours From <span class="star">*</span></label-->
                                          {!!  Form::text('availHoursFrom',null,['class'=>'form-control validate','placeholder'=>'Hours available from', 'id'=>'availTimeFrom']) !!}
                                          <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                       </div>
                                       <div class="form-group md-form col-md-6">
                                          <!--label class="campHours" for="hour-3" data-error="wrong" data-success="right">Available Hours To <span class="star">*</span></label-->
                                          {!!  Form::text('availHoursTo',null,['class'=>'form-control validate','placeholder'=>'Hours available to','id'=>'availTimeTo']) !!}
                                          <span class=""><i class="glyphicon glyphicon-time"></i></span>
                                       </div>
                                    </div>
                                    */?>

                                 </div>
                              </div>
                           </div>

                           <div class="row clearfix">
                              <div class="col-md-12 last-next-preview-btn">
                                 <div class="Submit-campaign">
                                    <a class="float-left backStepBtn" data-step="#step-4" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                    <button id="step_5_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Tab Step 6 -->
                     <style>
                        .confirm-campaign-box dl dt{color:#666;}
                        .confirm-campaign-box dl dd{padding:0 0 10px 10px;}
                     </style>
                     <div class="tab-pane" id="step-6">
                       <h2 class="campaign_name"><span class="campaignNameSpan">{{$campData['campaignsTitle']}}</span>  <img class="campaignNameEdit" style="cursor:pointer" src="{{asset('assets/customer/img/edi-d.png')}}"></h2>
                        <h2>You’re almost there!  Please review and confirm your Campaign is correct.</h2>
                        <p>Below are the campaign details you have set up.   You can come back and edit these settings at any time. Please click “Save and Finish” to confirm and activate your Campaign!</p>
                        <div class="confirm-campaign-box">
                           <div class="new-campaign-confirm-text">
                              <dl>
                                 <dt>Campaign Title</dt>
                                 <dd>
                                 @if(isset($campData['campaignsTitle']))
                                    {{$campData['campaignsTitle']}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                 <dt>Campaign Template</dt>
                                 <dd>
                                 @if(isset($campData['campaignTemplate']))
                                    {{$campData['campaignTemplate']}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                 <dt>Digital Whisper (Example)</dt>
                                 <dd>
                                 @if(isset($campData['parserOutput']))
                                    {{$campData['parserOutput']}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                 <dt>Country</dt>
                                 <dd>
                                    @if(isset($campData['campaignCountryName']))
                                       {{$campData['campaignCountryName']}}
                                       ({{$campData['time_zone']}})
                                    @else
                                       N/A
                                    @endif
                                 </d>
                                 <dt>Available Days</h4>
                                 <dd>
                                 @php $days =''; @endphp
                                 @if(isset($campData['available_days']))
                                    @foreach($campData['available_days'] as $day)
                                       @php $days .= $dayArr[$day].', ';  @endphp
                                    @endforeach
                                 @else
                                    @php $days = 'N/A'; @endphp
                                 @endif
                                 {{rtrim($days,', ')}}
                                 </dd>
                                 <dt>Available Hours</dt>
                                 <dd>Mon - Fri</dd>
                                 <dd>
                                 @if(isset($campData['availHoursFromData']->working) && isset($campData['availHoursToData']->working))
                                    {{$campData['availHoursFromData']->working}} - {{$campData['availHoursToData']->working}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                 <dd>Sat</dd>
                                 <dd>
                                 @if(isset($campData['availHoursFromData']->sat) && isset($campData['availHoursToData']->sat))
                                    {{$campData['availHoursFromData']->sat}} - {{$campData['availHoursToData']->sat}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                  <dd>Sun</dd>
                                 <dd>
                                 @if(isset($campData['availHoursFromData']->sun) && isset($campData['availHoursToData']->sun))
                                    {{$campData['availHoursFromData']->sun}} - {{$campData['availHoursToData']->sun}}
                                 @else
                                    N/A
                                 @endif
                                 </dd>
                                 <dt>Company Contacts</dt>
                                 <dd>
                                    @if(isset($campData['campaignContact']))
                                       @forelse($campData['campaignContact'] as $ct)
                                          @if(isset($ct->contact->name))
                                             <span class ="cont_name">{{$ct->contact->name}} </span><span class ="cont_number">,{{$ct->contact->contact}}</span><span class ="cont_email">,{{$ct->contact->email}}</span><br>
                                          @endif
                                       @empty
                                          No Contact Added !
                                       @endforelse
                                    @endif
                                 </dd>
                              </dl>
                           </div>
                        </div>
                        <div class="row clearfix">
                           <div class="col-md-12 last-next-preview-btn">
                              <div class="Submit-campaign clearfix">
                                 <a class="float-left backStepBtn" data-step="#step-5" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>

                                 <button id="step_6_btn" data-url="{!! $url.'/campaigns'!!}" type="button" class="btn float-right campFinish submitBtnCls btn-warning">save and Finish</button>
                                 <!-- @if(Gate::check('customer')) -->
                                 <!-- @elseif(Gate::check('Has-access-to-create-edit-campaigns'))

                                 <button id="step_6_btn" data-url="{{$url.'/customers'}}" type="button" class="btn float-right campFinish submitBtnCls">Save and Finish</button>
                                 @endif -->

                              </div>
                              <div class="if_happy">
                                <p>If you are happy with the campaign, select save & finish. If you'd like to make changes, or add tags, select back.</p>
                            </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  {{Form::close()}}
               </div>
            </div>
         </div>
      </div>
</div>

<!-- update campaign title -->
<div id="campaignTitleUpdate" style="display:none" class="hide dynamic-class">
   <div class="row">
       <div class="col-sm-12 tagp">
          <div class="form-group">
             <div class="form-group">
                <input type="text" id="campaignTitlePopover" name="title" class="form-control inputRequired">
             </div>
          </div>
          <div class="form-group">
            <input type="hidden" name="campId" class="campId inputRequired" value="{{ (isset($campData->id) ) ? $campData->id : '' }}">
            <button type="button" data-ref="" class="btn btn-primary" id="campaignNameUpdate">Save</button>
            <button type="button" data-ref="" class="btn btn-primary" id="closeCampaignNamePopover">Cancel</button>
          </div>
       </div>
   </div>
   <div  class="clearfix"></div>
</div>

<!-- Remove Campaign Contact -->
<div id="remove-campaign-contacts" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Delete</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               Are you sure, you want to delete this contact ?
            </div>
            <div class="modal-footer">
               <div class="text-right">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary removeContact">Confirm</button>
              </div>
            </div>
         </div>
   </div>
</div>


<div id="checkbox-campaign-contacts" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Uncheck</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               Are you sure, you want to uncheck this contact ?
            </div>
            <div class="modal-footer">
               <div class="text-right">
                  <button type="button" class="btn btn-primary cancel_uncheck_button_value" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary removeContact set_uncheck_button_value" id="" data-row-id=""
>Confirm</button>
              </div>
            </div>
         </div>
   </div>
</div>



<!-- Add Tag -->
<div id="add-new-uom" style="display:none" class="hide dynamic-class">
   <div class="row">
       <div class="col-sm-12 tagp">
          <div class="form-group">
             <div class="form-group">
                <select id="presetTagSelect" name="pretag" class="form-control inputRequired">
                    <option value="">Select Tag</option>
                    <option value="name">Name</option>
                    <option value="phone">Phone ( Mandatory )</option>
                    <option value="lead_track_no">Lead/Ticket ID (for CRM tracking)</option>
                    <option value="email">Email (for CRM tracking)</option>
                    <option value="message">Message</option>
                    <option value="location">Location</option>
                    <option value="product-service">Product/Service</option>
                    <option value="create-custom-tag">Create a custom tag</option>
                </select>
             </div>
             <div class="form-group">
                <input name="tagName" style="display:none" id="tagName" autofocus autocomplete="off" class="customTagInput form-control inputRequired unitName" placeholder="Tag" type="text">
             </div>
          </div>
          <div class="form-group">
            <input type="hidden" name="indexRow" class="indexRow inputRequired">
            <input type="hidden" name="positionStart" class="positionStart inputRequired">
            <input type="hidden" name="positionEnd" class="positionEnd inputRequired">
            <input type="hidden" name="campId" class="campId inputRequired" value="{{ (isset($campData->id) ) ? $campData->id : '' }}">
            <input type="hidden" name="label" id="label">
            <button type="button" data-ref="" class="btn btn-primary" id="createTag">Save</button>
            <button type="button" data-ref="" class="btn btn-primary" id="removeTag">Remove</button>

          </div>
       </div>
   </div>
   <div  class="clearfix"></div>
</div>
<!-- Update Tag -->
<div id="update-tag" style="display:none" class="hide dynamic-class">
   <div class="row">
       <div class="col-sm-12 tagp">
          <div class="form-group">
              <div class="form-group">
                 <select id="presetTagSelect" name="pretag" class="form-control inputRequired">
                     <option value="">Select Tag</option>
                     <option value="name">Name</option>
                     <option value="phone">Phone ( Mandatory )</option>
                     <option value="lead_track_no">Lead/Ticket ID (for CRM tracking)</option>
                     <option value="email">Email (for CRM tracking)</option>
                     <option value="message">Message</option>
                     <option value="location">Location</option>
                     <option value="product-service">Product/Service</option>
                     <option value="create-custom-tag">Create a custom tag</option>
                 </select>
              </div>
             <div class="form-group">
                <input name="tagName" style="display:none" id="tagName" autofocus autocomplete="off" class="customTagInput form-control inputRequired tagName unitName" placeholder="Tag" type="text">
             </div>
          </div>
          <div class="form-group text-right">
            <input type="hidden" name="tagRef" class="tagRef inputRequired">
            <input type="hidden" name="capId" class="campId inputRequired" value="{{ (isset($campData->id) ) ? $campData->id : '' }}">
            <input type="hidden" name="label" id="label">
<button type="button" data-ref="" class="btn btn-default closePOP" id="deleteTag">Delete</button>
            <button type="button" data-ref="" class="btn btn-warning" id="updateTag">Save</button>
          </div>
       </div>
   </div>
   <div  class="clearfix"></div>
</div>
<input type="hidden" name="indexRow" class="indexRow inputRequired">
<input type="hidden" name="positionStart" class="positionStart inputRequired">
<input type="hidden" name="positionEnd" class="positionEnd inputRequired">
<input type="hidden" name="tagLabel" class="tagLabel inputRequired">
<input type="hidden" name="isUpperLineLabel" class="isUpperLineLabel inputRequired">
<input type="hidden" name="campId" class="campId inputRequired" value="{{ (isset($campData->id) ) ? $campData->id : '' }}">
<!-- Add New Contact Modal -->
<div id="add-new-tag" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
        {{ Form::open(array('url'=>$url.'/save-contact','id'=>'contact_form','autocomplete' => 'off')) }}
        <input type="hidden" name="frontreq" value="1">
        <input name="recordId" type="hidden" value="">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Add New Contact</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Contact Name <span class="star">*</span></label>
                        {!! Form::text('name',null,['placeholder'=>"Name",'class'=>'form-control']) !!}


                        @if(isset($userID))
                            {!! Form::hidden('userId',$userID) !!}

                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Contact Email <span class="star">*</span></label>
                        {!! Form::text('email','',['placeholder'=>"Contact Email",'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Role/Title </label>
                        {!! Form::text('role',null,['placeholder'=>"Role/Title",'class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Department/Location </label>
                        {!! Form::text('dept',null,['placeholder'=>"Department/Location",'class'=>'form-control']) !!}
                        @if(isset($recordId))
                        {!! Form::hidden('recordId',$recordId) !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Country Code <span class="star">*</span></label>
                        {!! Form::select('code',$countryCode,Null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Phone Number <span class="star">*</span></label>
                        {!!  Form::text('number',null,['placeholder'=>"Phone Number",'class'=>'form-control','id'=>'phonenumber']) !!}
                    </div>
                </div>
              </div>

              <?php
              $daysArr = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
              ?>
              <div class="row">
                <div class="col-md-12" style="display:none">
                  <div class="form-group">
                        <label for="">Schedule <span class="star">*</span></label>
                  </div>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Days</th>
                          <th>From</th>
                          <th>To</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @for($i=0;$i<=6;$i++)
                        <tr>
                          <td>{{ $daysArr[$i] }}</td>
                          <td>
                            <input class="form-control validate timepicker" type="text" name="dayFrom[]" value="@if(isset($data['schedule'][$i]->from)) {{$data['schedule'][$i]->from}} @endif" id="dayF{{$i}}">
                            </td>
                          <td><input class="form-control validate timepicker" type="text" name="dayTo[]" value="@if(isset($data['schedule'][$i]->to)) {{$data['schedule'][$i]->to}} @endif" id="dayT{{$i}}"></td>

                          <td><span class="daySts" style="font-size: 30px; color: #007bff;" data-val="@if(isset($data['schedule'][$i]->status)) {{ $data['schedule'][$i]->status }} @else 0 @endif"><i class="fa @if(isset($data['schedule'][$i]->status) && $data['schedule'][$i]->status!=0) fa-toggle-on @else fa-toggle-off @endif" data-toggle="tooltip" title="" data-original-title="Change Status"></i></span><input class="sts-input" type="hidden" name="dayStatus[]" value="@if(isset($data['schedule'][$i]->status)) {{ $data['schedule'][$i]->status }} @else 0 @endif"></td>
                        </tr>
                        @endfor
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

               <div class="text-right">
                  <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
               </div>
            </div>

         </div>
      </form>
   </div>
</div>
<!-- Add Existing Contact Modal -->
<div id="campaign-contacts" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <form id="camp-contact-form" action="{!! $url.'/add-contact-to-campaign'!!}" method="post">
         <input type="hidden" class="form-control custId campId" value="{{ (isset($campData['campId']) ) ? $campData['campId'] : '' }}" name="campId">
         <input type="hidden" name="api_dept_id" class="api_dept_id_" value="{{$api_dept_id}}">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Select Contact</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            </div>
         </div>
         <div class="modal-footer" style="display:none">
            <div class="text-right">
               <div class="checkbox"><input type="checkbox" name="contactid[]" class="customerCheckbox" value=""></div>
               <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
               <button type="submit" class="btn btn-primary" id="camp-contact">Save</button>
            </div>
      </form>
   </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){



$(document).on('click', '.checkbox_campaign_contact_list', function(e)
{


       $(".set_uncheck_button_value").attr('id',$(this).val());
       $(".set_uncheck_button_value").attr('data-row-id','tableRow_'+$(this).val());


});
$(document).on('click', '.cancel_uncheck_button_value', function(e)
{
    $("#check"+ $(".set_uncheck_button_value").attr('id')).prop("checked",true);
});

    $(document).on('click', '#refreshCampaignTemplate', function(event)
    {
        var id = $(this).attr('data-id');
        if( id != '' || id != undefined )
        {
            $('#confirm-refrest-tempalte-modal').find('#refreshCampaignTemplateBtn').attr('data-id',id);
            $('#confirm-refrest-tempalte-modal').modal('show');
        }
        else
        {
            $.toast({
                heading             : 'Error',
                text                : 'Something went wrong. Please try again.',
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'error',
                hideAfter           : 3000,
                position            : 'top-right'
            });
        }
    });

    jQuery(document).on('click','#refreshCampaignTemplateBtn',function(eve)
    {
        var id = $(this).attr('data-id');
        if( id != '' || id != undefined )
        {
            $.ajax({
                url         : siteroot+'/reset-campaign-template',
                type        : "post",
                data        : { 'id':id },
                dataType    : "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                beforeSend  : function ()
                {
                    $(".loader_div").show();
                },
                complete: function ()
                {
                    $(".loader_div").hide();
                },
                success: function (response)
                {
                    var delayTime = '3000';
                    if(response.delayTime)
                        delayTime = response.delayTime;
                    $(".loader_div").hide();
                    if (response.success)
                    {
                        $('#confirm-refrest-tempalte-modal').modal('hide');
                        $.toast({
                            heading             : 'Success',
                            text                : response.success_message,
                            loader              : true,
                            loaderBg            : '#fff',
                            showHideTransition  : 'fade',
                            icon                : 'success',
                            hideAfter           :  delayTime,
                            position            : 'top-right'
                        });
                    }
                    else
                    {
                        $.toast({
                            heading             : 'Error',
                            text                : response.error_message,
                            loader              : true,
                            loaderBg            : '#fff',
                            showHideTransition  : 'fade',
                            icon                : 'error',
                            hideAfter           : delayTime,
                            position            : 'top-right'
                        });
                    }
                    if(response.reload)
                    {
                        location.reload(true);
                    }
                },
                error:function(response){
                    $.toast({
                        heading             : 'Error',
                        text                : 'Connection error.',
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : 3000,
                        position            : 'top-right'
                    });
                }
            });
        }
        else
        {
            $.toast({
                heading             : 'Error',
                text                : 'Something went wrong. Please try again.',
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'error',
                hideAfter           : 3000,
                position            : 'top-right'
            });
        }
    });


    $('#phonenumber').keyup(function(){
        var value = $('#phonenumber').val();
        if(value == '' || value == undefined){
          $('#phonenumber').val('');
        }else{
          $('#phonenumber').val(parseInt(value));
        }
   });



   $("#login").popover({
   html: true,
   placement : 'bottom',
   content: function() {
           return $('#popover-content').html();
         }
   });
     jQuery(document).on('click','#showChart',function(eve) {
       let startDate = $('input[name="startDate"]').val();
       let endate =  $('input[name="endDate"]').val();
       if($.trim(startDate).length > 0 && $.trim(endate).length == 0){
         tost('Please select Date to and submit request again.','Error',1000);
         return false;
       }
       $.ajax({
         url: "{{url('customer/get-charts')}}",
         method: "POST",
         data : {startDate : startDate , endDate : endate },
         beforeSend : function () {
           $('.loader_div').show();
           if (myLineChart != null) { myLineChart.destroy(); myPieChart.destroy();}
         },
         complete : function () {
           $('.loader_div').hide();
         },
         success: function(data) {
           var month = [];
           var count = [];
           var pieCampName = []
           var pieCount = []

           $('.totalLeads').html(data.lead_count)


           if (data.lead_count !=0 ) {
             for (var i = 0; i < data.camp_chart.length; i++) {
                month.push(data.camp_chart[i].monthname);
                count.push(data.camp_chart[i].lead_count);
             }
             for (var i = 0; i < data.campChart.length; i++) {
                pieCampName.push(data.campChart[i].title);
                pieCount.push(data.campChart[i].count);
             }
            }else{
             month = ['No Record'];
             count = [0];
             pieCampName = ['No Record'];
             pieCount = [0];

             if (eve.originalEvent != undefined) {
               $.toast({
                  heading             : 'Warning',
                  text                : 'No Data found please try with new entries.',
                  loader              : true,
                  loaderBg            : '#fff',
                  showHideTransition  : 'fade',
                  icon                : 'warning',
                  hideAfter           : 5000,
                  position            : 'top-right'
               });
             }

            }
           var ctx = document.getElementById("myBarChart");
           window.myLineChart = new Chart(ctx, {
             type: 'bar',
             data: {
               labels: month,
               datasets: [{
                 label: "Lead",
                 backgroundColor: "rgba(2,117,216,1)",
                 borderColor: "rgba(2,117,216,1)",
                 data: count,
               }],
             },
             options: {
               scales: {
                 xAxes: [{
                   time: {
                     unit: 'month'
                   },
                   gridLines: {
                     display: false
                   },
                   ticks: {
                     maxTicksLimit: 6
                   }
                 }],
                 yAxes: [{
                   ticks: {
                     min: 0,
                     max: Math.max(...count),
                     maxTicksLimit: 5
                   },
                   gridLines: {
                     display: true
                   }
                 }],
               },
               legend: {
                 display: false
               }
             }
           });


           var ctx1 = document.getElementById("myPieChart");
           window.myPieChart = new Chart(ctx1, {
             type: 'pie',
             data: {
               labels: pieCampName,
               datasets: [{
                 data: pieCount,
                 backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#28a745','#FF6633',
                                   '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6','#6666FF',
                             		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                             		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                             		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                             		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                             		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                             		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                             		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                             		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                             		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6',
                                 ],
               }],
             },
           });

         },
         error: function(data) {
           console.log(data);
         }
       });
     })
     $('#showChart').trigger('click');


        jQuery(document).on('change','#campaignCountry',function(eve)
        {
            let country = $(this).val();
            if ($.trim(country).length <= 0)
            {
                tost('Please select country.','Error',1000);
                $('#campaignTimezone').html('<option value="">Please select country</option>');
                return false;
            }
            $('#campaignTimezone').html('<option value="">Please select country</option>');
            var ajaxUrl = '{{url("")}}';
            ajaxUrl = ajaxUrl+'/'+roleNameJs+'/get-country-timezone';
            $.ajax({
                url     : ajaxUrl,
                method  : "POST",
                data    : { country : country },
                headers        : {
                  'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                },
                beforeSend : function ()
                {
                    // $('.loader_div').show();
                },
                complete : function ()
                {
                    $('.loader_div').hide();
                },
                success: function(response)
                {
                    if( response.success )
                    {
                        $('#campaignTimezone').html(response.data);
                    }
                    else
                    {
                        $('#campaignTimezone').html('<option value="">No Timezone found</option>');
                        tost(response.error_message,'Error',1000);
                        return false;
                    }
                },
                error: function(data) {
                    $('#campaignTimezone').html('<option value="">No Timezone found</option>');
                    tost('Something went wrong. Please try again.','Error',1000);
                    return false;
                }
            });
        });
    });

</script>
@if($campData['step'] > 1)
   <script>
      $('.loader_div').show();
      $(document).ready(function(){
         var step = $(".campaignStep").val();
         var invalidStep = $(".campaignInvalidStep").val();
         if(step < 6){
            if(step < invalidStep){

              $(document).find('.nav-campaigns a[href="#step-'+step+'"]').trigger('click');
            }else{

               if(invalidStep!=0){
                  $(document).find('.nav-campaigns a[href="#step-'+invalidStep+'"]').trigger('click');
               }else{
               $(document).find('.nav-campaigns a[href="#step-'+step+'"]').trigger('click');
               }
            }
         }else{
            if(invalidStep!=0){
               $(document).find('.nav-campaigns a[href="#step-'+invalidStep+'"]').trigger('click');
            }else{
            $(document).find('.nav-campaigns a[href="#step-'+step+'"]').trigger('click');
            }
         }
         $('.loader_div').hide();
      });
   </script>
@endif
@if(isset($campData['campaigns_tags']) && count($campData['campaigns_tags']) > 0)
<script type="text/javascript">
$('li.nav-item').on('click',function(){
$(this).nextAll().find('a').removeClass('active_prev');
$(this).prevAll().find('a').addClass('active_prev');
})
function hightLight (element, start, end,tagName , id, isCustomTag) {
    var str = element[0].innerHTML;
    str = `${ str.substr(0, start) }<mark data-name="${tagName.replace("\"", "")}" class="campaignTag" data-custom-tag="${isCustomTag}" data-id="${id}">${str.substr(start, end - start)}</mark>${str.substr(end)}`;
    element[0].innerHTML = str.replace("\'", "");

}
<?php
foreach ($campData['campaignTags'] as $key => $value) { ?>

  jQuery('.testMail').each(function(index) {
    var thiss = $(this);
    var tagnamee = '<?php echo $value->tagName;?>';
    if( tagnamee != '' )
    {
        if (index == '<?php echo $value->indexRow?>') {
          hightLight(thiss, '<?php echo $value->positionStart?>', '<?php echo $value->positionEnd?>','<?php echo $value->tagName?>','<?php echo $value->id?>','<?php echo $value->isCustomTag; ?>')
        }
    }
  });
<?php } ?>

// function closePOP() {
//   $('.campaignTag').popover('dispose');
// }
</script>
@endif


<!-- <script>
   $(document).ready(function(){
       $('[data-toggle="popover"]').popover();
   });
</script>
<script>
   $("[data-toggle=popover]").each(function(i, obj) {
      $(this).popover({
        html: true,
        content: function() {
          var id = $(this).attr('id')
          return $('#popover-content-' + id).html();
        }
      });
   });
</script> -->


@endsection
