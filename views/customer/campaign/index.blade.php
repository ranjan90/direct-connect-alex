@extends('layouts.customer')
@section('content')
<style>
.green{ color: green; }
.blue{ color: #ec2235; }
</style>
  <!--@if(!empty($user_status) && isset($user_status->status) && $user_status->status == 9)-->
  <!--  <div class="row alert alert-error alertPopups">-->
  <!--    <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>-->
  <!--  </div>-->
  <!--@endif-->
  <!--@if(!empty($user_status) && isset($user_status->status) && $user_status->status == 12)-->
  <!--  <div class="row alert alert-error alertPopups">-->
  <!--    <div class="col-md-12">{!! \Config::get('message.stripe_payment_fail') !!}</div>-->
  <!--  </div>-->
  <!--@endif-->
  <!--@if(!empty($user_status) && isset($user_status->status) && $user_status->status == 11)-->
  <!--  <div class="row alert alert-error alertPopups">-->
  <!--    <div class="col-md-12">{!! \Config::get('message.cancel_renewal_payment') !!}</div>-->
  <!--  </div>-->
  <!--@endif-->
  <?php 

   $BusinessOwner= App\Model\BusinessProfile::find(Auth::user()->getBusinessId())->get_users()->wherePivot('is_owner','1');
    if($BusinessOwner->where('register_step',3)->count()!=0)
    {
              $user_data_check= $BusinessOwner->first();
    }
    if(isset($user_data_check->is_group) && $user_data_check->is_group==1)
    {
            if(\App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->count()==0)
            { 
                ?>
                <div class="row alert alert-error alertPopups">
      <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>
    </div>

                <?php
            }
    }
else
{
    ?>


  @if(!empty($user_status) && isset($user_status->isSubscriptionActive) && $user_status->isSubscriptionActive == 1)
    <div class="row alert alert-error alertPopups">
      <div class="col-md-12">{!! \Config::get('message.cancel_plan') !!}</div>
    </div>
    @if(isset($user_status->message) && !empty($user_status->message))

    <div class="row alert alert-error alertPopups">
      <div class="col-md-12">Business plan cancel reason is: {{$user_status->message}}</div>
    </div>
    @endif
  @endif
  <?php 
}
?>

  <div class="row">
  <h2 class="main_title_head">Campaigns</h2>
  </div>
  <div class="container-fluid">
    <!-- Breadcrumbs-->
	<div class="row">
	<div class="col-md-12">
	<div class="heading-campaigns">
    @can(config('permissions.data.add-campaign.name'))
    <?php
     $main_url=$url.'/add-new-campaign/'.$customer_id;
      if(empty($customer_id))
     $main_url= $url.'/add-new-campaign';
    ?>
 {{--@if(!empty($user_status) && isset($user_status->isSubscriptionActive) && $user_status->isSubscriptionActive == 2) --}}
	  <div class="create-new-campaign-btn">
	     <a class="btn btn-primary create-latest-campaign take-tour-campaign" href="{!! $main_url !!}">Create New Campaign</a>
	  </div>
	  {{-- @endif --}}
    @endcan
	</div>
	</div>
	</div>
    <!-- Icon Cards-->
    <div class="row">
      <div class="col-md-12">

        <div class="recordsTable">

      </div>
    </div>
</div>
@section('js')
<script type="text/javascript">
/* getCampaign() */
function getCampaign(id="")
 {
       $.ajax({
          url                : prefixUrl + '/campaigns?business_id='+localStorage.getItem('business_data_id')+'&customer_id='+id,
          type               : 'GET',
          dataType           : 'html',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          },
          beforeSend : function() {
              // showLoader();
          },
          complete   : function() {
              hideLoader();
          },
          success    : function(response) {
              $('.recordsTable').html(response);
              hideLoader();

          }
      });
 }
  var customer_id="<?php echo $customer_id;?>";
  //if(customer_id=="")
  if(storge_customer_id=="")
  {
      getCampaign();
  }
  // getCampaign(customer_id);
 </script>
@endsection
@endsection
