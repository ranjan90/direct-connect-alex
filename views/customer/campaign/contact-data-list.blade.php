@php $exc = array();$priority_data = array();$inc_data=1; @endphp
                                    @foreach($campData['campaignContact'] as $c)
                                         @php $exc[] = $c->custId; $priority_data[$c->custId] = $c->priority;@endphp
                                    @endforeach
                                   
                                    @forelse($user_contacts as $contact)
                                    <?php


                                         $set_priority=1;
                                         $is_delete=0;
                                         if(array_key_exists($contact->id,$priority_data))
                                         {
                                             $is_delete=1; 
                                             $set_priority=$priority_data[$contact->id];
                                         }else{
                                             $set_priority=$inc_data;
                                         }

                                        $inc_data++;
                                    ?>
                                    <tr id="tableRow_{{$contact->id}}">
								               <td><div class="campaign-checkbox">
                                       <div class="form-group">
                                          <input class="camp_cont_class <?php if(in_array($contact->id,$exc)){?> checkbox_campaign_contact_list <?php } ?>" @if(in_array($contact->id,$exc)) data-toggle="modal" data-target="#checkbox-campaign-contacts" checked @endif type="checkbox" id="check{{$contact->id}}" value="{{$contact->id}}" name="contactid[]">
                                          <label for="check{{$contact->id}}"><span class="circle-checkbox"></span></label>
                                       </div></div></td>
                                       <td data-name="name"><span class="latest-campaign-name">{{$contact->name}}</span></td>
                                       <td data-name="email">{{$contact->email}}</td>
                                       <td data-name="contact">{{$contact->country->countryPhoneCode}}{{$contact->contact}}</td>
                                       <td data-name="priority"><input type="number" style="width: 50% !important;" class='form-control' name="priority[{{$contact->id}}]" min="1" max="100" value="<?php echo $set_priority ;?>"></td>
                                       <td>

                                          {{--<input type="hidden" name="role" id="current_role" value="{{$role = $userRelation->roles()->first()->id}}">--}}
                                          <input type="hidden" name="role" id="current_role" value="{{$role = \Auth::user()->roles()->first()->id}}">
                                          @can(config('permissions.data.edit-contact.name'))
                                              <a class="editContact" id="{{Crypt::encrypt($contact->id)}}" href="javascript:void(0)"><span data-toggle="tooltip" title="Edit" data-placement="top" ><img src="{{asset('assets/customer/img/edi-d.png')}}"></span></a>
                                          @endcan
                                         &nbsp;
                                         @if($is_delete==1)
										 <!--<a id="{{$contact->id}}" data-toggle="modal" data-target="#remove-campaign-contacts" href="javascript:void(0)" class="c_c_c_r"><span class="edit-icon" data-toggle="tooltip" title="Delete" data-placement="top" > <img src="{{asset('assets/customer/img/delete.png')}}"> </span></a>-->
                                        @endif
                                       </td>
                                    </tr>
                                    @empty
                                    <tr id="noContactsTr">
                                       <td colspan="3">No Contacts Available </td>
                                    </tr>
                                    @endforelse