@extends('layouts.customer')
@section('content')
<style>
.ui-datepicker td span, .ui-datepicker td a {
	width: 100%;
}
</style>
<?php
$getBusinessId = \Auth::user()->getBusinessId();
$callsettingsss  = \App\Model\CallSetting::select('*')->where('business_id',$getBusinessId);
if( $callsettingsss->count() > 0 )
{
    $callsettingsss = $callsettingsss->first();
    $recDisplay = $callsettingsss->call_recording_display;
}
else
{
    $callsettingsss = array();
    $recDisplay = '';
}
 ?>

@php
  $ratingArr = array('','Poor','Good','Excellent');
@endphp

<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a  href = " javascript:void(0)">Campaign Leads</a>
  </li>
</ol>

<!-- Icon Cards-->
<div class="row">
  <div class="col-md-12">
    {{ Form::open(array('url'=>url()->current(),'method'=>'GET')) }}
    <div class="row reports-tabs">
      <div class="col-md-3">
        <label>Date From</label>
        <div class="input-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
            </div>
            {!! Form::text('fromDate',null,['class'=>'form-control datepicker', 'id'=>'fromDate'])!!}
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <label>Date To</label>
        <div class="input-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text calIcon"><i class="fa fa-calendar"></i></span>
            </div>
            {!! Form::text('toDate',null,['class'=>'form-control datepicker', 'id'=>'toDate'])!!}
          </div>
        </div>
      </div>
      <div class="col-md-6">
	    <div class="submit-class">
        {!! Form::submit('Submit',['class'=>'btn btn-primary'])!!}
        <a href="{{ url()->current() }}" class="btn btn-default">Reset</a>
      </div>
	  </div>
      {{ Form::close() }}
    </div>
	  </div>
</div>
    <div class="clearfix"></div>
   <div class="row">
    <div class="col-md-12">
      <div class="campaign-name-id">
	    <div style="" class="camp-name">
         <b>Campaign Name:</b> {{ $campData->title }}
       </div>
	 <div class="campaign_back">
      @if($recDisplay==1)
        @if($leadsData->count() > 0)
          <a class="btn btn-primary" href="{{ URL('export-lead-data') }}/{{ $campData->id}}">Download</a>
        @endif
      @endif
      <a class="btn btn-default" href="{{ url()->previous() }}">Back</a>
    </div>
	</div>
  </div>
  </div>

    <div class="table-responsive recordsTable latest-champaign-table">
      <div class="table">
        <table class="table" style="width:100%">
          <thead>
            <tr>
              <th scope="col" width="">#</th>
              <th scope="col" style="min-width: 93px;">Lead ID</th>
              <th scope="col" style="min-width: 93px;">Answered by</th>
              <th scope="col" style="min-width: 93px;">Lead Name</th>
              <th scope="col" width="10%">Call Length</th>
              <!-- <th scope="col" width="20%">Call Date</th> -->
              @if($recDisplay==1)<th scope="col" width="20%">Call Summary</th>@endif
              <th scope="col" width="20%">Lead Received Time</th>
              <th scope="col" width="20%">Lead Triggered Time</th>
              <th scope="col" style="min-width: 93px;">Business</th>
              <th scope="col" style="min-width: 93px;">Customer</th>
              <th scope="col" width="10%">Status</th>
              <th scope="col" width="10%">Ratings <i class="fa fa-sort-amount-asc"></i></th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
          @php $i=0; @endphp
          @forelse($leadsData as $leads)
            @php $i++; $length = $leads->call_length/60; @endphp
            <tr id="{{ $leads->id }}">
              <td>{{ $i }}</td>
              <td>{{ $leads->lead_id }}</td>
              <td>{{ ucfirst($leads->answered_by) }}</td>
                <td>
                    <?php if( isset($leads->parse_emails)) {?>
                        {{ $leads->parse_emails->leadName }}
                    <?php } ?>
                </td>
              <td><span class="blue-color"><i data-toggle="tooltip" data-placement="top" title="Total Call Length: {{ $length }} Minute(s)" class="fa fa-info-circle"></i></span></td>

              @if($recDisplay==1)
                <td>
                  @if($leads->call_recording_display==1)
                    <audio controls controlsList="nodownload">
                        <source src="{{ $leads->recording }}" type="audio/wav">
                    </audio>
                    @if($leads->call_attempts_count > 1)
                      <span class="float-right attempt_text"><?php /* <i>+ {{$leads->call_attempts_count-1}} more call attempts</i>*/?></span>
                    @endif
                  @else
                  N/A
                  @endif
                </td>
              @endif
              <?php
                      $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($leads->parse_emails);
                      extract($return_data);
                ?>
                <td><?php echo App\Helpers\GlobalFunctions::getTimeZoneDateTime($leads->campaign_details,$leads->startdate); ?></td>
                <td>@if(!empty($leads->parse_emails->triggerAt)){!!  date('d/m/Y h:i:s A',strtotime($leads->parse_emails->triggerAt)) !!} @endif</td>
               <td scope="col">
                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo $business_btn_message; ?></button>
                                                   </td>
                                                   <td scope="col">

                                                     <button type="button" class="call-status-button btn <?php echo  $customer_btn_color;?> set_btn_width"><?php echo $customer_btn_message; ?></button>
                                                   </td>

 <td>
                                                          <?php
                                                         if(!empty($status_message))
                                                         {
                                                              echo $status_message;
                                                         }
                                                         else
                                                         {
                                                                  echo App\Helpers\GlobalFunctions::returnCallStatus($leads->parse_emails);
                                                         }

                                                        ?>


                                                   </td>

               <?php /*
               <td scope="col">
                                                <?php if($leads->sc1 == 'success' ){ ?><button type="button" class="call-status-button btn btn-success">Connected!</button><?php } else if($leads->sc1 == 'calling' ){?> <button type="button" class="call-status-button btn btn-warning"></button><?php } else {?> <button type="button" class="call-status-button btn btn-danger">Not Connected</button><?php } ?>
                                            </td>
                                            <td scope="col">
                                                <?php if($leads->sc2 == 'success' ){ ?><button type="button" class="call-status-button btn btn-success">Connected!</button><?php } else if($leads->sc2 == 'calling' ){?> <button type="button" class="call-status-button btn btn-warning"></button><?php } else {?> <button type="button" class="call-status-button btn btn-danger">Not Connected</button><?php } ?>
                                            </td>
                           */?>
              <td class="rateTD" title="@if($leads->rating!=0) {{$ratingArr[$leads->rating]}} @endif">
                @if($leads->rating==0)
                  @for($j=1; $j<=3; $j++)
                    <span class="star-rate" id="{{$j}}" title="{{$ratingArr[$j]}}"><i style="font-size: 14px;" class="stars far fa-star"></i></span>
                  @endfor
                @else
                  @for($j=1; $j<=3; $j++)
                    @if($j<=$leads->rating)
                      <span><i style="font-size: 14px;" class="stars rate-blue fas fa-star"></i></span>
                    @else
                      <span><i style="font-size: 14px;" class="stars far fa-star"></i></span>
                    @endif
                  @endfor
                @endif
              </td>
              <td>
                <!--data-toggle="tooltip" data-placement="top" title="View Lead Details"-->
                <a class="blue-color getLeadData" data-url="{{ URL('get-lead-data') }}/{{ $leads->id }}" href="javascript:void(0)" data-toggle="modal" data-target="#leadDetails"><i class="fa fa-eye" ></i></a>
                &nbsp;
                @if($recDisplay==1)
                  @if(strpos($leads->recording , 'recordings/False' ) === false ) <a href="{{$leads->recording}}" download="{{$leads->recording}}"><i  class="fa fa-download" aria-hidden="true"></i></a>
                  @endif
                @endif
              </td>
              <!--data-toggle="tooltip" data-placement="top" title="Download Lead"-->
          </tr>
          @empty
          <tr>
              <td colspan="8">No Lead Available !</td>
          </tr>
          @endforelse
          </tbody>
        </table>
        {{ $leadsData->links() }}
      </div>
    </div>
</div>
<div id="leadDetails" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Lead Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<!-- Lead Rating Modal -->
<div id="leadRating" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Are you sure to rate this lead ?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h6>Rate this call between 1 to 3 stars.</h6>
        <fieldset class="rating">
            <input type="radio" id="star3" name="rating" value="3" />
            <label class = "full" for="star3" title="Excellent"></label>
            <input type="radio" id="star2" name="rating" value="2" />
            <label class = "full" for="star2" title="Good"></label>
            <input type="radio" id="star1" name="rating" value="1" />
            <label class = "full" for="star1" title="Poor"></label>
        </fieldset>
        <input type="hidden" id="rateLeadID" value="">
      </div>
    </div>
  </div>
</div>
 <script>
     $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
     
 </script>
@endsection
