@extends('layouts.customer')
@section('content')
@php
$dayArr = array(
   'mon'=>'Monday',
   'tue'=>'Tuesday',
   'wed'=>'Wednesday',
   'thu'=>'Thursday',
   'fri'=>'Friday',
   'sat'=>'Saturday',
   'sun'=>'Sunday'
);
@endphp
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <ol class="breadcrumb">
             <li class="breadcrumb-item">
                <a  href = " javascript:void(0) ">View Campaign</a>
             </li>
          </ol>
        </div>
        <div class="col-md-6 text-right">
          <ol class="breadcrumb float-right">
             <li class="breadcrumb-item">
                <a href="{{$url.'/campaigns'}}" class="btn btn-primary create-latest-campaign">Back</a>
             </li>
          </ol>
        </div>
      </div>
      <!-- Icon Cards-->
      <div class="row">
          <div class="confirm-campaign-box">
                           <div class="row">
                              <div class="col-md-4 boxBorder">
                                 <div class="campaign-new-tick-img">
                                    <img src="{{ asset('assets/customer/img/detail-main.png') }}  ">
                                 </div>
                              </div>
                              <div class="col-md-4 boxBorder">
                                    <div class="new-campaign-confirm-text">
                                       <h4>Campaign Title</h4>
                                       <h6>
                                       @if(isset($campData['campaignsTitle']))
                                          {{$campData['campaignsTitle']}}
                                       @else
                                          N/A
                                       @endif
                                       </h6>
                                    </div>
                                    <div class="new-campaign-confirm-text">
                                       <h4>Call Script (Template)</h4>
                                       <h6>
                                       @if(isset($campData['campaignTemplate']))
                                          {{$campData['campaignTemplate']}}
                                       @else
                                          N/A
                                       @endif
                                       </h6>
                                    </div>
                                    <?php echo $campData['parserOutput'];?>
                                    <div class="new-campaign-confirm-text">
                                       <h4>Call Script (Example)</h4>
                                       <h6>
                                       @if(isset($campData['parserOutput']))
                                          {{$campData['parserOutput']}}
                                       @else
                                          N/A
                                       @endif
                                       </h6>
                                    </div>

                              </div>
                              <div class="col-md-4">
                                    <div class="new-campaign-confirm-text">
                                       <h4>Country</h4>
                                       <h6>
                                       @if(isset($campData['campaignCountryName']))
                                          {{$campData['campaignCountryName']}}({{$campData['time_zone']}})
                                       @else
                                          N/A
                                       @endif
                                    </h6>
                                    </div>

                                       @php $days =''; @endphp
                                       @if(isset($campData['available_days']))
                                          @foreach($campData['available_days'] as $day)
                                             <?php
                                                if( $day != 'sat' && $day != 'sun')
                                                    $days .= $dayArr[$day].', ';  
                                            ?>
                                          @endforeach
                                       @else
                                          @php $days = 'N/A'; @endphp
                                       @endif
                                       <?php if( $days == '' ) $days = 'N/A'; ?> 
                                       
                                    <div class="new-campaign-confirm-text">
                                       <h4>Available Hours</h4>
                                       <h6>
                                       <p>{{rtrim($days,', ')}}</p>
                                       @if(isset($campData['availHoursFromData']->working) && isset($campData['availHoursToData']->working))
                                          {{$campData['availHoursFromData']->working}} - {{$campData['availHoursToData']->working}}
                                       @else
                                          N/A
                                       @endif
                                        <p>Sat</p>
                                       @if(isset($campData['availHoursFromData']->sat) && isset($campData['availHoursToData']->sat))
                                          {{$campData['availHoursFromData']->sat}} - {{$campData['availHoursToData']->sat}}
                                       @else
                                          N/A
                                       @endif
                                       <p>Sun</p>
                                       @if(isset($campData['availHoursFromData']->sun) && isset($campData['availHoursToData']->sun))
                                          {{$campData['availHoursFromData']->sun}} - {{$campData['availHoursToData']->sun}}
                                       @else
                                          N/A
                                       @endif
                                       </h6>
                                    </div>
                                    <div class="new-campaign-confirm-text">
                                       <h4>Company Contacts</h4>
                                       <h6>
                                          @if(isset($campData['campaignContact']))
                                          <?php $k=1;?>
                                            @forelse($campData['campaignContact'] as $ct)
                                                @if(isset($ct->contact->name))
                                                   <span class ="cont_name">{{$k}}. {{$ct->contact->name}} </span><span class ="cont_number">,{{$ct->contact->contact}}</span>,<span class ="cont_email">,{{$ct->contact->email}}</span><br>
                                                    <?php $k++;?>
                                                @endif
                                            @empty
                                              No Contact Added !
                                            @endforelse
                                          @endif
                                       </h6>
                                    </div>

                              </div>
                           </div>
                        </div>
      </div>
</div>
</div>
@endsection
