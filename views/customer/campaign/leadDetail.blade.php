<?php
$getBusinessId = \Auth::user()->getBusinessId();
$callsettingsss  = \App\Model\CallSetting::select('*')->where('business_id',$getBusinessId);
if( $callsettingsss->count() > 0 )
{
    $callsettingsss = $callsettingsss->first();
    $recDisplay = $callsettingsss->call_recording_display;
}
else
{
    $callsettingsss = array();
    $recDisplay = '';
}
 ?>

@php
  $length = $lead->call_length/60;
@endphp

@php $role = \Auth::User()->roles()->first()->name; @endphp
@if($role == 'admin') @php  $recDisplay = 1 @endphp @endif
<style media="screen">
pre{
  white-space: pre-wrap;       /* Since CSS 2.1 */
  white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
  white-space: -pre-wrap;      /* Opera 4-6 */
  white-space: -o-pre-wrap;    /* Opera 7 */
  word-wrap: break-word;
}
</style>
<div class="table-responsive demo">
  <table class="table">

    <tr>
      <td style="width:18% !important"><b>Original Lead Format:</b></td>
      <td>@if(isset($lead->parse_emails->original_lead))
      <pre style="color:#000">{!!html_entity_decode($lead->parse_emails->original_lead) !!}</pre>
      @else N/A @endif</td>
    </tr>
    <tr>
      <td><b>Call Script:</b></td>
      <td>@if(isset($lead->parse_emails->callScript)) {{ $lead->parse_emails->callScript }} @else N/A @endif</td>
    </tr>
    <!--<tr>-->
    <!--  <td><b>Answered by:</b></td>-->
    <!--  <td>@if(isset($lead->answered_by)) {{ ucfirst($lead->answered_by) }} @else N/A @endif</td>-->
    <!--</tr>-->

      <tr><td width="100px">Answered By</td>
      
                             <?php
                                                      $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($lead->parse_emails);
                                                      extract($return_data);

                                                    ?>
                              <td>
                                   @if(isset($business_btn_color))

                                                     <button type="button" data-toggle="tooltip" title="@if(isset($title) && !empty($title)) {{$title}} @endif" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo isset($business_btn_message) ?  $business_btn_message :  'N/A'; ?></button>

                                                    @endif
                                 
                              </td>
                          </tr>
                          <tr><td width="100px">Visitor</td>
                              <td>
                                  @if(isset($customer_btn_color))

                                                     <button type="button" class="call-status-button btn <?php  echo $customer_btn_color;?> set_btn_width"><?php echo isset($customer_btn_message) ?  $customer_btn_message : 'N/A'; ?></button>

                                                    @endif
                                 
                              </td>
                          </tr>

     <tr>
      <td><b>Status:</b></td>
      <td>
         <?php 
                              $return_data=App\Helpers\GlobalFunctions::returnCallBusiness($lead->parse_emails);
                               extract($return_data);
                               if(!empty($status_message))
                               {
                                      echo $status_message;
                               }
                               else{
                                echo 'N/A';
                               } 
         ?>
      </td>
    </tr>
    <tr>
      <td colspan="2"><b>Call Attempt(s):</b></td>
    </tr>
    <tr>
      <td colspan="2">
        <table class="table">
          <tr>
            <th>#</th>
            @if($recDisplay==1)
            <th style="min-width:350px;">Recording</th>
            @endif
            <th>Contact Name</th>
            <th>Contact Number</th>
            <th>Disposition</th>
            <th>Duration</th>
          </tr>
          @php $a=0; @endphp
          @forelse($lead->call_attempts as $attempt)
          @php $a++; @endphp
            <tr>
              <td>{{$a}}.</td>
              @if($recDisplay==1)
              <td>
                <audio controls controlsList="nodownload"><source src="{{ $attempt->recording }}" type="audio/wav"></audio>
                @if(strpos($attempt->recording , 'recordings/False' ) === false ) <a href="{{$attempt->recording}}" download="{{$attempt->recording}}" style="position: relative;font-size: 25px;top: -16px;left: 10px;"><i data-toggle="tooltip" data-placement="top" title="Download Lead" class="fa fa-download" aria-hidden="true"></i></a>
                @endif
              </td>
              @endif
              <td>@if($attempt->contact_name != ''){{ucwords( $attempt->contact_name )}}@else N/A @endif</td>
              <td>{{$attempt->number}}</td>
                <td>{{ucwords( strtolower(str_replace('_',' ',$attempt->disposition)))}}</td>
              <td>{{$attempt->duration}} seconds</td>
            </tr>
          @empty
            No Call Attempts !
          @endforelse
        </table>
      </td>
    </tr>
    <tr>
      <td><b>Call Time:</b></td>
      <td><?php echo App\Helpers\GlobalFunctions::getTimeZoneDateTime($lead->campaign_details,$lead->startdate); ?> </td>
    </tr>
    <tr>
      <td><b>Call Length:</b></td>
      <td>{{ $length }} Minute(s)</td>
    </tr>
    @if(Auth::User()->roles()->first()->name=='admin')
    <tr>
      <td><b>Webmail Time:</b></td>
      <td>@if(isset($lead->parse_emails->call_logs->webmail_received)){{ date("m-d-Y h:i:s A", strtotime($lead->parse_emails->call_logs->webmail_received)) }} @else N/A @endif</td>
    </tr>
    <tr>
      <td><b>Mail Read:</b></td>
      <td>@if(isset($lead->parse_emails->call_logs->mail_read)){{ date("m-d-Y h:i:s A", strtotime($lead->parse_emails->call_logs->mail_read)) }} @else N/A @endif</td>
    </tr>
    <tr>
      <td><b>Mail Parsing:</b></td>
      <td>@if(isset($lead->parse_emails->call_logs->mail_parsed)){{ date("m-d-Y h:i:s A", strtotime($lead->parse_emails->call_logs->mail_parsed)) }} @else N/A @endif</td>
    </tr>
    <tr>
      <td><b>API Hit:</b></td>
      <td>@if(isset($lead->parse_emails->call_logs->api_hit)){{ date("m-d-Y h:i:s A", strtotime($lead->parse_emails->call_logs->api_hit)) }} @else N/A @endif</td>
    </tr>
    @endif
  </table>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
