<div class="latest-champaign-table"><div class="table-responsive">

              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Sr.</th>
                    <th scope="col" width="15%">Campaign Name</th>
                    <th scope="col" width="10%">Contact No</th>
                    <th scope="col" width="50%">Call Script</th>
                    <th scope="col" width="20%">Dated</th>
                  </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @if(isset($leads))
                    @forelse($leads as $lead)
                    <tr>
                      <td scope="col">{{ $i }}.</td>
                      <td scope="col"><span class="latest-campaign-name">{{ $lead->campaign_details->title }}</span></td>
                      <td scope="col">{{ $lead->mobileNo }}</td>
                      <td scope="col">{{ $lead->callScript }}</td>
                      <td scope="col">{{ date('m-d-Y h:i:s A', strtotime($lead->created_at)) }}</td>
                    </tr>
                    <?php $i++; ?>
                    @empty
                    <tr>
                      <td scope="col" colspan="5">No Record Found!</td>
                    </tr>
                    @endforelse
                  @else
                  <tr>
                    <td scope="col" colspan="5">No Record Found !</td>
                  </tr>
                  @endif
                </tbody>
              </table>
			  </div>
			  </div>

{!! $leads->appends(['business_id'=>$business_id])->links()!!}
 