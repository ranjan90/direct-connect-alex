@extends('layouts.customer')
@section('content')
  	<div class="container-fluid">
	    <!-- Breadcrumbs-->

  		<div class="profile-detail">
      <div>
        {{ Form::model($data,array('url'=>$url.'/update-profile', 'method'=>'post', 'id'=>'profile_form')) }}
          <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="yourName">First Name </label>
              {{ Form::text('fname',null,['class'=>'form-control']) }}
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="yourName">Last Name </label>
              {{ Form::text('lname',null,['class'=>'form-control']) }}
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="phone">Phone Number </label>
              {{ Form::text('phone',null,['class'=>'form-control']) }}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="email">Email</label>

                  {{ Form::text('email',null,['class'=>'form-control']) }}
              <span>@if(isset($data->email)) {{ $data->email }} @endif </span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="companyname">Company Name </label>
              {{ Form::text('companyName',null,['class'=>'form-control']) }}
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="companyurl">Company URL </label>
              {{ Form::text('companyUrl',null,['class'=>'form-control']) }}
            </div>
          </div>
        </div>
        <div class="row">
                <div class="col-md-12 text-right">
                      <input class="btn btn-primary" type="submit" value="Update">
                </div>
              </div>
            {{ Form::close() }}
      </div>

  		</div>
  		 <!-- Breadcrumbs-->
	    <div class="row">
			<div class="col-md-12">
				<div class="heading-campaigns">
		  			<h2>Change Password</h2>

				</div>
			</div>
		</div>
		  <!-- Change Password Tab -->
	    <div class="profile-detail">
        {{ Form::open(array('url'=>URL($url.'/change-password'), 'id'=>'change_password')) }}
	  			<div class="row">
				            <div class="col-md-4">
				              <div class="form-group">
				                <label for="pass">Enter Old Password <span class="star">*</span></label>
				                {!! Form::password('old_pass',['class'=>'form-control']) !!}
				              </div>
				            </div>
                     </div>
                     	<div class="row">
				            <div class="col-md-4">
				              <div class="form-group">
				                <label for="pass">Enter New Password <span class="star">*</span></label>
				                {!! Form::password('new_pass',['class'=>'form-control']) !!}
				              </div>
				            </div>
 </div>
       	<div class="row">
				            <div class="col-md-4">
				              <div class="form-group">
				                <label for="pass">Enter Confirm Password <span class="star">*</span></label>
				                {!! Form::password('new_pass2',['class'=>'form-control']) !!}
				              </div>
				            </div>
 </div>
 	<div class="row">
				            <div class="col-md-4">
				              {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
				            </div>
				          {{ Form::close() }}
		        </div>
        </div>
	      <!-- /////////////////-->



</div>
@endsection
