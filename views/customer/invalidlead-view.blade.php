<style>
.pullRightBtn{
float: right;
}
.callDataTable tr td,th {
font-size: 12px !important;
}
</style>

<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Lead Info</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="table-responsive ">
                <div class="table">
                    <table class="table" width="100%" border="1" borderColor="#e9ecef">
                        <tbody>
                            <?php if(isset($data[0]) && !empty($data[0])){?>
                                <tr><td width="150px">Campaign</td><td><?php if(isset($data[0]->campaign_details)) echo ucfirst($data[0]->campaign_details->title);?></td></tr>
                                <tr><td width="100px">Lead Name</td><td><?php echo ucfirst($data[0]->leadName);?></td></tr>
                                <tr><td width="100px">Email</td><td><?php echo ucfirst($data[0]->leadEmail);?></td></tr>
                                <tr><td width="100px">Phone</td><td><?php echo ucfirst($data[0]->mobileNo);?></td></tr>
                                <tr>
                                    <td width="100px">Time</td>
                                    <td>
                                        @if(!empty($data[0]->expectedCall) && isset($data[0]->campaign_details->coutry_details) && $data[0]->campaign_details->coutry_details->countryPhoneCode=="")
                                            {!! App\Helpers\GlobalFunctions::getTimeZoneDateTime($data[0]->campaign_details,$data[0]->expectedCall) !!} 
                                        @else 
                                            {!! date('h:i:s A', strtotime($data[0]->expectedCall))  !!} 
                                        @endif
                                    </td>
                                </tr>
                                <tr><td width="100px">Day of week</td><td>@if(isset($data[0]->created_at)){{ date('l', strtotime($data[0]->created_at)) }}@endif</td></tr>
                                <tr><td width="100px">Call Script</td><td><?php echo ucfirst($data[0]->callScript);?></td></tr>
                                <tr>
                                    <td width="100px">Message</td>
                                    <td>
                                        <?php
                                            if(!empty($data[0]->api_response))
                                            {
                                                $api_response = json_decode($data[0]->api_response);
                                                if(!empty($api_response) && isset($api_response->content))
                                                {
                                                    if( $data[0]->status == 1 )
                                                    {
                                                        if( isset($api_response->content->message))
                                                        {
                                                            echo $api_response->content->message;
                                                        }
                                                        else
                                                        {
                                                            echo 'N/A';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if(isset($api_response->content->error))
                                                        {
                                                            echo $api_response->content->error;
                                                        }
                                                        else if(isset($api_response->content->error_message))
                                                        {
                                                            echo $api_response->content->error_message;
                                                        }
                                                        else if(isset($api_response->content->message))
                                                        {
                                                            echo $api_response->content->message;
                                                        }
                                                        else
                                                        {
                                                            echo 'N/A';
                                                        }
                                                        if( $data[0]->status == 11 )
                                                        {
                                                            echo "<br>";
                                                            if(isset($api_response->content->error_description))
                                                            {
                                                                echo $api_response->content->error_description;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    echo "N/A";
                                                }
                                            }
                                            else
                                            {
                                                echo "N/A";
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50%">
                                        <span ><b>Original Lead Format :</b></span></br>
                                        <span class="details_now"> {!! $data[0]->campaign_details->testMail !!}</span>
                                    </td>
                                    <td style="width:50%">
                                        <span ><b>NEW lead format received :</b></span></br>
                                        <?php
                                            $lines = explode(PHP_EOL, $data[0]->original_lead);
                                            if( is_array($lines))
                                            {
                                                foreach ($lines as $key => $value)
                                                {
                                                    if( trim($value) != '' )
                                                        echo '<p class="testMail">'.$value."</p>";
                                                }
                                            }
                                            else
                                            {
                                                echo $data[0]->original_lead;
                                            }
                                        ?>
                                    </td>
                                 </tr>
                            <?php } else {?>
                                <tr><td width="100px">No data found.</td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <div class="text-right">
            <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).on('click','.testMail',function(event) {
      event.preventDefault();
      $('.popover').hide();
      return false;
});
function hightLight (element, start, end,tagName , id, isCustomTag) {
    var str = element[0].innerHTML;
    str = `${ str.substr(0, start) }<mark data-name="${tagName.replace("\"", "")}" class="campaignTag" data-custom-tag="${isCustomTag}" data-id="${id}">${str.substr(start, end - start)}</mark>${str.substr(end)}`;
    element[0].innerHTML = str.replace("\"", "");

}
<?php
if( !empty($campTags))
{
    foreach ($campTags as $key => $value) { ?>
        jQuery('.testMail').each(function(index)
        {
            var thiss = $(this);
            var tagnamee = '<?php echo $value->tagName;?>';
            if( tagnamee != '' )
            {
                if (index == '<?php echo $value->indexRow?>') {
                    hightLight(thiss, '<?php echo $value->positionStart?>', '<?php echo $value->positionEnd?>','<?php echo $value->tagName?>','<?php echo $value->id?>','<?php echo $value->isCustomTag; ?>')
                }
            }
        });
<?php }
} ?>
</script>
<style>
.testMail .mark, mark {
	padding: .2em;
	background-color: #CCEFFC;
}
</style>
