
               <div class="plan-section top-up">
                   <div class="container">
                       <div class="row">
                                @php
                                $totalTopup = count($topupData);
                                @endphp
                                @foreach($topupData as $key=>$plan)
                                <?php
                                       if($topupId == $plan->top_id){
                                           $btnSelect = 'Selected';
                                           $acClass   = 'activeCls';
                                       }
                                       else
                                       {
                                           $btnSelect = 'Select';
                                           $acClass   = '';
                                       }

                                       $check = 0;
                                       $patterns             = array('/{Credit_Amount}/');
                                       $replacements         = array($plan->credit);
                                       $emailstring          = preg_replace($patterns, $replacements,$plan->tops->description);
                                       $Class = 'first-plan';
                                       $color='#2961af';
                                       $clickClass="checkTopUpPrc";
                                   ?>
                                <div class="col-md-4 plan_row">
                                   <div class="{{$Class}}">
                                      <h5 <?php if( ($key+1) == $totalTopup ){?>style="padding:10px"<?php } ?>>Top Up<?php if( ($key+1) == $totalTopup ){?><span style="font-size:9px;display: block;">Best Value!</span><?php }?></h5>
                                      <div class="price-dollar" id="{{ $plan->plan_currency->currencySymbol }} {{$plan->price}}">
                                         <span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->price}} <sup style="left:-15px">*</sup></span>
                                          <p class="mb-0 text-center">{{ $plan->plan_currency->currencyCode }}</p>
                                      </div>
                                      <div class="plan-text">
                                         <ul>
                                            {!!html_entity_decode($emailstring)!!}
                                         </ul>
                                         <div class="plan-btn">
                                            <a href="javascript:void(0)" data-id="{{ $plan->top_id }}" data-price="{{ $plan->price }}" data-credit="{{ $plan->credit }}" data-plainid="{!! \Crypt::encrypt($plan->top_id)!!}" data-mod="{{$check}}" class="<?php echo $clickClass ?> btn btn-primary btn-block {{$acClass}} topPlanClass">{{$btnSelect}}</a>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <input type="hidden" name="countTopUpTotalPlans" class="countTopUpTotalPlans" value="{{ count($topupData) }}"/>
                                @endforeach
                          </div>
                          <input type="hidden" name="topUpPayId" class="topUpPayId"/>
                           <input type="hidden" name="chooseTopUpPlanId" class="usedTopUpPlainId"/>
                           <input type="hidden" name="TopUpPlanId" id="TopUpPlanId"/>

                    </div>
              </div>
