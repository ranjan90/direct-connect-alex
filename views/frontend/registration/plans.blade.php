<div class="plan-section">
	  <div class="container">
			<div class="row">
			@if(!$plansData->isEmpty())
			@foreach($plansData as $plan)


      <?php
      if ($plan->plans->billingType == 2) {
        $span = '<span class="planspan">'.$plan->plans->billing_Type->planType.'</span>';
        $Class = 'first-plan';
      } else if($plan->plans->billingType == 3) {
        $span = '<span class="planspan">'.$plan->plans->billing_Type->planType.'</span>';
        $Class = 'first-plan second-plan';
      }

         	              $patterns             = array('/{contactNumber}/','/{creditNumber}/');
                          $replacements         = array($plan->plans->no_of_contacts,$plan->credit);
                          $emailstring=(new \App\Helpers\GlobalFunctions)->emailReplacement($patterns,$replacements,$plan->plans->description);
                          $emailstring=$emailstring['content'];

       ?>
			<div class="col-md-4">
				 <div class="{{$Class}}">
				 <h5 <?php if( $plan->amount == 99 ){?>style="padding:8px"<?php } ?>>{{$plan->plans->name}}
           <?php if( $plan->amount == 99 ){?>
               <span style="font-size:12px;display: block;text-transform: initial;">Most popular</span>
           <?php } ?>
           <!--{!! $span !!}-->
       </h5>
				 <div class="price-dollar">
					<span class="dollar-price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</span>
                     <p class="text-center">{{ $plan->plan_currency->currencyCode }}/month</p>
                     <p class="package-ins">This package is perfect for company's who has upto {{$plan->plans->no_of_contacts}} user on their team</p>
				 </div>
				 <div class="plan-text">
    				 <ul>
        				 <!--<li>Valid upto <span class="text-plan"> {{$plan->plans->duration}} </span> Month</li>
        				 <li>Includes Maximum of <span class="text-plan"> {{$plan->plans->minutes_per_month}} </span> Minutes</li>-->
                 @if($plan->plans->billingType == 2)

                 <!--<li>No of contacts :  {{ $plan->plans->no_of_contacts }} </li>-->
                 <!--<li>{{ $plan->plans->no_of_contacts }} Contact User Variations</li>-->
                 <!--<li>Credit Amount :  {{ $plan->plan_currency->currencySymbol }} {{$plan->credit}}</li>-->
                 <!--<li>To Pay :  b   {{ $plan->plan_currency->currencySymbol }} {{$plan->amount + $plan->credit}}</li>-->

                 @else
                  <!--<li>No of contacts :  {{ $plan->plans->no_of_contacts }} </li>-->
                 <!--<li>{{ $plan->plans->no_of_contacts }} Contact User Variations</li>-->
                 <!--<li>To Pay :  {{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</li>-->
                 @endif
                {!!html_entity_decode($emailstring)!!}
    				 </ul>
						<div class="plan-btn">
					         <a href="javascript:void(0)" data-payId="{{$plan->stripe_plan_id}}" data-plainid="{{$plan->plan_id}}" class="checkPlainPrc btn btn-primary btn-block <?php if($planId == $plan->plan_id){?> activeCls<?php } ?> "><?php if($planId == $plan->plan_id){?>Selected<?php } else{?>Buy Now<?php }?></a>
				        </div>
				 </div>
			 </div>
			</div>


			@endforeach
			<input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($plansData) }}"/>
			@else
			<p>No plans found..</p>
			@endif
			<input type="hidden" name="is_usernew" value="<?php echo $is_usernew; ?>"/>
			<input type="hidden" name="plan_country_id" value="<?php echo $plan_country_id; ?>"/>
			<input type="hidden" name="choosePlanId" class="usedPlainId"/>
			<input type="hidden" name="payId" class="payId"/>





		</div>
	  </div>
 </div>


<!-- Second Step --
<div class="plan-main-bx mid-box">
	@if(!$plansData->isEmpty())
	@foreach($plansData as $plan)
	<div class="plan-bx">
		<h3>{{$plan->plans->name}}</h3>
		<div class="plan-detail">
				<a href="javascript:void(0)" class="price">{{ $plan->plan_currency->currencySymbol }} {{$plan->amount}}</a>
				<span>Per month</span>
				<h4>{{$plan->plans->minutes_per_month}}</h4>
		</div>
		<a href="javascript:void(0)" data-payId="{{$plan->stripe_plan_id}}" data-plainid="{{$plan->plan_id}}" class="checkPlainPrc btn btn-primary btn-block">Choose Plan</a>
	</div>
	@endforeach
	<input type="hidden" name="countTotalPlans" class="countTotalPlans" value="{{ count($plansData) }}"/>
	@else
	<p>No plans found..</p>
	@endif
	<input type="hidden" name="choosePlanId" class="usedPlainId"/>
	<input type="hidden" name="payId" class="payId"/>
</div>
<!-- End Second Step -->
