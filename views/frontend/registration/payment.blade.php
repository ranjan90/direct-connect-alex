<style media="screen">

</style>
<div class="row">
  <div class="col-lg-4 cstm-col col-md-6">
      <h3>Client Information</h3>
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <label for="">Name</label>
        </div>
        <div class="col-lg-8 col-md-6">
          <span>
            @if(isset($payMentData->users[0]->firstName))
              {{$payMentData->users[0]->firstName}}
            @else
              N/A
            @endif</span>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <label for="">Email</label>
        </div>
        <div class="col-lg-8 col-md-6">
          <span>
            @if(isset($payMentData->users[0]->email))
              {{$payMentData->users[0]->email}}
            @else
              N/A
            @endif</span>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <label for="">Phone No</label>
        </div>
        <div class="col-lg-8 col-md-6">
          <span>

            @if(isset($payMentData->users[0]->phoneNo))
              {{$payMentData->users[0]->phoneNo}}
            @else
              N/A
            @endif</span>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <label for="">Company Name</label>
        </div>
        <div class="col-lg-8 col-md-6">
          <span>
            @if(isset($payMentData->users[0]->companyName))
              {{$payMentData->users[0]->companyName}}
            @else
              N/A
            @endif</span>
        </div>
      </div>
  </div>
  <div class="col-lg-5 cstm-col col-md-6">
    <h3>Plan Information</h3>
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <label for="">Plan Name</label>
      </div>
      <div class="col-lg-8 col-md-6">
        <span>

          @if(isset($payMentData->user_plan->plans->name))
            {{$payMentData->user_plan->plans->name}}
          @else
            N/A
          @endif
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <label for="">Monthly Subscription</label>
      </div>
      <div class="col-lg-8 col-md-6">
        <span>
          <?php
             $creditNumber="";
             $contactNumber="";

           ?>

          @if(isset($payMentData->user_plan->plans->plan_prices[0]->amount))
            <?php
              $creditNumber=$payMentData->user_plan->plans->plan_prices[0]->credit;
            ?>
            {{$payMentData->user_plan->plans->plan_prices[0]->plan_currency->currencySymbol}} {{$payMentData->user_plan->plans->plan_prices[0]->amount}}
          @else
            N/A
          @endif
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <label for="">Top Up</label>
      </div>
      <div class="col-lg-8 col-md-6">
        <span>

          @if(isset($topUpData) && !empty($topUpData))
            @if(isset($payMentData->user_plan->plans->plan_prices[0]->plan_currency))
                {{$payMentData->user_plan->plans->plan_prices[0]->plan_currency->currencySymbol}} {{$topUpData->price}} for {{$topUpData->credit}} credits
            @else
                {{$topUpData->price}} for {{$topUpData->credit}} credits
            @endif
          @else
            N/A
          @endif
        </span>
      </div>
    </div>
    <div class="row" id="paymentViewPromoCode" style="display:none">
        <div class="col-lg-4 col-md-6">
          <label for="">Promo Code</label>
        </div>
        <div class="col-lg-8 col-md-6">
          <span id="paymentViewPromoCodeSpan" style="display:none">
          </span>
        </div>
    </div>
    <!--div class="row">
      <div class="col-lg-4 col-md-6">
        <label for="">Plan Duration</label>
      </div>
      <div class="col-lg-8 col-md-6">
        <span>
          @if(isset($payMentData->user_plan->plans->duration))
            {{$payMentData->user_plan->plans->duration}} Month
          @else
            N/A
          @endif
        </span>
      </div>
  </div-->
    <div class="row">
      <div class="col-md-12">
        <label for="">Description</label>
      </div>
      <div class="col-md-12">
        <span>

          @if(isset($payMentData->user_plan->plans->description))
            <?php
                          $patterns             = array('/{contactNumber}/','/{creditNumber}/');
                          $replacements         = array($payMentData->user_plan->plans->no_of_contacts,$creditNumber);
                          $emailstring=(new \App\Helpers\GlobalFunctions)->emailReplacement($patterns,$replacements,$payMentData->user_plan->plans->description);
            ?>
            {!! $emailstring['content'] !!}

          @else
            N/A
          @endif
          </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 cstm-col pl-0">
      <div class="row"><h3>Payment Details</h3></div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                  <label for="cardHolderName" class="control-label">Card Holder Name <span class="star">*</span></label>
                  <input id="cardHolderName" type="tel" class="input-lg form-control" name="cardHolderName" placeholder="Card Holder Name" required="">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                  <label class="control-label">Card Number <span class="star">*</span></label>
                  <input id="cardNumber" type="tel" class="input-lg form-control cc-number mastercard identified" name="cardNumber" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required="">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                  <label for="ccExpiryMonth" class="control-label">Card Expiry <span class="star">*</span></label>
                  <input id="ccExpiryMonth" type="tel" class="input-lg form-control cc-exp" name="ccExpiryMonth" autocomplete="cc-exp" placeholder="mm / yy" required="">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                  <label class="control-label">Card CVC <span class="star">*</span></label>
                  <input id="cvvNumber" type="tel" class="input-lg form-control cc-cvc" name="cvvNumber" autocomplete="off" placeholder="•••" required="">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group promo pr-3 pl-3">
                  <div class="row">
                        <label class="control-label">Have Promo Code</label>
                      <div class="col-lg-8">
                          <input id="signupPromoCode" type="text" class="input-lg form-control" placeholder="" >
                          <input type="hidden" name="promocode" id="promocodehidden" />
                          <label id="signupPromoCode-error" class="has-error" style="display:none" for="signupPromoCode">This field is required.</label>
                      </div>
                      <div class="col-lg-4 pl-0 text-right">
                          <button id="signupPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                      </div>
                  </div>
                    <div class="row" style="padding-top: 15px; display:none;font-size: 13px;" id="appliedPromoCode">
                        <div class="col-md-12" style="padding-right: 0px;">
                            <div class="alert alert-success" role="alert">
                                <div class="row vertical-align" style="display: flex;align-items: center;">
                                    <div class="col-md-2 p-0 text-center">
                                        <i class="fas fa-smile"></i>&nbsp;
                                    </div>
                                    <div class="col-md-10 p-0" id="promoSuccessMessage">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
          </div>
      </div>
  </div>

  <div class="clearfix">

  </div>
    <hr>
      <br>



<script type="text/javascript">
jQuery(function($) {
  $(document).find('[data-numeric]').payment('restrictNumeric');
  $(document).find('.cc-number').payment('formatCardNumber');
  $(document).find('.cc-exp').payment('formatCardExpiry');
  $(document).find('.cc-cvc').payment('formatCardCVC');
  $.fn.toggleInputError = function(erred) {
    this.parent('.form-group').toggleClass('has-error', erred);
    return this;
  };


});
</script>
<input type="hidden" name="planAmount" class="planAmount" value="{{$payMentData->user_plan->plans->plan_prices[0]->amount}}">
<input type="hidden" name="planName" class="planName" value="{{$payMentData->user_plan->plans->name}}">

