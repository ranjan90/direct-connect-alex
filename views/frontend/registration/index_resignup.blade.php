@extends('layouts.app')
@section('content')
<div class="register-main-sec">
	<div class="main-wrapper">
<!--		<div><center><h2>Register Now</h2></center></div>-->
		<div class="choose-sec">
			<div class="card">
				<div class="card-body mb-4">
					<!-- Stepper -->
					<div class="steps-form">
						<div class="steps-row setup-panel">
							<div class="steps-step step-1-bar sign_up_active">
								<a href="#step-1" class=" ">
								  Register
								</a>
							</div>
							<div class="steps-step step-2-bar ">
								<a href="#step-2" class=" ">
                                    Choose Plan
                                </a>
							</div>
								<div class="steps-step step-3-bar ">
								<a href="#step-3" class=" ">
                                    Payment Details
								</a>
							</div>
							<div class="steps-step step-4-bar ">
								<a href="#step-4" class=" " disabled="disabled">
								    Your Payment</a>
							</div>
							<div class="steps-step step-5-bar ">
								<a href="#step-5" class="btn btn-default btn-circle" disabled="disabled">
								Complete</a>
							</div>
						</div>
					</div>
					{{ Form::open(array('url' => '/registeration','id' => 'client-signup-form','class' => 'client-signup-form','autocomplete' => 'off')) }}
						<!-- First Step -->
						<div class="row setup-content mt-5" id="step-1">
							<div class="container">
								<h3>Fill Details</h3>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group  md-form form-group-placeholder">
											<div class="palceholder">
												<label for="yourName">First Name</label>
												<span class="star">*</span>
											</div>
											<input type="hidden" id="_token" name="remember_token" value="{{ csrf_token() }}"/>
											<input type="hidden" id="userIDCheck" name="userIDCheck" value="{{ $user->id }}" class="userIDCheck"/>
											<input type="hidden" class="formStep" name="formStep" value=""/>
											{!!  Form::text('firstName',$user->firstName,['placeholder'=>"First Name",'class'=>'form-control']); !!}
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
<!--											<label for="yourLastName" >Last Name </label>-->
											{!!  Form::text('lastName',$user->lastName,['placeholder'=>"Last Name",'class'=>'form-control']); !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group form-group-placeholder">
											<div class="palceholder">
												<label for="yourEmail">Email</label>
												<span class="star">*</span>
											</div>
											{!!  Form::text('email',$user->email,['placeholder'=>"Email",'class'=>'form-control checkEmail']); !!}
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-3 info-yel">
											<img src="{{ asset('assets/frontend/img/u.png')}}" class="tip-info">
                                        <div class="yel-tip">Password should contain one lowercase letter, one uppercase letter, one number and one special symbol atleast</div>
										<div class="form-group md-form form-group-placeholder">
											<div class="palceholder">
												<label for="yourPassword">Password</label>
												<span class="star">*</span>
											</div>
											<input type="password" autocomplete="off" placeholder="Password" id="password" name="password" class="form-control">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group md-form">
											<div class="form-group md-form form-group-placeholder">
											<div class="palceholder">
												<label for="confirmPassword">Confirm Password</label>
												<span class="star">*</span>
											</div>
											<input type="password" autocomplete="off" placeholder="Confirm Password" id="confirmPassword" name="confirmPassword" class="form-control">
										</div>
									</div>
                                    </div>
									<div class="col-md-3">
										<div class="form-group md-form form-group-placeholder">
                                            <div class="palceholder">
												<label for="phoneNumber">Phone Number</label>
												<span class="star">*</span>
											</div>
<!--											<label for="yourLastName" >Phone Number <span class="star">*</span></label>-->
											{!!  Form::text('phoneNo',$user->phoneNo,['placeholder'=>"Phone Number",'class'=>'form-control']); !!}
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group md-form">
<!--											<label for="companyname" >Company Name </label>-->
											{!!  Form::text('companyName',$user->companyName,['placeholder'=>"Company Name",'class'=>'form-control']); !!}
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6">
										<div class="form-group md-form">
<!--											<label for="companyurl" >Company URL </label>-->
											{!!  Form::text('companyUrl',$user->companyUrl,['placeholder'=>"Company URL",'class'=>'form-control']); !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group md-form">
<!--											<label for="country" >Country <span class="star">*</span></label>-->
											<!-- Previously its name was regionId but it was not saving into the database so thats why name changed to country
													Ravinder Kaur 2 August, 2018 -->
											{!!	Form::select('country',$regions,null,['class'=>'form-control countryRegions']) !!}
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 text-right">
										<button class="sign-in-back btn-primary btn btn-indigo btn-rounded  signUpNext" type="button">Next</button>
									</div>
								</div>
							</div>
						</div>

						<!-- Second Step -->
						<div class="row setup-content" id="step-2">
							<div class="col-md-12">
								<h3>Choose Plan</h3>
								<div class="plains_main_div">
									<p>Choose country for plans..</p>
									<input type="hidden" id="noplans" value="1"/>
								</div>
								<div class="btn-detail">
<!--									<button class="btn btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>-->
									<button class="btn btn-warning btn-indigo btn-rounded nextBtn float-right signUpNext" type="button">Next</button>
								</div>
							</div>
						</div>
							<!-- Second Step -->
						<div class="row setup-content" id="step-3">
							<div class="col-md-12">
								<h3>Top Up</h3>
								<div class="top_up_main_div">
									<p>Choose country for top up..</p>
									<input type="hidden" id="notopupplans" value="1"/>
								</div>
								<div class="btn-detail">
<!--									<button class="btn btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>-->
									<button class="btn-warning btn btn-indigo btn-rounded nextBtn float-right signUpNext" type="button">Next</button>
								</div>
							</div>
						</div>

						<!-- Third Step -->
						<div class="row setup-content" id="step-4">
							<div class="col-md-12">
								<h3>Your Payment</h3>
								<div class="payment_main_div">
								</div>
								<div class="text-right">
<!--									<button class="btn btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>-->
								<button class="btn btn-indigo btn-warning btn-rounded nextBtn signUpNext" type="button">Next</button>
								</div>
							</div>
						</div>
						<div class="row setup-content" id="step-5" >
	                        <div class="col-md-12">
	                            <div class="sucess-main-sec">
	                                <div class="suceesfully-text clearfix">
                                    <h3 class="text-center">Congratulations <strong id="Uname"></strong>!</h3>
                                    <p class="text-center">Welcome to Direct Connect.</p>
<!--                                    <p>Didn't receive your email? Please check your spam folder or <a href="{{url('verify-form')}}">Click here to resend</a>.</p>-->
<!--                                    <p>Once your email has been verified you will be able to access the dashboard and set up your first campaign: <a href="{{url('login')}}">Click here to login</a><p>-->
                                    <p class="text-center">To begin, let's set up first campaign.</p>
                                    <a href="{{url('login')}}" class="campaign btn">Create new campaign</a>
                                    <p class="mt-4">If you haven't received your verification email yet,please check your spam folder or <a href="{{url('verify-form')}}"> Click here to resend</a>.</p>
                                </div>
	                            </div>
	                        </div>
	                    </div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@section('js')
<script src="{{ asset('assets/js/localStorage.js')}} "></script>
<script type="text/javascript">
  removeStorage();
  var prefixUrl="<?php echo $url?>";

		$(document).ready(function(){

			$(document).on('change','#signupPromoCode',function(e)
			{
				var promocode   = $('#signupPromoCode').val();
				$('#appliedPromoCode').css('display','none');
				$('#paymentViewPromoCode').css('display','none');
				$('#paymentViewPromoCodeSpan').css('display','none');
				$('#promocodehidden').val('');
			});
			$(document).on('click','#signupPromoCodeBtn',function(e)
			{
				$('#signupPromoCode-error').css('display','none').html('');
				$('#signupPromoCodeBtn').parents('.form-group').removeClass('has-error');
				var promocode   = $('#signupPromoCode').val();
				$('#appliedPromoCode').css('display','none');
				$('#paymentViewPromoCode').css('display','none');
				$('#paymentViewPromoCodeSpan').css('display','none');
				$('#promocodehidden').val('');
				if( promocode != '' )
				{
					$.ajax({
						url                : prefixUrl + '/validate-promo-code',
						type               : 'POST',
						dataType           : 'json',
						data				: { 'code':promocode },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						beforeSend : function() {
							$(".loader_div").show();
						},
						success    : function(response)
						{
							$(".loader_div").hide();
							if( response.success )
							{
								tost(response.message,"Success",4000);
								$('#appliedPromoCode').css('display','block');
								$('#promoSuccessMessage').html(response.msg);
								$('#paymentViewPromoCode').css('display','flex');
								$('#paymentViewPromoCodeSpan').css('display','flex').html(response.credits+' credits');
								$('#promocodehidden').val(promocode);
							}
							else
							{
								$('#signupPromoCode-error').css('display','block').html(response.message);
								$('#signupPromoCodeBtn').parents('.form-group').addClass('has-error');
							}
						},
						complete   : function(){
							$(".loader_div").hide();
						}
					});
				}
				else
				{
					$('#signupPromoCode-error').css('display','block').html('This field is required.');
					$('#signupPromoCodeBtn').parents('.form-group').addClass('has-error');
				}
			});

		 });

		 function tost(message,type, delay = null) {
				 $.toast().reset('all');
				 return $.toast({
				   heading             : type,
				   text                : message,
				   loader              : true,
				   loaderBg            : '#fff',
				   showHideTransition  : 'fade',
				   icon                : type.toLowerCase(),
				   hideAfter           : delay || 5000,
				   position            : 'top-right'
				 });
		 }
</script>
<script>
$(document).on('mouseover','.tip-info',function(){
	$('.yel-tip').show();
})
    $(document).on('mouseout','.tip-info',function(){
	$('.yel-tip').hide();
})
</script>
@endsection

@endsection
