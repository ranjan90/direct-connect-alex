@extends('layouts.app')
@section('content')



<div class="check">
	<div class="main-wrapper">
		<div class="choose-sec">
			<div class="card">
				<div class="card-body mb-4">
					<!-- Stepper -->
					<div class="steps-form">
						<div class="steps-row setup-panel" >
							<div class="steps-step">
								<a href="#step-1" type="button" class="btn btn-indigo btn-circle"><img src="{{ asset('assets/'.config("app.frontendtemplatename").'/img/your-detail-h.png') }}"/></a>
								<p>Fill Details</p>
							</div>
							<div class="steps-step">
								<a href="#step-2" type="button" class="btn btn-default btn-circle"><img src="{{ asset('assets/'.config("app.frontendtemplatename").'/img/plan-h.png') }}"/></a>
								<p> Choose Plan</p>
							</div>
							<div class="steps-step">
								<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"><img src="{{ asset('assets/'.config("app.frontendtemplatename").'/img/payment-h.png') }}"/></a>
								<p> Your Payment</p>
							</div>
							<div class="steps-step">
								<a href="#step-4" id="register_confirm" type="button" class="btn btn-default btn-circle" disabled="disabled"><img src="{{ asset('assets/'.config("app.frontendtemplatename").'/img/register-h.png') }}"/></a>
								<p>Your Registration</p>
							</div>
						</div>
					</div>


                    <div class="row setup-content" id="step-4" >
                        <div class="col-md-12">
                            <div class="sucess-main-sec">
                                <div class="suceesfully-text clearfix">
                                    <h3 class="text-center">Congratulations <strong id="Uname"></strong>!</h3>
                                    <p class="text-center">Welcome to Direct Connect.</p>
<!--                                    <p>Didn't receive your email? Please check your spam folder or <a href="{{url('verify-form')}}">Click here to resend</a>.</p>-->
<!--                                    <p>Once your email has been verified you will be able to access the dashboard and set up your first campaign: <a href="{{url('login')}}">Click here to login</a><p>-->
                                    <p class="text-center">To begin, let's set up first campaign.</p>
                                    <a href="{{url('login')}}" class="campaign btn">Create new campaign</a>
                                    <p>If you have'nt received your verification email yet,please check your spam folder or<a href="{{url('verify-form')}}">Click here to resend</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
