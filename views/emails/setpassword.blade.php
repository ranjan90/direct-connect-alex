<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ $title }}</title>
</head>

<body>
<div style="margin:0 auto;width:600px;padding:30px;border: 1px solid #ddd;">
  <table style="width:100%;">
    <tr>
   	  <td><img style="width:140px;" src="{{ asset('assets/frontend/img/logo-r.png') }}" alt="logo"></td>
    </tr>

   	  <h2>Welcome <strong>{{$name}}</strong></h2>
		  <br/>
		  Your registered email-id is :-<strong> {{$email}} </strong>, Please click on the below link to Set  your password
		  <br/>
		  <br/>
		 <a href="{{ $link }}" style="background-color: #007bff; border: none;color: white;padding: 10px 13px !important;;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;border-radius: 6px !important;;">Set Password</a>
    
 </table>
</div>

</body>
</html>
