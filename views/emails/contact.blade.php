<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ $title }}</title>
</head>

<body>
<div style="margin:0 auto;width:600px;padding:30px;border: 1px solid #ddd;">
 <table style="width:100%;">
   <tr>
     <td><img style="width:140px;" src="{{ asset('assets/frontend/img/logo-r.png') }}" alt="logo"></td>
   </tr>
   <tr>
     <td colspan="2">Hi Admin</td>
   </tr>
   <tr>
     <td colspan="4"></td>
   </tr>
   <tr>
     <td colspan="4"></td>
   </tr>
   <tr>
     <td colspan="4"></td>
   </tr>
   <tr>
     <td colspan="4"></td>
   </tr>
   <tr><td>One of your valuable customers has tried to contact you.
   Please find the followings details: </td><tr>
     <tr> <td colspan="4"></td></tr>
     <tr> <td colspan="4"></td></tr>
     <tr>
       <td colspan="4"></td>
     </tr>
     <tr>
       <td colspan="4"></td>
     </tr>
     <tr>
       <td colspan="4"></td>
     </tr>
     <tr>
       <td>Name: @if(isset($name)) {{$name}} @else {{"-"}}@endif</td>
     </tr>
     <tr>
       <td colspan="4"></td>
     </tr>
     <tr> <td colspan="4"></td></tr>
     <tr>
       <td>Email: @if(isset($email)) {{$email}} @else {{"-"}}@endif</td>
     </tr>
     <tr>
       <td colspan="4"></td>
     </tr>
     <tr> <td colspan="4"></td></tr>
     <tr>
       <td>Phone Number: @if(isset($phone)) {{$phone}} @else {{"-"}}@endif</td>
     </tr>
     <tr> <td colspan="4"></td></tr>
     <tr> <td colspan="4"></td></tr>
     <tr>
       <td>Message: @if(isset($messageData)) {{$messageData}} @else {{"-"}}@endif</td>
     </tr>
     <tr>
       <td colspan="4"></td>
     </tr>
</table>
</div>

</body>
</html>
