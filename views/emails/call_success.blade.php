<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>{{ $title }}</title>

    </head>
<body style="margin-top:20px">
    <div style="margin:0 auto;width:600px;padding:30px;border: 1px solid #ddd;">
        <table style="width:100%;border-bottom: 1px solid #ccc;">
            <tr>
                <td style="72%;"><img style="width:140px;" src="{{ asset('assets/frontend/img/logo-r.png') }}" alt="logo"></td>
                <td style="28%;text-align:right;font-size:12px;"></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <?php
          $return_data=App\Helpers\GlobalFunctions::returnCallBusiness("",$lead_id);
          extract($return_data);

         ?>
        <table style="width:100%;border-collapse:collapse;margin-top: 30px;">
			<tbody style="text-align:left;">
	            <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Date</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $startdate !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Campaign Name</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $department !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Answered By</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $answered_by !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Lead ID</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_id !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Lead Name</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_name !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Lead Contact</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_number !!}</td>
                </tr>
                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Message</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_message !!}</td>
                </tr>


                <tr>
                    <td style="background:#6fcbd8;padding:5px 20px;">Business</td>
                </tr>
                <tr>
                    <td style="background:#f6f6f6;padding:5px 20px;">

                                                   @if(isset($business_btn_color))
                                                     <button type="button" class="call-status-button btn <?php echo  $business_btn_color;?> set_btn_width"><?php echo isset($business_btn_message) ?  $business_btn_message : 'N/A'; ?></button>
                                                    @endif

                    </td>
                </tr>

                 <tr>
                    <td style="background:#6fcbd8;padding:5px 20px;">Visitor</td>
                </tr>
                <tr>
                    <td style="background:#f6f6f6;padding:5px 20px;">

                                                  @if(isset($customer_btn_color))

                                                     <button type="button" class="call-status-button btn <?php echo  $customer_btn_color;?> set_btn_width"><?php echo isset($customer_btn_message) ?  $customer_btn_message : 'N/A'; ?></button>

                                                    @endif

                    </td>
                </tr>

                <tr>
                    <td style="background:#6fcbd8;padding:5px 20px;">Status</td>
                </tr>
                <tr>
                    <td style="background:#f6f6f6;padding:5px 20px;">

                           <?php

                            if(isset($status_message) && !empty($status_message))
                             {
                                    echo $status_message;
                             }
                             else
                             {
                                     echo App\Helpers\GlobalFunctions::returnCallStatus($lead_id);
                             }

                            ?>

                    </td>
                </tr>



                <?php if( $call_recording_display == 1 ){?>
                    <tr>
    	                <td style="background:#6fcbd8;padding:5px 20px;">Call Recording</td>
                    </tr>
                    <tr>
    	                <td style="background:#f6f6f6;padding:5px 20px;">
                        <?php if($recording != '' ) {?>
                            <a href="<?php echo $recording;?>" target="_blank"><?php echo $recording;?></a>
                        <?php } ?>
                        </td>
                    </tr>
                <?php } ?>

                <tr>
	                <td style="background:#6fcbd8;padding:5px 20px;">Call Length</td>
                </tr>
                <tr>
	                <td style="background:#f6f6f6;padding:5px 20px;">{!! $call_length.' Sec' !!}</td>
                </tr>
                <?php if( isset($lead_track_no) && $lead_track_no != '' ){?>
                    <tr>
                        <td style="background:#6fcbd8;padding:5px 20px;">Lead/Ticket ID (for CRM tracking)</td>
                    </tr>
                    <tr>
                        <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_track_no !!}</td>
                    </tr>
                <?php } ?>
                <?php if( isset($lead_email) && $lead_email != '' ){?>
                    <tr>
                        <td style="background:#6fcbd8;padding:5px 20px;">Email (for CRM tracking)</td>
                    </tr>
                    <tr>
                        <td style="background:#f6f6f6;padding:5px 20px;">{!! $lead_email !!}</td>
                    </tr>
                <?php } ?>
			</tbody>
        </table>
    </div>
</body>
</html>
