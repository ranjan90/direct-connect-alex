@extends('layouts.customer')
@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <div class="row">
        <div class="col-md-12">
            <div class="heading-campaigns">
                <h2>Please select Business you want to access (You can switch business anytime)</h2>
                <div class="create-new-campaign-btn"></div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="row">
        <div class="col-md-12 contact_listing">
            <div class="latest-champaign-table">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">S.No.</th>
                                <th scope="col">Business Name</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; ?>
                            @forelse($business as $business)
                                <?php $i++; ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ ucfirst($business->business_name) }}</td>
                                    <td>
                                        <a href="{{ $url.'/choose-business/'.Crypt::encrypt($business->id)}}" data-toggle="tooltip" title="Go To Business" data-placement="bottom"><i class="fas fa-sign-in-alt"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">No business found!</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
