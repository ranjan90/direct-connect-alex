@include('includes.customer.head')

    @if (Auth::check())
    	<?php $checkPlanStatus=0; $url=url(request()->route()->getPrefix()); ?>
    	 @if(session()->has('checkPlanStatus') && !empty(session('checkPlanStatus')))
    	 <?php $checkPlanStatus=1;?>
    	 @endif
    @else
    	<?php $url=url('');?>
    @endif

@include('includes.customer.header')
@include('includes.customer.aside',['url'=>$url])
   @yield('content')
@include('includes.customer.footer')
   @yield('js')
