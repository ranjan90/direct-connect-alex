@include('includes.admin.head')
 @if (Auth::check())
    <script type="text/javascript">
         url="<?php //echo url(request()->route()->getPrefix())?>";
    </script>
    <?php $url=url(request()->route()->getPrefix()); ?>
    @else
    <?php $url=url(''); ?>
    @endif
@include('includes.admin.header')
@include('includes.admin.aside',['url'=>$url])
   @yield('content')
@include('includes.admin.footer')
@yield('js')