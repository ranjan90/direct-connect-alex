<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/home') active @endif" href="{{ URL('/admin/home') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/regions') active @endif" href="{{ URL('/admin/regions') }}"><i class="app-menu__icon fa fa-globe"></i><span class="app-menu__label">Regions</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/plans') active @endif" href="{{ URL('/admin/plans') }}"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Plans</span></a>
        </li>
       @endcan
       @canany(['admin','Has-access-to-create-edit-campaigns'])
        <li>
            <a class="app-menu__item @if($current_url=='/admin/customers' || $current_url=='/admin/inactive-customers' || $current_url=='/admin/incomplete-customers') active @endif" href="{{ $url.'/customers' }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Customers</span></a>
        </li>
        <!-- @endcanany -->

        <li>
            <a class="app-menu__item @if($current_url=='/admin/reporting') active @endif" href="{!! $url.'/contacts' !!}"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Leads Contacts</span></a>
        </li>
        @endcanany
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/reporting') active @endif" href="{{ URL('/admin/reporting') }}"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Reporting</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/email-templates') active @endif" href="{{ URL('/admin/email-templates') }}"><i class="app-menu__icon fa fa-envelope"></i><span class="app-menu__label">Email Templates</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
            <a class="app-menu__item @if($current_url=='/admin/call-setting') active @endif" href="{{ URL('/admin/call-setting') }}"><i class="app-menu__icon fa fa-phone"></i><span class="app-menu__label">Call Settings</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
              <a class="app-menu__item @if($current_url=='/admin/role-management') active @endif" href="{{ URL('/admin/role-management') }}"><i class="app-menu__icon fa fa-window-restore"></i><span class="app-menu__label">Role Management</span></a>
        </li>
        @endcan
        @can('admin')
        <li>
              <a class="app-menu__item @if($current_url=='/admin/user-management') active @endif" href="{{ URL('/admin/user-management') }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">User Management</span></a>
        </li>
        @endcan
<!--//////////////////////////////////////////////////////////////-->
        @can('customer')
         <li>
            <a class="app-menu__item" href="{!! $url.'/dashboard' !!}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Customers</span></a>
        </li>
        <li>
            <a class="app-menu__item" href="{!! $url.'/campaigns' !!}"><i class="app-menu__icon fa fa-globe"></i><span class="app-menu__label">Campaigns</span></a>
        </li>
         <li>
            <a class="app-menu__item" href="javascript:void(0);" data-toggle="modal" data-target="#businessModal"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Manage Business</span></a>
        </li>
        <li>
            <a class="app-menu__item" href="{!! $url.'/reporting' !!}"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Reports</span></a>
        </li>
        <li>
            <a class="app-menu__item" href="{!! $url.'/contacts' !!}"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Leads Contacts</span></a>
        </li>
        <li>
              <a class="app-menu__item" href="{!! $url.'/plans' !!}"><i class="app-menu__icon fa fa-window-restore"></i><span class="app-menu__label">Plans & Billing</span></a>
        </li>
        <li>
              <a class="app-menu__item" href="{!! $url.'/purchase' !!}"><i class="app-menu__icon fa fa-window-restore"></i><span class="app-menu__label">Purchase History</span></a>
        </li>
        <li>
              <a class="app-menu__item" href="{!! $url.'/setting' !!}"><i class="app-menu__icon fa fa-window-restore"></i><span class="app-menu__label">Call Settings</span></a>
        </li>

        @endcan
    </ul>
</aside>
