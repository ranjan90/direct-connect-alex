<?php
if(\Auth::guard('web')->user())
    $checkAdminLoggedInAsCustomer = \Auth::user()->checkAdminLoggedInAsCustomer();
else
    $checkAdminLoggedInAsCustomer = 'no';

 if( Auth::user()->isFirstLogin == 1 && \Auth::user()->getBusinessId() > 0 ) {   ?>
<script type="text/javascript">
localStorage.setItem('isFirstLogin',1);
</script>
<script type="text/javascript"> 
// Tooltips Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
}) 
</script>

<?php }
$loggedUserRole = '';
if(\Auth::user()->roles()->count()!=0)
{
  $loggedUserRole = \Auth::user()->roles()->first()->id;
}
?>
<script type="text/javascript">
$(document).ready(function ()
{
    var isBusinessChoosed = '<?php echo \Auth::user()->getBusinessId();?>';
    var loggedUserRole    = '<?php echo $loggedUserRole;?>';
    var isFirstLogin = localStorage.getItem('isFirstLogin');
    if( isFirstLogin == 1 && isBusinessChoosed > 0 && loggedUserRole == '2' )
    {
        $('#firstTimeLoginPopup').modal('show');
    }
    else if( isBusinessChoosed > 0 )
    {
        $('#firstTimeLoginPopup').modal('hide');
    }

    $(document).on('click', '.createCampaignPopUpBtn', function(event)
	{
        localStorage.setItem('isFirstLogin',2);
        $('#firstTimeLoginPopup').modal('hide');
        $('#createCampaignPopUp').modal('show');
    });

});

</script>

<!-- /.content-wrapper-->
<footer class="sticky-footer">
    <small>Copyright &copy; directconnect.com</small>
</footer>


    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
      <!-- Change Status Modal -->
  <div id="checkPlanStatus" class="tol-tip cstm-tol upgrate-take-tour-show" role="dialog">
      <div class="">
       <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
              <div class="">
                  <h4 class="">Inactive Plan</h4>
              </div>
              <div class="">
                  <h6>Your plan is currently inactive, please upgrade now to update your payment details and re-active your account immediately.</h6>

              </div>
          </div>
                         <div class="limit-btn d-inline-block w-100">
                                <a class="btn btn-default tour-gotit-upgrate dismiss-action" data-dismiss="modal">Okay! got it!</a>
                                <a class="btn btn-warning float-right upgrate-now-button dismiss-action" data-dismiss="modal">Upgrade now!</a>
                            </div>
  </div>
  <!-- Change Status Modal Ends -->
    <!-- Delete Modal -->
    <div id="confirm-unsub-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['id'=>'comfirm_unsubscriber_form']) !!}

                        <div class="modal-header">
                            <h4 class="modal-title">Confirm Unsubscribe</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h6>Are you sure you want to unsubscribe this plan ?</h6>
                            <div class="form-check">
                                    <label>
                                      <input type="radio" name="cancel_radio" value="0"> <span class="label-text">Cancel immediately</span>
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label>
                                      <input type="radio" name="cancel_radio" value="1"> <span class="label-text">Cancel at the end of the current period on <span id="uptoDate"></span> </span>
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label for="">Reason for cancel plan</label>
                                   {!! Form::textarea('plan_cancel_reason', null, ['id' => 'plan_cancel_reason', 'rows' => 4, 'cols' => 20,"class"=>"form-control"]) !!}

                                  </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default cancel-now" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-primary confirmUnsubscribed">Yes</button>
                        </div>
                   {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- Delete Ends -->
    <!-- Promo Code -->
    <div id="promo-confirm-unsub-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              {!! Form::open(['id'=>'promo_comfirm_unsubscriber_form']) !!}

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirm Unsubscribe</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h6>Are you sure you want to unsubscribe this promo plan ?</h6>
                    </div>
                    <div class="form-check">
                                    <label>
                                      <input type="radio" name="cancel_radio" value="0"> <span class="label-text">Cancel immediately</span>
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label>
                                      <input type="radio" name="cancel_radio" value="1"> <span class="label-text">Cancel at the end of the current period on <span id="uptoPromoDate"></span> </span>
                                    </label>
                                  </div>
                      <div class="form-check">
                       <label for="">Reason for cancel plan</label>
                    {!! Form::textarea('promo_plan_cancel_reason', null, ['id' => 'promo_plan_cancel_reason', 'rows' => 4, 'cols' => 20,"class"=>"form-control"]) !!}
                     </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default cancel-now" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary confirmUnsubscribed">Yes</button>
                    </div>

                </div>
              {!! Form::close() !!}
            </div>
       </div>
     <!-- Promo Code -->

    <!-- Unassgin business Modal -->
    <div id="confirm-Unassgin-business-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Unassgin</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to unassign this business ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Unassgin</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Unassgin business  Ends -->
    <!-- Delete Modal -->
    <div id="confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary deleteRecordBtn">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->



       <!-- Delete Modal -->
    <div id="business-confirm-delete-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to delete this record ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" id="business_yes" class="btn btn-primary businessConfirmDeleteModal">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Ends -->

    <!-- Confirm Payment Modal -->
    <div id="confirm-payment-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Subscription Change</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!--<h6>Your subscription amount is <span id="confirmAmount"></span>.<br>Are you sure you want to change your subscription plan ?</h6>-->
                    <h6>Are you sure you want to change plan from <span id="currentAmountData"></span> to <span id="confirmAmount"></span> ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary confirmBtn">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Confirm Payment Ends -->

    <!-- Confirm Payment Modal -->
    <div id="confirm-audio-file-payment-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row audio_payment_text_div">
                    <div class="col-lg-12">
                      <h6>Please confirm the payment of $25 to upload a custom call recording.  The payment will be processed on your stored credit card ending in (<span id='last_four_digit'></span>).</h6>
                            <h6 class="after_approve_file" style="display:none;">Please ignore if you want to use existing audio file.</h6>

                    </div>
                    <div class="col-lg-12">
                      <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary confirmAudioPaymentBtn">Confirm</button>
                        <br>
                        <br>
                        <a id="have_promo_link" href="javascript:void(0)">Have a promo code?</a>
                      </div>
                    </div>
                  </div>
                  <!-- Promo code div -->
                  <div class="row promo_div d-none">
                    <div class="col-lg-12">
                        <div class="form-group">
                           <input id="callAudioPromoCodeInput" type="text" class="input-lg form-control" placeholder="Have Promo Code"/>
                           <label id="callAudioPromoCode-error" class="has-error" style="display:none;width:92%" for="callAudioPromoCode">This field is required.</label>
                        </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="text-right">
                        <button id="hide_promo_div" type="button" class="btn btn-default">Cancel</button>
                        <button id="callAudioPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                      </div>
                    </div>
                  </div>
                  <!-- Promo code div ends -->
                </div>
            </div>
        </div>
    </div>
    <!-- Confirm Payment Ends -->

    <!-- Change Status Modal -->
  <div id="confirm-status-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Confirm Action</h4>
                  <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body campStatus">
                  <h6>Are you sure you want to make this campaign <span class="status"></span>?</h6>
                  <div class="text-right">
                      <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary statusRecordBtn">Save</button>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Change Status Modal Ends -->


      <!-- Permissions info Modal -->
    <div id="permissions-info-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:900px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Default role permissions</h4>
                    <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body campStatus">
                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-3">
                            <a href="JavaScript:;" class="btn btn-primary permisionInfoBtn" data-id="editorPermissionDiv">Editor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="JavaScript:;" class="btn btn-primary permisionInfoBtn" data-id="reporterPermissionDiv">Reporter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="JavaScript:;" class="btn btn-primary permisionInfoBtn" data-id="agencyPermissionDiv">Agency</a>
                        </div>
                        <div class="col-md-3">
                            <a href="JavaScript:;" class="btn btn-primary permisionInfoBtn" data-id="adminPermissionDiv">Admin</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row permissionsInfoContainers show" id="editorPermissionDiv">
                        <div class="col-md-12">
                            <?php
                            $editorDefaultPermissions = config('default_editor_permission.data');
                            $neweditorDefaultPermissions = array();
                            $editorGroupName = '';
                            foreach ($editorDefaultPermissions as $key => $value)
                            {
                                $neweditorDefaultPermissions[$value['group_name']][] = $value['title'];
                            }
                            if(!empty($neweditorDefaultPermissions))
                            {
                                foreach ($neweditorDefaultPermissions as $permission_group => $editorPermissionValue)
                                { ?>
                                    <h4>{{$permission_group}}</h4>
                                    <hr style="border-top: 1px solid #a5a5a6;margin-top:0px;margin-bottom:10px;" />
                                    <div class="row">
                                    <?php
                                        if(!empty($editorPermissionValue))
                                        {
                                            foreach ($editorPermissionValue as $editorPermissionkey => $editorPermissionval)
                                            { ?>
                                                <div class="col-md-3">
                                                    <p><?php echo ucfirst($editorPermissionval);?></p>
                                                </div>
                                    <?php   }
                                        } ?>
                                    </div>
                            <?php  }
                            } ?>
                        </div>
                    </div>
                    <div class="row permissionsInfoContainers" id="reporterPermissionDiv" style="display:none">
                        <div class="col-md-12">
                            <?php
                            $editorDefaultPermissions = config('default_reporter_permission.data');
                            $neweditorDefaultPermissions = array();
                            $editorGroupName = '';
                            foreach ($editorDefaultPermissions as $key => $value)
                            {
                                $neweditorDefaultPermissions[$value['group_name']][] = $value['title'];
                            }
                            if(!empty($neweditorDefaultPermissions))
                            {
                                foreach ($neweditorDefaultPermissions as $permission_group => $editorPermissionValue)
                                { ?>
                                    <h4>{{$permission_group}}</h4>
                                    <hr style="border-top: 1px solid #a5a5a6;margin-top:0px;margin-bottom:10px;" />
                                    <div class="row">
                                    <?php
                                        if(!empty($editorPermissionValue))
                                        {
                                            foreach ($editorPermissionValue as $editorPermissionkey => $editorPermissionval)
                                            { ?>
                                                <div class="col-md-3">
                                                    <p><?php echo ucfirst($editorPermissionval);?></p>
                                                </div>
                                    <?php   }
                                        } ?>
                                    </div>
                            <?php  }
                            } ?>
                        </div>
                    </div>
                    <div class="row permissionsInfoContainers" id="agencyPermissionDiv" style="display:none">
                        <div class="col-md-12">
                            <?php
                            $editorDefaultPermissions = config('default_agency_permission.data');
                            $neweditorDefaultPermissions = array();
                            $editorGroupName = '';
                            foreach ($editorDefaultPermissions as $key => $value)
                            {
                                $neweditorDefaultPermissions[$value['group_name']][] = $value['title'];
                            }
                            if(!empty($neweditorDefaultPermissions))
                            {
                                foreach ($neweditorDefaultPermissions as $permission_group => $editorPermissionValue)
                                { ?>
                                    <h4>{{$permission_group}}</h4>
                                    <hr style="border-top: 1px solid #a5a5a6;margin-top:0px;margin-bottom:10px;" />
                                    <div class="row">
                                    <?php
                                        if(!empty($editorPermissionValue))
                                        {
                                            foreach ($editorPermissionValue as $editorPermissionkey => $editorPermissionval)
                                            { ?>
                                                <div class="col-md-3">
                                                    <p><?php echo ucfirst($editorPermissionval);?></p>
                                                </div>
                                    <?php   }
                                        } ?>
                                    </div>
                            <?php  }
                            } ?>
                        </div>
                    </div>

                    <div class="row permissionsInfoContainers" id="adminPermissionDiv" style="display:none">
                        <div class="col-md-12">
                            <?php
                            $editorDefaultPermissions = config('default_customeradmin_permission.data');
                            $neweditorDefaultPermissions = array();
                            $editorGroupName = '';
                            foreach ($editorDefaultPermissions as $key => $value)
                            {
                                if( $key == 'schedule-report')
                                    continue;
                                $neweditorDefaultPermissions[$value['group_name']][] = $value['title'];
                            }
                            if(!empty($neweditorDefaultPermissions))
                            {
                                foreach ($neweditorDefaultPermissions as $permission_group => $editorPermissionValue)
                                { ?>
                                    <h4>{{$permission_group}}</h4>
                                    <hr style="border-top: 1px solid #a5a5a6;margin-top:0px;margin-bottom:10px;" />
                                    <div class="row">
                                    <?php
                                        if(!empty($editorPermissionValue))
                                        {
                                            foreach ($editorPermissionValue as $editorPermissionkey => $editorPermissionval)
                                            { ?>
                                                <div class="col-md-3">
                                                    <p><?php echo ucfirst($editorPermissionval);?></p>
                                                </div>
                                    <?php   }
                                        } ?>
                                    </div>
                            <?php  }
                            } ?>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Permissions info Modal Ends -->


  <!-- Make audio file active Modal -->
<div id="confirm-make-audiofile-active-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Action</h4>
                <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body campStatus">
                <h6>Are you sure you want to make this file active ?</h6>
                <div class="text-right">
                    <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary makeAudioActiveConfirmed">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Make audio file inactive Modal -->
<div id="confirm-make-audiofile-inactive-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Confirm Action</h4>
              <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body campStatus">
              <h6>Are you sure you want to upload new file ?</h6>
              <div class="text-right">
                  <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-primary makeAudioInactiveConfirmed">Yes</button>
              </div>
          </div>
      </div>
  </div>
</div>

<!-- Delete audio file  Modal -->
<div id="confirm-remove-audiofile-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Confirm Action</h4>
              <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body campStatus">
              <h6>Are you sure you want to remove this file ?</h6>
              <div class="text-right">
                  <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-primary" id="removeAudioConfirmedBtn" data-id="">Yes</button>
              </div>
          </div>
      </div>
  </div>
</div>

  <!-- Change Status Modal -->
<div id="confirm-refrest-tempalte-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Action</h4>
                <button type="button" class="close dismiss-action" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body campStatus">
                <h6>Are you sure you want to reset template <span class="status"></span>?</h6>
                <p>Your campaign will be inactive during setting up template and existing tags will be removed.</p>
                <p>You have to create tags again and if you're not creating the same tags in that case you need to update Call Script as well. </p>
                <div class="text-right">
                    <button type="button" class="btn btn-default dismiss-action" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="refreshCampaignTemplateBtn">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Change Status Modal Ends -->



<!--default_business_confirm_pop_up-->




<div id="default_business_confirm_pop_up_model_data" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Set Default Card</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to set this default card ?</h6>
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" id="business_default_yes" class="btn btn-primary businessDefaultModal">Set Default</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




  <!-- Contact us modal -->
  <div id="contact-us-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header form-group-placeholder">
                  <h4 class="modal-title">Contact Us</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="contact_Page">


                        <form id="contact-form-customer" method="post" action="{{$url.'/contact-us'}}">
                        <div class="messages"></div>

                        <div class="controls">

                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group form-group-placeholder">
                                <input id="form_name" type="text" name="name" class="form-control"  required="required" data-error="Name is required.">
                                  <div class="palceholder">
												<label for="CName">Please enter your Name</label>
												<span class="star">*</span>
								  </div>
                                <div class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group form-group-placeholder">
                                <input id="form_email" type="email" name="email" class="form-control"  required="required" data-error="Valid email is required.">
                                   <div class="palceholder">
												<label for="CEmail">Please enter your Email</label>
												<span class="star">*</span>
								  </div>
                                <div class="help-block with-errors"></div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group form-group-placeholder">
                                <input id="form_phone" type="tel" name="phone" class="form-control" >
                                <div class="palceholder">
                                    <label for="CEmail">Please enter your phone</label>
                                    <span class="star">*</span>
                                </div>
                                <div class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group form-group-placeholder">
                                <textarea id="form_message" name="message" class="form-control" rows="4" required data-error="Please,leave us a message."></textarea>
                                <div class="palceholder">
                                    <label for="CEmail">Please enter your message</label>
                                    <span class="star">*</span>
                                </div>
                                <div class="help-block with-errors"></div>
                              </div>
                            </div>
                            <div class="col-md-12 text-right">
                              <input type="submit" class="btn btn-success btn-send" value="Send message">
                            </div>
                          </div>

                        </div>

                        </form>
                </div>
              </div>
          </div>
      </div>
  </div>
<!-- Contact us modal end-->

<!-- add/update call keyword -->
<div id="addUpdateCallKeywords" style="display:none" class="hide dynamic-class">
   <div class="row">
       <div class="col-sm-12 tagp">
          <div class="form-group">
             <div class="form-group">
                <input type="text" id="keywordTitlePopover" name="title" class="form-control inputRequired">
             </div>
          </div>
          <div class="form-group">
            <input type="hidden" name="keywordId" id="keywordId" class="keywordId inputRequired" value="">
            <button type="button" data-ref="" class="btn btn-primary" id="saveCallKeywordBtn">Save</button>
            <button type="button" data-ref="" class="btn btn-default" id="deleteCallKeywordBtn">Delete</button>
          </div>
       </div>
   </div>
   <div  class="clearfix"></div>
</div>

      <script>
  var prefixUrl="<?php echo $url?>";
      var siteroot = '{{ URL('/') }}'</script>
    <!-- Bootstrap core JavaScript-->
    <!--script src="{{ asset('assets/customer/js/jquery.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script-->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/7.0.0/mark.min.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script src="{{ asset('assets/customer/js/jquery.highlight-within-textarea.js')}} "></script>
    <script type="text/javascript">
    function mentionInput() {
      $('.mention').highlightWithinTextarea({
        highlight: [
            {
                highlight: /{([^}]+)}/g,
                className: 'yellow'
            }
        ]
      });
    }
    </script>

  <!-- <script src="{{ asset('assets/customer/js/bootstrap-datepicker.js')}} "></script> -->
  <!-- <script src="{{ asset('assets/customer/js/marked.min.js')}} "></script> -->
  <script src="{{ asset('assets/customer/js/highlight.pack.js')}} "></script>
  <script src="{{ asset('assets/customer/js/bootstrap-suggest.js')}} "></script>

  <script src="{{ asset('assets/admin/js/jquery.validate.min.js')}}"></script>
  <script src="{{ asset('assets/admin/js/jquery.toast.js') }}"></script>
  <script src="{{ asset('assets/customer/js/form-validate.js')}}"></script>
  <script src="{{ asset('assets/customer/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/customer/js/jquery.timepicker.js')}}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('assets/customer/js/jquery.easing.min.js')}}"></script>
  <!-- Page level plugin JavaScript-->
  <script src="{{ asset('assets/customer/js/Chart.min.js')}}"></script>
  <script src="{{ asset('assets/customer/js/jquery.dataTables.js')}}"></script>
  <script src="{{ asset('assets/customer/js/dataTables.bootstrap4.js')}}"></script>
  <!-- Custom scripts for all pages-->
  <script src=" {{ asset('assets/customer/js/sb-admin.min.js')}} "></script>
  <!-- Custom scripts for this page-->
  <script src="{{ asset('assets/customer/js/sb-admin-datatables.min.js')}}"></script>
  <script src="{{ asset('assets/customer/js/sb-admin-charts.min.js')}}"></script>
  <script src="{{ asset('assets/customer/js/sb-admin-charts.js')}} "></script>
  <script src="{{ asset('assets/customer/js/customer.js')}} "></script>
  <script src="{{ asset('assets/customer/js/campaign_func.js')}} "></script>
   <script src="{{ asset('assets/js/localStorage.js')}} "></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
  <script src="{{ asset('assets/js/loader.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
  <script type="text/javascript">
  var checkPlanStatus="<?php echo $checkPlanStatus;?>";
  var checkAdminLoggedInAsCustomer="<?php echo $checkAdminLoggedInAsCustomer;?>";
  var storge_customer_id="<?php echo isset($_GET['customer_id']) ? $_GET['customer_id'] :''?>";
  var business_is_deleted="<?php echo isset($_GET['business_is_deleted']) ? $_GET['business_is_deleted'] :''?>";
  var reporting_tabs_li="";
  if(storge_customer_id!="")
  {
    localStorage.removeItem("business_name");
    localStorage.removeItem("business_data_id");
  }


  jQuery(document).on('click','#custom_paginate li a', function(e){
          e.preventDefault();
          var url = $(this).attr('data-url');
          var id = $(this).attr('data-id');
          $.ajax({
              url                : siteroot + '/leads-api-ajax',
              type               : 'post',
              data               : { data: url,campId: id},
              dataType           : 'html',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              },
              beforeSend : function() {
                  $(".loader_div").show();
              },
              complete   : function() {
                  $(".loader_div").hide();
              },
              success    : function(response) {
                  $('.recordsTable').html(response);
                  $(".loader_div").hide();

              }
          });

    });
    /* list_business() get list of business */
    function list_business()
          {

                  $(".business_btn_data").hide();
                  var op="";
                  var business_namescookie=[];
                  //business_namescookie=document.cookie.replace(/(?:(?:^|.*;\s*)business_name\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                   business_namescookie= localStorage.getItem('business_name');
                   $.each(JSON.parse(business_namescookie),function(k,v){
                           if(localStorage.getItem('business_data_id')==v.id)
                           {
                              $(".business_name_select").text(v.business_name);
                           }
                           else
                           {
                              // if(v.is_active==1 && localStorage.getItem('business_data_id')==null)
                              // {
                              //   localStorage.setItem('business_data_id',v.id);
                              //     $(".business_name_select").text(v.business_name);
                              // }
                           }
                          op+="<li><a class='select_business_data' href='javascript:void(0)' data-id='"+v.id+"'>"+v.business_name+"</a></li>";
                    });
                    op+="<li><a href='"+prefixUrl+"/business-profile'><i class='fa fa-plus' aria-hidden='true'></i>Add /Edit Business</a></li>";
                    //$(".business_name_select").text('Select Business Name')
                    $('.business_data').html(op);
                    $(".business_btn_data").show();
                    if(localStorage.getItem('business_data_id')===null)
                    {
                       //$('.select_business_data').trigger('click');
                    }
                    //return true;

          }
          function  setBusinessData(business_data_id)
          {
                   //if(storge_customer_id=="")
                   //{
                   //getContactData
                   if(typeof getContactData == 'function'){
                      getContactData();
                   }
                   //getDashboardCampaign
                   if(typeof getDashboardCampaign == 'function'){
                      getDashboardCampaign();
                   }
                   //getFetchReportData
                   if(typeof getFetchReportData == 'function'){
                      getFetchReportData();
                   }
                   //}
                   //getCampaign
                    if(typeof getCampaign == 'function'){
                             getCampaign(storge_customer_id);
                    }
                    //getCallSetting
                    if(typeof getCallSetting == 'function'){
                             getCallSetting();
                    }
                    //fetchPlanData
                    if(typeof fetchPlanData == 'function'){
                             fetchPlanData();
                    }
                    //fetchPurchaseData
                    if(typeof fetchPurchaseData == 'function'){
                             fetchPurchaseData();
                    }



          }
          if(localStorage.getItem('business_name')===null || business_is_deleted==1)
                {
                   if(business_is_deleted==1)
                   {
                       localStorage.setItem('business_data_id','');
                   }
                   var op="";
                    $.ajax({
                    url                : prefixUrl + '/get-business-profile-list?customer_id='+storge_customer_id,
                    type               : 'GET',
                    dataType           : 'json',
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    beforeSend : function() {

                    },
                    success    : function(response) {
                       //$(".loader_div").hide();
                       ddd=response.data;
                       localStorage.setItem("business_name",JSON.stringify(ddd));
                       if( localStorage.getItem('business_data_id') == null || localStorage.getItem('business_data_id')=="")
                       {
                           localStorage.setItem('business_data_id',response.active);
                       }
                       list_business();
                       setBusinessData(response.active);
                    },
                    complete   : function() {
                    }
                  });

              }
              else
              {
                  //console.log("sdf");
                  list_business();
              }

        $(document).on('click','.select_business_data',function(k,v)
        {
              var businessId   = $(this).attr('data-id');
              var businessName = $(this).text();
              if( businessId > 0 )
              {
                  $.ajax({
                      url                : prefixUrl + '/switch-business/'+businessId,
                      type               : 'GET',
                      dataType           : 'json',
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      },
                      beforeSend : function() {
                          $(".loader_div").show();
                      },
                      success    : function(response)
                      {
                          $(".loader_div").hide();
                          if( response.success ) {
                              localStorage.setItem('business_data_id',businessId);
                              $(".business_name_select").text(businessName);
                              setBusinessData(businessId);
                              window.location.href=response.url;
                          } else {
                              setBusinessData(businessId);
                              tost(response.message,"Error",4000);
                          }
                     },
                     complete   : function(){}
                  });
              }
        });


    if (paginationClass === undefined || paginationClass === null) {
       // do something
       var paginationClass='recordsTable';
    }
    /* tost*/
    function tost(message,type, delay = null) {
            $.toast().reset('all');
            return $.toast({
              heading             : type,
              text                : message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : type.toLowerCase(),
              hideAfter           : delay || 5000,
              position            : 'top-right'
            });
    }

    $(document).on('click','.pagination li a',function(e)
    {
        e.preventDefault();
        var link = $(this).attr("href");
        $.ajax({
            url:link,
            beforeSend  : function () {
                $(".loader_div").show();
            },
            complete: function () {
                $(".loader_div").hide();
            },
        }).done(function(data){
            if(data.paginationClassShow!== undefined && data.paginationClassShow!==null)
            {
                          $("."+data.paginationClassShow).html(data.result);

            }
            else{
                         $("."+paginationClass).html(data);

            }

        });
    });
    </script>
  <script>
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
      var selectedBusiness = '<?php echo \Auth::user()->getBusinessId();?>';
        if( localStorage.getItem('business_data_id') == null )
        {
            if( selectedBusiness > 0 )
                localStorage.setItem('business_data_id',selectedBusiness);
        }
  });


</script>
@if(strpos(url()->current(), 'edit-campaign') )
<script type="text/javascript">
  $(document).ready(function() {
   var navListItems = $('div.setup-panel-3 div a.waves-effect'),
    allWells = $('.setup-content-3');
    navListItems.click(function(e) {
        // console.log(';asdfasdfasdfas');
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-info').addClass('btn-pink');
            $item.addClass('btn-info');
            allWells.hide();
            $target.show();
            $('.contact-list-media-active').removeClass('contact-list-media-active');
            $item.parent().addClass('contact-list-media-active');
            $target.find('input:eq(0)').focus();
        }
    });

  })
</script>
@endif
<script>
$(function() {
              $(document).find('.mention').suggest('@', {
                data: function(q) {
                  if (q) {
                  return  $.ajax({
                        dataType: "json",
                        url: siteroot+'/gettags',
                        data:  { q: q , campId : $('input[name="campId"]').val() },
                        success: function(response){
                          $.toast().reset('all');
                          if (response.length === 0) {
                            // $.toast({
                            //     heading             : 'Info',
                            //     text                : 'Tag Not Found.<a href="javascript:void(0)" class="openModal" data-target="#add-new-tag">Click to add</a>',
                            //     loader              : true,
                            //     loaderBg            : '#fff',
                            //     showHideTransition  : 'fade',
                            //     icon                : 'info',
                            //   hideAfter : false,
                            //     position            : 'top-right'
                            // });
                          }
                          return response;
                        }
                      });
                     // var item = $.getJSON<?php if(isset($campData['step'] )  && $campData['step'] ==  4) {?>contact-list-media-active<?php } ?>(siteroot+'/gettags', { q: q });
                     // console.log(item);
                     // if (item.length === 0) {
                     // }
                     // return item;
                  }
                },

                map: function(user) {
                  return {
                    value: $.trim('{'+user.tagName+'}'),
                    text: $.trim(user.tagName)
                  }
                }
              })





  jQuery(document).on('click','.openModal',function(e) {e.preventDefault();var ref = $(this).attr('data-target'); if(ref == '#add-new-tag'){ $('#contact_form').find('input[name="name"]').val($(document).find('#customerSearch').val()) } $(ref).modal('show'); })
  // jQuery(document).on('click','.dropdown .dropdown-menu li a',function(e) {e.preventDefault();
  //   setTimeout(function () {mentionInput();},200);
  // })


 mentionInput()

  })
</script>
<script>


</script>
<?php
$data = Session()->has('error');
if($data){
  $response = json_decode(Session()->get('error'));
  if(isset($response->error_message) && !empty($response->error_message))
  {
    ?>
    <script>
      tost("<?php echo $response->error_message; ?>","Info",5000);
    </script>
  <?php
  }
}
Session()->forget('error');
?>

</div>
</div>
</div>



<script>
    function ajaxPageCallBackCreateCampaignPopup(response)
    {
        if( response.success )
        {
            document.getElementById("create-camp_form_popup").reset();
            $('#createCampaignPopUp').modal('hide');
            $('#leadModal').modal('show');
            $('#createCampaignContinueBtn').attr('data-link',response.urll);
        }

    }
    $(document).ready(function()
    {
        $('.modal').on('hidden.bs.modal', function(){
            if( $(this).attr('id') == 'firstTimeLoginPopup' )
                localStorage.setItem('isFirstLogin',2);
            $('.modal-backdrop').remove();
        })

        $("#create-camp_form_popup").validate({
            errorClass   : "has-error",
            highlight    : function(element, errorClass) {
                $(element).parents('.form-group').addClass(errorClass);
            },
            unhighlight  : function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass(errorClass);
            },
            rules:
            {
                campaignsTitle:{
                    required: true,
                    noSpace: true,
                }
            },
            messages:{
            },
            submitHandler: function (form)
            {
                $('#create-camp_form_popup').append('<input type="hidden" name="submittype" value="popup" />');
                formSubmit(form);
            }
        });

        $(document).on('click', '#createCampaignContinueBtn', function(event)
    	{
            var url = $(this).attr('data-link');
            console.log(url);
            if( url == '' || url == undefined )
            {
                tost("Something went wrong. Please try again.","Error",2000);
            }
            else
            {
                $('#leadModal').modal('hide');
                window.location.href = url;
            }
        });

  $('.app-sidebar__toggle').on('click', function () {

      $('.main_wrapper').toggleClass('active')
  });
  });
    $(function() {
          $( ".datepicker" ).datepicker();
      });
      $(document).on('change', '#date1', function(){
          var date1 = $(this).val();
          $("#date2").datepicker("option","minDate",date1);
          $("#date1-error").hide();
      });

       $(document).on('change', '#date2', function(){
          $("#date2-error").hide();
      });
      $(document).on('change', '#fromDate', function(){
          var date1 = $(this).val();
          $("#toDate").datepicker("option","minDate",date1);
      });

    </script>



    <!--Tour Modals-->
        <div class="modal-bg">
            <div class="modal" id="firstTimeLoginPopup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                        <div class="modal-body">
                            <div class="img-modal text-center">
                                <img src="{{asset('assets/customer/img/welco.png')}}">
                            </div>
                            <h3>Welcome !</h3>
                            <p>You can get clear understanding of Direct Connect's features by taking our short 11 step tour.</p>
                            <p>If you're busy, you can set up your first campaign right away!</p>
                            <p>You can take the tour at any time by clicking the inforamation icon in the top right hand corner of the dashboard.</p>
                        </div>
                        <div class="modal-btn">
                            <ul>
                                <li><a href="javascript:;" class="btn-default btn createCampaignPopUpBtn">create campaign</a></li>
                                <li class="text-right"><a href="javascript:;" class="btn-warning btn start-take-tour">Take the tour</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!--lead sent-->
        <div class="modal-bg">
            <div class="modal" id="leadModal" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                        <div class="modal-body mt-3">
                            <div class="img-modal text-center">
                                <img src="{{asset('assets/customer/img/lead-sent.png')}}">
                            </div>
                            <h3>Sample lead sent.</h3>
                            <p>We've assigned a custom email address to your campaign.<br>Select containue to configure your first campaign.</p>
                            <div class="modal-btn text-center">
                                <a data-link="" id="createCampaignContinueBtn" class="btn-default btn">continue</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--end of lead sent-->

    <!--Tour-1-modal-->
    <div class="modal-bg TOur">
    <div class="modal" id="TOurModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
          </div>
          <div class="modal-body">
            <div class="img-modal text-center">
                <img src="{{asset('assets/customer/img/welco.png')}}">
              </div>
              <h2 class="mt-4">Hi Tim!</h2>
              <p>See why Direct Connect's powerful features make it an essential solution for organisations of all sizes</p>
              <p>We are going to give you a quick tour of your dashboard to show you how to get the very best out of Direct Connect.</p>
              <p>If you wish to skip the tour at any point, just click cancel in the top right hand corner. You can skip ahead at any time. Enjoy!</p>
              <ul class="step mt-4 mb-0"><li>Step</li><li><a class="circle-cstm">1</a></li><li>of 12</li></ul>
          </div>
             <div class="modal-btn text-center">
                 <a class="btn-warning btn">Okay! Got it!</a>
            </div>
        </div>
      </div>
    </div>
    </div>
    <!--End-Tour-1-modal-->

    <!--Lets get started-->
    <div class="modal-bg TOur Get-start">
    <div class="modal" id="Get-start">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
          </div>
          <div class="modal-body">
            <div class="img-modal text-center">
                <img src="{{asset('assets/customer/img/face-pop.png')}}">
              </div>
              <h2 class="mt-4">Let's get Started!</h2>
              <p>Congratulations you are all ready to go.</p>
              <p>Direct Connect will enable you to repsond within minutes, putting you in the driver's seat.</p>
              <p>We've done the hard work to make tagging and scripting a breeze. Time to set up your first campaign.</p>
              <ul class="step mt-4 mb-0"><li>Step</li><li><a class="circle-cstm">12</a></li><li>of 12</li></ul>
              <div class="modal-btn text-center">
                 <a class="btn-warning btn">Create Campaign</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <!--End-Lets get started-->
    <!--End Tour Modals-->

    <!-- Create campaign popup modal-->
    <div class="modal-bg">
        <div class="modal"  id="createCampaignPopUp">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
                    </div>
                    <div class="modal-body text-left">
                        <h4>Welcome {{ucfirst(\Auth::user()->firstName)}}!</h4>
                        <p class="mb-3">Creating Your first campaign is easy! Simply choose a Campaign Name,then select a setup method.</p>
                        {{Form::open(array('id'=>'create-camp_form_popup','url'=>$url.'/create-new-campaign'))}}
                            <div class="form-group">
                                <input type="text" class="form-control" name="campaignsTitle" placeholder="Campaign Name*">
                            </div>
                            <div class="setup">
                                <h2 class="mt-4 mb-3">Create a unique campaign for each lead source, including website forms, social media leads, landing pages, etc.</h2>
                                <div class="active-head head">
                                    <img src="{{asset('assets/customer/img/modal-mail.png')}}"><h3>EMAIL</h3><span> (Recommended for a FAST start!)</span>
                                </div>
                                <div class="radio-sec">
                                    <div class="radio">
                                        <input id="radio-1" value="1" name="campaignType" type="radio" checked>
                                        <label for="radio-1" class="radio-label"></label>
                                    </div>
                                    <div class="content-set"><p>This option will generate a unique email address that you can add as a lead recipient to your lead source. When an email is received a call is triggered within seconds.</p>
                                        <p class="mb-0">No coding or API integration required!</p>
                                    </div>
                                </div>
                                <div class="head mt-4">
                                    <img src="{{asset('assets/customer/img/api-icon.png')}}"><h3>API</h3><span></span>
                                </div>
                                <div class="radio-sec">
                                    <div class="radio">
                                        <input id="radio-API" value="2" name="campaignType" type="radio">
                                        <label for="radio-API" class="radio-label"></label>
                                    </div>
                                    <div class="content-set"><p>Trigger Direct Connect via API.</p>
                                        <p class="mb-0">This option is ideal for advanced users who want to trigger calls from a CRM or other software. Calls are triggered within seconds.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-btn mt-4 p-0">
                                <!-- <a class="btn-warning btn float-left">Create Campaign</a> -->
                                <button type="submit" class="btn-warning btn float-left" >Create Campaign</button>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Create campaign popup modal-->

</body> 

<script>
    $(document).ready(function(){

			$('.palceholder').click(function() {
			  $(this).siblings('input').focus();
			});
			$('.form-control').keydown(function() {
			  $(this).siblings('.palceholder').hide();
			});


});
 $( document ).ajaxComplete(function() {
     var upgratenow_Data=localStorage.getItem('popData');
     //if(checkPlanStatus==1 && upgratenow_Data!=1)
     if(checkPlanStatus==1 && checkAdminLoggedInAsCustomer == 'no')
  {
      var isNewBusiness = localStorage.getItem('isNewBusiness');
      if( isNewBusiness != 'yes' )
      {
          $("#checkPlanStatus").modal('show');
          localStorage.setItem("popData", 0);
          localStorage.setItem("isNewBusiness", '');
      }
  }
 });
 
</script>
@if(\Route::current()->getName() == 'plans'))
<script>
 </script>
  @else
  <script>
     localStorage.setItem("popData", 0);
       $(document).on('click','.upgrate-now-button',function(){
          localStorage.setItem("upgratenow", 1);
          localStorage.setItem("popData", 1);
          window.location.replace(prefixUrl+'/plans');
      });
 </script>
@endif
<script>
                        $(document).ready(function () {
    $("button").tooltip({container:'body'});
});
                        </script>
</html>
