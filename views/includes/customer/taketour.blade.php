@php
$getBusinessId= \Auth::user()->getBusinessId();
$data_user_id =\DB::table('business_users')->select('user_id')->where('business_id',$getBusinessId)->where('is_owner','1');
$user_id= '';
if( $data_user_id->count() > 0)
{
    $user_id=$data_user_id->first()->user_id;
}

if(!empty($user_id))
{
 $user_status= DB::table('users')->where('id',$user_id )->first();
}
else
{
    $user_status="";
}
@endphp
<!--Tour-1-modal-->
<div class="modal-bg TOur">
<div class="modal take-tour-1" id="TOurModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
      </div>
      <div class="modal-body">
        <div class="img-modal text-center">
            <img src="{{asset('assets/customer/img/welco.png')}}">
          </div>
          <h2>Welcome Tour</h2>
          <h2 class="mt-2">Hi {{ucfirst(\Auth::user()->firstName)}}!</h2>
          <p>Direct Connect is an easy-to-use tool designed to help you respond to leads faster than ever.</p><p> Most businesses take hours (and sometimes days!) to respond to an online lead or enquiry.</p>
          <p>But not you, you’ve now got Direct Connect as your secret weapon to help convert your online leads in to inbound phone calls –within seconds!</p>
          <p>We are going to give you a quick tour of how you can use your dashboard to make the best use of Direct Connect.</p>
          <p>Just click cancel at any point to skip the tour and jump straight into action. Enjoy!</p>
          <ul class="step mt-4 mb-0"><li>Step</li><li><a class="circle-cstm">1</a></li><li>of 11</li></ul>
      </div>
         <div class="modal-btn text-center">
             <a class="btn-warning btn tour-gotit" data-id="1">Okay! Got it!</a>
        </div>
    </div>
  </div>
</div>
</div>
<!--End-Tour-1-modal-->
<!--                     2nd   tour tip-->
                        <div class="tol-tip dash-pop d-none take-tour-2">
                            <div class="text-right"><button type="button" class="close tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Dashboard Menu</h4>
                                <p>Our dashboard menu is where everything lives.</p>
                                <p>Here, you will find all the functions you need to start and manage your projects.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">2</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="2">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--End of 2nd step-->

<!--                     3rd   tour tip-->
                        <div class="tol-tip dash-pop step-3-pop d-none take-tour-3">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Campaigns</h4>
                                <p>A Campaign is an individual lead source that triggers a Direct Connect call.</p>
                                <p>In this section you will create and manage your campaigns.</p><p> Choose between starting a new campaign or edit and existing campaign to update the call scripting or call hours.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">3</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="3">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       3rd   tour tip-->
<!--              End       4th   tour tip-->
                        <div class="tol-tip dash-pop step-4-pop d-none take-tour-4 ">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Reports</h4>
                                <p>Visit this section to track the performance of each Campaign. </p><p>In this section you can view reporting on call numbers, answer rates, and view other account performance data that will help you improve how you receive and handle online enquiries.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">4</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="4">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       4th   tour tip-->
<!--              End       5th   tour tip-->
                        <div class="tol-tip dash-pop step-5-pop d-none take-tour-5">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Company Contacts</h4>
                                <p>Company Contacts are your team members who are able to receive calls when a new online lead or enquiry is submitted.</p><p> In this section you can add or remove team members based on their availability.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">5</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="5">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       5th   tour tip-->
<!--              End       6th   tour tip-->
                        <div class="tol-tip dash-pop step-6-pop d-none take-tour-6">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Users</h4>
                                <p>In this section you can manage user access levels.</p><p> Choose who can edit or view your account, or set custom access levels for different team members based on what permission you want to give them.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">6</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="6">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       6th   tour tip-->

<!--              End       7th   tour tip-->
                        <div class="tol-tip dash-pop step-7-pop d-none take-tour-7">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Plans & Billing</h4>
                                <p>In this section is details of your current Plan/Subscription details.</p><p> Choose to switch plans, add additional Credits or update your payment details.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">7</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="7">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       7th   tour tip-->


<!--              End       8th   tour tip-->
                        <div class="tol-tip dash-pop step-8-pop  d-none take-tour-8">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Purchase History</h4>
                                <p>Review the purchase history or your account, including any additional purchases for premium services.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">8</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="8">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       8th   tour tip-->

<!--              End      9th   tour tip-->
                        <div class="tol-tip dash-pop step-9-pop d-none take-tour-9">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Call Settings</h4>
                                <p>Visit this section to adjust some key features for your account such as: Enable Call Recordings, Turn on/off Call Summary Emails, and more.</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">9</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="9">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       9th   tour tip-->


<!--              End      10th   tour tip-->
                        <div class="tol-tip dash-pop step-10-pop d-none take-tour-10">
                            <div class="text-right"><button type="button" class="close  tour-close" >cancel <i class="fa fa-times" aria-hidden="true"></i></button></div>
                            <div class="modal-btn text-left">
                                <h4>Need More Help?</h4>
                                <p>If you still have questions, fear not! Click the “Info” icon at any time to start this tour again or click our “Support” button to contact our team.</p>
                                <p>Our team is also available via live chat, we’ll be happy to help you with anything you need!</p>
                                <div class="content"><ul class="step"><li>Step</li><li><a class="circle-cstm">10</a></li><li>of 11</li></ul>
                                    <div class="text-right"><a class="btn-warning btn tour-gotit" data-id="10">Okay! Got it!</a></div></div>
                            </div>
                        </div>
<!--              End       10th   tour tip-->

   <!--  Breadcrumbs-->
   <!--Lets get started-->
   <div class="modal-bg TOur Get-start take-tour-11">
   <div class="modal" id="Get-start">
     <div class="modal-dialog">
       <div class="modal-content">

         <!-- Modal Header -->
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">cancel <i class="fa fa-times" aria-hidden="true"></i></button>
         </div>
         <div class="modal-body">
           <div class="img-modal text-center">
               <img src="{{asset('assets/customer/img/face-pop.png')}}">
             </div>
             <h2 class="mt-4">Let's get Started!</h2>
             <p>Direct Connect allows you to convert online leads into inbound phone calls, which means your team can be on the phone with a potential customer within seconds –not hours!</p>
             <p>Your competitors might think this gives you an unfair advantage... And let’s be honest, it does! <i class="fas fa-smile"></i></p>
             <p>Direct Connect is your secret weapon for delivering market leading response times, and helping you maximise the opportunity to convert a lead in to an appointment or a sale.</p>
             <p>Now then, let’s get your first Campaign up and running!</p>
             <ul class="step mt-4 mb-0"><li>Step</li><li><a class="circle-cstm">11</a></li><li>of 11</li></ul>
             @can(config('permissions.data.add-campaign.name'))
             <?php
              $main_url=$newUrl.'/add-new-campaign';
             ?>
             <div class="modal-btn text-center">
               @if( isset($user_status->status) && $user_status->status == 1)
                <a href="{!! $main_url !!}" class="btn-warning btn tour-gotit" data-id="12">Create Campaign</a>
                @else
                <a class="btn-warning btn tour-gotit" data-id="12">Finish</a>
                @endif
             </div>
             @endcan
         </div>
       </div>
     </div>
   </div>
   </div>
   <!--End-Lets get started-->
<script>
$(document).on('click','.start-take-tour',function(){
    $('#firstTimeLoginPopup').modal('hide');
    $('#TOurModal').modal('show');
    $('div.dash-pop').addClass('d-none');
    $('.upgrate-take-tour-show').addClass('d-none');
})
$(document).on('click',".tour-gotit",function(){
  var a=$(this);
  var id = a.data('id');
  console.log(id);
  let classname="d-none";
  if(id == 1){
    $('#TOurModal').modal('hide');
    $('.take-tour-'+(id+1)).removeClass(classname);
    $('li.nav-item[data-id="'+(id+1)+'"]').addClass('tour-side-active');
  }else if(id ==11){
    $('.take-tour-'+id).addClass(classname);
    $('#Get-start').modal('show');
    $('.take-tour-campaign').css({'border-color':'#f7a402','background':'#f7a402'});
  }else if(id==12){
    $('#Get-start').modal('hide');
  }else{
    $('.take-tour-'+id).addClass(classname);
    $('.take-tour-'+(id+1)).removeClass(classname);
    $('li.nav-item[data-id="'+id+'"]').removeClass('tour-side-active');
    $('li.nav-item[data-id="'+(id+1)+'"]').addClass('tour-side-active');
    if(id==9){
      $('.take-tour-help-2').show();
      $('.take-tour-help-1').parent().addClass('border-r-tour');
      $('.take-tour-help-1').parent().removeClass('border-r');
      $('.take-tour-help-1').hide();
    }
    if(id==10){
      $('.take-tour-'+id).addClass(classname);
      $('#Get-start').modal('show');
      $('.take-tour-campaign').css({'border-color':'#f7a402','background':'#f7a402'});
    }
  }
})
$(document).on('click','.tour-close',function(){
  $('div.dash-pop').addClass('d-none');
})
</script>
