<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Direct connect</title>
<meta name="_token" content="<?php echo csrf_token() ?>">

<link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/gif" sizes="16x16">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> -->
<style media="screen">
.dropdown, .dropup {position: relative;}.dropdown {position: relative;}.open > .dropdown-menu {display: block;}.dropdown-menu {position: absolute;top: 100%;left: 0;z-index: 1000;display: none;float: left;min-width: 10rem;padding: .5rem 0;margin: .125rem 0 0;font-size: 1rem;color: #212529;text-align: left;list-style: none;background-color: #fff;background-clip: padding-box;border: 1px solid rgba(0,0,0,.15);border-radius: .25rem;}
.dropdown-menu {font-size: 1rem;color: #212529;text-align: left;list-style: none;}
</style>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css"> -->
<!-- Bootstrap core CSS-->
<!--link href="{{ asset('assets/customer/css/bootstrap.min.css') }}" rel="stylesheet"-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">




<link href="{{ asset('assets/customer/css/jquery.highlight-within-textarea.css') }}" rel="stylesheet">
<!-- Custom fonts for this template-->
<!-- Page level plugin CSS-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link href="{{ asset('assets/customer/css/dataTables.boostrap4.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('assets/customer/css/bootstrap-datepicker.css') }}" rel="stylesheet"> -->
<!-- Custom styles for this template-->
    <!--    tour css custom-->
<link href="{{ asset('assets/customer/css/dashboard.css') }}" rel="stylesheet">
<!--    tour css custom-->

<link href="{{ asset('assets/customer/css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('assets/admin/css/toaster.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/customer/css/jquery.timepicker.css') }}" rel="stylesheet" type="text/css">
<!--link href="{{ asset('assets/customer/css/sb-admin.css') }}" rel="stylesheet"-->
<link href="{{ asset('assets/customer/css/jquery-ui.css') }}" rel="stylesheet">
<link href="{{ asset('assets/customer/css/date-picker.css') }}" rel="stylesheet">
<!-- TIME PICKER CSS LINK -->
<link href="{{ asset('assets/customer/css/jquery.datetimepicker.css') }}" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="texr/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('assets/customer/js/jquery-ui-1.10.4.custom.min.js') }}"></script>
<link href="{{ asset('assets/customer/css/dcmain.css') }}" rel="stylesheet">
<?php
$roleNamejs = '';
if(Auth::check()) {
  $roleNamejs = Auth::User()->roles()->first()->name;
  if( $roleNamejs == 'business-admin' )
      $roleNamejs = 'admin';
} else {
   $roleNamejs = 'customer';
}
?>
<script type="text/javascript">
var roleNameJs = '<?php echo $roleNamejs;?>';
</script>
<!-- Hotjar Tracking Code for https://members.directconnect.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1701746,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
</head>
