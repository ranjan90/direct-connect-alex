<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <div class="loader_div" style="display:none">
    <div class="spinner"></div>
  </div>
  <div class="email_loader_div" style="display:none">
    <div class="spinner"></div>
  </div>
  <!-- Delete Modal -->
  <div id="confirm-unsadsafsdfub-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Confirm Unsubscribe</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <h6>Are you sure you want to unsubscribe this plan ?</h6>
                  <div class="form-check">
                          <label>
                            <input type="radio" name="radio" value="0"> <span class="label-text">Cancel immediately</span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label>
                            <input type="radio" name="radio" checked value="1"> <span class="label-text">Cancel at the end of the current period on <span id="uptoDate"></span> </span>
                          </label>
                        </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary cancel-now" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary confirmUnsubscribed">Yes</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Delete Ends -->
