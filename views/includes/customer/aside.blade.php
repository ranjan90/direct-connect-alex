<div class="main_wrapper">
<header id="header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <div class="header_left">
    <a class="logo" href="#">
      <img class="logoBlack" src="<?php echo URL :: to(config('app.logo')); ?>">
    </a>
    <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar">
      <i class="fa fa-bars"></i>
    </a>
</div>


 <div class="header_right">
    <?php
    if(\Auth()->user()->isCustomer() || \Auth()->user()->isReporter() || \Auth()->user()->isEditior() || \Auth()->user()->isAgency() || \Auth()->user()->isBusinessAdmin() )
    {
        if(Auth::user()->getBusinessId())
        { ?>
            <!-- <a href="#" class="btn btn-primary support-btn1"> <img src="{{asset('assets/customer/img/support.png')}}">Support</a> -->
            <a class="btn btn-primary support-btn1" href="javascript:void(0);" data-toggle="modal" data-target="#contact-us-modal" > <img src="{{asset('assets/customer/img/support.png')}}">Support</a>
            @if(\Request::route()->getName()!='business-no-access' )
                <div class="btn-group connon_toyota">
                   <button type="button" class="btn btn-default dropdown-toggle business_btn_data" data-toggle="dropdown">
                         <span class="business_name_select"></span>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu business_data" role="menu">
                  </ul>
                </div>
            @endif
<?php   }
    } else {  ?>
    @if(\Request::route()->getName()!='business-no-access' )
        <div class="btn-group connon_toyota">
           <button type="button" class="btn btn-default dropdown-toggle business_btn_data" data-toggle="dropdown">
                 <span class="business_name_select"></span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu business_data" role="menu">
          </ul>
        </div>
    @endif
<?php } ?>
<?php
$remaining_mintsheader = 0;
$totalActiveCampaigns  = 0;
if(Auth::user()->getBusinessId() > 0)
{
    $user_data_check="";
    $BusinessOwner= App\Model\BusinessProfile::find(Auth::user()->getBusinessId())->get_users()->wherePivot('is_owner','1');
    if($BusinessOwner->where('register_step',3)->count()!=0)
    {
              $user_data_check= $BusinessOwner->first();
    }
    if(isset($user_data_check->is_group) && $user_data_check->is_group==1)
    {
            if(\App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->count()!=0)
            { 
                $remaining_mintsheader= \App\Model\UserGroupAccount::where('user_id',$user_data_check->id)->where('is_active',1)->select('balanceAmount')->first()->balanceAmount;
            }
    }
    else
    {

            $uPlan = App\Model\UserPlan::where('business_id',Auth::user()->getBusinessId());
            if($uPlan->count()!=0)
            {
                $remaining_mintsheader =$uPlan->first()->balanceAmount;
            }

   }  
     
    $totalActiveCampaigns = DB::select('select COUNT(title) as toalcount from campaigns where status = 1 and business_id='.Auth::user()->getBusinessId());
    if( !empty($totalActiveCampaigns) )
        $totalActiveCampaigns = $totalActiveCampaigns[0]->toalcount;

}
?>
    <div class="header_actions active-camp">
      <ul>
          <?php if(Auth::user()->getBusinessId()){?>
          <li class="nav-item cam"> Active Campaigns(<?php echo $totalActiveCampaigns;?>)</li>
          <li class="nav-item cam"><span><?php echo $remaining_mintsheader;?> Credits</span>
            <?php 
            if(isset($user_data_check->is_group) && $user_data_check->is_group==1)
            {
            }
            else
            { 
            ?>
              @can(config('permissions.data.switch-plan.name'))
                <a href="{!! $url.'/plans' !!}">
                    Buy
                </a>
                @endcan
               <?php
             }
             ?>
          </li>
      <?php } ?>
        <li class="nav-item">
          <a class="nav-link mr-lg-2" >
          <span class="profile-text">Hi {{ucfirst(\Auth::user()->firstName)}}</span>
              <img src="{{asset('assets/customer/img/user-dash.png')}}">
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link mr-lg-2 border-r" href="JavaScript:;">
           <img class="take-tour-help-1" src="{{asset('assets/customer/img/info-dash-min.png')}}">
           <img class="take-tour-help-2" style="display:none;"src="{{asset('assets/customer/img/u.png')}}">
          </a>
            <ul class="child">
                <?php if(\Auth()->user()->isCustomer()) { ?>
                    <li class="start-take-tour"><a style="cursor:pointer" >Welcome Tour</a></li>
                <?php } ?>
                <li><a href="{!! $url.'/profile' !!}">Profile</a></li>
                <li><a href="{{ route('logout') }}">Logout</a></li></ul>
        </li>
        </ul>
    </div>
                </div>
  </header>
  <?php
      $showSideBar = true;
      if(\Auth()->user()->isCustomer() || \Auth()->user()->isReporter() || \Auth()->user()->isEditior() || \Auth()->user()->isAgency() || \Auth()->user()->isBusinessAdmin())
      {
          if(!Auth::user()->getBusinessId())
          {
              $showSideBar = false;
          }
      } ?>
      <?php if( $showSideBar == true ) {?>
        <div id="sidebar">
            @if(\Auth()->user()->isCustomer() || \Auth()->user()->isReporter() || \Auth()->user()->isEditior() || \Auth()->user()->isAgency() || \Auth()->user()->isBusinessAdmin())
                <ul>
                    <li class="nav-item" data-id="2">
                        <a class="nav-link" href="{!! $url.'/dashboard' !!}"><span class="nav-link-text dashboard_icon">Dashboard</span></a>
                    </l i>
                    <li class="nav-item" data-id="3">
                        <a class="nav-link" href="{!! $url.'/campaigns' !!}">
                            <span class="nav-link-text campaigns_icon">Campaigns</span>
                        </a>
                    </li>
                    @can(config('permissions.data.reporting-list.name'))
                    <li class="nav-item" data-id="4">
                        <a class="nav-link" href="{!! $url.'/reporting' !!}"><span class="nav-link-text reports_icon">Reports</span></a>
                    </li>
                    @endcan
                    @can(config('permissions.data.view-contact.name'))
                    <li class="nav-item" data-id="5">
                        <a class="nav-link" href="{!! $url.'/contacts' !!}"><span class="nav-link-text leads_icon">Company Contacts</span></a>
                    </li>
                    @endcan
                    @can(config('permissions.data.user-listing.name'))
                    <li class="nav-item" id="usermanagementLi" data-id="6">
                        <a class="nav-link" href="{!! $url.'/user-management' !!}"><span class="nav-link-text leads_icon">Users</span></a>
                    </li>
                    @endcan
                    @if(\Auth()->user()->can('plans-list') || \Auth()->user()->can('credit-recharge') || \Auth()->user()->can('cards'))
                    <li class="nav-item" data-id="7">
                        <a class="nav-link" href="{!! $url.'/plans' !!}"><span class="nav-link-text plans_icon">Plans & Billing</span></a>
                    </li>
                    @endif
                    <!-- @can(config('permissions.data.view-campaign.name')) -->
                    @if(\Auth()->user()->can('plan-history-list') || \Auth()->user()->can('balance-history-list'))
                    <li class="nav-item" data-id="8">
                        <a class="nav-link" href="{!! $url.'/purchase' !!}"><span class="nav-link-text purchase_icon">Purchase History</span></a>
                    </li>
                    @endif
                    <!-- @endcan -->
                    @can(config('permissions.data.call-settings.name'))
                    <li class="nav-item" data-id="9">
                        <a class="nav-link" href="{!! $url.'/setting' !!}"><span class="nav-link-text settings_icon">Call Settings</span></a>
                    </li>
                    @endcan
                    @can(config('permissions.data.report-scheduling.name'))
                    <!--<li class="nav-item" data-id="9">
                        <a class="nav-link" href="{!! $url.'/schedule-report' !!}"><span class="nav-link-text">Schedule Report</span></a>
                    </li>-->
                    @endcan
                    @can(config('permissions.data.log-list.name'))
                        <li class="nav-item" data-id="9">
                            <a class="nav-link" href="{!! $url.'/logs' !!}"><span class="nav-link-text phones_icon">Logs</span></a>
                        </li>
                    @endcan
                </ul>
             @else
                <ul id="admin_sidebar">
                    @if(\Auth()->user()->isAdministrator())
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/home' !!}"><span class="nav-link-text dashboard_icon">Dashboard</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/regions' !!}"><span class="nav-link-text dashboard_icon">Regions</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/plans' !!}"><span class="nav-link-text plans_icon">Plans</span></a>
                        </li>
                    @endif
                    @if(\Auth()->user()->isAdministrator() )
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/customers' !!}"><span class="nav-link-text leads_icon">Customers</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/reporting' !!}"><span class="nav-link-text plans_icon">Reporting</span></a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/user-management' !!}"><span class="nav-link-text leads_icon">User Management</span></a>
                        </li>-->
                    @endif
                    @if(\Auth()->user()->isAdministrator())
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/email-templates' !!}"><span class="nav-link-text plans_icon">Email Templates</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/call-setting' !!}"><span class="nav-link-text settings_icon">Call Settings</span></a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/role-management' !!}"><span class="nav-link-text settings_icon">Role Management</span></a>
                        </li>-->
                        <li class="nav-item">
                            <a class="nav-link" href="{!! $url.'/manage-top-up' !!}"><span class="nav-link-text leads_icon">Manage Top Up</span></a>
                        </li>
                    @endif
                </ul>
            @endif
        </div>
    <?php } ?>

  @php
  $newUrl = $url;
  $url = '';
  $search = 0;
  $placeholder = '';
  switch(Request::url())
  {
      case url($url.'/dashboard'):
        $url = url($url.'/campaigns');
        $search = 1;
        $placeholder = 'Search for campaign names or email address';
      break;
      case url($url.'/campaigns'):
        $url = url($url.'/campaigns');
        $search = 1;
        $placeholder = 'Search for campaign names or email address';
      break;
      case url($url.'/contacts'):
        $url = url($url.'/contacts');
        $search = 1;
        $placeholder = 'Search for contact names or phone number';
      break;
  }
  $qSearch = 0;
  if(Request::segment(2)=='campaign-leads'){
    $qSearch = 1;
    $url= Request::url();
    $placeholder = 'Search for leads id or lead name';
  }
  @endphp

<?php
$hideSideBarStyle = '';
if( $showSideBar == false ) {
    $hideSideBarStyle = 'margin-left:0px';
}
?>

    @include('includes.customer.taketour')

<div id="wrapper" style="<?php echo $hideSideBarStyle;?>">
