<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        @if(\Auth()->user()->isAdministrator())
         @if(\Auth()->user()->is_technical!=1)
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/home') active @endif" href="{!! $url.'/home' !!}"><img class="mr-3 img-responsive" src="{{ asset('assets/admin/images/home.png') }}" alt="" /><span class="app-menu__label">Dashboard</span></a>
        </li>

        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/regions') active @endif" href="{!! $url.'/regions' !!}">
                <img class="img-responsive mr-3" src="{{ asset('assets/admin/images/campaign_hover.png') }}" alt="" /><span class="app-menu__label">Regions</span></a>
        </li>
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/plans') active @endif" href="{!! $url.'/plans' !!}"><i class="app-menu__icon fa fa-list mr-2"></i><span class="app-menu__label">Plans</span></a>
        </li>
         @endif
        @endif

        @if(\Auth()->user()->isAdministrator() || \Auth()->user()->isEditior())
        @if(\Auth()->user()->is_technical!=1)
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/customers' || $current_url=='/ctwdr_dmlogin/inactive-customers' || $current_url=='/ctwdr_dmlogin/incomplete-customers') active @endif" href="{{ $url.'/customers' }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Customers</span></a>
        </li>
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/users') active @endif" href="{{ $url.'/users' }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Users</span></a>
        </li>
          @endif
        @endif
        <!-- <li>
            <a class="app-menu__item @if($current_url=='/admin/reporting') active @endif" href="{!! $url.'/contacts' !!}"><i class="app-menu__icon fa fa-file-o"></i><span class="app-menu__label">Leads Contacts</span></a>
        </li> -->
<!--
        <li>
            <a class="app-menu__item @if($current_url=='/admin/reporting') active @endif" href="{{ $url.'/reporting' }}"> <img class="img-responsive mr-3" src="{{ asset('assets/admin/images/reports.png') }}" alt="" />
                <i class="app-menu__icon fa fa-file-o"></i>
                <span class="app-menu__label">Reporting</span></a>
        </li>
-->
        <!--<li>
              <a class="app-menu__item @if($current_url=='/admin/user-management') active @endif" href="{!! $url.'/user-management' !!}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">User Management</span></a>
        </li>-->
         @if(\Auth()->user()->isAdministrator())
          @if(\Auth()->user()->is_technical!=1)
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/email-templates') active @endif" href="{!! $url.'/email-templates' !!}"><i class="app-menu__icon fa fa-envelope mr-2"></i><span class="app-menu__label">Email Templates</span></a>
        </li>
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/call-setting') active @endif" href="{!! $url.'/call-setting' !!}"><i class="app-menu__icon fa fa-phone mr-2"></i><span class="app-menu__label">Master Call Settings</span></a>
        </li>

       <!-- <li>
              <a class="app-menu__item @if($current_url=='/admin/role-management') active @endif" href="{!! $url.'/role-management' !!}"><i class="app-menu__icon fa fa-window-restore"></i><span class="app-menu__label">Role Management</span></a>
        </li-->



           <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/manage-top-up') active @endif" href="{!! $url.'/manage-top-up' !!}"><img class="img-responsive mr-3" src="{{ asset('assets/admin/images/plans_billing.png') }}" alt="" />
<!--                <i class="app-menu__icon fa fa-money"></i>-->
                <span class="app-menu__label">Manage Top Up</span></a>
        </li>
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/promo-codes') active @endif" href="{!! $url.'/promo-codes' !!}"><i class="app-menu__icon fa fa-tag mr-2"></i><span class="app-menu__label">Promo Codes</span></a>
        </li>
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/logs') active @endif" href="{!! $url.'/logs' !!}"><i class="app-menu__icon fa fa-history mr-2"></i><span class="app-menu__label">Logs</span></a>
        </li>
       <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/welcome-audio-files') active @endif" href="{!! $url.'/welcome-audio-files' !!}"><i class="app-menu__icon fa fa-file-audio-o mr-2"></i><span class="app-menu__label">Welcome Audio Files</span></a>
        </li>
        @endif
        @if(\Auth()->user()->is_technical==1)
        <li>
            <a class="app-menu__item @if($current_url=='/ctwdr_dmlogin/technical-error') active @endif" href="{!! $url.'/technical-error' !!}"><i class="app-menu__icon fa-exclamation-triangle  mr-2"></i><span class="app-menu__label">Technical Error</span></a>
        </li>
        @endif
       <!-- <li>

            <a class="app-menu__item @if($current_url=='/admin/business-profile') active @endif" href="{{ URL('/admin/business-profile') }}"><i class="app-menu__icon fa fa-address-book"></i><span class="app-menu__label">Business Profile</span></a>
        </li>-->
        @endif



        <!-- <li class="treeview"><a class="app-menu__item"  href = " javascript:void(0) " data-toggle="treeview"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Orders</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="order-list.html"><i class="icon fa fa-circle-o"></i> Order List</a></li>
            </ul>
         </li> -->
    </ul>
</aside>
