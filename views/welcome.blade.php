<!DOCTYPE html>
<html lang="en">
<head>
    <title>Direct Connect Lead Form</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/vendor/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/vendor/css-hamburgers/hamburgers.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/vendor/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sample/css/main.css')}}">
    <link href="{{asset('assets/admin/css/toaster.css')}}" rel="stylesheet" type="text/css">
</head>
<body>


    <div class="container-contact100" style='background-image: url("{{asset("assets/sample/images/bg-01.jpg")}}");'>
        <div class="wrap-contact100">
            {{ Form::open(array('method'=>'post', 'url'=>URL('send-contact-form'),'class'=>'contact100-form','id'=>'contact_enquiry_form')) }}
            {{Form::hidden('type','2')}}
                <span class="contact100-form-title">
                    Get in Touch
                </span>
                <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                    <span class="label-input100">Tell us your name *</span>
                    {{ Form::text('name',null,['class'=>'input100','placeholder'=>'Enter your name'])}}
                </div>
                <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                    <span class="label-input100">Enter your email *</span>
                    {{ Form::text('email',null,['class'=>'input100','placeholder'=>'Enter your email'])}}
                </div>
                <div class="wrap-input100">
                    <span class="label-input100">Your Phone</span>
                    {{ Form::text('phone',null,['class'=>'input100','placeholder'=>'Enter your Phone'])}}
                </div>
                <div class="wrap-input100 validate-input" data-validate = "Message is required">
                    <span class="label-input100">Message</span>
                    {{ Form::textarea('message',null,['rows'=>3,'cols'=>30,'class'=>'input100','placeholder'=>'Your message here...'])}}
                </div>
                <div class="container-contact100-form-btn">
                    <div class="wrap-contact100-form-btn">
                        <div class="contact100-form-bgbtn"></div>
                        <button type="submit" class="contact100-form-btn">
                            Submit
                        </button>
                    </div>
                </div>
                {{ Form::close() }}
        </div>
    </div>



    <div id="dropDownSelect1"></div>
    <script src="{{asset('assets/sample/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assets/sample/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('assets/sample/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/sample/vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/sample/js/main.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/customer/js/form-validate.js')}}"></script>
</body>
</html>
