<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Set Password</title>
    <link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/toaster.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/gif" sizes="16x16">
</head>

<body>

  <div class="register-main-sec h-cstm">
      <div class="container">
          <div class="register-bx reset-pass">
              <div class="register-inner-bx">
              <div class="logo">
                  <a href="javascript:void(0)"><img src="{{ asset('assets/frontend/img/logo-r.png')}}" /></a>
              </div>
              <div class="form-gm btn-sec">
                <form class="form-horizontal" id="set_password_form" method="POST" action="{!! url('save-pwd')!!}">
                   {{ csrf_field() }}
                  <input type="hidden" name="userid" value="{{ $id }}">
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      <div class="form-group" id="check-form-email">
                          <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>

                          @if ($errors->has('password'))
                              <span class="help-block" >
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                      <div class="form-group" id="check-form-email" >
                          <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                          @if ($errors->has('password_confirmation'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                <div class="form-group">
                    <div class="form-group mt-4 text-center">
                        <button type="submit" class="btn btn-primary">
                            Set Password
                        </button>
                    </div>
                </div>
              </form>
              </div>
                  </div>
          </div>
      </div>
  </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('assets/admin/js/jquery.toast.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/form-validate.js') }}"></script>

</body>

</html>
