<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer login</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/toaster.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/custom.css') }}" rel="stylesheet">
      <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/gif" sizes="16x16">

</head>
<body>
  <div class="loader_div" style="display:none">
    <div class="spinner"></div>
  </div>
    <div class="register-main-sec cstm-fixed home-login">
        <div class="container">
               <div class="logo text-center">
                    <a href="javascript:void(0)"><img src="{{ asset('assets/frontend/img/logo-rOLd.png')}}" /></a>
                </div>
            <div class="row">
            <div class="col-md-6 col-sm-12">
            <div class="register-bx clearfix">
                 @if (session('status'))
                        <div class="alert alert-success message_show">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning message_show">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            {{ session('warning') }}
                        </div>
                    @endif
                <div class="register-inner-bx">
                      <h3 class="signUpText">Sign in</h3>
                <div class="form-gm btn-sec">
                <form id="login-customer" method="post" action="{{ route('login') }}" autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group mb-10">
                        <input  autocomplete="off" class="form-control form-control-lg"  name="email" type="text" placeholder="Email">
                    </div>
					<div class="form-group">
                        <input  autocomplete="new-password" class="form-control" type="password" name="password" placeholder="Password">
                    </div>

					<div class="form-group">
						<span class="signUpText trouble_logging"><a class="forgot-password" href="{{url('password/reset')}}">Forgotten password ?</a></span>
<!--
						<span class="signUpText">Don't have an account yet?  <a class="sign-in-back" href="{{url('sign-up')}}">Sign up here</a></span>

            <span class="signUpText">Did not get email yet? <a class="sign-in-back" href="{{url('verify-form')}}">Get verify email again</a></span>
-->

              </div>
                    <div class="form-group">
						<button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </form>
                </div>
              </div>
            </div>
            </div>
                  <div class="col-md-6 col-sm-12">
                      <div class="form-group register-inner-bx">
						<h3 class="signUpText">Don't have an account?</h3>
                          <p>Business is more competitive than ever, so it’s critical to react quickly to every lead. Direct Connect is a new platform that converts key information from your website, landing page, third party forms, or even live chat in to a phone call. You can be converting leads within seconds.</p>
                          <span class=""><a class="sign-in-back btn-warning btn btn-primary" href="{{url('sign-up')}}">Create Account</a></span>

<!--            <span class="signUpText">Did not get email yet? <a class="sign-in-back" href="{{url('verify-form')}}">Get verify email again</a></span>-->

              </div>
            </div>
        </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('assets/admin/js/jquery.toast.js') }}"></script>
	<script src="{{ asset('assets/admin/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/form-validate.js') }}"></script>
    <script src="{{ asset('assets/js/localStorage.js')}} "></script>
    <script type="text/javascript">
      removeStorage();
    </script>
</body>
</html>
