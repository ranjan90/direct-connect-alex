@extends('layouts.customer')
@section('content')
     <div class="container-fluid">
            <!-- Breadcrumbs-->
        <div class="row">
                <div class="col-md-12">
                <div class="heading-campaigns">
                  <h2>Business Profiles</h2>
                  <div class="create-new-campaign-btn">
                      <a href="{!! $url.'/business-profile/create'!!}" class="btn btn-primary">Add Business Profile</a>
                  </div>
                </div>
                </div>
          </div>
            @can(config('permissions.data.business-listing.name'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="tile">
                            <div class="latest-champaign-table">
                                <div class="tile-body   table-responsive recordsTable">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Business Name</th>
                                                <th>Plan Name</th>
                                                <th>Number of Campaigns</th>
                                                <th>Credit Balance</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody class="show_records">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan
    </div>
@include('includes.delete_pop_up')
@section('js')
<script type="text/javascript">
var route_url="<?php echo $url?>";
var get_data_url=route_url+'/fetch-business-profile-list';
var confirm_delete=route_url+'/business-profile';
var redirect_url=route_url+'/business-profile';
var confirm_delete=redirect_url+'/delete';
var deleteMethod='get';
var deleteLocalStorge=1;
</script>
<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
<script src="{{ asset('assets/js/fetch.js') }}"></script>
<script>
  var modalConfirm = function(callback){

          $(document).on("click",'.confirm_delete_pop_up',function(){
               $('#confirm-delete-modal').modal('show');
               $("#yes").attr('data-id',$(this).attr('data-id'));
          });
          $(document).on("click",'#yes',function(){
             callback($(this).attr('data-id'));
             $("#customerLogin").modal('hide');
          });
          
        };
    modalConfirm(function(confirm)
    {
        if(confirm)
        {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: deleteMethod,
                url:confirm_delete+'/'+confirm,
                dataType: "json",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoader();
                },
                success: function (data)
                {
                    //localStorage.setItem("business_name",JSON.stringify(data.business));
                    //localStorage.setItem('business_data_id',data.set_id);
                    if(deleteLocalStorge==1)
                    {
                        $('#confirm-delete-modal').modal('hide');
                         tostrOnHidden(redirect_url+'?business_is_deleted=1','reload');
                        tostrsuccess(data.message);
                    }
                    
                },
                complete: function() {
                    hideLoader();
                },
                error: function (xhr) {
                    $('#confirm-delete-modal').modal('hide');
                    jQuery.each(xhr.responseJSON.errors,function(k,message){
                        tostrerror(message);
                    });
                }
            });
        }
    });

    
</script>
@endsection
@endsection
