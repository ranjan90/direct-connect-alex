@extends('layouts.customer')
@section('content')
      <div class="container-fluid">
            <!-- Breadcrumbs-->
        <div class="row">
                <div class="col-md-12">
                <div class="heading-campaigns">
                  <h2>Business Profile</h2>
                  <div class="create-new-campaign-btn">
                      <a href="{!! $url.'/business-profile'!!}" class="btn btn-primary">Back</a>
                  </div>
                </div>
                </div>
          </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h2>{{$business->business_name}}</h2>
                   <div class="row">
					<div class="col-md-12 table-responsive">
					<table class="table">
					   <tr>
					       <td>Business Name:</td><td>{{$business->business_name}}</td>
					   </tr>

                        <tr>
                           <td>Address:</td><td>@if(!empty($business->address)){{$business->address}} @else N/A @endif</td>
                       </tr>
                        <tr>
                           <td>Street Number:</td><td>@if(!empty($business->street_number)){{$business->street_number}} @else N/A @endif</td>
                       </tr>
                       <tr>
                           <td>Suburb:</td><td>@if(!empty($business->suburb)){{$business->suburb}} @else N/A @endif</td>
                       </tr>
                       <tr>
                           <td>State:</td><td>@if(!empty($business->state)){{$business->state}} @else N/A @endif</td>
                       </tr>
                       <tr>
                           <td>Postal Code:</td><td>@if(!empty($business->postal_code)){{$business->postal_code}} @else N/A @endif</td>
                       </tr>
                       <tr>
                           <td>Country:</td><td>@if(!empty($business->region->countries->countryName)){{$business->region->countries->countryName}} @else N/A @endif</td>
                       </tr>
                       <tr>
                           <td>Web Site Url:</td><td>@if(!empty($business->website)){{$business->website}} @else N/A @endif</td>
                       </tr>
					   
					</table>
					</div>

				 
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
