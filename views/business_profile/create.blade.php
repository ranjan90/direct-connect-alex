@extends('layouts.customer')
@section('content')
<style>
.form-group-placeholder .palceholder {
	position: absolute;
	top: 10px;
	left: 13px;
	color: #B1B1B1;
	display: none;
}
.form-group-placeholder label {
    color:  #938d8d;
    font-size:14px !important;
}
</style>
    <div class="row">
        <h2 class="main_title_head">New Business</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-panel Campaigns-tabs">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs nav-campaigns">
							<?php $i = 0;
							 if($nextstep == 5 || (isset($UserDataWithoutStatus->is_group) && $UserDataWithoutStatus->is_group==1)){
                            	$tabArr = array('BUSINESS PROFILE','COMPLETE');
							 } else {
								$tabArr = array('BUSINESS PROFILE','CHOOSE PLAN','CREDIT RECHARGE','SUMMARY','COMPLETE');
							} ?>
                        @foreach($tabArr as $tab)
                            @php $i++; @endphp
                            <li class="nav-item">
                                <a class="<?php if( $i <= $nextstep  ){?>active_business_prev<?php }else{?> disabled<?php } ?> next_business_me{{$i}}  @if( $i==$nextstep ) active @endif" href="#business-step-{{$i}}" data-toggle="tab">{{$tab}}</a>
                            </li>
                        @endforeach
                        </ul>
                        <div class="tab-content newBusinessTabs">
                            <!-- Tab Step 1 -->
                            <div class="tab-pane <?php if($nextstep == 1){?>active<?php } ?>" id="business-step-1">
                                {{ Form::model($business,array('url' =>$url.'/create-update-business-profile','id' => 'business-profile-form','class' => 'business-profile-form','autocomplete' => 'off')) }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tile">
                                            <div class="row">
                                                <div class="slimField col-md-6">
                                                    <div class="form-group form-group-placeholder">
                                                        <div class="palceholder">
            												<label for="yourName">Business Name</label>
            												<span class="star">*</span>
            											</div>
                                                        {!! Form::hidden('id', $hidden_id) !!}
                                                        {!!  Form::text('business_name',null,['class'=>'form-control','rows'=>'4']) !!}
                                                    </div>
                                                </div>
                                                <div class="slimField col-md-6">
                                                    <div class="form-group">
                                                        <input autocomplete = 'off' class="form-control" type="text" name="state"  id="administrative_area_level_1" placeholder="State" value="{{ isset($business->state) ? $business->state : null }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="slimField col-md-6">
                                                    <div class="form-group form-group-placeholder">
                                                        <div class="palceholder">
            												<label for="yourName">Address</label>
            											</div>
                                                        {!!  Form::text('address', isset($business->address) ? $business->address : null , array('class' => 'form-control address', 'id' => 'address-stop','autocomplete' => 'off')); !!}
                                                    </div>
                                                </div>
                                                <div class="slimField col-md-6">
                                                    <div class="form-group">
                                                        <input autocomplete = 'off' class="form-control" type="text" name="postal_code" id="postal_code"  placeholder="Post Code" value="{{ isset($business->postal_code) ? $business->postal_code : null }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="slimField col-md-6">
                                                    <div class="form-group">
                                                        <input autocomplete = 'off' class="form-control"  type="text" name ="street_number" id="street_number" placeholder="Street Number" value="{{ isset($business->street_number) ? $business->street_number : null }}">
                                                    </div>
                                                </div>
                                                <div class="slimField col-md-6">
                                                    <div class="form-group">
                                                        <span id="countryAstrik" style="display:none;color: red;position: absolute;margin-left: 137px;margin-top: 11px;">*</span>
                                                        {!! Form::select('country',$regions, !empty($business->country) ? $business->country : null,['class'=>'form-control countryRegions','id'=>'country']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="slimField col-md-6">
                                                    <div class="form-group">
                                                        <input autocomplete = 'off' class="form-control" type="text" name="suburb" id="locality" placeholder="Suburb" value="{{ isset($business->suburb) ? $business->suburb : null }}">
                                                    </div>
                                                </div>
                                                <div class="slimField col-md-6">
                                                    <div class="form-group form-group-placeholder">
                                                        <div class="palceholder">
                                                            <label for="yourName">Website Url</label>
                                                        </div>
                                                        {!!  Form::text('website',null,['class'=>'form-control','rows'=>'4']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                     <div class="col-md-12 last-next-preview-btn">
                                         <div class="Submit-campaign step1BtnRow text-right">
											 <?php if($nextstep == 5 ){?>
                                             	<button id="step_1_btn" type="submit" class="btn submitBtn add-campaign submitBtnCls">Update</button>
										 	<?php } else {?>
                                             	<button id="step_1_btn" type="submit" class="btn submitBtn add-campaign submitBtnCls">Next</button>
										 	<?php } ?>
                                         </div>
                                     </div>
                                 </div>
                                 {{ Form::close() }}
                            </div>
							<?php if($nextstep < 5){ ?>
							<!-- Tab Step 2 -->
                            <div class="tab-pane <?php if($nextstep == 2){?>active<?php } ?>" id="business-step-2">
								<div class="row" id="newBusinessPlansContainer"></div>
                                <div class="row clearfix">
                                    <div class="col-md-12 last-next-preview-btn">
                                        <div class="Submit-campaign">
                                            <a class="float-left backStepBtn" data-step="#business-step-1" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                            <button id="newBusinessPlanChooseBtn" type="button" class="btn float-right">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 3 -->
                            <div class="tab-pane <?php if($nextstep == 3){?>active<?php } ?>"  id="business-step-3">
								<div class="row" id="newBusinessTopUpContainer"></div>
                                <div class="row clearfix">
                                    <div class="col-md-12 last-next-preview-btn">
                                        <div class="Submit-campaign">
                                            <a class="float-left backStepBtn" data-step="#business-step-2" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                            <button id="newBusinessTopupChooseBtn" type="button" class="btn btn-warning float-right">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 4 -->
                            <div class="tab-pane <?php if($nextstep == 4){?>active<?php } ?>" id="business-step-4">
                                <div class="campaign-time select-contact">
									<div class="row" id="newBusinessSummaryContainer"></div>
                                    <div class="row clearfix">
                                        <div class="col-md-12 last-next-preview-btn">
                                            <div class="Submit-campaign">
                                                <a class="float-left backStepBtn" data-step="#business-step-3" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                                <button id="newBusinessSummarySaveBtn" type="button" class="btn float-right btn-success">Save & Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php } ?>
                            <!-- Tab Step 5 -->
                            <div class="tab-pane" <?php if($nextstep == 5){?>id="business-step-2"<?php } else {?>id="business-step-5" <?php }?> >
								<div class="row">
									<div class="col-md-12">
										<div class="sucess-main-sec">
											<div class="suceesfully-text clearfix">
												<h1 class="text-center">Congratulations</h1>
												<p style="margin-bottom:0px;" class="text-center">Welcome to Direct Connect.</p>
												<p class="mb-0 text-center">Your new business has been successfully completed.</p>
												<p style="margin-top:20px;" class="mb-0 text-center"><a href="{{$url.'/add-new-campaign'}}" class="btn btn-success">Create new campaign</a></p>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>

                    </div>
                 </div>
              </div>
           </div>

    </div>
 @section('js')
 <script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
 <script src="{{ asset('assets/'.config("app.frontendtemplatename").'/js/jquery.payment.js') }}"></script>
 <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaGr_xjEuq7SiJpSabheOAlh5SKVGiVN0&libraries=places&callback=initAutocomplete&language=en-AU" async defer></script>
 <script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>-->
     <script>

     $(document).ready(function()
     {
		 localStorage.setItem('paymentType','');
		 var businessStep    = '<?php echo $nextstep;?>';
		 var businessCountry = '<?php echo $country;?>';
		 var newPlanId		 = '<?php echo $newPlanId;?>';
		 var newCreditId	 = '<?php echo $newCreditId;?>';
		 if(businessStep >= 2)
		 {
			 getPlansByCountryId(businessCountry,newPlanId,newCreditId);
		 }
		 if(businessStep >= 4)
		 {
			 getNewBusinessPaymentSummary(newPlanId,newCreditId);
		 }
		 if(businessStep >= 5)
		 {
			 var showCompleteStep = localStorage.getItem('showCompleteStep');
			 if( showCompleteStep == 'yes' )
			 {
				 $('.next_business_me2').trigger('click');
				 localStorage.setItem('showCompleteStep','');
			 }
			 else
			 {
				 $('.next_business_me1').trigger('click');
			 }
		 }
         $('.countryRegions').change(function() {
             var country = $(this).val();
             if( country != '' )
                $('#countryAstrik').hide();
            else
                $('#countryAstrik').show();
         });
         $('.palceholder').click(function() {
           $(this).siblings('input').focus();
         });
         $('.form-control').keydown(function() {
           $(this).siblings('.palceholder').hide();
         });
         $('.form-control').keyup(function() {
             var $this = $(this);
           if ($this.val().length == 0)
             $(this).siblings('.palceholder').show();
         });
         $('.form-control').blur(function() {
           var $this = $(this);
           if ($this.val().length == 0)
             $(this).siblings('.palceholder').show();
        });
        $('.form-control').blur();
        $('.countryRegions').change();
     });

      var ddd=[];

      $("#business-profile-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
                {
                    country:
                    {
                         required: true,
                    },
                    business_name:
                    {
                      required: true,
                    }
                    // address:
                    // {
                    //   required: true,
                    // },website:
                    // {
                    //   required: true,
                    // }
                },
        messages:
                {
                    business_name: {
                        required: "Business name is required.",
                    },
                    address: {
                        required: "Address is required.",
                    },
                    country:{
                       required: "Country is required.",
                    },
                    website: {
                        required: "Website Url is required.",
                        url:"Enter correct url"
                    }
        },
        submitHandler: function (form)
        {


            $.ajax({
                url         : form.action,
                type        : form.method,
                //data        : $(form).serialize(),
                data        : new FormData(form),
                headers     : {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType : false,
                cache       : false,
                processData : false,
                dataType    : "json",
                beforeSend  : function () {
                    $("input[type=submit]").prop("disabled", true);
                    $("button[type=submit]").prop("disabled", true);
                    $(".loader_div").show();
                },

                success: function (response) {
                          //tr.remove();
                          ddd=response.data;
                          localStorage.setItem("business_name",JSON.stringify(ddd));
                          if(response.set_id=="")
                          {

                          }
                          else
                          {
                            localStorage.setItem('business_data_id',response.set_id);
                          }
						  if(response.isNewBusiness)
                          {
							  localStorage.setItem('isNewBusiness','yes');
                              $("#checkPlanStatus").modal('hide');
                              setTimeout(function() { $("#checkPlanStatus").modal('hide');}, 100);
                          }
                          //document.cookie = "business_name="+JSON.stringify(ddd);
                          tostrOnHidden(response.url,'reload');
                          tostrsuccess(response.message);
                },
                complete: function () {
                    $(".loader_div").hide();
                    $("input[type=submit]").prop("disabled", false);
                    $("button[type=submit]").prop("disabled", false);
                    list_business();
                  },
                error:function(xhr, status, error){
                     jQuery.each(xhr.responseJSON.errors,function(k,message){
                                 if(k=='newBusinessPromoCode')
                                 {
                                      $('#newBusinessPromoCode-error').css('display','block').html(message);
                                     $('#newBusinessPromoCodeBtn').parents('.form-group').addClass('has-error');

                                 }else
                                 {
                                  tostrerror(message);
                                 }

                     });
                }
            });


    }
  });
	$(document).on('keypress','#newBusinessPromoCode',function(e)
	{
		var promocode   = $('#newBusinessPromoCode').val();
		if( promocode != '' )
		{
			$('#newBusinessPromoCode-error').css('display','none').html('');
			$('#newBusinessPromoCodeBtn').parents('.form-group').removeClass('has-error');
		}
	});
	$(document).on('change','#newBusinessPromoCode',function(e)
	{
		$('#appliedPromoCode').css('display','none');
		$('#paymentViewPromoCode').css('display','none');
		$('#paymentViewPromoCodeSpan').css('display','none');
		$('#promocodehidden').val('');
		$('#newBusinessPromoCode-error').css('display','none').html('');
		$('#newBusinessPromoCodeBtn').parents('.form-group').removeClass('has-error');
	});
	$(document).on('click','#newBusinessPromoCodeBtn',function(e)
	{
		$('#newBusinessPromoCode-error').css('display','none').html('');
		$('#newBusinessPromoCodeBtn').parents('.form-group').removeClass('has-error');
		var promocode   = $('#newBusinessPromoCode').val();
		$('#appliedPromoCode').css('display','none');
		$('#paymentViewPromoCode').css('display','none');
		$('#paymentViewPromoCodeSpan').css('display','none');
		$('#promocodehidden').val('');
		if( promocode != '' )
		{
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url                : prefixUrl + '/validate-promo-code',
				type               : 'GET',
				dataType           : 'json',
				data       		   : { 'code':promocode },
				beforeSend : function() {
					$(".loader_div").show();
				},
				success    : function(response)
				{
					$(".loader_div").hide();
					if( response.success )
					{
						tost(response.message,"Success",4000);
						$('#appliedPromoCode').css('display','block');
						$('#promoSuccessMessage').html(response.msg);
						$('#paymentViewPromoCode').css('display','flex');
						$('#paymentViewPromoCodeSpan').css('display','flex').html(response.credits);
						$('#promocodehidden').val(promocode);
					}
					else
					{
						$('#newBusinessPromoCode-error').css('display','block').html(response.message);
						$('#newBusinessPromoCodeBtn').parents('.form-group').addClass('has-error');
					}
				},
				complete   : function(){
					$(".loader_div").hide();
				}
			});
		}
		else
		{
			$('#newBusinessPromoCode-error').css('display','block').html('This field is required.');
			$('#newBusinessPromoCodeBtn').parents('.form-group').addClass('has-error');
		}
	});
  </script>
 @endsection
@endsection
