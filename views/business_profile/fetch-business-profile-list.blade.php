@if($data->count()==0)
  <tr >
      <td colspan="4"  class="text-center" style="text-align:center !important">
        No Record Found.
      </td>
  </tr>
  @else
  <?php $no=1;?>
  @foreach($data as $key=>$value)
  <?php $add = $value->address.' '.$value->street_number.' '.$value->suburb.' '.$value->state.' '.$value->postal_code.' '.$value ->country; ?>
      <tr>

        <td>{!! $value->business_name !!}</td>
        <td>
            @if(!empty($plan_name_show))
            {{$plan_name_show}}
            @else
            @if(isset($value->user_plan->plans->name)){!! $value->user_plan->plans->name !!}@else N/A @endif
            @endif
            </td>
        <td>{!! $value->user_campaigns->count()!!}</td>
        <td>
             @if(empty($balanceAmount))
             @if(isset($value->user_plan->balanceAmount)){!! $value->user_plan->balanceAmount!!}@else 0 @endif
             @else
             {{$balanceAmount}}
             @endif
            </td>
        <td>
        @can(config('permissions.data.business-detail.name'))
        <a href="{!! $url.'/business-profile/'.Crypt::encrypt($value->id) !!}"><i class="fa fa-eye"></i></a>
        @endcan
        @can(config('permissions.data.edit-business.name'))
        <a href="{!! $url.'/business-profile/'.Crypt::encrypt($value->id).'/edit' !!}" data-toggle="tooltip" title="Edit" data-placement="bottom"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a>
         @endcan
         @can(config('permissions.data.delete-business.name'))
         @if($value->is_active!=1)<a class="confirm_delete_pop_up" href="javascript:void(0)" data-toggle="modal" data-target="#confirm-delete-modal" data-id="{!! Crypt::encrypt($value->id) !!}"><span  data-toggle="tooltip" title="Delete" data-placement="bottom"><img src="{{asset('assets/customer/img/delete.png')}}"></span></a>@endif
        @endcan



       </td>
     </tr>
  @endforeach
@endif
