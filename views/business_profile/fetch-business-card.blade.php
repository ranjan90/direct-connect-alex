  @if(empty($data))
  <tr >
      <td colspan="7"  class="text-center" style="text-align:center !important">
        No Record Found.
      </td>
  </tr>
  @else
  <?php $no=1;?>
  @foreach($data as $key=>$value)
  <?php  ?>
      <tr>
        
        @if(!empty($customer_id))
         <td>
        {{ Form::radio('card_id', $value->card_id , false,['class'=>'card_radio_value']) }}

       </td>
       @endif
        <td>{!! $value->business_profile->business_name !!}</td>
        <td>{!! $value->name !!}</td>
        <td>{!! $value->last4 !!}</td>
        <td>{!! $value->exp_month !!} </td>
        <td>{!! $value->exp_year !!}</td>
        <td>
         {!! $value->brand !!}
         </td>
        <td>@if($value->is_default==1) Yes @else No @endif</td>
         @if(empty($customer_id))
         <td>
          @if($value->is_default!=1)
                <a class="business_confirm_delete_pop_up" href="javascript:void(0)" data-toggle="modal" data-target="#business-confirm-delete-modal" data-id="{!! Crypt::encrypt($value->id) !!}" data-table="{!! Crypt::encrypt('business_card') !!}"><span  data-toggle="tooltip" title="Delete" data-placement="bottom"><img src="{{asset('assets/customer/img/delete.png')}}"></span></a>
         @else
       

         @endif

         @if($value->is_default!=1)
            <a class="default_business_confirm_pop_up" href="javascript:void(0)" data-toggle="modal" data-target="#default_business_confirm_pop_up_model_data" data-id="{!! Crypt::encrypt($value->id) !!}"><span  data-toggle="tooltip" title="Set Default Card" data-placement="bottom"><i class="fas fa-cog"></i></span></a> 
         @endif

     

   <!--       <a href="{!! $url.'/plans?business_card_edit='.Crypt::encrypt($value->id)!!}" data-toggle="tooltip" title="Edit" data-placement="bottom"><img src="{{asset('assets/customer/img/edi-d.png')}}"></a> -->
 
       </td>
        @endif

     </tr>
  @endforeach
@endif
