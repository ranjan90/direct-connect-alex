@extends('layouts.customer')
@section('content')
    <div class="row">
        <h2 class="main_title_head">New Business</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-panel Campaigns-tabs">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs nav-campaigns">
                        @php
                            $tabArr = array('BUSINESS PROFILE','CHOOSE PLAN','CREDIT RECHARGE','SUMMARY','COMPLETE');
                            $i = 0;
                        @endphp
                        @foreach($tabArr as $tab)
                            @php $i++; @endphp
                            <li class="nav-item">
                                <a class="active_business_prev next_business_me{{$i}}  @if($i==1) active @endif" href="#step-{{$i}}" data-toggle="tab">{{$tab}}</a>
                            </li>
                        @endforeach
                        </ul>
                        <div class="tab-content newBusinessTabs">
                            <!-- Tab Step 1 -->
                            <div class="tab-pane active" id="step-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tile">
                                            {{ Form::model($business,array('url' =>$url.'/create-update-business-profile','id' => 'business-profile-form','class' => 'business-profile-form','autocomplete' => 'off')) }}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Business Name<span class="star">*</span></label>
                                                            {!! Form::hidden('id', $hidden_id) !!}
                                                            {!!  Form::text('business_name',null,['placeholder'=>"Business Name",'class'=>'form-control','rows'=>'4']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""> Address<span class="star">*</span></label>
                                                            {!!  Form::text('address', isset($business->address) ? $business->address : null , array('class' => 'form-control address','placeholder' => 'Address', 'id' => 'address-stop','autocomplete' => 'off')); !!}
                                                        </div>
                                                        <div class="slimField">
                                                            <div class="form-group">
                                                                <input autocomplete = 'off' class="form-control"  type="text" name ="street_number" id="street_number" placeholder="Street Number" value="{{ isset($business->street_number) ? $business->street_number : null }}">
                                                            </div>
                                                        </div>
                                                        <div class="slimField">
                                                            <div class="form-group">
                                                                <input autocomplete = 'off' class="form-control" type="text" name="suburb" id="locality" placeholder="Suburb" value="{{ isset($business->suburb) ? $business->suburb : null }}">
                                                            </div>
                                                        </div>
                                                        <div class="slimField">
                                                            <div class="form-group">
                                                                <input autocomplete = 'off' class="form-control" type="text" name="state"  id="administrative_area_level_1" placeholder="State" value="{{ isset($business->state) ? $business->state : null }}">
                                                            </div>
                                                        </div>
                                                        <div class="slimField">
                                                            <div class="form-group">
                                                                <input autocomplete = 'off' class="form-control" type="text" name="postal_code" id="postal_code"  placeholder="Post Code" value="{{ isset($business->postal_code) ? $business->postal_code : null }}">
                                                            </div>
                                                        </div>
                                                        <div class="slimField">
                                                            <div class="form-group">
                                                                {!! Form::select('country',$regions, !empty($business->country) ? $business->country : null,['class'=>'form-control countryRegions','id'=>'country']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Website Url<span class="star">*</span></label>
                                                            {!!  Form::text('website',null,['placeholder'=>"Website Url",'class'=>'form-control','rows'=>'4']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(empty($business))
                                <div class="row">
                                <div class="">
                                <div class="form-group promo pr-3 pl-3">
                                <label class="control-label">Have Promo Code</label>

                                <div class="row">
                                <div class="col-lg-8">
                                <input id="signupPromoCode" name="signupPromoCode" type="text" class="input-lg form-control" placeholder="" >
                                <input type="hidden" name="promocode" id="promocodehidden" />
                                <label id="signupPromoCode-error" class="has-error" style="display:none" for="signupPromoCode">This field is required.</label>
                                </div>
                                <div class="col-lg-4 pl-0">
                                <button id="signupPromoCodeBtn" style="padding-left:15px;padding-right:15px;" type="button" class="btn btn-success">Apply</button>
                                </div>
                                </div>
                                <div class="row" style="padding-top: 15px; display:none;font-size: 13px;" id="appliedPromoCode">
                                <div class="col-md-12" style="padding-right: 0px;">
                                <div class="alert alert-success cstm-alert" role="alert">
                                <div class="vertical-align" style="display: flex;align-items: center;">
                                <div class="col-md-2 p-0 text-center">
                                <i class="fas fa-smile"></i>
                                </div>
                                <div class="col-md-10 p-0" id="promoSuccessMessage">

                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>

                                </div>

                                @endif

                                <div class="row">
                                <div class="col-md-12 text-right">
                                @if(!empty($business))
                                <button type="submit" class="btn btn-primary submitBtn">Update</button>
                                @else
                                <button type="submit" class="btn btn-primary submitBtn">Save</button>
                                @endif
                                </div>
                                {{ Form::close() }}
                                </div>
                                </div>
                                </div>
                                </div>
                                 <div class="row clearfix">
                                     <div class="col-md-12 last-next-preview-btn">
                                         <div class="Submit-campaign step1BtnRow text-right">
                                             <button id="step_1_btn" type="button" class="btn add-campaign submitBtnCls">Next</button>
                                         </div>
                                     </div>
                                 </div>
                            </div>
                            <!-- Tab Step 2 -->
                            <div class="tab-pane" id="step-2">
                                <div class="row clearfix">
                                    <div class="col-md-12 last-next-preview-btn">
                                        <div class="Submit-campaign">
                                            <a class="float-left backStepBtn" data-step="#step-1" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                            <button id="step_2_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 3 -->
                            <div class="tab-pane" id="step-3">
                                <div class="row clearfix">
                                    <div class="col-md-12 last-next-preview-btn">
                                        <div class="Submit-campaign">
                                            <a class="float-left backStepBtn" data-step="#step-2" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                            <button id="step_3_btn" type="button" class="btn btn-warning float-right add-campaign submitBtnCls">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 4 -->
                            <div class="tab-pane" id="step-4">
                                <div class="campaign-time select-contact">
                                    <div class="row clearfix">
                                        <div class="col-md-12 last-next-preview-btn">
                                            <div class="Submit-campaign">
                                                <a class="float-left backStepBtn" data-step="#step-3" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                                <button id="step_4_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 5 -->
                            <div class="tab-pane" id="step-5">
                                <div class="campaign-time">
                                    <div class="row clearfix">
                                        <div class="col-md-12 last-next-preview-btn">
                                            <div class="Submit-campaign">
                                                <a class="float-left backStepBtn" data-step="#step-4" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                                <button id="step_5_btn" type="button" class="btn float-right add-campaign submitBtnCls">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Step 6 -->
                            <div class="tab-pane" id="step-6">
                                <div class="row clearfix">
                                    <div class="col-md-12 last-next-preview-btn">
                                        <div class="Submit-campaign clearfix">
                                            <a class="float-left backStepBtn" data-step="#step-5" href="javascript:void(0)"><i class="fa fa-arrow-left backArrow"> </i>Back</a>
                                            <button id="step_6_btn" data-url="{!! $url.'/campaigns'!!}" type="button" class="btn float-right campFinish submitBtnCls btn-warning">Save and Finish</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                 </div>
              </div>
           </div>

    </div>
 @section('js')
 <script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>
 <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaGr_xjEuq7SiJpSabheOAlh5SKVGiVN0&libraries=places&callback=initAutocomplete&language=en-AU" async defer></script>
 <script src="{{ asset('assets/js/toastrvalidation.js') }}"></script>-->
     <script>
      var ddd=[];
  // var placeSearch, autocomplete;
  //     var componentForm = {
  //       street_number: 'short_name',
  //       locality: 'long_name',
  //       administrative_area_level_1: 'short_name',
  //       //area: 'short_name',
  //       country: 'long_name',
  //       postal_code: 'short_name'
  //     };
  //     var input = document.getElementById('address');
  //     var options = {
  //       types: ['geocode']
  //       //componentRestrictions: {country: 'CA'}
  //     };
  //     function initAutocomplete() {
  //       autocomplete = new google.maps.places.Autocomplete(input, options);
  //       autocomplete.addListener('place_changed', fillInAddress);
  //     }

  //     function fillInAddress() {
  //       // console.log('ssss');
  //       var place = autocomplete.getPlace();
  //       for (var component in componentForm) {
  //         document.getElementById(component).value = '';
  //         document.getElementById(component).disabled = false;
  //       }
  //       for (var i = 0; i < place.address_components.length; i++) {
  //         var addressType = place.address_components[i].types[0];
  //         if (componentForm[addressType]) {
  //           var val = place.address_components[i][componentForm[addressType]];
  //            document.getElementById(addressType).value = val;
  //           //console.log(val);
  //         }
  //       }
  //     }
  //     function geolocate() {
  //       if (navigator.geolocation) {
  //         navigator.geolocation.getCurrentPosition(function(position) {
  //           var geolocation = {
  //             lat: position.coords.latitude,
  //             lng: position.coords.longitude
  //           };
  //           var circle = new google.maps.Circle({
  //             center: geolocation,
  //             radius: position.coords.accuracy
  //           });
  //           autocomplete.setBounds(circle.getBounds());
  //         });
  //       }
  //     }
      $("#business-profile-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
                {
                    country:
                    {
                         required: true,
                    },
                    business_name:
                    {
                      required: true,
                    },
                    address:
                    {
                      required: true,
                    },website:
                    {
                      required: true,
                    }
                },
        messages:
                {
                    business_name: {
                        required: "Business name is required.",
                    },
                    address: {
                        required: "Address is required.",
                    },
                    country:{
                       required: "Country is required.",
                    },
                    website: {
                        required: "Website Url is required.",
                        url:"Enter correct url"
                    }
        },
        submitHandler: function (form)
        {


            $.ajax({
                url         : form.action,
                type        : form.method,
                //data        : $(form).serialize(),
                data        : new FormData(form),
                headers     : {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType : false,
                cache       : false,
                processData : false,
                dataType    : "json",
                beforeSend  : function () {
                    $("input[type=submit]").prop("disabled", true);
                    $("button[type=submit]").prop("disabled", true);
                    $(".loader_div").show();
                },

                success: function (response) {
                          //tr.remove();
                          ddd=response.data;
                          localStorage.setItem("business_name",JSON.stringify(ddd));
                          if(response.set_id=="")
                          {

                          }
                          else
                          {
                            localStorage.setItem('business_data_id',response.set_id);
                          }
                          //document.cookie = "business_name="+JSON.stringify(ddd);
                          tostrOnHidden(response.url,'reload');
                          tostrsuccess(response.message);
                },
                complete: function () {
                    $(".loader_div").hide();
                    $("input[type=submit]").prop("disabled", false);
                    $("button[type=submit]").prop("disabled", false);
                    list_business();
                  },
                error:function(xhr, status, error){
                     jQuery.each(xhr.responseJSON.errors,function(k,message){
                                 if(k=='signupPromoCode')
                                 {
                                      $('#signupPromoCode-error').css('display','block').html(message);
                                     $('#signupPromoCodeBtn').parents('.form-group').addClass('has-error');

                                 }else
                                 {
                                  tostrerror(message);
                                 }

                     });
                }
            });


    }
  });
       $(document).on('change','#signupPromoCode',function(e)
           {
           var promocode   = $('#signupPromoCode').val();
           $('#appliedPromoCode').css('display','none');
           $('#paymentViewPromoCode').css('display','none');
           $('#paymentViewPromoCodeSpan').css('display','none');
           $('#promocodehidden').val('');
         });
         $(document).on('click','#signupPromoCodeBtn',function(e)
           {
               $('#signupPromoCode-error').css('display','none').html('');
               $('#signupPromoCodeBtn').parents('.form-group').removeClass('has-error');
               var promocode   = $('#signupPromoCode').val();
           $('#appliedPromoCode').css('display','none');
           $('#paymentViewPromoCode').css('display','none');
           $('#paymentViewPromoCodeSpan').css('display','none');
           $('#promocodehidden').val('');
               if( promocode != '' )
               {
                   $.ajax({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url                : prefixUrl + '/validate-promo-code',
                       type               : 'GET',
                       dataType           : 'json',
                       data       : { 'code':promocode },

                       beforeSend : function() {
                           $(".loader_div").show();
                       },
                       success    : function(response)
                       {
                           $(".loader_div").hide();
                           if( response.success )
                 {
                   tost(response.message,"Success",4000);
                   $('#appliedPromoCode').css('display','block');
                   $('#promoSuccessMessage').html(response.msg);
                   $('#paymentViewPromoCode').css('display','flex');
                   $('#paymentViewPromoCodeSpan').css('display','flex').html(response.credits);
                   $('#promocodehidden').val(promocode);
                           }
                 else
                 {
                   $('#signupPromoCode-error').css('display','block').html(response.message);
                         $('#signupPromoCodeBtn').parents('.form-group').addClass('has-error');
                           }
                       },
                       complete   : function(){
                 $(".loader_div").hide();
               }
                   });
               }
               else
               {
                   $('#signupPromoCode-error').css('display','block').html('This field is required.');
                   $('#signupPromoCodeBtn').parents('.form-group').addClass('has-error');
               }
           });
  </script>
 @endsection
@endsection
